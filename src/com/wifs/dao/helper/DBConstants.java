package com.wifs.dao.helper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.wifs.dao.dbconnection.WIFSSQLCon;

public class DBConstants {

	public static Logger logger = Logger.getLogger("DBConstants.class");
//--------------- DB Current Date in string ----------------------
public String getDBCurrentDate() throws Exception
{
	Connection objwifsConnection = null;
	PreparedStatement pstmDBCurrentDate = null;
	ResultSet rstDBCurrentDate = null;
	String strDBCurrentDate = "";
	try{
		WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
		objwifsConnection = objWIFSSQLCon.getWifsSQLConnect();
		pstmDBCurrentDate = objwifsConnection.prepareStatement("SELECT DATEADD(MI, 330, GETDATE())");
		rstDBCurrentDate = pstmDBCurrentDate.executeQuery();
		if (rstDBCurrentDate.next()){
			strDBCurrentDate = rstDBCurrentDate.getString(1);
		}
	}
	finally{
		if (objwifsConnection != null )objwifsConnection.close();
		if (rstDBCurrentDate != null)rstDBCurrentDate.close();
		if (pstmDBCurrentDate != null)pstmDBCurrentDate.close();
	}
	return strDBCurrentDate;
}
//--------------- Current Date for File Creation Format ---------------------- 
public String getFileFormatDBCurrentDate() throws Exception
{
	Connection objwifsConnection = null;
	String strFileFormatDBCurrentDate = "";
	try{
		WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
		objwifsConnection = objWIFSSQLCon.getWifsSQLConnect();
		PreparedStatement pstmDBCurrentDate = objwifsConnection
		.prepareStatement("SELECT REPLACE(CONVERT(VARCHAR(26), DATEADD(MI, 330, GETDATE()), 109), ':', '')");
		ResultSet rstDBCurrentDate = pstmDBCurrentDate.executeQuery();
		if (rstDBCurrentDate.next()){
			strFileFormatDBCurrentDate = rstDBCurrentDate.getString(1);
		}
	}
	finally{
		if (objwifsConnection != null )objwifsConnection.close();
	}
	return strFileFormatDBCurrentDate;
}

public String getCurrentDateDDMMYYYY()
{
	Calendar cal = Calendar.getInstance();
	int date =cal.get(cal.DATE);
	int year =cal.get(cal.YEAR);
	int month = cal.get(Calendar.MONTH);
	month = month+1;
	
	return date+"-"+month+"-"+year;
}
public String getCurrentDateMMDDYYYY()
{
	Calendar cal = Calendar.getInstance();
	int date =cal.get(cal.DATE);
	int year =cal.get(cal.YEAR);
	int month = cal.get(Calendar.MONTH);
	month = month+1;
	
	return month+"-"+date+"-"+year;
}

}
