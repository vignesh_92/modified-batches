// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 8/1/2011 12:17:38 PM
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   WIFSSQLCon.java

package com.wifs.dao.dbconnection;

import com.wifs.utilities.PropertyFileReader;
import java.sql.*;
import java.util.Properties;
import org.apache.log4j.Logger;

public class WIFSSQLCon
{

    private Connection Obj_Connection;
    public static Logger logger = Logger.getLogger("WIFSSQLCon");
    public Connection getWifsSQLConnect() throws SQLException
    {
        try
        {
            try
            {
                PropertyFileReader objPropertyFileReader = new PropertyFileReader();
                Properties properties = objPropertyFileReader.getFIMSSQLDBConfig();
                String sqlDriver = properties.getProperty( "SQL.Driver").trim();
                String sqlURL = properties.getProperty("SQL.URL").trim();
                String sqlUserID = properties.getProperty("SQL.UserID").trim();
                String sqlPssWrd = properties.getProperty("SQL.PssWrd").trim();
                Class.forName(sqlDriver);
                Obj_Connection = DriverManager.getConnection(sqlURL, sqlUserID, sqlPssWrd);
            }
            catch(SQLException se)
            {
                logger.error((new StringBuilder("Error in Getting Connection : ")).append(se.getMessage()).toString());
                se.printStackTrace();
            }
            catch(Exception DAOEx)
            {
                logger.error((new StringBuilder("Error in Getting Connection # Properties : ")).append(DAOEx).toString());
            }
        }
        catch(Exception ex)
        {
            logger.error((new StringBuilder("FAIL!! SQL CONNECT. ERROR : ")).append(ex.getMessage()).toString());
        }
        return Obj_Connection;
    }
    public Connection getNetMagicWifsSQLConnect() throws SQLException
    {
        try
        {
            try
            {
                PropertyFileReader objPropertyFileReader = new PropertyFileReader();
                Properties properties = objPropertyFileReader.getFIMSSQLDBConfig();
                String sqlDriver = properties.getProperty( "SQL.Driver").trim();
                String sqlURL = "jdbc:sqlserver://180.179.123.98:1433;databaseName=EKYC";
                String sqlUserID = "ekyadmin";
                String sqlPssWrd = "admin1@32016";
                Class.forName(sqlDriver);
                Obj_Connection = DriverManager.getConnection(sqlURL, sqlUserID, sqlPssWrd);
            }
            catch(SQLException se)
            {
                logger.error((new StringBuilder("Error in Getting Connection : ")).append(se.getMessage()).toString());
                se.printStackTrace();
            }
            catch(Exception DAOEx)
            {
                logger.error((new StringBuilder("Error in Getting Connection # Properties : ")).append(DAOEx).toString());
            }
        }
        catch(Exception ex)
        {
            logger.error((new StringBuilder("FAIL!! SQL CONNECT. ERROR : ")).append(ex.getMessage()).toString());
        }
        return Obj_Connection;
    }
}
