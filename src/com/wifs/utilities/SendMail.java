package com.wifs.utilities;

import com.wifs.DBFForwardFeed.ConstansBO;
import com.wifs.dao.helper.DBConstants;
import java.io.File;
import java.util.*;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.*;

import org.apache.log4j.Logger;

public class SendMail extends Authenticator
{

    private static String userName = "";
    private static String userPassword = "";
    private static String bccAddr = "";
    
    public static Logger logger = Logger.getLogger("SendMail.class");

    public PasswordAuthentication getPasswordAuthentication()
    {
        return new PasswordAuthentication(userName, userPassword);
    }

    public boolean SendMailWithDBFFile(String filename, String misReport, String newFolio, String subject)
        throws Exception
    {
        String [] fileNames = new String [] {filename};
        return SendMailWithDBFFile(fileNames, misReport, newFolio, subject);
    }
    
    /**
     * Send DBF in mail to ops
     * @param fileNames - To attach multiple files
     * @param misReport
     * @param newFolio
     * @param subject
     * @return
     * @throws Exception 
     */
    public boolean SendMailWithDBFFile(String [] fileNames, String misReport, String newFolio, String subject)
        throws Exception
    {
        logger.info("## -- START -Send Mail -- #");
        boolean returnFlag = false;
    	ConstansBO	objConstansBO	=	new ConstansBO("");
    	Properties dbfProperties = new Properties();
    	DBConstants objDBConstants	=	new DBConstants();
		PropertyFileReader objPropertyFileReader = new PropertyFileReader();
		dbfProperties		=	objPropertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);
		
    	String to = dbfProperties.getProperty("To");
    	String cc = dbfProperties.getProperty("CC");
		  
    	Properties property = new Properties();
    	property = objPropertyFileReader.getEmailConfig();

		Properties propertyMail = new Properties();
		this.userName		=	property.getProperty("userName");
		this.userPassword	=	property.getProperty("userPassword");
		this.bccAddr = property.getProperty("bccAddress");

		propertyMail.put("mail.smtp.host", property.getProperty("host"));
		propertyMail.put("mail.smtp.port", property.getProperty("port"));
		propertyMail.put("mail.smtp.auth", "true");
		
		String from = property.getProperty("userName");
		
		logger.info("from id : "+from);
		
		Authenticator auth = new SendMail();
        Session mailSession = Session.getInstance(propertyMail, auth);
        
		Message simpleMessage = new MimeMessage(mailSession);

		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		

		try {
			fromAddress = new InternetAddress(from);
			toAddress = new InternetAddress(to);
			
	        StringTokenizer st = new StringTokenizer(cc, ";");
	        InternetAddress[] ccAddress = new InternetAddress[st.countTokens()];
	        for(int i=0;st.hasMoreTokens();i++){
	        	ccAddress[i] = new InternetAddress(st.nextToken());
	        }
		
			simpleMessage.setFrom(fromAddress);
			simpleMessage.setRecipient(RecipientType.TO, toAddress);
			simpleMessage.setRecipients(RecipientType.CC, ccAddress);
			
			simpleMessage.setSubject(subject+" ("+objConstansBO.getTodayDate("MM/dd/yyyy")+")");
			simpleMessage.setSentDate(new Date());
			
			// attach the file to the message
			Multipart mp = new MimeMultipart();
			//MimeBodyPart mbp2 = new MimeBodyPart();
			MimeBodyPart mbp3 = new MimeBodyPart();
			MimeBodyPart mbp4 = new MimeBodyPart();
			
			// attach the DBF Forwardfeed Report file to the message
                        if (fileNames != null){
                            logger.info("Attachments: "+ Arrays.toString(fileNames));
                            
                        for (String fileName : fileNames){
                            try {
			if(!ConstansBO.isEmpty(fileName) && new File(fileName).isFile()){
                            MimeBodyPart mbp2 = new MimeBodyPart();
                            FileDataSource fds = new FileDataSource(fileName);
                            mbp2.setDataHandler(new DataHandler(fds));
                            mbp2.setFileName(fds.getName());
                            mp.addBodyPart(mbp2);
			}
                            }catch (Exception smEx){
                                logger.info("SM - Failed in attachment. $ File: "+ fileName +" # EX: "+smEx);
                            }
                        }
                        }
			
			// attach the MIS Report file to the message
			if(!misReport.equals(""))
			{
				FileDataSource fds1 = new FileDataSource(misReport);
				mbp3.setDataHandler(new DataHandler(fds1));
				mbp3.setFileName(fds1.getName());
	    		mp.addBodyPart(mbp3);
			}
			
			// attach the New Folio Report files
			if(!newFolio.equals(""))
			{
				FileDataSource fds2 = new FileDataSource(newFolio);
				mbp4.setDataHandler(new DataHandler(fds2));
				mbp4.setFileName(fds2.getName());
	    		mp.addBodyPart(mbp4);
			}
			
		      
			MimeBodyPart textPart = new MimeBodyPart();
		    textPart.setContent(" Forward Feed on "+objDBConstants.getDBCurrentDate(),"text/html");
		    mp.addBodyPart(textPart);
    		
				
    		simpleMessage.setContent(mp);
			Transport.send(simpleMessage);
			returnFlag = true;
		} catch (MessagingException e) {
			logger.error(to+"SEND MAIL Messaging Exception : "+e);
		}
        logger.info("## -- END -Send Mail -- #");
        return returnFlag;
    }

    public boolean sendDailyReport(String reportText)
        throws Exception
    {
		ConstansBO	objConstansBO	=	new ConstansBO("");
    	Properties dbfProperties = new Properties();
		PropertyFileReader objPropertyFileReader = new PropertyFileReader();
		dbfProperties		=	objPropertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);
		
    	String to = dbfProperties.getProperty("ToReport");
    	String cc = dbfProperties.getProperty("CCReport");
		  
    	Properties property = new Properties();
    	property = objPropertyFileReader.getEmailConfig();

		Properties propertyMail = new Properties();
		this.userName		=	property.getProperty("userName");
		this.userPassword	=	property.getProperty("userPassword");
		this.bccAddr = property.getProperty("bccAddress");

		propertyMail.put("mail.smtp.host", property.getProperty("host"));
		propertyMail.put("mail.smtp.port", property.getProperty("port"));
		propertyMail.put("mail.smtp.auth", "true");
		
		String from = property.getProperty("userName");
		
		Authenticator auth = new SendMail();
        Session mailSession = Session.getInstance(propertyMail, auth);
        
		Message simpleMessage = new MimeMessage(mailSession);

		InternetAddress fromAddress = null;

		try 
		{
			fromAddress = new InternetAddress(from);
			
			StringTokenizer stTo = new StringTokenizer(to, ";");
	        InternetAddress[] toAddress = new InternetAddress[stTo.countTokens()];
	        for(int i=0;stTo.hasMoreTokens();i++){
	        	toAddress[i] = new InternetAddress(stTo.nextToken());
	        }
			
	        StringTokenizer st = new StringTokenizer(cc, ";");
	        InternetAddress[] ccAddress = new InternetAddress[st.countTokens()];
	        for(int i=0;st.hasMoreTokens();i++){
	        	ccAddress[i] = new InternetAddress(st.nextToken());
	        }
		
			simpleMessage.setFrom(fromAddress);
			simpleMessage.setRecipients(RecipientType.TO, toAddress);
			simpleMessage.setRecipients(RecipientType.CC, ccAddress);
			
			simpleMessage.setSubject(dbfProperties.getProperty("subjectReport")+" ("+objConstansBO.getTodayDate("MM/dd/yyyy")+")");
			simpleMessage.setSentDate(new Date());
			
			// attach the file to the message
			Multipart mp 	= new MimeMultipart();
			String greating	=	"";
			String senderSign	=	"";
		     
			greating	=	dbfProperties.getProperty("Greeting");
			senderSign	=	dbfProperties.getProperty("SenderSign");
			
			MimeBodyPart textPart = new MimeBodyPart();
		    textPart.setContent(greating+reportText+senderSign,"text/html");
		    mp.addBodyPart(textPart);
    		
				
    		simpleMessage.setContent(mp);
			Transport.send(simpleMessage);
			logger.info("Mail Success");
			return true;
			
		} catch (MessagingException e) {
			logger.error(to+"SEND MAIL Messaging Exception : "+e);
			return false;
		}
	}

    public boolean sendReportFile(String xmlFile, String textFile, String subject, String content)
        throws Exception
    {
    	Properties dbfProperties = new Properties();
		PropertyFileReader objPropertyFileReader = new PropertyFileReader();
		dbfProperties		=	objPropertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);
		
    	String to = dbfProperties.getProperty("To");
    	String cc = dbfProperties.getProperty("CC");
		  
    	Properties property = new Properties();
    	property = objPropertyFileReader.getEmailConfig();

		Properties propertyMail = new Properties();
		this.userName		=	property.getProperty("userName");
		this.userPassword	=	property.getProperty("userPassword");
		this.bccAddr = property.getProperty("bccAddress");

		propertyMail.put("mail.smtp.host", property.getProperty("host"));
		propertyMail.put("mail.smtp.port", property.getProperty("port"));
		propertyMail.put("mail.smtp.auth", "true");
		
		String from = property.getProperty("userName");
		
		//logger.info("from id : "+from);
		
		Authenticator auth = new SendMail();
        Session mailSession = Session.getInstance(propertyMail, auth);
        
		Message simpleMessage = new MimeMessage(mailSession);

		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		

		try {
			fromAddress = new InternetAddress(from);
			toAddress = new InternetAddress(to);
			
	        StringTokenizer st = new StringTokenizer(cc, ";");
	        InternetAddress[] ccAddress = new InternetAddress[st.countTokens()];
	        for(int i=0;st.hasMoreTokens();i++){
	        	ccAddress[i] = new InternetAddress(st.nextToken());
	        }
		
			simpleMessage.setFrom(fromAddress);
			simpleMessage.setRecipient(RecipientType.TO, toAddress);
			simpleMessage.setRecipients(RecipientType.CC, ccAddress);
			
			//simpleMessage.setSubject(dbfProperties.getProperty("subjectNFO")+" ("+objConstansBO.getTodayDate("MM/dd/yyyy")+")");
			simpleMessage.setSubject(subject);
			simpleMessage.setSentDate(new Date());
			
			// attach the file to the message
			Multipart mp = new MimeMultipart();
			MimeBodyPart mbp2 = new MimeBodyPart();
			MimeBodyPart mbp3 = new MimeBodyPart();
			
			// attach the xml file to the message
			if(!xmlFile.equals(""))
			{
				FileDataSource fds = new FileDataSource(xmlFile);
				mbp2.setDataHandler(new DataHandler(fds));
				mbp2.setFileName(fds.getName());
				mp.addBodyPart(mbp2);
			}
			
			// attach the xml file to the message
			if(!textFile.equals(""))
			{
				FileDataSource fds = new FileDataSource(textFile);
				mbp3.setDataHandler(new DataHandler(fds));
				mbp3.setFileName(fds.getName());
				mp.addBodyPart(mbp3);
			}
			
			MimeBodyPart textPart = new MimeBodyPart();
		    //textPart.setContent("NFO Schemes List Attached... "+objDBConstants.getDBCurrentDate(),"text/html");
			textPart.setContent(content,"text/html");
			
		    mp.addBodyPart(textPart);
    		
				
    		simpleMessage.setContent(mp);
			Transport.send(simpleMessage);
			return true;
		} catch (MessagingException e) {
			logger.error(to+"SEND MAIL Messaging Exception : "+e);
			return false;
		}
    }

    /**
     * Send direct feed upload status mail to operation
     *
     * @param uploadStatus
     * @param uploadToServer
     * @return
     * @throws Exception
     */
    public boolean SendDirectUploadStatusMail(boolean uploadStatus, String uploadToServer) throws Exception {
        logger.info("## -- START - Upload status Send Mail. UPLOAD: " + uploadStatus + " -- #");
        boolean returnFlag = false;

        //Send mail only in failed case 
        if (!uploadStatus) {

            try {

                Properties dbfProperties = new Properties();
                PropertyFileReader objPropertyFileReader = new PropertyFileReader();
                dbfProperties = objPropertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);

                String to = dbfProperties.getProperty("DIRECT.UPLOAD.STATUS.TO");
                String cc = dbfProperties.getProperty("DIRECT.UPLOAD.STATUS.CC");
                String subject = uploadToServer + ": Direct upload failed."//Since uploadStatus == false,
                        , bodyContent = "Direct upload to " + uploadToServer + " server is failed, Error happens in our server.";

                Properties property = new Properties();
                property = objPropertyFileReader.getEmailConfig();

                Properties propertyMail = new Properties();
                this.userName = property.getProperty("userName");
                this.userPassword = property.getProperty("userPassword");
                this.bccAddr = property.getProperty("bccAddress");

                propertyMail.put("mail.smtp.host", property.getProperty("host"));
                propertyMail.put("mail.smtp.port", property.getProperty("port"));
                propertyMail.put("mail.smtp.auth", "true");

                String from = property.getProperty("userName");

                logger.info("from id : " + from);

                Authenticator auth = new SendMail();
                Session mailSession = Session.getInstance(propertyMail, auth);

                Message simpleMessage = new MimeMessage(mailSession);

                InternetAddress fromAddress = null;
                InternetAddress toAddress = null;

                fromAddress = new InternetAddress(from);
                toAddress = new InternetAddress(to);

                StringTokenizer st = new StringTokenizer(cc, ";");
                InternetAddress[] ccAddress = new InternetAddress[st.countTokens()];
                for (int i = 0; st.hasMoreTokens(); i++) {
                    ccAddress[i] = new InternetAddress(st.nextToken());
                }

                simpleMessage.setFrom(fromAddress);
                simpleMessage.setRecipient(RecipientType.TO, toAddress);
                simpleMessage.setRecipients(RecipientType.CC, ccAddress);
                simpleMessage.setSubject(subject);
                simpleMessage.setSentDate(new Date());

                Multipart mp = new MimeMultipart();
                MimeBodyPart textPart = new MimeBodyPart();
                textPart.setContent(bodyContent, "text/html");
                mp.addBodyPart(textPart);

                simpleMessage.setContent(mp);
                Transport.send(simpleMessage);
                returnFlag = true;
            } catch (MessagingException mEx) {
                logger.error("Direct upload failed. Exception: " + mEx);
                throw mEx;
            }
        }
        logger.info("## -- END -Send Mail -- #");
        return returnFlag;
    }
}