package com.wifs.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyFileReader { 
	public static Logger logger = Logger.getLogger("PropertyFileReader.class");
	
	public Properties getFIMSSQLDBConfig() throws Exception {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(PropertiesConstant.FIMSSQLDB_PROPERTY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getFIMSSQLDBConfig! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	
	
	public Properties getEmailConfig() throws Exception {
		Properties properties = new Properties();
		try {
/*			
			InputStream IS = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("SendMailConfig.properties");
			properties.load(IS);
			IS.close();
*/
			properties.load(new FileInputStream(PropertiesConstant.SEND_MAIL_CONFIG));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.EmailConfig! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	public Properties userMailDirectory() throws Exception
	{
		Properties properties = new Properties();
		
		try {
/*			
			InputStream IS = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("MailDirectory.properties");
			properties.load(IS);
			IS.close();
*/
			properties.load(new FileInputStream(PropertiesConstant.MAIL_DIRECTORY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.userMailDirectory! : " + e);
			throw new Exception(e);
		}		
		return properties;
	}
	public Properties getGeneralConfig() throws Exception {
		Properties properties = new Properties();
		try {
/*
			InputStream IS = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("General.properties");
			properties.load(IS);
			IS.close();
*/
			properties.load(new FileInputStream(PropertiesConstant.GENERAL_PROPERTY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getGeneralConfig! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	public static Properties getGeneralConfig_Static() throws Exception {
		PropertyFileReader propertyFileReader = new PropertyFileReader();
		return propertyFileReader.getGeneralConfig();
	}
	
	public Properties templatesFileReader(String dirPath) throws Exception
	{
		Properties properties = new Properties();
		
		try {
			InputStream IS = new FileInputStream(dirPath);
			properties.load(IS);
			IS.close();
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.FileReader! : " + e);
			throw new Exception(e);
		}		
		return properties;
	}
	
	public Properties getValueResearchRatings() throws Exception {
		Properties properties = new Properties();
		try {
/*
			InputStream IS = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("ValueResearchRatings.properties");
			properties.load(IS);
			IS.close();
*/
			properties.load(new FileInputStream(PropertiesConstant.VALUE_RESEARCH_RATINGS));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getValueResearchRatings! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	
	public Properties getQuickProcessProperties() throws Exception {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(PropertiesConstant.QUICKPROCESS_PROPERTY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getQuickProcessProperties! : " + e);
			throw new Exception(e);
		}
		return properties;
	}

	public Properties getMessagesProperties() throws Exception {
		Properties properties = new Properties();
		try {
/*
			InputStream IS = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("Messages.properties");
			properties.load(IS);
			IS.close();
*/
			properties.load(new FileInputStream(PropertiesConstant.MESSAGES_PROPERTY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getMessagesProperties! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	
	public Properties getEqTransactionMailTemplate() throws Exception {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(PropertiesConstant.EQ_TRNS_MAIL_PROPERTY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getEqTransactionMailTemplate! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	public Properties getEqEnrollmentMailTemplate() throws Exception {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(PropertiesConstant.EQ_ENROLL_MAIL_PROPERTY));
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getEqTransactionMailTemplate! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
	
	public static boolean isWindowsOS(){
		boolean isWindowsOS=false;
		try{
			String OS = System.getProperty("os.name").toLowerCase();
			logger.error("OS : "+OS);
			if(OS.indexOf("win") >= 0){
				isWindowsOS=true;
			}
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return isWindowsOS;
	}
	public Properties getConfigProperties() throws Exception {
		Properties properties = new Properties();
		try {
			if(isWindowsOS()){
				properties.load(new FileInputStream(PropertiesConstant.CONFIG_PROPERTIES));
			}else{
				properties.load(new FileInputStream(PropertiesConstant.LINUX_CONFIG_PROPERTIES));
			}
			
		} catch (Exception e) {
			logger.error("Exception in PropertyFileReader.getConfigProperties! : " + e);
			throw new Exception(e);
		}
		return properties;
	}
}
