package com.wifs.utilities;

import org.apache.log4j.Logger;

import java.io.File;
import java.util.Properties;
public  final class PropertiesConstant 
{ 
	public static final Logger logger= Logger.getLogger("PropertiesConstant.class");
	
	public static final String CAMS_DIRECT_UPLOAD = "CAMS_DIRECT_UPLOAD";
        public static final String CAMS_DIRECT_UPLOAD_FAILED_DBF = "CAMS_DIRECT_UPLOAD_FAILED_DBF";
        public static final String CAMS_DIRECT_UPLOAD_ALL_DBF = "CAMS_DIRECT_UPLOAD_ALL_DBF";
        public static final String KARVY_DIRECT_UPLOAD = "KARVY_DIRECT_UPLOAD";

	public static PropertyFileReader propertyFileReader = new PropertyFileReader();
	public static Properties properties = new Properties();
	static {
		try {
			properties =	propertyFileReader.getConfigProperties();
		}catch(Exception ex)
		{
			logger.error(" Error in loading property file ..");
		}
	}
	
	public static final String DATA_FOLDER_PATH =properties.getProperty("DATA_FOLDER_PATH");
	
	public static final String FIMSSQLDB_PROPERTY = properties.getProperty("FIMSSQLDB_PROPERTY");
	public static final String GENERAL_PROPERTY =properties.getProperty("GENERAL_PROPERTY");
	public static final String SEND_MAIL_CONFIG = properties.getProperty("SEND_MAIL_CONFIG");
	public static final String MAIL_DIRECTORY =properties.getProperty("MAIL_DIRECTORY");
	public static final String VALUE_RESEARCH_RATINGS = properties.getProperty("VALUE_RESEARCH_RATINGS");
	public static final String MESSAGES_PROPERTY = properties.getProperty("MESSAGES_PROPERTY");
	public static final String SEND_SMS_CONFIG = properties.getProperty("SEND_SMS_CONFIG"); 
	public static final String SEND_SMS_CONFIG_PROFILE_1  = properties.getProperty("SEND_SMS_CONFIG_PROFILE_1");
	public static final String SEND_SMS_CONFIG_PROFILE_2 = properties.getProperty("SEND_SMS_CONFIG_PROFILE_2");
	public static final String REVERSERFEED_CONFIG = properties.getProperty("REVERSERFEED_CONFIG");
	public static final String INVESTOR_ACTIVATION_UPLOADS_PATH = properties.getProperty("INVESTOR_ACTIVATION_UPLOADS_PATH");
	public static final String FORWARDFEED_CONFIG  = properties.getProperty("FORWARDFEED_CONFIG");
	public static final String TEMPLATE_PATH = properties.getProperty("TEMPLATE_PATH");
	public static final String KYC_APPLICATION_DISCREPANCY = properties.getProperty("KYC_APPLICATION_DISCREPANCY");
	public static final String KYC_APPLICATION_DISCREPANCY_MAIL = properties.getProperty("KYC_APPLICATION_DISCREPANCY_MAIL");
	public static final String FIRC_MAIL = properties.getProperty("FIRC_MAIL");
	public static final String FIA_Location = properties.getProperty("FIA_Location");
	public static final String FIA_Mail = properties.getProperty("FIA_Mail");
	public static final String HTML_EMAIL_PATH = properties.getProperty("HTML_EMAIL_PATH");
	public static final String USER_REMINDER_PATH = properties.getProperty("USER_REMINDER_PATH");
	public static final String PRE_PACKAGE_PORTFOLIOS = properties.getProperty("PRE_PACKAGE_PORTFOLIOS");
	public static final String TOP_40 = properties.getProperty("TOP_40");
	public static final String DYNAMIC_SCHEMES_EXPLORE = properties.getProperty("DYNAMIC_SCHEMES_EXPLORE");
	public static final String FI_PLAN_CONFIG = properties.getProperty("FI_PLAN_CONFIG"); 

	// public static final String AOF_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\pdf\\";
	public static final String AOF_PDF_PATH = properties.getProperty("AOF_PDF_PATH");
	//public static final String SIP_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\sipPdf\\";
	public static final String SIP_PDF_PATH = properties.getProperty("SIP_PDF_PATH");
	//public static final String QUICK_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\quicktransfer\\";
	public static final String QUICK_PDF_PATH = properties.getProperty("QUICK_PDF_PATH");
	
	public static final String JAVAIMAGES_PATH = properties.getProperty("JAVAIMAGES_PATH");
	public static final String INVESTOR_COURIER_SENT_PATH = properties.getProperty("INVESTOR_COURIER_SENT_PATH");
	public static final String INVESTOR_QTSTATUS_SENT_PATH = properties.getProperty("INVESTOR_QTSTATUS_SENT_PATH");
	public static final String INVESTOR_FIRC= properties.getProperty("INVESTOR_FIRC");
	public static final String INVESTOR_IDS= properties.getProperty("INVESTOR_IDS");
	public static final String INVESTOR_PANS= properties.getProperty("INVESTOR_PANS");
	
	public static final String INVESTOR_PAN_ZIP= properties.getProperty("INVESTOR_PAN_ZIP");	
	public static final String INVESTOR_PAN_FOLIO_PATH = properties.getProperty("PANORFOLIO");


	public static final String INVESTOR_DOCSTATUS_SENT_PATH = properties.getProperty("INVESTOR_DOCSTATUS_SENT_PATH");
	public static final String QUICKPROCESS_PROPERTY = properties.getProperty("QUICKPROCESS_PROPERTY");
	public static final String CONFIG_PROPERTIES = "C:\\WIFS_LOGS\\JBoss\\wifs\\config\\Properties\\docConfig.properties";
	public static final String LINUX_CONFIG_PROPERTIES = "/data/WIFS_LOGS/JBoss/wifs/config/Properties/docConfig.properties";
	public static final String INVESTOR_TRANSACTION_STATEMENT = properties.getProperty("INVESTOR_TRANSACTION_STATEMENT");
//	public static final String INVESTOR_TRANSACTION_STATEMENT = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\statementPdf\\";
	public static final String FD_PDF_PATH = properties.getProperty("FD_PDF_PATH");
	//public static final String FD_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\fixedDeposites\\";
	//public static final String TRANSACTION_PDF_PATH = "C:\\WIFS_LOGS\\JBoss\\wifs\\TransactionPDF\\";
	public static final String TRANSACTION_PDF_PATH = properties.getProperty("TRANSACTION_PDF_PATH");
	public static final String MONTHLY_TRANSACTION_STATEMENT = properties.getProperty("MONTHLY_TRANSACTION_STATEMENT");
	//public static final String MONTHLY_TRANSACTION_STATEMENT = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\monthlyStatementPDF\\";
	
	public static final String ADVISOR_PHOTOS_PATH = properties.getProperty("ADVISOR_PHOTOS_PATH");
	public static final String ADVISOR_PHOTOS_PATH_WEB = properties.getProperty("ADVISOR_PHOTOS_PATH_WEB");
	public static final String ADVISOR_PHOTOS_PATH_WEB_WIFS = properties.getProperty("ADVISOR_PHOTOS_PATH_WEB_WIFS");
	
	public static final String PROPOSAL_PDF_PATH = properties.getProperty("PROPOSAL_PDF_PATH");
//	public static final String PROPOSAL_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\proposals\\";
	
	public static final String OFF_LINE_REDEMPTION_PDF_PATH = properties.getProperty("OFF_LINE_REDEMPTION_PDF_PATH");
	//public static final String OFF_LINE_REDEMPTION_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\offLinePDF";
	
	public static final String INVESTOR_FORM_DOWNLOAD = properties.getProperty("INVESTOR_FORM_DOWNLOAD");
	//public static final String INVESTOR_FORM_DOWNLOAD = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\investorForms\\";	
	
	
	public static final String VIP_PDF_PATH = properties.getProperty("VIP_PDF_PATH");
	//public static final String VIP_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\vipPdf\\";
	
	public static final String HOLDINGS_XL_PATH = properties.getProperty("HOLDINGS_XL_PATH");
	
	public static final String EQ_TEMPLATE_PATH = properties.getProperty("EQ_TEMPLATE_PATH");
	
	public static final String EQ_PDF_PATH = properties.getProperty("EQ_PDF_PATH");
	//public static final String EQ_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\equiytPdf\\";
	public static final String EQ_TRNX_BILL_PDF_PATH = EQ_PDF_PATH+"<<USER_ID>>"+File.separator+"TransactionBill"+File.separator;
	
	public static final String GN_TEMPLATE_PATH = properties.getProperty("GN_TEMPLATE_PATH");
	//public static final String GN_TEMPLATE_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\config\\Templates\\general\\";
	
	public static final String EMKAYPDFS = properties.getProperty("EMKAYPDFS");
	//public static final String EMKAYPDFS = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\config\\Templates\\Emkay\\";
	
	public static final String EQ_TRNS_MAIL_PROPERTY = properties.getProperty("EQ_TRNS_MAIL_PROPERTY");
	public static final String EQ_ENROLL_MAIL_PROPERTY = properties.getProperty("EQ_ENROLL_MAIL_PROPERTY");
	
	
	public static final String CHANGEOFBANK_PDF_PATH = properties.getProperty("CHANGEOFBANK_PDF_PATH");
	//public static final String CHANGEOFBANK_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\bankChange\\";
	
	public static final String QT_DOC_PDF_PATH = properties.getProperty("QT_DOC_PDF_PATH");
	//public static final String QT_DOC_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\QtDoc\\";
	
	public static final String TPSL_DOC_UPLOAD_PATH = properties.getProperty("TPSL_DOC_UPLOAD_PATH");
	//public static final String TPSL_DOC_UPLOAD_PATH = "C:\\WIFS_LOGS\\JBoss\\wifs\\TPSL\\";
	
	public static final String TI_TRNS_UPLOAD_PATH = properties.getProperty("TI_TRNS_UPLOAD_PATH");
	
	public static final String SIP_TEMPLATE_PATH = properties.getProperty("SIP_TEMPLATE_PATH");
	public static final String INVESTOR_TRANSACTION_ACCESS_EMAIL_TMPLT = properties.getProperty("INVESTOR_TRANSACTION_ACCESS_EMAIL_TMPLT");
	public static final String ETF_SIP_TEMPLATE_PATH = properties.getProperty("ETF_SIP_TEMPLATE_PATH");
	
	
	
	public static final String VIP_TEMPLATE_PATH = properties.getProperty("VIP_TEMPLATE_PATH");
	
	public static final String ETF_SIP_PDF_PATH = properties.getProperty("ETF_SIP_PDF_PATH");
	//public static final String ETF_SIP_PDF_PATH = "\\\\wifsserver\\WIFS_LOGS\\JBoss\\wifs\\etfsipPdf\\";
	
	public static final String SIP_TPSL_FAILED_TRANSACTION_EMAIL_TMPLT = properties.getProperty("SIP_TPSL_FAILED_TRANSACTION_EMAIL_TMPLT");
	public static final String NPS_REGISTRATION_EMAIL_TMPLT = properties.getProperty("NPS_REGISTRATION_EMAIL_TMPLT");
	
	//private static final String SIP_EXCEL_DIR = "C:\\WIFS_LOGS\\JBoss\\wifs\\SipTPSLExcel\\";
	
	public static final String SIP_EXCEL_DIR = properties.getProperty("SIP_EXCEL_DIR");
	//private static final String SIP_EXCEL_DIR = "C:\\WIFS_LOGS\\JBoss\\wifs\\sipWithOutPayPdf\\";
	public static final String SIP_WITH_OUT_PAY_PDF = properties.getProperty("SIP_WITH_OUT_PAY_PDF");
	
	public static final String INFRA_FD_PDF = properties.getProperty("IDFC_FD"); 
	public static final String IDFC_FD = properties.getProperty("IDFC_FD");
	public static final String LANDT_FD = properties.getProperty("IDFC_FD");
	public static final String IDFC_INFRA_PDF_PATH = properties.getProperty("IDFC_INFRA_PDF_PATH");
		
	//public static final String SECONDBANK_PDF_PATH = "C:\\WIFS_LOGS\\JBoss\\wifs\\secondBank\\";
	
	public static final String SECONDBANK_PDF_PATH = properties.getProperty("SECONDBANK_PDF_PATH");
	public static final String NOMINEE_PDF_PATH = properties.getProperty("NOMINEE_PDF_PATH");
	
	//public static final String CAPITAL_GAIN_XL_PATH = "C:\\WIFS_LOGS\\JBoss\\wifs\\CapitalGain\\";
	
	public static final String CAPITAL_GAIN_XL_PATH = properties.getProperty("CAPITAL_GAIN_XL_PATH");
	public static final String ACCT_ACTIVATION_XL_PATH = properties.getProperty("ACCT_ACTIVATION_XL_PATH");
	
	public static final String SIP_TPSL_IMPORT_FEED_MAIL_TMPLT = properties.getProperty("SIP_TPSL_IMPORT_FEED_MAIL_TMPLT");
	
	public static final String IPO_TEMPLATE_PATH = properties.getProperty("IPO_TEMPLATE_PATH");
	public static final String IPO_EMAIL_TMPLT = properties.getProperty("IPO_EMAIL_TMPLT");
	public static final String LIC_TEMPLATE_PATH = properties.getProperty("LIC_TEMPLATE_PATH");
	public static final String KOTAK_TEMPLATE_PATH = properties.getProperty("KOTAK_TEMPLATE_PATH");
	public static final String ICICI_TEMPLATE_PATH = properties.getProperty("ICICI_TEMPLATE_PATH");
	public static final String BIRLA_TEMPLATE_PATH = properties.getProperty("BIRLA_TEMPLATE_PATH");
	
	
	public static final String INSURANCE_PDF_PATH = properties.getProperty("INSURANCE_PDF_PATH");

	public static final String TRNSACTION_FILE_UPLOAD_EMAIL_TMPLT = properties.getProperty("TRNSACTION_FILE_UPLOAD_EMAIL_TMPLT");
	
	public static final String FOLLOW_UP_USER_TMPLT = properties.getProperty("FOLLOW_UP_USER_TMPLT");	

	//Temporarily keep + create file
	public static final String TEMPORARY_FILE_PATH = properties.getProperty("TEMPORARY_FILE_PATH");
	public static final String INSURANCE_MAIL_PROPERTY = properties.getProperty("INSURANCE_MAIL_PROPERTY");
	public static final String CONTACT_IMPORT_PROPERTY = properties.getProperty("CONTACT_IMPORT_PROPERTY");

	public static final String INVESTORS_TEMPLATE_PATH = properties.getProperty("INVESTORS_TEMPLATE_PATH");
	
	public static final String EQ_ADMIN_DOWNLOADS = properties.getProperty("EQ_ADMIN_DOWNLOADS");
	public static final String NPS_NDL_DOC_PATH = properties.getProperty("NPS_NDL_DOC_PATH");
	public static final String USER_DOCUMENTS_UPLOADED = properties.getProperty("USER_DOCUMENTS_UPLOADED");
	public static final String ADVISOR_DIRECT_REGISTRATION_PDF = properties.getProperty("ADVISOR_DIRECT_REGISTRATION_PDF");
	public static final String ADVISOR_DIRECT_REGISTRATION_SAVE_PDF = properties.getProperty("ADVISOR_DIRECT_REGISTRATION_SAVE_PDF");
	public static final String INS_BROCHURE_PATH = properties.getProperty("INS_BROCHURE_PATH");
	public static final String RELIANCE_ATM_PDF_PATH = properties.getProperty("RELIANCE_ATM_PDF_PATH");
//	public static final String RELIANCE_ATM_PDF_PATH = "C:\\WIFS_LOGS\\JBoss\\wifs\\RELIANCE_ATM_FORM\\";

	public static final String ZENDESK_CONFIG = properties.getProperty("ZENDESK_CONFIG");
	public static final String MORNING_STAR_CONFIG = properties.getProperty("MORNING_STAR_CONFIG");
	public static final String SEND_INSURANCE_EDUCATION_MAIL_CONFIG = properties.getProperty("SEND_INSURANCE_EDUCATION_MAIL_CONFIG");
	public static final String PLAN_PART1_PDF_TEMPLATE = properties.getProperty("PLAN_PART1_PDF_TEMPLATE");
	public static final String PLAN_PART2_EVERY_GOAL_PDF_TEMPLATE = properties.getProperty("PLAN_PART2_EVERY_GOAL_PDF_TEMPLATE");
	public static final String PLAN_PART2_EVERY_GOAL_CASH_PDF_TEMPLATE = properties.getProperty("PLAN_PART2_EVERY_GOAL_CASH_PDF_TEMPLATE");
	public static final String PLAN_PART3_PDF_TEMPLATE = properties.getProperty("PLAN_PART3_PDF_TEMPLATE");
	public static final String BROKERAGE_FILE_PATH = properties.getProperty("BROKERAGE_FILE_PATH");
	public static final String GEN_PAN_FOL_EXC = properties.getProperty("GEN_PAN_FOL_EXC");
	public static final String GEN_CONSUMERCODE_EXCEL = properties.getProperty("GEN_CONSUMERCODE_EXCEL");
//	public static final String PORTFOLIO_REVIEW_PDF_TEMPLATE = properties.getProperty("PORTFOLIO_REVIEW_PDF_TEMPLATE");
	//public static final String INVESTOR_PDF_DOC = "C:\\WIFS_LOGS\\JBoss\\wifs\\FinancialAdvisorUploads\\";
	public static final String INVESTOR_PDF_DOC =properties.getProperty("FINANCIAL_ADVISOR_UPLOAD_PDF"); 
	public static final String RelianceGoldMoneyPDF =properties.getProperty("RelianceGoldMoneyPDF");
	public static final String AUTOMATED_EMAILERS_REPORT =properties.getProperty("AUTOMATED_EMAILERS_REPORT"); 
	public static final String CAMPAIGN_REPORT =properties.getProperty("CAMPAIGN_REPORT"); 
	public static final String MAILER_SCHEDULE_REPORT =properties.getProperty("MAILER_SCHEDULE_REPORT");
	public static final String PAN_MOVING_XL_PATH =properties.getProperty("PAN_MOVING_XL_PATH"); 
	public static final String PAN_MOVING_FROM_FILES =properties.getProperty("PAN_MOVING_FROM_FILES"); 
	public static final String PAN_MOVING_TO_FILES =properties.getProperty("PAN_MOVING_TO_FILES"); 
	public static final String ADVISOR_SUGGESTION_EXCEL =properties.getProperty("ADVISOR_SUGGESTION_EXCEL"); 
        
        public static final String CAMS_TEXT_FILE_DIRECTORY = "CAMSText" + File.separator; //Dynamic parent directory path will be prefixed + CAMSText
        public static final String CAMS_STP_FAIL_MESSAGE_01 = "Message from FundsIndia server - Error in upload CAMS server";
        public static final String TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");//Temporary directory path
}