
package com.wifs.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class FolderZiper 
{
	List<String> filesListInDir = new ArrayList<String>();
	public static Logger logger = Logger.getLogger("FolderZiper.class");
    public void zipFolder(String srcFolder, String destZipFile)
        throws Exception
    {
        ZipOutputStream zip = null;
        FileOutputStream fileWriter = null;
        fileWriter = new FileOutputStream(destZipFile);
        zip = new ZipOutputStream(fileWriter);
        addFolderToZip("", srcFolder, zip);
        zip.flush();
        zip.close();
    }

    private void addFileToZip(String path, String srcFile, ZipOutputStream zip)
        throws Exception
    {
        File folder = new File(srcFile);
        if(folder.isDirectory())
        {
            addFolderToZip(path, srcFile, zip);
        } else
        {
            byte buf[] = new byte[1024];
            FileInputStream in = new FileInputStream(srcFile);
            zip.putNextEntry(new ZipEntry((new StringBuilder(String.valueOf(path))).append("/").append(folder.getName()).toString()));
            int len;
            while((len = in.read(buf)) > 0) 
                zip.write(buf, 0, len);
        }
    }

    private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
        throws Exception
    {
        File folder = new File(srcFolder);
        String as[];
        int j = (as = folder.list()).length;
        for(int i = 0; i < j; i++)
        {
            String fileName = as[i];
            if(path.equals(""))
                addFileToZip(folder.getName(), (new StringBuilder(String.valueOf(srcFolder))).append("/").append(fileName).toString(), zip);
            else
                addFileToZip((new StringBuilder(String.valueOf(path))).append("/").append(folder.getName()).toString(), (new StringBuilder(String.valueOf(srcFolder))).append("/").append(fileName).toString(), zip);
        }

    }

    public void CopyFileToTodaysFOlder(String sourcePath, String targetdirname)
        throws Exception
    {
        try
        {
        	File sourceParentFolder = new File(sourcePath);
    		String listOfFiles[] = sourceParentFolder.list();
    		
    		for(String fileName : listOfFiles){
        	
				String sourceFolder = sourcePath + File.separator + fileName;
				File sourceFile = new File(sourceFolder);
                                String targetZipFile = "";
				
				if (sourceFile.isDirectory()){
					if(fileName.contains(".zip")){
                                            targetZipFile = targetdirname + File.separator + fileName;   
                                        } else {
                                            targetZipFile = targetdirname + File.separator + fileName + ".zip"; 
                                        }
					zipDirectory(sourceFile, targetZipFile);
					
					FileUtils.deleteDirectory(sourceFile);
				}
				else {
					String sourceFile_Single = sourcePath + File.separator + fileName; 
//					if(fileName.contains(".zip")){
//                                            targetZipFile = targetdirname + File.separator + fileName;   
//                                        } else {
//                                            targetZipFile = targetdirname + File.separator + fileName + ".zip"; 
//                                        }
//					
//					zipFile(sourceFile_Single, targetZipFile);
					new File(sourceFile_Single).delete();
				}
    		}
				
			/*		
            File source = new File(fileName+".zip");
            File sourcePath = new File(strDirPath);
            File targetDir = new File(targetdirname);
            //logger.info((new StringBuilder("Copying ")).append(source).append(" file to ").append(targetDir).toString());
            FileUtils.copyFileToDirectory(source, targetDir);
            FileUtils.forceDelete(source);
            //FileUtils.cleanDirectory(sourcePath);
            FileUtils.deleteDirectory(sourcePath);
            */
        }
        catch(Exception e) {
            logger.error("File not moved. EX: "+e);}
    }

    /**
     * This method zips the directory
     * @param dir
     * @param zipDirName
     */
    private void zipDirectory(File dir, String zipDirName) {
        try {
            populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for(String filePath : filesListInDir){
                logger.info("Zipping "+filePath);
                if (new File(filePath).exists()){
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
                }
            }
            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * This method zips the single file
     * @param filePath - Source file path
     * @param zipDirName - Target zip file
     */
    private void zipFile(String filePath, String zipDirName) {
        try {
            //populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            //for(String filePath : filesListInDir){
                logger.info("Zipping "+filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(filePath.lastIndexOf(File.separator)+1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            //}
            zos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * This method populates all the files in a directory to a List
     * @param dir
     * @throws IOException
     */
    private void populateFilesList(File dir) throws IOException {
        File[] files = dir.listFiles();
        for(File file : files){
            if(file.isFile()) filesListInDir.add(file.getAbsolutePath());
            else populateFilesList(file);
        }
    }
}