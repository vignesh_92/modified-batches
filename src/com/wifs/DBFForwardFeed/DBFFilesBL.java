package com.wifs.DBFForwardFeed; 

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.http.entity.mime.content.StringBody;
import org.apache.log4j.Logger;

import com.wifs.DBFForwardFeed.karvy.ProcessBL;
import com.wifs.dao.dbconnection.WIFSSQLCon;
import com.wifs.utilities.FolderZiper;
import com.wifs.utilities.PropertiesConstant;
import com.wifs.utilities.PropertyFileReader;
import com.wifs.utilities.SendMail;
import org.apache.commons.io.FileUtils;

public class DBFFilesBL {
	/*
	 * Pass the Connection Object
	 * Field Information values from database
	 * All Transaction values from database
	 */
	public static Logger logger = Logger.getLogger("DBFFilesBL.class");
        public static BackgroundThreadProcess bkThreadProc = new BackgroundThreadProcess();
        public static String excelFile = "";
        public static boolean excelFileStatus = false;
        public static String failedDbfFiles = "";
	
	public boolean getDBFFiles(String liquidScheme, String searchKey, String searchAMC, String bankVerified, int userId, String finalFeed) throws Exception 
	{
		return getDBFFiles(liquidScheme, searchKey, searchAMC, bankVerified, userId, null);
	}

	/**
	 * 
	 * [Senthil - 10112016 Comment: Old method re-written to handle direct upload to CAMS / KARVY Server]
         * @param doProcess - To do process 1)Get data & upload to CAMS / KARVY server directly 2) Get CAMS / KARVY failed transaction
	 */

	public boolean getDBFFiles(String liquidScheme, String searchKey, String searchAMC, String bankVerified, int userId, String doProcess, String finalFeed) throws Exception 
	{
		
                boolean [] whosProcess = whosProcess(doProcess);
                boolean isCAMS_DirectUpload = whosProcess[0], 
                        isCAMS_DirectUpload_FailedDBF = whosProcess[1], 
                        isCAMS_DirectUpload_AllDBF = whosProcess[2],
                        isKARVY_DirectUpload = whosProcess[3];
		
		boolean mailFlag	=	false;
		ConstansBO objConstansBO	=	new ConstansBO("");
		STPProcessBO objSTPProcessBO	=	new STPProcessBO();
		STPProcessDAO objSTPProcessDAO	=	new STPProcessDAO();
		DBFFilesDAO objDBFFilesDAO			=	new DBFFilesDAO(); // DAO Object
		//DBConstants objDBConstants	=	new DBConstants();
		Connection objConnection	=	null;
		
		PreparedStatement PStmtAutoNo		=	null;
		PreparedStatement PStmtUpdateAutoNo	=	null;
		PreparedStatement NewFolioDocInsert	=	null;
		PreparedStatement pstmForwardFeedUpdate = null;
		
		String strFileDir	=	""; // For writing DBF File
		String currentDate	=	"";
                String strFatcaFileDir  =       "";
		Properties properties = new Properties();
		PropertyFileReader objPropertyFileReader = new PropertyFileReader();
		properties		=	objPropertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);
		String fileDirectoryParent = properties.getProperty("Directory"), 
				newFolioDirectoryParent = properties.getProperty("NewFolioReport"),
                                fatcaDirectoryParent = properties.getProperty("FatcaDirectory");
		String date_formatted_String = (objConstansBO.getTodayDate("MM/dd/yyyy/HH/mm/SS")).replace("/", "");
		strFileDir		= fileDirectoryParent + File.separator + date_formatted_String +"DBF";
                strFatcaFileDir         = fatcaDirectoryParent + File.separator + date_formatted_String + "DBF";
		//String misReportDir	=	properties.getProperty("MISReport")+File.separator+(objConstansBO.getTodayDate("MM/dd/yyyy")).replace("/", "")+"MIS";
		String newFolioDir	=	newFolioDirectoryParent+File.separator+date_formatted_String+"NF";
		//String NFOFileDir	=	properties.getProperty("NFOReport").trim()+File.separator+(objConstansBO.getTodayDate("MM/dd/yyyy")).replace("/", "");
		String stpFileXMLPath	=	properties.getProperty("stpXML").trim()+File.separator+(objConstansBO.getTodayDate("MM/dd/yyyy")).replace("/", "");
		//String newFolioReportForFT	=	properties.getProperty("NewFolioReportForFT")+File.separator+(objConstansBO.getTodayDate("MM/dd/yyyy")).replace("/", "");
		
		String dbfFiles	=	"";
		//String misFiles	=	"";
		String nfFiles	=	"";
		String reportText	=	"";	
		//String ftNewFolioUserTransRefId	=	"";
		
		currentDate		=	objConstansBO.getTodayDateMonthView().replace("/", "");
		
		NewFolioReport objNewFolioReport	=	new NewFolioReport();
		SendMail objSendMail	=	new SendMail();
		
		WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
		objConnection = objWIFSSQLCon.getWifsSQLConnect();
		PreparedStatement objHolidayDate	=	null;
		objHolidayDate	=	objDBFFilesDAO.getPreparedStmtHoliday(objConnection);
		Calendar c1 = Calendar.getInstance();
		
		PreparedStatement pstmtSTPProcessInsert	=	null;
		pstmtSTPProcessInsert	=	objSTPProcessDAO.pstmtSTPProcessInsert(userId, objConnection);
		
		PreparedStatement pstmtSTPProcessTransactionInsert	=	null;
		pstmtSTPProcessTransactionInsert	=	objSTPProcessDAO.pstmtSTPProcessTransactionWiseInsert(userId, objConnection);
		
		if(objDBFFilesDAO.getHoliday(objConstansBO.getTodayDate("MM/dd/yyyy"), objHolidayDate))		// For Holiday Validation
		{
			logger.debug("Today is holiday");
		}
		else // For Business day
		{
			try{
				
				DBFFielsWriter objDBFFielsWriter	=	new DBFFielsWriter(); // For File Write 
				FolderZiper objFolderZiper			=	new FolderZiper();
				
				DBFFileMainBO	objDBFFileMainBO	=	new DBFFileMainBO();
				Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
				Hashtable<Integer, ArrayList<TransactionMainBO>> hashTransactionatcaBO	=	new Hashtable<Integer, ArrayList<TransactionMainBO>>();
				//Hashtable<String, ArrayList<TransactionMainBO>> hashMISReportTransactionBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
				//Hashtable<String, ArrayList<TransactionMainBO>> hashMFUpfrontTrialTransactionBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
				Hashtable<String, ArrayList<TransactionMainBO>> hashKarvyTransactionMainSBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
				//Hashtable<String, ArrayList<TransactionMainBO>> hashirTransactionMainSBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
				Hashtable<String, ArrayList<SIPInfoBO>> hashSIPInfoBO	=	new Hashtable<String, ArrayList<SIPInfoBO>>();
				Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO		=	new Hashtable<String, ArrayList<FieldInfoBO>>();
				Hashtable<String, NewFolioBO> hashNewFolioBO=	new Hashtable<String, NewFolioBO>();
				Hashtable<String, TransactionMainBO> hashFATCANotavailable	=	new Hashtable<String, TransactionMainBO>();
				Hashtable<String,InvestorDetailsBO> hashCKCDetails	=	new Hashtable<String,InvestorDetailsBO>();
				//Deleting all files before creation
				try{
                                    if (isKARVY_DirectUpload){
                                    objFolderZiper.CopyFileToTodaysFOlder(fileDirectoryParent, properties.getProperty("DirectoryHistory"));
					//objFolderZiper.CopyFileToTodaysFOlder(misReportDir, properties.getProperty("MISReportHistory"));
					objFolderZiper.CopyFileToTodaysFOlder(newFolioDirectoryParent, properties.getProperty("NewFolioReportHistory"));
                                    }
					
					//File sourcePath = new File(fileDirectoryParent);
					//FileUtils.deleteDirectory(sourcePath);
					
					/*sourcePath = new File(misReportDir);
					FileUtils.deleteDirectory(sourcePath);*/
					
					//File sourcePath = new File(newFolioDir);
					//FileUtils.deleteDirectory(sourcePath);
					
					/*sourcePath = new File(NFOFileDir);
					FileUtils.deleteDirectory(sourcePath);*/
					
				}catch(Exception e){
					//logger.info("Exp whn deleting : "+e);
				}
				
				hashFieldInfoBO			=	objDBFFilesDAO.getFieldInfoTransaction(objConnection); // DBF Field Name ---> Hashtable(RTCode, ArrayList<FieldInfoBO>)
				objDBFFileMainBO		=	objDBFFilesDAO.getInvestorTransaction(liquidScheme, searchKey, searchAMC, bankVerified,properties, objConnection, doProcess);// Transaction Details ---> Hashtable(RTCode@AMCSchemeCode, ArrayList<TransactionMainBO>)
				//hashMISLot				=	objDBFFilesDAO.getTransactionLot(objDBFFileMainBO.getFDate(), objDBFFileMainBO.getTDate(),objConnection);
				
				try{
					hashSIPInfoBO	=	objDBFFileMainBO.getHashSIPInfoBO();
					if(hashSIPInfoBO.size()>0) // DBF Field name creation
					{
						//long startTime = System.currentTimeMillis();
						objDBFFielsWriter.DBFFileSIPFeedWriter(currentDate, strFileDir, hashSIPInfoBO, hashFieldInfoBO); // For Writing DBF File
						//long totalTime = System.currentTimeMillis() - startTime;
						//logger.info("####TIMER : Total SIP DBF = " + totalTime);
					}
				}catch(Exception e){
					logger.info("Exp:"+e);
				}
				
				hashTransactionMainBO		=	objDBFFileMainBO.getHashTransactionMainBO();
                                logger.info("Hash Transaction Size - "+doProcess+"  : "+hashTransactionMainBO.size());

				//KARVY server - Direct forward feed XML upload to server
				if (isKARVY_DirectUpload){
					ProcessBL karvyProcessBL = new ProcessBL();
					DBFFilesDAO dbfFilesDAO = new DBFFilesDAO();
					
					//ArrayList<TransactionMainBO> transactionMainBO_List = hashTransactionMainBO.get(2);//Since entire feed file upload is fail
					pstmForwardFeedUpdate = dbfFilesDAO.setPreparedFeedUpdate(userId, objConnection);
                                        bkThreadProc.hashTransactionMainBO = hashTransactionMainBO;
					karvyProcessBL.uploadXML_ToKARVY(userId, strFileDir, hashTransactionMainBO, pstmForwardFeedUpdate);
                                        
				}
				//Rest of forward feed process (Includes CAMS server - Direct forward feed DBF upload to server)
				else {
					
				hashTransactionatcaBO		=	objDBFFileMainBO.getHashTransactionFatcaBO();
				hashKarvyTransactionMainSBO	=	objDBFFileMainBO.getHashKarvyTransactionMainSBO();
				//hashirTransactionMainSBO	=	objDBFFileMainBO.getHashirTransactionMainSBO();
				//hashMISReportTransactionBO	=	objDBFFileMainBO.getHashMISReportTransactionBO();
				//hashMFUpfrontTrialTransactionBO	=	objDBFFileMainBO.getHashMFUpfrontTrialTransactionBO();
				hashNewFolioBO				=	objDBFFileMainBO.getHashNewFolioBO();
				reportText	=	objDBFFileMainBO.getDailyReportContent();
				//ftNewFolioUserTransRefId	=	objDBFFileMainBO.getFtNewFolioUserTransRefId();
				
				
				try{
					//long startTime = System.currentTimeMillis();
					
					if(hashTransactionMainBO.size()>0){
						objDBFFielsWriter.DBFFileTransactionFeedWriter("N", "", currentDate, strFileDir, hashTransactionMainBO, hashFieldInfoBO,userId, properties, isCAMS_DirectUpload); // For Writing DBF File
                                        } else
						logger.info("Investor Normal Transaction Record not found. ");
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("####TIMER : Total DBF = " + totalTime);
					
				}catch(Exception e){
					logger.info("Exp in writingfile:"+e);
				}
				
				
				try{
					//long startTime = System.currentTimeMillis();
					
					if(hashKarvyTransactionMainSBO.size()>0) // For Kavy Subtrxn type SIP for separate file
						objDBFFielsWriter.DBFFileTransactionFeedWriter("N", "_S", currentDate, strFileDir, hashKarvyTransactionMainSBO, hashFieldInfoBO,userId, properties, isCAMS_DirectUpload); // For Writing DBF File
					else
						logger.info("Investor SIP Transaction Record not found. ");
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("####TIMER : Total SIP DBF = " + totalTime);
					
				}catch(Exception e){
					logger.info("Exp in writingSIPfile:"+e);
				}
				
				try{
					hashCKCDetails	=	objDBFFileMainBO.getHashCKCDetails();
					if((hashCKCDetails != null)&&(hashCKCDetails.size()>0))
					{
						objDBFFielsWriter.DBFFileCKYCFeedWriter(strFileDir, hashCKCDetails, hashFieldInfoBO);
						
					}
				}catch(Exception e){
					logger.info("Exp:CKYC details for SMF"+e);
				}
				
				if (!isCAMS_DirectUpload){
					try{
						//long startTime = System.currentTimeMillis();
						
						if(hashTransactionatcaBO.size()>0){//For Fatca feed
							hashFATCANotavailable	=	objDBFFielsWriter.DBFFileFATCAFeedWriter(strFatcaFileDir, hashTransactionatcaBO, hashFieldInfoBO, objConnection);
							
							try{
								if((hashFATCANotavailable!=null)&&(hashFATCANotavailable.size()>0)){
									String fatcaNotavailablePanPath	=	strFileDir+File.separator+"FATANotAvailablePAN "+(objConstansBO.getTodayDate("dd/MM/yy")).replace("/", "")+".xls";
									objNewFolioReport.genarateFATCANotAvailablepan(fatcaNotavailablePanPath, hashFATCANotavailable);
								}
							}catch(Exception e){
								logger.info("Exp:"+e);
							}
						}
						else
							logger.info("Investor Normal Transaction Record not found. ");
						
						//long totalTime = System.currentTimeMillis() - startTime;
						//logger.info("####TIMER : Total FATCA DBF = " + totalTime);
						
					}catch(Exception e){
						logger.info("Exp in writing FATCA file:"+e);
					}
				}
				
				
				try{
					//long startTime = System.currentTimeMillis();
					
					//New Folio DBF File Creation part
					if(hashNewFolioBO.size()>0)
					{
						try{
							PStmtAutoNo			=	objDBFFilesDAO.PreparedStmtAutoNo(objConnection);
							PStmtUpdateAutoNo	=	objDBFFilesDAO.PreparedStmtUpdateAutoNo(objConnection);
							NewFolioDocInsert	=	objDBFFilesDAO.PreparedStmtNewFolioDocInsert(objConnection);
							objDBFFielsWriter.DBFFileNewFolioWriter(currentDate, newFolioDir, hashNewFolioBO, hashFieldInfoBO, PStmtAutoNo, PStmtUpdateAutoNo, NewFolioDocInsert);
							
						}catch(Exception e){	logger.info("Exception in New Folio DBF File Creation part "+e);		}
					}
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("####TIMER : Total DBF NEW Folio = " + totalTime);
				}catch(Exception e){
					logger.info("Exp in writing new folio file:"+e);
				}
				
				//CAMS server - Direct forward feed DBF upload to server
				if (isCAMS_DirectUpload){
					STPProcessBL stpProcessBL = new STPProcessBL();
                                        String directoryPath_CAMSText = strFileDir + File.separator + PropertiesConstant.CAMS_TEXT_FILE_DIRECTORY;
                                        bkThreadProc.hashTransactionMainBO = hashTransactionMainBO;
					stpProcessBL.uploadFiles_ToCAMS(userId, directoryPath_CAMSText);
					logger.info("DIRECT UPLOADED TO CAMS SERVER. STATUS: COMPLETED");
                                        getDBFFiles("",searchKey,"","",1, PropertiesConstant.CAMS_DIRECT_UPLOAD_FAILED_DBF, finalFeed); 
				}
				//Rest of forward feed process
				else {
				/*if(hashirTransactionMainSBO.size()>0) // For FT Online enrollment file
					objDBFFielsWriter.DBFFileTransactionFeedWriter("O","_OE", currentDate, strFileDir, hashirTransactionMainSBO, hashFieldInfoBO,userId); // For Writing DBF File
				else
					logger.info("Investor FT OE Type Transaction Record not found. ");
				*/
                                
				try{
					if((hashTransactionMainBO.size()>0)||(hashKarvyTransactionMainSBO.size()>0))
					{
						//long startTime = System.currentTimeMillis();
                                                String allExcelFile = PropertiesConstant.TEMP_DIRECTORY + File.separator + "RT_1_STP_ALL_"+date_formatted_String+".xls";
                                                boolean allExcelFileStatus = false;

                                
                                //Generate direct upload failed status excel & send mail
                                if (isCAMS_DirectUpload_FailedDBF){
                                    
                                    STPProcessBL stpProcessBL = new STPProcessBL();
                                    excelFile = PropertiesConstant.TEMP_DIRECTORY + File.separator + "RT_1_STP_FAILED_"+date_formatted_String+".xls";
                                    excelFileStatus = true;
                                    excelFileStatus = stpProcessBL.generateDirectUploadFailedExcel(excelFile, hashTransactionMainBO);
                                    bkThreadProc.hashTransactionFailedMainBO = hashTransactionMainBO;
                                    objFolderZiper.zipFolder(strFileDir, strFileDir+".zip"); 
                                    failedDbfFiles	=	strFileDir+".zip";
//                                    String [] attachments = new String [] 
//                                                                {dbfFiles, (excelFileStatus?excelFile:"")};
//                                    mailFlag = objSendMail.SendMailWithDBFFile(attachments, "", "", "Direct upload (STP) failed forward feed dbf files ");
//                                    objSendMail.sendDailyReport(reportText);
                                    getDBFFiles("",searchKey,"","",1, PropertiesConstant.CAMS_DIRECT_UPLOAD_ALL_DBF, finalFeed);
                                    
                                }
                                //Generate direct upload all status excel & send mail
                                else if (isCAMS_DirectUpload_AllDBF){
                                    
                                    STPProcessBL stpProcessBL = new STPProcessBL();
                                    allExcelFileStatus = stpProcessBL.generateDirectUploadStatusExcel(allExcelFile, objDBFFileMainBO.getReportBO_List());
                                    
                                    String [] attachments = new String [] 
                                                                {failedDbfFiles, (excelFileStatus?excelFile:""), (allExcelFileStatus?allExcelFile:"")};
                                    mailFlag = objSendMail.SendMailWithDBFFile(attachments, "", "", "Direct upload (STP) all forward feed dbf files ");
                                    objSendMail.sendDailyReport(reportText);
                                    if (!dbfFiles.equals("")) {
                                        String zipFolderFull = properties.getProperty("Directory");
                                        objFolderZiper.CopyFileToTodaysFOlder(zipFolderFull, properties.getProperty("DirectoryHistory"));
                                    }
                                    if(!finalFeed.equals(null) && finalFeed.equalsIgnoreCase("CAMS")){
                                        bkThreadProc.startFinalDBFWriter(userId, 1, bkThreadProc.hashTransactionFailedMainBO);
                                    } else if (!finalFeed.equals(null) && finalFeed.equalsIgnoreCase("Karvy")){
                                        bkThreadProc.startFinalDBFWriter(userId, 2, bkThreadProc.hashTransactionFailedMainBO);
                                    }
                                }
                                else {
						mailFlag	=	objSendMail.SendMailWithDBFFile(dbfFiles, "", "", "Forward feed dbf files ");
						objSendMail.sendDailyReport(reportText);
						
						//long totalTime = System.currentTimeMillis() - startTime;
						//logger.info("####TIMER : Forward feed mail = " + totalTime);
                                }
					
                                //Delete the zip file, since it would be backed up at the end.
                                //new File(dbfFiles).delete();
                                        
                                        }
				}catch(Exception e){
					logger.error("Exp in sending FF mail:"+e);
				}
				
				
				try{
					if((hashTransactionMainBO.size()>0)||(hashKarvyTransactionMainSBO.size()>0))
					{
						if((hashTransactionMainBO.size()>0)||(hashKarvyTransactionMainSBO.size()>0))
						{
							objFolderZiper.zipFolder(strFileDir, strFileDir+".zip"); 
							dbfFiles	=	strFileDir+".zip";
						}
						// MIS Report
						//logger.info("hashMISReportTransactionBO  : "+hashMISReportTransactionBO.size());
						/*if(hashMISReportTransactionBO.size()>0)
						{
							if(!(new File(misReportDir)).exists()){
								new File(misReportDir).mkdirs();
							}
							MISReport objMISReport	=	new MISReport();
							objMISReport.generateMISReportExcel(misReportDir, hashMISReportTransactionBO);
							objFolderZiper.zipFolder(misReportDir, misReportDir+".zip"); 
							misFiles	=	misReportDir+".zip";
						}*/
						
						if(hashNewFolioBO.size()>0)
						{
							//long startTime = System.currentTimeMillis();
							
							//New Folio XL Creation part
							try{
								if(!(new File(newFolioDir+File.separator)).exists()){
									new File(newFolioDir+File.separator).mkdirs();
								}
								
								objNewFolioReport.getNewFolioExcel(newFolioDir+File.separator+c1.getTimeInMillis()+".xls", hashNewFolioBO);
								
								objFolderZiper.zipFolder(newFolioDir, newFolioDir+".zip"); 
								//Zip the files in final list
								//nfFiles	=	newFolioDir+".zip";

							}catch(Exception e){	logger.info("Exp in New Folio XL Creation in BL "+e);	}
							
							//long totalTime = System.currentTimeMillis() - startTime;
							//logger.info("####TIMER : Total NEW Folio XL time = " + totalTime);
						}
						
						if(!nfFiles.equals("")){
							
							//long startTime = System.currentTimeMillis();
							
							//mailFlag	=	objSendMail.SendMailWithDBFFile("", "", nfFiles,"Forward feed new folio reports ");
							//objSendMail.sendDailyReport(reportText);
							
							//long totalTime = System.currentTimeMillis() - startTime;
							//logger.info("####TIMER : Forward feed repor mail = " + totalTime);
						}
						else
							logger.info("No mail send");
					}	
				}catch(Exception e){
					logger.error("Exp in MIS mail:"+e);
				}
				
				//Block for NFO closed Schemes transaction list
				/*try{
					String NFOFileName	=	c1.getTimeInMillis()+".xls";
					if(!(new File(NFOFileDir+File.separator)).exists()){
						new File(NFOFileDir+File.separator).mkdirs();
					}
					ArrayList<TransactionBO> arrNFOTransactionBO	=	new ArrayList<TransactionBO>();
					
					// Current date NFO closed schemes and on that previous transactions.
					arrNFOTransactionBO	=	objDBFFilesDAO.getNFOSchemes(objDBFFileMainBO.getFDate(), objDBFFileMainBO.getTDate(), objConnection);
					if(arrNFOTransactionBO.size()>0)
					{
						objNewFolioReport.getNFOSchemes(NFOFileDir+File.separator+NFOFileName, arrNFOTransactionBO);
						//Adding folder into ZIP file
						objFolderZiper.zipFolder(NFOFileDir, NFOFileDir+".zip");
						//Sending ZIP to Corresponding Mails
						String subject 	=	properties.getProperty("subjectNFO")+" ("+objConstansBO.getTodayDate("MM/dd/yyyy")+")";
						String content	=	"NFO Schemes List Attached... "+objDBConstants.getDBCurrentDate();
						objSendMail.sendReportFile(NFOFileDir+".zip","",subject,content);
						//Deleting the folder
						File source = new File(NFOFileDir); 
						source.delete();
					}
				}catch(Exception Exp){
					logger.info("NFO Schemes Exp :"+Exp);
				}*/
				
				try{
					logger.info("...STP Process Beging...");
					STPProcessBL objSTPProcessBL	=	new STPProcessBL();
					ArrayList<TransactionMainBO> arrTransactionMainBO	=	new ArrayList<TransactionMainBO>();
					int rowSize	=	hashTransactionMainBO.get("6@F").size();
					arrTransactionMainBO	=	hashTransactionMainBO.get("6@F");
					logger.info("Franklin list size::::"+rowSize);
					if(rowSize>0){
						
						String stpURL	   =	properties.getProperty("stpURL").trim();
						StringBody stpUser = new StringBody(properties.getProperty("stpUserIdForFT"));
			            StringBody stpPswd = new StringBody(properties.getProperty("stpPswdForFT"));
			            
						objSTPProcessBO	=	objSTPProcessBL.FranklinSTPProcess(rowSize, strFileDir, stpFileXMLPath, stpURL, stpUser, stpPswd);
						
						if(objSTPProcessBO.isStpProcessFlag()){
							
							objSTPProcessBO	=	objSTPProcessDAO.STPProcessInsert(objSTPProcessBO, pstmtSTPProcessInsert);
							objSTPProcessDAO.STPProcessTransactionWiseInsert(objSTPProcessBO.getArrSTPProcessBO(), arrTransactionMainBO, pstmtSTPProcessTransactionInsert);
							
						}
					}
					logger.info("...STP Process end...");
				}catch(Exception e){
					logger.error("STPProcess Exp:"+e);
				}
				
				// MF Upfront calculation begin - commented on 12Mar2012
				/*if(hashMFUpfrontTrialTransactionBO.size()>0)
				{
					MFUpfrontTrial objMFUpfrontTrial	=	new MFUpfrontTrial();
					
					try{
						logger.info("****** Up front calculation Begin ******");
						objMFUpfrontTrial.TrialFeeManuplation(hashMFUpfrontTrialTransactionBO, objConnection);
						logger.info("****** Up front calculation End ******");
					}catch(Exception e){
						logger.info("Exception in TrialFeeManuplation  "+e);
					}
				}
				else
				{
					logger.info("No Upfront Transaction Record found. ");
				}*/
				/* GUI
				// MIS Report --New Format
				logger.info("hashMISReportTransactionBO  : "+hashMISReportTransactionBO.size());
				if(hashMISReportTransactionBO.size()>0)
				{
					if(!(new File(misReportDir)).exists()){
						new File(misReportDir).mkdirs();
					}
					MISReport objMISReport	=	new MISReport();
					objMISReport.generateMISReportLotBased(objDBFFileMainBO.getTDate(), misReportDir, hashMISLot, hashMISReportTransactionBO);
					objFolderZiper.zipFolder(misReportDir, misReportDir+".zip"); 
					misFiles	=	misReportDir+".zip";
					
					if((!misFiles.equals(""))||(!nfFiles.equals(""))){
						mailFlag	=	objSendMail.SendMailWithDBFFile("", misFiles, "","Forward Feed MIS,New Folio Reports - New Format");
					}
				}
				*/

				//FT New folio XL Sheet generation
				/*try{
					if(ftNewFolioUserTransRefId.length()>3){
						ArrayList<NewFolioBO> arrNewFolioBO	=	new ArrayList<NewFolioBO>(); 
						
						ftNewFolioUserTransRefId	=	ftNewFolioUserTransRefId.substring(0, ftNewFolioUserTransRefId.length()-1);
						
						if(!(new File(newFolioReportForFT)).exists()){
							new File(newFolioReportForFT).mkdirs();
						}
						
						newFolioReportForFT	=	newFolioReportForFT+File.separator+"FT "+(objConstansBO.getTodayDate("dd/MM/yy")).replace("/", "")+".xls";
						
						arrNewFolioBO	=	objDBFFilesDAO.getFTNewFolioDetails(ftNewFolioUserTransRefId, objConnection);
						objNewFolioReport.genarateXLForFTNewFolio(newFolioReportForFT, arrNewFolioBO);
						
						mailFlag	=	objSendMail.SendMailWithDBFFile(newFolioReportForFT, "", "","New Folio report for FT ");
					}
					
				}catch(Exception e){
					logger.info("Creation of new folio for FT"+e);
				}*/
				
				}
				
				try{
                                    if(!finalFeed.equals(null) && finalFeed.equalsIgnoreCase("CAMS")){

                                    }
					/*if(!misFiles.equals(""))
						objFolderZiper.CopyFileToTodaysFOlder(misReportDir, properties.getProperty("MISReportHistory"));*/
					if(!nfFiles.equals(""))
						//objFolderZiper.CopyFileToTodaysFOlder(newFolioDir, properties.getProperty("NewFolioReportHistory"));
						objFolderZiper.CopyFileToTodaysFOlder(newFolioDirectoryParent, properties.getProperty("NewFolioReportHistory"));
					
				}catch(Exception e){
					logger.error("Exp:"+e);
				}
				}
			}catch(Exception e){	
                            logger.error("Exception "+e); mailFlag	=	false;		
                        }
			finally{
				if(PStmtAutoNo!=null) 		PStmtAutoNo.close();
				if(PStmtUpdateAutoNo!=null)	PStmtUpdateAutoNo.close();
				if(NewFolioDocInsert!=null)	NewFolioDocInsert.close();
				if(pstmForwardFeedUpdate!=null)	pstmForwardFeedUpdate.close();
				if(objConnection!=null)		objConnection.close(); 
			}
		}

		return mailFlag;
		
	}
	
	public String getAnnualIncomeCode(double annualIncome) throws Exception{
		String annualIncomeCode	=	"";
		
		/*if(((rtCode == 2)||(rtCode == 6)) && (annualIncome<=100000))
			annualIncomeCode	=	"Below 1 Lakh";
		else if(((rtCode == 2)||(rtCode == 6)) && (annualIncome>100000) && (annualIncome<=500000) )
			annualIncomeCode	=	"1 to 5 Lakh";
		else if(((rtCode == 2)||(rtCode == 6)) && (annualIncome>500000) && (annualIncome<=1000000) )
			annualIncomeCode	=	"5 to 10 Lakh";
		else if(((rtCode == 2)||(rtCode == 6)) && (annualIncome>1000000) && (annualIncome<=2500000) )
			annualIncomeCode	=	"10 to 25 Lakh";
		else if(((rtCode == 2)||(rtCode == 6)) && (annualIncome>2500000) && (annualIncome<=10000000) )
			annualIncomeCode	=	"25 to 1 Crore";
		else if(((rtCode == 2)||(rtCode == 6)) && (annualIncome>=10000000))
			annualIncomeCode	=	"Above 1 Crore";
		else*/ 
		if(annualIncome<=100000)
			annualIncomeCode	=	"31";
		else if((annualIncome>100000) && (annualIncome<=500000) )
			annualIncomeCode	=	"32";
		else if((annualIncome>500000) && (annualIncome<=1000000) )
			annualIncomeCode	=	"33";
		else if((annualIncome>1000000) && (annualIncome<=2500000) )
			annualIncomeCode	=	"34";
		else if((annualIncome>2500000) && (annualIncome<=10000000) )
			annualIncomeCode	=	"35";
		else if((annualIncome>=10000000))
			annualIncomeCode	=	"36";
		else
			annualIncomeCode	=	String.valueOf(annualIncome);
		
		
		return annualIncomeCode;
		
	}

        /**
         * To get boolean value of which process need to be done.
         * @param doProcess
         * @return boolean [] {isCAMS_DirectUpload, isCAMS_DirectUpload_FailedDBF, isCAMS_DirectUpload_AllDBF, isKARVY_DirectUpload}
         */
        public static boolean [] whosProcess (String doProcess){
            
            boolean [] returnValues = new boolean [] {false, false, false, false};
            
            boolean isCAMS_DirectUpload = false, isCAMS_DirectUpload_FailedDBF = false, isCAMS_DirectUpload_AllDBF = false,
                    isKARVY_DirectUpload = false;
            
		if (!ConstansBO.isEmpty(doProcess)){
			if (doProcess.equals(PropertiesConstant.CAMS_DIRECT_UPLOAD))
				isCAMS_DirectUpload = true;
                        else if (doProcess.equals(PropertiesConstant.CAMS_DIRECT_UPLOAD_FAILED_DBF))
				isCAMS_DirectUpload_FailedDBF = true;
                        else if (doProcess.equals(PropertiesConstant.CAMS_DIRECT_UPLOAD_ALL_DBF))
				isCAMS_DirectUpload_AllDBF = true;
			else if (doProcess.equals(PropertiesConstant.KARVY_DIRECT_UPLOAD))
				isKARVY_DirectUpload = true;
		}
                
                returnValues = new boolean [] {isCAMS_DirectUpload, isCAMS_DirectUpload_FailedDBF, isCAMS_DirectUpload_AllDBF, isKARVY_DirectUpload};
                
                return returnValues;
        }
}
