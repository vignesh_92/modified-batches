package com.wifs.DBFForwardFeed; 

public class AddtionalInfoBO {  
	
	private String strInvestorName	=	"";
	private String strPanNo			=	"";
	
	private String strAMCBankName		=	"";
	private String strAMCAccountNo		=	"";
	
	
	public void setInvestorName(String strInvestorName){this.strInvestorName=strInvestorName;	}
	public void setPanNo(String strPanNo){this.strPanNo	=	strPanNo;	}
	public void setAMCBankName(String strAMCBankName){this.strAMCBankName	=	strAMCBankName;	}
	public void setAMCAccountNo(String strAMCAccountNo){this.strAMCAccountNo	=	strAMCAccountNo;	}
	
	public String getInvestorName(){ return (this.strInvestorName);	}
	public String getPanNo(){return (this.strPanNo);	}
	public String getAMCBankName(){return (this.strAMCBankName);	}
	public String getAMCAccountNo(){return (this.strAMCAccountNo);	}
	

}
