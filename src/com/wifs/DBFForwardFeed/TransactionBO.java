package com.wifs.DBFForwardFeed;

import java.math.BigDecimal;

public class TransactionBO {
	
	private String strAmcCode		=	"";
	private int rtCode				=	0;
	private String strUserTransactionNo	=	"";
	private String strApplNo		=	"";
	private String strFolioNo		=	"";
	private String strCk_Dig_No		=	"";
	private String strCk_Dig_FolioNo		=	"";
	private String strTransactionType	=	"";
	private String strSchemeCode	=	"";
	private int aftSchemeCode		=	0;
	private String schemeName		=	"";
	private String amcName			=	"";
	private String amcAddress		=	"";
	private ConstansBO strTransactionDate;
	private String strTransactionTime	=	"";
	private double dblUnits			=	0.0d;
	private String strTransactionStatus		=	"";
	private String strPaymentId		=	"";
	private String paidThru			=	"";
	private BigDecimal bigAmount	=	new BigDecimal(0);
	private String strPricing		=	"";
	private String IPAddress		=	"";
	private String ITCValue			=	"";
	private String existForwardFeed	=	"";
	private String investorName		=	"";
	private String amcShortName		=	"";
	private String remarks			=	"";
	private String dpId				=	"";
	private String clientCode		=	"";
	
	private String euinOpted		=	"N";
	private String euinId			=	"";
	private String freshInvestor	=	"";
	private String noOfInstalment	=	"0";
	private String paidInstallments	=	"0";
	
	private ConstansBO transactionDateMIS;
	private String transactionTimeMIS	=	"";
	private String misRemarks		=	"";
	private int createdUser			=	0;
	private ConstansBO createdDate;
	
	private String Forex_MCS	=	"";
	private String GAME_GABLE	=	"";
	private String LS_ML_PA		=	"";
	
	private ConstansBO sipStartDate;
	private ConstansBO sipEndDate;
	private String sipFrequency	=	"OM";
	private int sipDate	=	0;
	private double sipAmount	=	0.0d;
	private String fatcaIPAddress	=	"";
	private String fatcaTime		=	"00:00:00";
	private ConstansBO fatcaDate;
	
	private String sipReferenceId	=	"0";
	private String subTransactionType	=	"";
	private ConstansBO SIP_Reg_Date	=	new ConstansBO("");
        private String directUploadStatus;  //Feed direct upload to the RT server
        private String directUploadRemarks;
	
	
	public String getSipReferenceId() {
		return sipReferenceId;
	}
	public void setSipReferenceId(String sipReferenceId) {
		this.sipReferenceId = sipReferenceId;
	}
	
	public ConstansBO getSIP_Reg_Date() {
		return SIP_Reg_Date;
	}
	public void setSIP_Reg_Date(ConstansBO sIP_Reg_Date) {
		SIP_Reg_Date = sIP_Reg_Date;
	}
	
	public String getSubTransactionType() {
		return subTransactionType;
	}
	public void setSubTransactionType(String subTransactionType) {
		this.subTransactionType = subTransactionType;
	}
	public String getEuinOpted() {
		return euinOpted;
	}
	public void setEuinOpted(String euinOpted) {
		this.euinOpted = euinOpted;
	}
	
	public String getEuinId() {
		return euinId;
	}
	public void setEuinId(String euinId) {
		this.euinId = euinId;
	}
	public void setAmcCode(String strAmcCode){this.strAmcCode=strAmcCode;}
	//public void setBrokerCode(String strBrokerCode){this.strBrokerCode=strBrokerCode;}
	//public void setSubBrokerCode(String strSubBrokerCode){this.strSubBrokerCode=strSubBrokerCode;}
	//public void setUserCode(String strUserCode){this.strUserCode=strUserCode;}
	public void setUserTransactionNo(String strUserTransactionNo){this.strUserTransactionNo=strUserTransactionNo;}
	public void setApplNo(String strApplNo){this.strApplNo=strApplNo;}
	public void setFolioNo(String strFolioNo){if(strFolioNo.equals("0"))this.strFolioNo =""; else this.strFolioNo=strFolioNo;}
	public void setTransactionType(String strTransactionType){this.strTransactionType=strTransactionType;}
	public void setSchemeCode(String strSchemeCode){this.strSchemeCode=strSchemeCode;}
	public void setTransactionDate(ConstansBO strTransactionDate){this.strTransactionDate=strTransactionDate;}
	public void setTransactionTime(String strTransactionTime){this.strTransactionTime=strTransactionTime;}
	public void setUnits(double dblUnits){this.dblUnits=dblUnits;}
	public void setAmount(BigDecimal bigAmount){this.bigAmount=bigAmount;}
	public void setTransactionStatus(String strTransactionStatus){ this.strTransactionStatus = strTransactionStatus; }
	public void setPaymentId(String strPaymentId){ this.strPaymentId = strPaymentId; }
	public void setCk_Dig_No(String strCk_Dig_No){this.strCk_Dig_No=strCk_Dig_No;}
	//public void setClos_Ac_Ch(String strClos_Ac_Ch){this.strClos_Ac_Ch=strClos_Ac_Ch;}
	public void setCk_Dig_FolioNo(String strCk_Dig_FolioNo) {		this.strCk_Dig_FolioNo = strCk_Dig_FolioNo;	}
	public void setPricing(String strPricing){this.strPricing=strPricing;}
	
	public String getAmcCode(){return (this.strAmcCode);}
	//public String getBrokerCode(){return (this.strBrokerCode);}
	//public String getSubBrokerCode(){return (this.strSubBrokerCode);} 
	//public String getUserCode(){return (this.strUserCode);}
	public String getUserTransactionNo(){return (this.strUserTransactionNo);}
	public String getFolioNo(){return (this.strFolioNo);}
	public String getApplNo(){return (this.strApplNo);}
	public String getTransactionType(){return (this.strTransactionType);}
	public String getSchemeCode(){return (this.strSchemeCode);}
	public ConstansBO getTransactionDate(){return (this.strTransactionDate);}
	public String getTransactionTime(){return (this.strTransactionTime);}
	public double getUnits(){return (this.dblUnits);}
	public BigDecimal getAmount(){return (this.bigAmount);}
	public String getTransactionStatus(){return(this.strTransactionStatus); }
	public String getPaymentId(){return(this.strPaymentId); }
	public String getCk_Dig_No(){return (this.strCk_Dig_No);}
	//public String getClos_Ac_Ch(){return (this.strClos_Ac_Ch);}
	public String getCk_Dig_FolioNo() {		return strCk_Dig_FolioNo;	}
	public String getPricing(){return(this.strPricing);}
	public String getIPAddress() {
		return IPAddress;
	}
	public void setIPAddress(String address) {
		IPAddress = address;
	}
	public String getSchemeName() {
		return schemeName;
	}
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	public String getAmcName() {
		return amcName;
	}
	public void setAmcName(String amcName) {
		this.amcName = amcName;
	}
	public String getAmcAddress() {
		return amcAddress;
	}
	public void setAmcAddress(String amcAddress) {
		this.amcAddress = amcAddress;
	}
	public String getITCValue() {
		return ITCValue;
	}
	public void setITCValue(String value) {
		ITCValue = value;
	}
	public String getExistForwardFeed() {
		return existForwardFeed;
	}
	public void setExistForwardFeed(String existForwardFeed) {
		this.existForwardFeed = existForwardFeed;
	}
	public int getRtCode() {
		return rtCode;
	}
	public void setRtCode(int rtCode) {
		this.rtCode = rtCode;
	}
	public String getPaidThru() {
		return paidThru;
	}
	public void setPaidThru(String paidThru) {
		this.paidThru = paidThru;
	}
	public int getAftSchemeCode() {
		return aftSchemeCode;
	}
	public void setAftSchemeCode(int aftSchemeCode) {
		this.aftSchemeCode = aftSchemeCode;
	}
	public String getInvestorName() {
		return investorName;
	}
	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}
	public String getAmcShortName() {
		return amcShortName;
	}
	public void setAmcShortName(String amcShortName) {
		this.amcShortName = amcShortName;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getDpId() {
		return dpId;
	}
	public void setDpId(String dpId) {
		this.dpId = dpId;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getMisRemarks() {
		return misRemarks;
	}
	public void setMisRemarks(String misRemarks) {
		this.misRemarks = misRemarks;
	}
	public ConstansBO getTransactionDateMIS() {
		return transactionDateMIS;
	}
	public void setTransactionDateMIS(ConstansBO transactionDateMIS) {
		this.transactionDateMIS = transactionDateMIS;
	}
	public String getTransactionTimeMIS() {
		return transactionTimeMIS;
	}
	public void setTransactionTimeMIS(String transactionTimeMIS) {
		this.transactionTimeMIS = transactionTimeMIS;
	}
	public String getFreshInvestor() {
		return freshInvestor;
	}
	public void setFreshInvestor(String freshInvestor) {
		this.freshInvestor = freshInvestor;
	}
	public String getNoOfInstalment() {
		return noOfInstalment;
	}
	public void setNoOfInstalment(String noOfInstalment) {
		this.noOfInstalment = noOfInstalment;
	}
	public String getPaidInstallments() {
		return paidInstallments;
	}
	public void setPaidInstallments(String paidInstallments) {
		this.paidInstallments = paidInstallments;
	}
	public int getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(int createdUser) {
		this.createdUser = createdUser;
	}
	public ConstansBO getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(ConstansBO createdDate) {
		this.createdDate = createdDate;
	}
	public String getForex_MCS() {
		return Forex_MCS;
	}
	public void setForex_MCS(String forex_MCS) {
		Forex_MCS = forex_MCS;
	}
	public String getGAME_GABLE() {
		return GAME_GABLE;
	}
	public void setGAME_GABLE(String gAME_GABLE) {
		GAME_GABLE = gAME_GABLE;
	}
	public String getLS_ML_PA() {
		return LS_ML_PA;
	}
	public void setLS_ML_PA(String lS_ML_PA) {
		LS_ML_PA = lS_ML_PA;
	}
	public ConstansBO getSipStartDate() {
		return sipStartDate;
	}
	public void setSipStartDate(ConstansBO sipStartDate) {
		this.sipStartDate = sipStartDate;
	}
	public ConstansBO getSipEndDate() {
		return sipEndDate;
	}
	public void setSipEndDate(ConstansBO sipEndDate) {
		this.sipEndDate = sipEndDate;
	}
	public String getSipFrequency() {
		return sipFrequency;
	}
	public void setSipFrequency(String sipFrequency) {
		this.sipFrequency = sipFrequency;
	}
	public double getSipAmount() {
		return sipAmount;
	}
	public void setSipAmount(double sipAmount) {
		this.sipAmount = sipAmount;
	}
	public int getSipDate() {
		return sipDate;
	}
	public void setSipDate(int sipDate) {
		this.sipDate = sipDate;
	}
	public String getFatcaIPAddress() {
		return fatcaIPAddress;
	}
	public void setFatcaIPAddress(String fatcaIPAddress) {
		this.fatcaIPAddress = fatcaIPAddress;
	}
	public ConstansBO getFatcaDate() {
		return fatcaDate;
	}
	public void setFatcaDate(ConstansBO fatcaDate) {
		this.fatcaDate = fatcaDate;
	}
	public String getFatcaTime() {
		return fatcaTime;
	}
	public void setFatcaTime(String fatcaTime) {
		this.fatcaTime = fatcaTime;
	}
    public String getDirectUploadStatus() {
        return directUploadStatus;
    }

    public void setDirectUploadStatus(String directUploadStatus) {
        this.directUploadStatus = directUploadStatus;
    }

    public String getDirectUploadRemarks() {
        return directUploadRemarks;
    }

    public void setDirectUploadRemarks(String directUploadRemarks) {
        this.directUploadRemarks = directUploadRemarks;
    }
}