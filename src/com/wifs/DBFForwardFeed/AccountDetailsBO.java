package com.wifs.DBFForwardFeed; 

public class AccountDetailsBO {
	 
	private String strAccountNo		=	"";
	private String strAccountType	=	"";
	private String strBankLookUpId	=	"";
	private String strBankName		=	"";
	private String strBranchName	=	"";
	private String strBankCity	=	"";
	private String strHoldingNature	=	"";
	private String strTaxStatus		=	"";
	private String strRemarks		=	"";
	
	private String strDepBankName	=	"";
	private String strDepAccountNo	=	"";
	private ConstansBO DepDate		=	new ConstansBO("");
	private String strDepReferenceNo= 	"";
	
	//private String strSub_TRXN_T	=	"";
	//private String strSipReferenceNo=	"0";
	
	//private String strKYC_Flag		=	"Y";
	private String strNomineeName	=	"";
	private String strNomineeRelation	=	"";
	private String nomineeOpted		=	"";
	private int nomineePercent		=	0;
	private int nomineePercent1		=	0;
	private String nomineeName2	=	"";
	private String nomineeRelation2	=	"";
	private int nomineePercent2		=	0;	
	private String nomineeName3	=	"";
	private String nomineeRelation3	=	"";
	private int nomineePercent3		=	0;
	
	private String strPaymentMechanism	=	"";
	private String strRTGS_CD		=	"";
	private String strNEFT_CD		=	"";
	private String strMICR_CD		=	"";
	private String strClos_Ac_Ch	=	"";
	private String strReinv_Tag		=	"";
	private String strUINNo			=	"";
	/*private String strForm6061		=	"";
	private String strForm6061J1	=	"";
	private String strForm6061J2	=	"";*/

	//Camps extra columns
	private String strDIV_PY_MEC	=	"";
	private int intECS_NO			=	0;
	private String strBank_Code		=	"";
	private String strALT_Folio		=	"";
	private String strALT_Broker	=	"";
	private String strInstrmno		=	"";
	//private String strGF6061RE		=	"";
	private String strGSIP_Code		=	"";
//	private ConstansBO SIP_Reg_Date	=	new ConstansBO("");
	private String strFirst_Hldr_Min=	"";
	private String strJH1_Min		=	"";
	private String strJH2_Min		=	"";
	private String strGuardian_Min	=	"";
	
	private boolean nriInvestor		=	false;
	
	public void setAccountNo(String strAccountNo){this.strAccountNo=strAccountNo;}
	public void setAccountType(String strAccountType){this.strAccountType=strAccountType;}
	public void setBankName(String strBankName){this.strBankName=strBankName;}
	public void setBranchName(String strBranchName){this.strBranchName=strBranchName;}
	public void setBankCity(String strBankCity){this.strBankCity=strBankCity;}
	public void setHoldingNature(String strHoldingNature){this.strHoldingNature=strHoldingNature;}
	public void setTaxStatus(String strTaxStatus){this.strTaxStatus=strTaxStatus;}
	public void setRemarks(String strRemarks){this.strRemarks=strRemarks;}
	public void setDepBankName(String strDepBankName){this.strDepBankName=strDepBankName;}
	public void setDepAccountNo(String strDepAccountNo){this.strDepAccountNo=strDepAccountNo;}
	public void setDepDate(ConstansBO DepDate){this.DepDate=DepDate;}
	public void setDepReferenceNo(String strDepReferenceNo){this.strDepReferenceNo=strDepReferenceNo;}
	//public void setSub_TRXN_T(String strSub_TRXN_T){this.strSub_TRXN_T=strSub_TRXN_T;}
	//public void setSipReferenceNo(String strSipReferenceNo){this.strSipReferenceNo=strSipReferenceNo;}
	public void setNomineeName(String strNomineeName){this.strNomineeName=strNomineeName;}
	public void setNomineeRelation(String strNomineeRelation){this.strNomineeRelation=strNomineeRelation;}
	//public void setKYC_Flag(String strKYC_Flag){this.strKYC_Flag=strKYC_Flag;}
	public void setPaymentMechanism(String strPaymentMechanism){this.strPaymentMechanism=strPaymentMechanism;}
	public void setRTGS_CD(String strRTGS_CD){this.strRTGS_CD=strRTGS_CD;}
	public void setNEFT_CD(String strNEFT_CD){this.strNEFT_CD=strNEFT_CD;}
	public void setMICR_CD(String strMICR_CD){this.strMICR_CD=strMICR_CD;}
	public void setClos_Ac_Ch(String strClos_Ac_Ch){this.strClos_Ac_Ch=strClos_Ac_Ch;}
	public void setReinv_Tag(String strReinv_Tag){this.strReinv_Tag=strReinv_Tag;}
	public void setUINNo(String strUINNo){this.strUINNo=strUINNo;}
	/*public void setForm6061(String strForm6061){this.strForm6061=strForm6061;}
	public void setForm6061J1(String strForm6061J1){this.strForm6061J1=strForm6061J1;}
	public void setForm6061J2(String strForm6061J2){this.strForm6061J2=strForm6061J2;}*/
	//Camps Fields
	public void setDIV_PY_MEC(String strDIV_PY_MEC){this.strDIV_PY_MEC=strDIV_PY_MEC;}
	public void setECS_NO(int intECS_NO){this.intECS_NO=intECS_NO;}
	public void setBank_Code(String strBank_Code){this.strBank_Code=strBank_Code;}
	public void setALT_Folio(String strALT_Folio){this.strALT_Folio=strALT_Folio;}
	public void setALT_Broker(String strALT_Broker){this.strALT_Broker=strALT_Broker;}
	public void setInstrmno(String strInstrmno){this.strInstrmno=strInstrmno;}
	/*public void setGF6061RE(String strGF6061RE){this.strGF6061RE=strGF6061RE;}*/
	public void setGSIP_Code(String strGSIP_Code){this.strGSIP_Code=strGSIP_Code;}
	//public void setSIP_Reg_Date(ConstansBO SIP_Reg_Date){this.SIP_Reg_Date=SIP_Reg_Date;}
	public void setFirst_Hldr_Min(String strFirst_Hldr_Min){this.strFirst_Hldr_Min=strFirst_Hldr_Min;}
	public void setJH1_Min(String strJH1_Min){this.strJH1_Min=strJH1_Min;}
	public void setJH2_Min(String strJH2_Min){this.strJH2_Min=strJH2_Min;}
	public void setGuardian_Min(String strGuardian_Min){this.strGuardian_Min=strGuardian_Min;}
	public void setBankLookUpId(String strBankLookUpId) {		this.strBankLookUpId = strBankLookUpId;	}
	
	public String getAccountNo(){return (this.strAccountNo);}
	public String getAccountType(){return (this.strAccountType);}
	public String getBankName(){return (this.strBankName);}
	public String getBranchName(){return (this.strBranchName);}
	public String getBankCity(){return (this.strBankCity);}
	public String getHoldingNature(){return (this.strHoldingNature);}
	public String getTaxStatus(){return (this.strTaxStatus);}
	public String getRemarks(){return (this.strRemarks);}
	public String getDepBankName(){return (this.strDepBankName);}
	public String getDepAccountNo(){return (this.strDepAccountNo);}
	public ConstansBO getDepDate(){return (this.DepDate);}
	public String getDepReferenceNo(){return (this.strDepReferenceNo);}
//	public String getSub_TRXN_T(){return (this.strSub_TRXN_T);}
	//public String getSipReferenceNo(){return (this.strSipReferenceNo);}
	public String getNomineeName(){return (this.strNomineeName);}
	public String getNomineeRelation(){return (this.strNomineeRelation);}
	//public String getKYC_Flag(){return (this.strKYC_Flag);}
	
	public String getPaymentMechanism(){return (this.strPaymentMechanism);}
	public String getRTGS_CD(){return (this.strRTGS_CD);}
	public String getNEFT_CD(){return (this.strNEFT_CD);}
	public String getMICR_CD(){return (this.strMICR_CD);}
	public String getClos_Ac_Ch(){return (this.strClos_Ac_Ch);}
	public String getReinv_Tag(){return (this.strReinv_Tag);}
	public String getUINNo(){return (this.strUINNo);}
	/*public String getForm6061(){return (this.strForm6061);}
	public String getForm6061J1(){return (this.strForm6061J1);}
	public String getForm6061J2(){return (this.strForm6061J2);}*/
	//Camps Fields
	public String getDIV_PY_MEC(){return (this.strDIV_PY_MEC);}
	public int getECS_NO(){return(this.intECS_NO);}
	public String getBank_Code(){return(this.strBank_Code);}
	public String getALT_Folio(){return(this.strALT_Folio);}
	public String getALT_Broker(){return(this.strALT_Broker);}
	public String getInstrmno(){return (this.strInstrmno);}
	/*public String getGF6061RE(){return (this.strGF6061RE);}*/
	public String getGSIP_Code(){return (this.strGSIP_Code);}
	//public ConstansBO getSIP_Reg_Date(){return (this.SIP_Reg_Date);}
	public String getFirst_Hldr_Min(){return (this.strFirst_Hldr_Min);}
	public String getJH1_Min(){return (this.strJH1_Min);}
	public String getJH2_Min(){return (this.strJH2_Min);}
	public String getGuardian_Min(){return (this.strGuardian_Min);}
	public String getBankLookUpId() {	return strBankLookUpId;	}
	public boolean isNriInvestor() {
		return nriInvestor;
	}
	public void setNriInvestor(boolean nriInvestor) {
		this.nriInvestor = nriInvestor;
	}
	public String getNomineeOpted() {
		return nomineeOpted;
	}
	public void setNomineeOpted(String nomineeOpted) {
		this.nomineeOpted = nomineeOpted;
	}
	public int getNomineePercent() {
		return nomineePercent;
	}
	public void setNomineePercent(int nomineePercent) {
		this.nomineePercent = nomineePercent;
	}
	public String getNomineeName2() {
		return nomineeName2;
	}
	public void setNomineeName2(String nomineeName2) {
		this.nomineeName2 = nomineeName2;
	}
	public String getNomineeRelation2() {
		return nomineeRelation2;
	}
	public void setNomineeRelation2(String nomineeRelation2) {
		this.nomineeRelation2 = nomineeRelation2;
	}
	public int getNomineePercent2() {
		return nomineePercent2;
	}
	public void setNomineePercent2(int nomineePercent2) {
		this.nomineePercent2 = nomineePercent2;
	}
	public String getNomineeName3() {
		return nomineeName3;
	}
	public void setNomineeName3(String nomineeName3) {
		this.nomineeName3 = nomineeName3;
	}
	public String getNomineeRelation3() {
		return nomineeRelation3;
	}
	public void setNomineeRelation3(String nomineeRelation3) {
		this.nomineeRelation3 = nomineeRelation3;
	}
	public int getNomineePercent3() {
		return nomineePercent3;
	}
	public void setNomineePercent3(int nomineePercent3) {
		this.nomineePercent3 = nomineePercent3;
	}
	public int getNomineePercent1() {
		return nomineePercent1;
	}
	public void setNomineePercent1(int nomineePercent1) {
		this.nomineePercent1 = nomineePercent1;
	}
	
	
}
