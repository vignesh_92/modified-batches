package com.wifs.DBFForwardFeed;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class MISReport {
	
	public static Logger logger = Logger.getLogger("MISReport.class");
	public void generateMISReportExcel(String excelFilePath, Hashtable<String, ArrayList<TransactionMainBO>>  hashMISReportTransactionBO) throws Exception
	{
		
		Set<String> set = hashMISReportTransactionBO.keySet(); 

	    Iterator<String> itr = set.iterator();
	    while (itr.hasNext()) {
	    	
			double subTotal		=	0.0d;
			double grandTotal	=	0.0d;
			
			String strKeyValue	=	"";
			String paidThru		=	"";
			String remarks		=	"";
			TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
			ConstansBO objConstansBO	=	new ConstansBO("");
			
			ArrayList<TransactionMainBO> arryMISReportTransactionBO	=	new ArrayList<TransactionMainBO>();
			ArrayList<TransactionMainBO> arryTPSLTransactionBO	=	new ArrayList<TransactionMainBO>();
			ArrayList<TransactionMainBO> arryWIFSTransactionBO	=	new ArrayList<TransactionMainBO>();

	    	strKeyValue = itr.next().trim();
	    	
	    	arryMISReportTransactionBO	=	hashMISReportTransactionBO.get(strKeyValue);
    	
	    	// Creating XL Sheet
	    	HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet();
			HSSFRow  row =	sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			
			HSSFCellStyle cellStyle = wb.createCellStyle();
			HSSFFont font = wb.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			cellStyle.setFont(font);
			
			
			HSSFCellStyle cellStyleNormal = wb.createCellStyle();
			HSSFFont fontNormal = wb.createFont();
			fontNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			cellStyleNormal.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			cellStyleNormal.setFont(fontNormal);
			
			HSSFCellStyle cellStyleBorder = wb.createCellStyle();
			HSSFFont fontBorder = wb.createFont();
			fontBorder.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fontBorder.setUnderline(HSSFFont.U_SINGLE);
			cellStyleBorder.setFont(fontBorder);
			//cellStyleBorder.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			
			HSSFCellStyle cellStyleBottom = wb.createCellStyle();
			HSSFFont fontBottom = wb.createFont();
			fontBottom.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			//fontBottom.setUnderline(HSSFFont.U_SINGLE);
			cellStyleBottom.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			cellStyleBottom.setBorderTop(HSSFCellStyle.BORDER_THICK);

			cellStyleBottom.setFont(fontBottom);
			
			HSSFRichTextString richTextString = new HSSFRichTextString("Date");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(1);
			richTextString = new HSSFRichTextString(objConstansBO.getTodayDateMonthView());
			cell.setCellValue(richTextString);
			
			row =	sheet.createRow(1);
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("From :");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString("To :");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			row =	sheet.createRow(2);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("WIFS (ARN-69583)");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString(arryMISReportTransactionBO.get(0).getTransactionBO().getAmcName());
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			row =	sheet.createRow(3);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Chennai");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString(arryMISReportTransactionBO.get(0).getTransactionBO().getAmcAddress());
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			row =	sheet.createRow(5);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Dear Sir / Madam , ");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			
			row =	sheet.createRow(7);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Ref : Transfer of Funds ");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			
			row =	sheet.createRow(9);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("AMC");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(1);
			richTextString = new HSSFRichTextString("Scheme A/C");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(2);
			richTextString = new HSSFRichTextString("Bank");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(3);
			richTextString = new HSSFRichTextString("Scheme CD");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(4);
			richTextString = new HSSFRichTextString("Scheme Name");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(5);
			richTextString = new HSSFRichTextString("Investor Name");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(6);
			richTextString = new HSSFRichTextString("User Tr. No");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString("Paid thru");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(8);
			richTextString = new HSSFRichTextString("Tran Date");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(9);
			richTextString = new HSSFRichTextString("Tran Time");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(10);
			richTextString = new HSSFRichTextString("Amount");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(11);
			richTextString = new HSSFRichTextString("Remarks");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			for(int i = 0; i<arryMISReportTransactionBO.size();i++)
			{
				objTransactionMainBO	=	arryMISReportTransactionBO.get(i);
				paidThru	=	objTransactionMainBO.getTransactionBO().getPaidThru();
				
				if(paidThru.equals("TPSL"))
				{
					remarks	=	"From TPSL to "+objTransactionMainBO.getTransactionBO().getAmcShortName();
					objTransactionMainBO.getTransactionBO().setPaidThru(paidThru);
					objTransactionMainBO.getTransactionBO().setRemarks(remarks);
					
					arryTPSLTransactionBO.add(objTransactionMainBO);
				}
				else
				{
					remarks	=	"From WIFS to "+objTransactionMainBO.getTransactionBO().getAmcShortName();
					objTransactionMainBO.getTransactionBO().setRemarks(remarks);
					if(paidThru.equals("HDFC"))
						objTransactionMainBO.getTransactionBO().setPaidThru(paidThru+"PG");
					else if((paidThru.equals("DD"))||(paidThru.equals("ECS")))
						objTransactionMainBO.getTransactionBO().setPaidThru("TPSL"+" - "+paidThru);
					else
						objTransactionMainBO.getTransactionBO().setPaidThru(paidThru);
					
					arryWIFSTransactionBO.add(objTransactionMainBO);
				}
			}
			
			MISReport objMISReport =	new MISReport();
			int rowSize = 10;
			
			if(arryWIFSTransactionBO.size()>0)
			{
				logger.info(strKeyValue+" AMC's WIFS Payment Size ; "+arryWIFSTransactionBO.size());
				rowSize		=	rowSize+2;
				subTotal	=	objMISReport.writeMISReportCell(rowSize, sheet, cellStyleNormal, cellStyle, arryWIFSTransactionBO);
				grandTotal	+=	subTotal;
				rowSize		=	rowSize+arryWIFSTransactionBO.size()+3;
			}
			
			if(arryTPSLTransactionBO.size()>0)
			{
				logger.info(strKeyValue+" AMC's TPSL Payment Size ; "+arryTPSLTransactionBO.size());
				
				rowSize		=	rowSize+2;
				subTotal	=	objMISReport.writeMISReportCell(rowSize, sheet, cellStyleNormal, cellStyle, arryTPSLTransactionBO);
				grandTotal	+=	subTotal;
				rowSize		=	rowSize+arryTPSLTransactionBO.size()+3;
			}
			
			row = sheet.createRow(rowSize+4);
			
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Grand Total");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(10);
			cell.setCellValue(grandTotal);
			cell.setCellStyle(cellStyleBottom);
			cell.getCellStyle().setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			
			FileOutputStream fileOut = new FileOutputStream(excelFilePath+File.separator+arryMISReportTransactionBO.get(0).getTransactionBO().getRtCode()+arryMISReportTransactionBO.get(0).getTransactionBO().getAmcCode()+"MIS"+objConstansBO.getTodayDateMonthView().replace("/", "")+".xls");
			wb.write(fileOut);
			fileOut.close();
    	
	    }
	}
	public double writeMISReportCell(int rowSize, HSSFSheet sheet, HSSFCellStyle cellStyleNormal, HSSFCellStyle cellStyle, ArrayList<TransactionMainBO> arryMISReportTransactionBO) throws Exception
	{
		double subTotal	=	0.0d;
		for(int i = 0; i<arryMISReportTransactionBO.size();i++)
		{
			TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
			objTransactionMainBO	=	arryMISReportTransactionBO.get(i);
			
			int j = i + 1;
			
			HSSFRow  row =	sheet.createRow(j+rowSize);
			HSSFCell cell = row.createCell(0);
			HSSFRichTextString richTextString = new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getAmcCode());
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			cell = row.createCell(1);
			
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getAccountDetailsBO().getDepAccountNo()));
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(2);				
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.objAccountDetailsBO.getDepBankName()));
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(3);		
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.objTransactionBO.getSchemeCode()));
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(4);		
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getSchemeName()));
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(5);		
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.objInvestorDetailsBO.getFirstName()));
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(6);
			cell.setCellStyle(cellStyleNormal);
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getUserTransactionNo()));
			
			cell = row.createCell(7);
			cell.setCellStyle(cellStyleNormal);
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getPaidThru()));
			
			cell = row.createCell(8);	
			cell.setCellStyle(cellStyleNormal);
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getTransactionDateMIS().getDateMonthView()));
			
			cell = row.createCell(9);	
			cell.setCellStyle(cellStyleNormal);
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getTransactionTimeMIS()));
			
			cell = row.createCell(10);		
			cell.getCellStyle().setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			cell.setCellValue(objTransactionMainBO.getTransactionBO().getAmount().doubleValue());
			
			cell = row.createCell(11);		
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getRemarks()));
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(12);		
			cell.setCellValue(new HSSFRichTextString(objTransactionMainBO.getTransactionBO().getMisRemarks()));
			cell.setCellStyle(cellStyleNormal);
			
			
			subTotal	+=	objTransactionMainBO.getTransactionBO().getAmount().doubleValue();
		}
		
		HSSFRow row =	sheet.createRow(rowSize+arryMISReportTransactionBO.size()+3);	
		HSSFCell cell1 = row.createCell(0);
		HSSFRichTextString richTextString = new HSSFRichTextString("Sub Total");
		cell1.setCellValue(richTextString);
		cell1.setCellStyle(cellStyle);
		
		cell1 = row.createCell(10);
		cell1.setCellValue(subTotal);
		cell1.setCellStyle(cellStyle);
		cell1.getCellStyle().setAlignment(HSSFCellStyle.ALIGN_RIGHT);
		
		return subTotal;
	}
	
	public void generateMISReportLotBased(String endDate, String excelFilePath, Hashtable<String, Integer> hashMISLot, Hashtable<String, ArrayList<TransactionMainBO>>  hashMISReportTransactionBO) throws Exception
	{
		int lot	=	0;
		String bankDetailsKey	=	"";
		BigDecimal trxnAmount	=	new BigDecimal(0);
		ArrayList<TransactionMainBO> arryMISReportTransactionBO	=	new ArrayList<TransactionMainBO>();
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		ConstansBO objConstansBO	=	new ConstansBO("");
		DBFFilesDAO objDBFFilesDAO	=	new DBFFilesDAO();
		
		
		String nextWorkingDay	=	objDBFFilesDAO.getNextWorkingDay(endDate);
		
		Set<String> set = hashMISReportTransactionBO.keySet(); 
	    Iterator<String> itr = set.iterator();
	    while (itr.hasNext()) {
	    	
	    	
	    	lot	=	0;
			double subTotal		=	0.0d;
			double grandTotal	=	0.0d;
			trxnAmount			=	new BigDecimal(0);
			bankDetailsKey		=	"";
			
			String strKeyValue	=	"";
			String paidThru		=	"";
			String remarks		=	"";
			
			TransactionMainBO objTransactionBankSummaryBO	=	new TransactionMainBO();
			Hashtable<Integer, ArrayList<TransactionMainBO>> hashTPSLMISLot			=	new Hashtable<Integer, ArrayList<TransactionMainBO>>();
			Hashtable<String, TransactionMainBO> hashTPSLMISBankSummary			=	new Hashtable<String, TransactionMainBO>();
			ArrayList<TransactionMainBO> arryTPSLTransactionBO	=	new ArrayList<TransactionMainBO>();
			ArrayList<TransactionMainBO> arryWIFSTransactionBO	=	new ArrayList<TransactionMainBO>();

	    	strKeyValue = itr.next().trim();
	    	
	    	arryMISReportTransactionBO	=	hashMISReportTransactionBO.get(strKeyValue);
    	
	    	// Creating XL Sheet
	    	//Sheet1 trxn. details ------
	    	HSSFWorkbook wb = new HSSFWorkbook();

	    	//Summary
			HSSFSheet sheet1 = wb.createSheet("Summary");
			HSSFRow  row1 =	sheet1.createRow(0);
			HSSFCell cell1 = row1.createCell(0);
			
			HSSFSheet sheet = wb.createSheet("Details");
			HSSFRow  row =	sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			
			HSSFCellStyle cellStyle = wb.createCellStyle();
			HSSFFont font = wb.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			cellStyle.setFont(font);
			
			HSSFCellStyle cellStyleNormal = wb.createCellStyle();
			HSSFFont fontNormal = wb.createFont();
			fontNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			cellStyleNormal.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			cellStyleNormal.setFont(fontNormal);
			
			HSSFCellStyle cellStyleBorder = wb.createCellStyle();
			HSSFFont fontBorder = wb.createFont();
			fontBorder.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fontBorder.setUnderline(HSSFFont.U_SINGLE);
			cellStyleBorder.setFont(fontBorder);
			//cellStyleBorder.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			
			HSSFCellStyle cellStyleBottom = wb.createCellStyle();
			HSSFFont fontBottom = wb.createFont();
			fontBottom.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			//fontBottom.setUnderline(HSSFFont.U_SINGLE);
			cellStyleBottom.setBorderBottom(HSSFCellStyle.BORDER_THICK);
			cellStyleBottom.setBorderTop(HSSFCellStyle.BORDER_THICK);

			cellStyleBottom.setFont(fontBottom);
			
			HSSFRichTextString richTextString = new HSSFRichTextString("Date");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			
			cell = row.createCell(1);
			richTextString = new HSSFRichTextString(objConstansBO.getTodayDateMonthView());
			cell.setCellValue(richTextString);
			
			row =	sheet.createRow(1);
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("From :");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString("To :");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			row =	sheet.createRow(2);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("WIFS (ARN-69583)");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString(arryMISReportTransactionBO.get(0).getTransactionBO().getAmcName());
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			row =	sheet.createRow(3);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Chennai");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString(arryMISReportTransactionBO.get(0).getTransactionBO().getAmcAddress());
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			row =	sheet.createRow(5);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Dear Sir / Madam , ");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			
			row =	sheet.createRow(7);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Ref : Transfer of Funds ");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleNormal);
			
			row =	sheet.createRow(9);	
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("AMC");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(1);
			richTextString = new HSSFRichTextString("Scheme A/C");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(2);
			richTextString = new HSSFRichTextString("Bank");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyle);
			
			cell = row.createCell(3);
			richTextString = new HSSFRichTextString("Scheme CD");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(4);
			richTextString = new HSSFRichTextString("Scheme Name");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(5);
			richTextString = new HSSFRichTextString("Investor Name");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(6);
			richTextString = new HSSFRichTextString("User Tr. No");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(7);
			richTextString = new HSSFRichTextString("Paid thru");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(8);
			richTextString = new HSSFRichTextString("Tran Date");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(9);
			richTextString = new HSSFRichTextString("Tran Time");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(10);
			richTextString = new HSSFRichTextString("Amount");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(11);
			richTextString = new HSSFRichTextString("Remarks");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			for(int i = 0; i<arryMISReportTransactionBO.size();i++)
			{
				lot	=	0;
				
				arryTPSLTransactionBO		=	new ArrayList<TransactionMainBO>();
				
				objTransactionMainBO		=	arryMISReportTransactionBO.get(i);
				paidThru	=	objTransactionMainBO.getTransactionBO().getPaidThru();
				
				if(paidThru.equals("TPSL")) //For TPSL Lot basis
				{
					remarks	=	"From TPSL to "+objTransactionMainBO.getTransactionBO().getAmcShortName();
					objTransactionMainBO.getTransactionBO().setPaidThru(paidThru);
					objTransactionMainBO.getTransactionBO().setRemarks(remarks);
					
					try{
						lot	= ConstansBO.NullCheckINT(hashMISLot.get(objTransactionMainBO.getTransactionBO().getPaymentId()));
					}catch(Exception e){
						lot	=	0;
					}
					
					if(lot==0)
						objTransactionMainBO.getTransactionBO().setMisRemarks("Funds will get transferred on "+nextWorkingDay);
					
					
					if(hashTPSLMISLot.containsKey(lot)){
						arryTPSLTransactionBO	=	hashTPSLMISLot.get(lot);
						arryTPSLTransactionBO.add(objTransactionMainBO);
						hashTPSLMISLot.put(lot, arryTPSLTransactionBO);
						
					}else{
						arryTPSLTransactionBO.add(objTransactionMainBO);
						hashTPSLMISLot.put(lot, arryTPSLTransactionBO);
					}
				}
				else
				{
					remarks	=	"From WIFS to "+objTransactionMainBO.getTransactionBO().getAmcShortName();
					objTransactionMainBO.getTransactionBO().setRemarks(remarks);
					if(paidThru.equals("HDFC"))
						objTransactionMainBO.getTransactionBO().setPaidThru(paidThru+"PG");
					else if((paidThru.equals("DD"))||(paidThru.equals("ECS")))
						objTransactionMainBO.getTransactionBO().setPaidThru("TPSL"+" - "+paidThru);
					else
						objTransactionMainBO.getTransactionBO().setPaidThru(paidThru);
					
					arryWIFSTransactionBO.add(objTransactionMainBO);
				}
			}
			
			
			MISReport objMISReport =	new MISReport();
			int rowSize = 9;
			
			if(arryWIFSTransactionBO.size()>0)
			{
				logger.info(strKeyValue+" AMC's WIFS Payment Size ; "+arryWIFSTransactionBO.size());
				rowSize		=	rowSize+2;
				subTotal	=	objMISReport.writeMISReportCell(rowSize, sheet, cellStyleNormal, cellStyle, arryWIFSTransactionBO);
				grandTotal	+=	subTotal;
				rowSize		=	rowSize+arryWIFSTransactionBO.size()+3;
			}
			
			int keyValueLot	=	0;
			if(hashTPSLMISLot.size()>0){
				
				Set<Integer> setLot = hashTPSLMISLot.keySet(); 

			    Iterator<Integer> itrLot = setLot.iterator();
			    while (itrLot.hasNext()) {
			    	keyValueLot	=	0;
			    	arryTPSLTransactionBO	=	new ArrayList<TransactionMainBO>();
			    	keyValueLot = itrLot.next();
			    	
			    	arryTPSLTransactionBO	=	hashTPSLMISLot.get(keyValueLot);
			    	
			    	logger.info(strKeyValue+" AMC's TPSL Payment Size ; "+arryTPSLTransactionBO.size()+" Lot:"+keyValueLot);
					
					rowSize		=	rowSize+2;
					subTotal	=	objMISReport.writeMISReportCell(rowSize, sheet, cellStyleNormal, cellStyle, arryTPSLTransactionBO);
					grandTotal	+=	subTotal;
					rowSize		=	rowSize+arryTPSLTransactionBO.size()+3;
			    }
			}
					
			//For Bank Summary
			for(int i = 0; i<arryMISReportTransactionBO.size();i++)
			{
				objTransactionBankSummaryBO	=	new TransactionMainBO();
				
				bankDetailsKey	=	arryMISReportTransactionBO.get(i).getAccountDetailsBO().getDepAccountNo().trim()+arryMISReportTransactionBO.get(i).getTransactionBO().getRemarks().trim();
				if(hashTPSLMISBankSummary.containsKey(bankDetailsKey)){
					objTransactionBankSummaryBO	=	hashTPSLMISBankSummary.get(bankDetailsKey);
					trxnAmount	=	objTransactionBankSummaryBO.getTransactionBO().getAmount().add(arryMISReportTransactionBO.get(i).getTransactionBO().getAmount());
					objTransactionBankSummaryBO.getTransactionBO().setAmount(trxnAmount);
					hashTPSLMISBankSummary.put(bankDetailsKey, objTransactionBankSummaryBO);
				}else{
					hashTPSLMISBankSummary.put(bankDetailsKey, arryMISReportTransactionBO.get(i));
				}
			}
			
			row = sheet.createRow(rowSize+4);
			
			cell = row.createCell(0);
			richTextString = new HSSFRichTextString("Grand Total");
			cell.setCellValue(richTextString);
			cell.setCellStyle(cellStyleBorder);
			
			cell = row.createCell(10);
			cell.setCellValue(grandTotal);
			cell.setCellStyle(cellStyleBottom);
			cell.getCellStyle().setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			
			//Sheet1 trxn. details ------
			cell1 = row1.createCell(0);
			richTextString = new HSSFRichTextString("Funds Transferred Date");
			cell1.setCellValue(richTextString);
			cell1.setCellStyle(cellStyleBorder);
			
			cell1 = row1.createCell(1);
			richTextString = new HSSFRichTextString("Bank A/cno");
			cell1.setCellValue(richTextString);
			cell1.setCellStyle(cellStyleBorder);
			
			cell1 = row1.createCell(2);
			richTextString = new HSSFRichTextString("Bank Name");
			cell1.setCellValue(richTextString);
			cell1.setCellStyle(cellStyleBorder);
			
			cell1 = row1.createCell(3);
			richTextString = new HSSFRichTextString("Total Amount");
			cell1.setCellValue(richTextString);
			cell1.setCellStyle(cellStyleBorder);
			
			cell1 = row1.createCell(4);
			richTextString = new HSSFRichTextString("Remarks");
			cell1.setCellValue(richTextString);
			cell1.setCellStyle(cellStyleBorder);
			
			
			
			int bank	=	0;
			Set<String> setBank = hashTPSLMISBankSummary.keySet(); 
		    Iterator<String> itrBank = setBank.iterator();
		    while (itrBank.hasNext()) {
		    	bank++;
		    	bankDetailsKey	=	itrBank.next().trim();
		    	
		    	objTransactionBankSummaryBO	=	hashTPSLMISBankSummary.get(bankDetailsKey);
		    	
		    	row1 =	sheet1.createRow(bank);
		    	
		    	cell1 = row1.createCell(0);
				richTextString = new HSSFRichTextString(objConstansBO.getTodayDate("MM/dd/yyyy"));
				cell1.setCellValue(richTextString);
				cell1.setCellStyle(cellStyleNormal);
				
				cell1 = row1.createCell(1);
				richTextString = new HSSFRichTextString(objTransactionBankSummaryBO.getAccountDetailsBO().getDepAccountNo());
				cell1.setCellValue(richTextString);
				cell1.setCellStyle(cellStyleNormal);
				
				cell1 = row1.createCell(2);
				richTextString = new HSSFRichTextString(objTransactionBankSummaryBO.getAccountDetailsBO().getDepBankName());
				cell1.setCellValue(richTextString);
				cell1.setCellStyle(cellStyleNormal);
				
				cell1 = row1.createCell(3);
				cell1.getCellStyle().setAlignment(HSSFCellStyle.ALIGN_RIGHT);
				cell1.setCellValue(objTransactionBankSummaryBO.getTransactionBO().getAmount().doubleValue());
				
				cell1 = row1.createCell(4);
				richTextString = new HSSFRichTextString(objTransactionBankSummaryBO.getTransactionBO().getRemarks());
				cell1.setCellValue(richTextString);
				cell1.setCellStyle(cellStyleNormal);
				
		    }
			
			
			FileOutputStream fileOut = new FileOutputStream(excelFilePath+File.separator+arryMISReportTransactionBO.get(0).getTransactionBO().getRtCode()+arryMISReportTransactionBO.get(0).getTransactionBO().getAmcCode()+"MIS"+objConstansBO.getTodayDateMonthView().replace("/", "")+".xls");
			wb.write(fileOut);
			fileOut.close();
    	
	    }
	}
}
