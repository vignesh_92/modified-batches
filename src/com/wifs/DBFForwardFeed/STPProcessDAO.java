package com.wifs.DBFForwardFeed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.wifs.dao.helper.AutoNumberGenerator;

public class STPProcessDAO {

	public static Logger logger = Logger.getLogger("STPProcessDAO.class");
	public PreparedStatement pstmtSTPProcessInsert(int userId, Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	" Insert into STPProcess (STPId ,Acknowledge ,UploadDate, STPFileName, TotalRecords, SuccessRecords, " +
				" ErrorRecords, ErrorMessage, Createdate, CreatedUser) values (?, ?, ?, ?, ?, ?, ?, ?, DATEADD(MI, 330, GETDATE()) , "+userId+" ) ";  
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public STPProcessBO STPProcessInsert(STPProcessBO objSTPProcessBO, PreparedStatement objPreparedStatement) throws Exception
	{
		AutoNumberGenerator numberGenerator=new AutoNumberGenerator();
		ConstansBO ojbConstansBO	=	new ConstansBO("");
		
		int transactionId = numberGenerator.getAutoNumberGenerated("STPProcessId");
		objSTPProcessBO.setSTPId(transactionId);
		
		objPreparedStatement.setInt(1, transactionId);
		objPreparedStatement.setString(2, objSTPProcessBO.getAcknowledge());
		
		if(objSTPProcessBO.getUploadDateTime().length()>13)
			objPreparedStatement.setString(3, objSTPProcessBO.getUploadDateTime().substring(0,11)+" "+objSTPProcessBO.getUploadDateTime().substring(11,objSTPProcessBO.getUploadDateTime().length()));
		else
			objPreparedStatement.setString(3, ojbConstansBO.getTodayDate("yyyy-MM-dd hh:mm:ss"));
		
		objPreparedStatement.setString(4, objSTPProcessBO.getFileName());
		objPreparedStatement.setInt(5, objSTPProcessBO.getTotalRecords());
		objPreparedStatement.setInt(6, objSTPProcessBO.getSuccessRecords());
		objPreparedStatement.setInt(7, objSTPProcessBO.getErrorRecords());
		objPreparedStatement.setString(8, objSTPProcessBO.getErrorMessage());
		
		objPreparedStatement.executeUpdate();
		return objSTPProcessBO;
	}
	public PreparedStatement pstmtSTPProcessTransactionWiseInsert(int userId, Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = 	" insert into STPProcessTransaction (STPId, Code, RecordNumber, UserTransRefId, Message, CreatedDate, CreatedUser)" +
				" values (?, ?, ?, ?, ?, DATEADD(MI, 330, GETDATE()) , "+userId+" ) ";  
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public void STPProcessTransactionWiseInsert(ArrayList<STPProcessBO> arrSTPProcessBO, ArrayList<TransactionMainBO> arrTransactionMainBO, PreparedStatement objPreparedStatement) throws Exception
	{
		STPProcessBO objSTPProcessBO	=	new STPProcessBO();
		try{
			for(int i=0; i<arrSTPProcessBO.size(); i++){
				objSTPProcessBO	=	new STPProcessBO();
				objSTPProcessBO	=	arrSTPProcessBO.get(i);
				
				objPreparedStatement.setInt(1, objSTPProcessBO.getSTPId());
				objPreparedStatement.setString(2, objSTPProcessBO.getCode());
				objPreparedStatement.setString(3, objSTPProcessBO.getRecordNumber());
				objPreparedStatement.setString(4, arrTransactionMainBO.get(i).getTransactionBO().getUserTransactionNo());
				objPreparedStatement.setString(5, objSTPProcessBO.getMessage());
				objPreparedStatement.addBatch();
			}
			objPreparedStatement.executeBatch();
		}catch(Exception e){
			logger.error("STPTrxn insert EXP:"+e);
		}finally{
			objPreparedStatement.clearBatch();
		}
	}
}
