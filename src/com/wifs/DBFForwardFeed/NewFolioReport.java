package com.wifs.DBFForwardFeed;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class NewFolioReport {
	
	public static Logger logger = Logger.getLogger("NewFolioReport.class");
	public boolean getNewFolioExcel(String excelFilePath, Hashtable<String, NewFolioBO> hashNewFolioBO) throws Exception
	{
		try 
		{	
			NewFolioBO objNewFolioBO	=	new NewFolioBO();
			
			HSSFWorkbook wb = new HSSFWorkbook(); 
			HSSFSheet sheet = wb.createSheet();
			HSSFRow  row =	sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			HSSFRichTextString richTextString = new HSSFRichTextString("R&T Code");
			cell.setCellValue(richTextString);
			HSSFCell cell1 = row.createCell(1);
			richTextString = new HSSFRichTextString("Investor Name");
			cell1.setCellValue(richTextString);
			HSSFCell cell2 = row.createCell(2);
			richTextString = new HSSFRichTextString("PAN No.");
			cell2.setCellValue(richTextString);
			HSSFCell cell3 = row.createCell(3);
			richTextString = new HSSFRichTextString("IH No.");
			cell3.setCellValue(richTextString);
			HSSFCell cell4 = row.createCell(4);
			richTextString = new HSSFRichTextString("InvestorId");
			cell4.setCellValue(richTextString);
			HSSFCell cell5 = row.createCell(5);
			richTextString = new HSSFRichTextString("Investor From");
			cell5.setCellValue(richTextString);
			
			
			 // Sort hashtable.
		    Vector<String> vectorNewFolio = new Vector(hashNewFolioBO.keySet());
		    Collections.sort(vectorNewFolio);

			
			int j = 0;
			// Display (sorted) hashtable.
		    for (Enumeration<String> e = vectorNewFolio.elements(); e.hasMoreElements();) {
		    	
		    	String key = (String)e.nextElement();
		    	objNewFolioBO = (NewFolioBO)hashNewFolioBO.get(key);

				j++;
				row = sheet.createRow(j);
				cell = row.createCell(0);
				cell.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getRtCode())));
				cell1 = row.createCell(1);
				cell1.setCellValue(new HSSFRichTextString(objNewFolioBO.getInvestorName()));
				cell2 = row.createCell(2);
				cell2.setCellValue(new HSSFRichTextString(objNewFolioBO.getPanNo()));
				cell3 = row.createCell(3);
				//For gaurdian 
				if(!objNewFolioBO.getMinorIHNo().equals(""))
					cell3.setCellValue(new HSSFRichTextString(objNewFolioBO.getMinorIHNo()+" U/G "+objNewFolioBO.getInHouseNo()));
				else
					cell3.setCellValue(new HSSFRichTextString(objNewFolioBO.getInHouseNo()));
				cell4 = row.createCell(4);
				cell4.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getInvestorId())));
				cell5 = row.createCell(5);
				cell5.setCellValue(new HSSFRichTextString(objNewFolioBO.getInvestorType()));
			}
		    
			if(j>0)
			{
				FileOutputStream fileOut = new FileOutputStream(excelFilePath);
				wb.write(fileOut);
				fileOut.close();
			}
		}catch(Exception e)
		{
			logger.error("Error while generationg NewFolio excel sheet"+e);
		}
		return false;
	}
	public boolean getNFOSchemes(String excelFilePath, ArrayList<TransactionBO> arrNFOTransactionBO) throws Exception
	{
		try 
		{	
			double amountNFO	=	0.0d;
			
			HSSFWorkbook wb = new HSSFWorkbook(); 
			HSSFSheet sheet = wb.createSheet();
			HSSFRow  row =	sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			HSSFRichTextString richTextString = new HSSFRichTextString("RTAMCCode");
			cell.setCellValue(richTextString);
			HSSFCell cell1 = row.createCell(1);
			richTextString = new HSSFRichTextString("AMCSchemeCode");
			cell1.setCellValue(richTextString);
			HSSFCell cell2 = row.createCell(2);
			richTextString = new HSSFRichTextString("BankAccNo");
			cell2.setCellValue(richTextString);
			HSSFCell cell3 = row.createCell(3);
			richTextString = new HSSFRichTextString("S_NAME");
			cell3.setCellValue(richTextString);
			HSSFCell cell4 = row.createCell(4);
			richTextString = new HSSFRichTextString("InvestorName");
			cell4.setCellValue(richTextString);
			HSSFCell cell5 = row.createCell(5);
			richTextString = new HSSFRichTextString("UserTransRefID");
			cell5.setCellValue(richTextString);
			HSSFCell cell6 = row.createCell(6);
			richTextString = new HSSFRichTextString("TransactionDate");
			cell6.setCellValue(richTextString);
			HSSFCell cell7 = row.createCell(7);
			richTextString = new HSSFRichTextString("TransactionTime");
			cell7.setCellValue(richTextString);
			HSSFCell cell8 = row.createCell(8);
			richTextString = new HSSFRichTextString("Amount");
			cell8.setCellValue(richTextString);
											
			TransactionBO	objTransactionBO	=	new TransactionBO();
			
		    for (int i=0;i<arrNFOTransactionBO.size();i++) {
		    	
		    	objTransactionBO	=	arrNFOTransactionBO.get(i);
		    	
				row = sheet.createRow(i+1);
				cell = row.createCell(0);
				cell.setCellValue(new HSSFRichTextString(objTransactionBO.getAmcCode()));
				cell1 = row.createCell(1);
				cell1.setCellValue(new HSSFRichTextString(objTransactionBO.getSchemeCode()));
				cell2 = row.createCell(2);
				cell2.setCellValue(new HSSFRichTextString(objTransactionBO.getApplNo()));
				cell3 = row.createCell(3);
				cell3.setCellValue(new HSSFRichTextString(objTransactionBO.getSchemeName()));
				cell4 = row.createCell(4);
				cell4.setCellValue(new HSSFRichTextString(objTransactionBO.getInvestorName()));
				cell5 = row.createCell(5);
				cell5.setCellValue(new HSSFRichTextString(objTransactionBO.getUserTransactionNo()));
				cell6 = row.createCell(6);
				cell6.setCellValue(new HSSFRichTextString(objTransactionBO.getTransactionDate().getDateMonthView()));
				cell7 = row.createCell(7);
				cell7.setCellValue(new HSSFRichTextString(objTransactionBO.getTransactionTime()));
				cell8 = row.createCell(8);
				cell8.setCellValue(new HSSFRichTextString(objTransactionBO.getAmount().toString()));
				
				amountNFO	+=	objTransactionBO.getAmount().doubleValue();
			}
		    
		    row = sheet.createRow(arrNFOTransactionBO.size()+3);
		    cell7 = row.createCell(7);
			cell7.setCellValue(new HSSFRichTextString("Total"));
			cell8 = row.createCell(8);
			cell8.setCellValue(new HSSFRichTextString(String.valueOf(amountNFO)));
			
			if(arrNFOTransactionBO.size()>0)
			{
				FileOutputStream fileOut = new FileOutputStream(excelFilePath);
				wb.write(fileOut);
				fileOut.close();
			}
		}catch(Exception e)
		{
			logger.error("Error while generationg getNFOSchemes excel sheet"+e);
		}
		return true;
	}
	public boolean genarateXLForFTNewFolio(String excelFilePath, ArrayList<NewFolioBO> arrNewFolioBO) throws Exception
	{
		try 
		{	
			NewFolioBO objNewFolioBO	=	new NewFolioBO();
			
			HSSFWorkbook wb = new HSSFWorkbook(); 
			HSSFSheet sheet = wb.createSheet();
			HSSFRow  row =	sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			HSSFRichTextString richTextString = new HSSFRichTextString("USR_TRXN_NO");
			cell.setCellValue(richTextString);
			HSSFCell cell1 = row.createCell(1);
			richTextString = new HSSFRichTextString("FOLIO_ID");
			cell1.setCellValue(richTextString);
			HSSFCell cell2 = row.createCell(2);
			richTextString = new HSSFRichTextString("ACCOUNT_NO");
			cell2.setCellValue(richTextString);
			HSSFCell cell3 = row.createCell(3);
			richTextString = new HSSFRichTextString("FIRST_NAME");
			cell3.setCellValue(richTextString);
			HSSFCell cell4 = row.createCell(4);
			richTextString = new HSSFRichTextString("NATIONALITY");
			cell4.setCellValue(richTextString);
			HSSFCell cell5 = row.createCell(5);
			richTextString = new HSSFRichTextString("COUNTRY_OF_RESIDENCE");
			cell5.setCellValue(richTextString);
			HSSFCell cell6 = row.createCell(6);
			richTextString = new HSSFRichTextString("ADDRESS");
			cell6.setCellValue(richTextString);
			HSSFCell cell7 = row.createCell(7);
			richTextString = new HSSFRichTextString("IT_PAN_NO");
			cell7.setCellValue(richTextString);
			HSSFCell cell8 = row.createCell(8);
			richTextString = new HSSFRichTextString("SOCIAL_STATUS");
			cell8.setCellValue(richTextString);
			HSSFCell cell9 = row.createCell(9);
			richTextString = new HSSFRichTextString("Broker_Code");
			cell9.setCellValue(richTextString);
			HSSFCell cell10 = row.createCell(10);
			richTextString = new HSSFRichTextString("Fin_sta1");
			cell10.setCellValue(richTextString);
			HSSFCell cell11 = row.createCell(11);
			richTextString = new HSSFRichTextString("Net_wor1");
			cell11.setCellValue(richTextString);
			HSSFCell cell12 = row.createCell(12);
			richTextString = new HSSFRichTextString("Fin_sta2");
			cell12.setCellValue(richTextString);
			HSSFCell cell13 = row.createCell(13);
			richTextString = new HSSFRichTextString("Net_wor2");
			cell13.setCellValue(richTextString);
			HSSFCell cell14 = row.createCell(14);
			richTextString = new HSSFRichTextString("Fin_sta3");
			cell14.setCellValue(richTextString);
			HSSFCell cell15 = row.createCell(15);
			richTextString = new HSSFRichTextString("Net_wor3");
			cell15.setCellValue(richTextString);
			HSSFCell cell16 = row.createCell(16);
			richTextString = new HSSFRichTextString("Fin_stag");
			cell16.setCellValue(richTextString);
			HSSFCell cell17 = row.createCell(17);
			richTextString = new HSSFRichTextString("Net_worg");
			cell17.setCellValue(richTextString);
			HSSFCell cell18 = row.createCell(18);
			richTextString = new HSSFRichTextString("PEP1");
			cell18.setCellValue(richTextString);
			HSSFCell cell19 = row.createCell(19);
			richTextString = new HSSFRichTextString("PEP2");
			cell19.setCellValue(richTextString);
			HSSFCell cell20 = row.createCell(20);
			richTextString = new HSSFRichTextString("PEP3");
			cell20.setCellValue(richTextString);
			HSSFCell cell21 = row.createCell(21);
			richTextString = new HSSFRichTextString("PEPg");
			cell21.setCellValue(richTextString);
			HSSFCell cell22 = row.createCell(22);
			richTextString = new HSSFRichTextString("OCC_COD2");
			cell22.setCellValue(richTextString);
			HSSFCell cell23 = row.createCell(23);
			richTextString = new HSSFRichTextString("OCC_COD3");
			cell23.setCellValue(richTextString);
			HSSFCell cell24 = row.createCell(24);
			richTextString = new HSSFRichTextString("OCC_CODG");
			cell24.setCellValue(richTextString);
				

		    for (int i=0; i<arrNewFolioBO.size();i++) {
		    	
		    	objNewFolioBO = arrNewFolioBO.get(i);
				row = sheet.createRow(i+1);
				cell = row.createCell(0);
				cell.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getUserTransRefId())));
				//cell1 empty
				//cell2 empty
				cell3 = row.createCell(3);
				cell3.setCellValue(new HSSFRichTextString(objNewFolioBO.getInvestorName()));
				cell4 = row.createCell(4);
				cell4.setCellValue(new HSSFRichTextString(objNewFolioBO.getNationality()));
				cell5 = row.createCell(5);
				cell5.setCellValue(new HSSFRichTextString(objNewFolioBO.getNationality()));
				
				cell6 = row.createCell(6);
				cell6.setCellValue(new HSSFRichTextString(objNewFolioBO.getAddress()));
				
				cell7 = row.createCell(7);
				cell7.setCellValue(new HSSFRichTextString(objNewFolioBO.getPanNo()));
				//cell8 empty
								
				cell9 = row.createCell(9);
				cell9.setCellValue(new HSSFRichTextString("WEALTH"));
				
				cell10 = row.createCell(10);
				cell10.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getFAnnualIncome())));
				
				cell11 = row.createCell(11);
				cell11.setCellValue(new HSSFRichTextString("0"));
				
				cell12 = row.createCell(12);
				cell12.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getSAnnualIncome())));
				
				cell13 = row.createCell(13);
				cell13.setCellValue(new HSSFRichTextString("0"));
				
				cell14 = row.createCell(14);
				cell14.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getTAnnualIncome())));
				
				cell15 = row.createCell(15);
				cell15.setCellValue(new HSSFRichTextString("0"));
				
				cell16 = row.createCell(16);
				cell16.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getGAnnualIncome())));
				
				cell17 = row.createCell(17);
				cell17.setCellValue(new HSSFRichTextString("0"));
				
				cell18 = row.createCell(18);
				cell18.setCellValue(new HSSFRichTextString("NA"));
				
				cell19 = row.createCell(19);
				cell19.setCellValue(new HSSFRichTextString("NA"));
				
				cell20 = row.createCell(20);
				cell20.setCellValue(new HSSFRichTextString("NA"));
				
				cell21 = row.createCell(21);
				cell21.setCellValue(new HSSFRichTextString("NA"));

				cell22 = row.createCell(22);
				cell22.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getSOccupation())));
				
				cell23 = row.createCell(23);
				cell23.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getTOccupation())));
				
				cell24 = row.createCell(24);
				cell24.setCellValue(new HSSFRichTextString(String.valueOf(objNewFolioBO.getGOccupation())));
			}
			FileOutputStream fileOut = new FileOutputStream(excelFilePath);
			wb.write(fileOut);
			fileOut.close();
		}catch(Exception e)
		{
			logger.error("Error while generationg NewFolio excel sheet"+e);
		}
		return false;
	}
	public boolean genarateFATCANotAvailablepan(String excelFilePath, Hashtable<String, TransactionMainBO> hashFATCANotavailable) throws Exception
	{
		try 
		{
			ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
			
			HSSFWorkbook wb = new HSSFWorkbook(); 
			HSSFSheet sheet = wb.createSheet();
			HSSFRow  row =	sheet.createRow(0);
			HSSFCell cell = row.createCell(0);
			HSSFRichTextString richTextString = new HSSFRichTextString("PAN");
			cell.setCellValue(richTextString);
			HSSFCell cell1 = row.createCell(1);
			richTextString = new HSSFRichTextString("Investor name");
			cell1.setCellValue(richTextString);
			HSSFCell cell2 = row.createCell(2);
			richTextString = new HSSFRichTextString("Usertrxn no");
			cell2.setCellValue(richTextString);
			HSSFCell cell3 = row.createCell(3);
			richTextString = new HSSFRichTextString("RTCode");
			cell3.setCellValue(richTextString);
			HSSFCell cell4 = row.createCell(4);
			richTextString = new HSSFRichTextString("RTAMCCode");
			cell4.setCellValue(richTextString);
			HSSFCell cell5 = row.createCell(5);
			richTextString = new HSSFRichTextString("AMCSChemeCode");
			cell5.setCellValue(richTextString);
			HSSFCell cell6 = row.createCell(6);
			richTextString = new HSSFRichTextString("Folio Number");
			cell6.setCellValue(richTextString);
			HSSFCell cell7 = row.createCell(7);
			richTextString = new HSSFRichTextString("Minor Flag");
			cell7.setCellValue(richTextString);
				
			int i	=	0;
			Set<String> set = hashFATCANotavailable.keySet(); 
		    Iterator<String> itr = set.iterator();
		    while (itr.hasNext()) {
		    	
		    	String strKeyValue = itr.next();
	    	
		    	TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		    	objTransactionMainBO	=	hashFATCANotavailable.get(strKeyValue);
		    	InvestorDetailsBO objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
		    	TransactionBO objTransactionBO		=	objTransactionMainBO.getTransactionBO();
				
		    	arrFatcaDocBO	=	objInvestorDetailsBO.getArrFatcaDocBO();
		    	
		    	for(int fatca=0; fatca<arrFatcaDocBO.size(); fatca++){
		    		
		    		if(!arrFatcaDocBO.get(fatca).getFatcaNotAvailablePan().equals("")){
		    			i++;
			    		row = sheet.createRow(i);
						cell = row.createCell(0);
						cell.setCellValue(new HSSFRichTextString(arrFatcaDocBO.get(fatca).getFatcaNotAvailablePan()));
						cell1 = row.createCell(1);
						cell1.setCellValue(new HSSFRichTextString(arrFatcaDocBO.get(fatca).getInvestorName()));
						cell2 = row.createCell(2);
						cell2.setCellValue(new HSSFRichTextString(objTransactionBO.getUserTransactionNo()));
						cell3 = row.createCell(3);
						cell3.setCellValue(new HSSFRichTextString(String.valueOf(objTransactionBO.getRtCode())));
						cell4 = row.createCell(4);
						cell4.setCellValue(new HSSFRichTextString(objTransactionBO.getAmcCode()));
						cell5 = row.createCell(5);
						cell5.setCellValue(new HSSFRichTextString(objTransactionBO.getSchemeCode()));
						cell6 = row.createCell(6);
						cell6.setCellValue(new HSSFRichTextString(objTransactionBO.getFolioNo()));
						if(arrFatcaDocBO.get(fatca).getMinorFlag()==1){
							cell7 = row.createCell(7);
							cell7.setCellValue(new HSSFRichTextString("Minor Investor"));
						}
		    		}
		    	}
		    }
			FileOutputStream fileOut = new FileOutputStream(excelFilePath);
			wb.write(fileOut);
			fileOut.close();
			}catch(Exception e)
			{
				logger.error("Error while generationg NewFolio excel sheet"+e);
			}
			return false;
	}
}
