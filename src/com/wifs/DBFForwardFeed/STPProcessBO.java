package com.wifs.DBFForwardFeed;

import java.util.ArrayList;

public class STPProcessBO {

    private int userTransRefId;
	int STPId			=	0;
	String errorMessage	=	"";
	String acknowledge	=	"";
	String uploadDateTime	=	"";
	String fileName		=	"";
	int totalRecords	=	0;
	int successRecords	=	0;
	int errorRecords	=	0;
	
	String code			=	"";
	String recordNumber	=	"";
	String message		=	"";
	
	boolean stpProcessFlag	=	false;
	
	ArrayList<STPProcessBO> arrSTPProcessBO	=	new ArrayList<STPProcessBO>();
        
        public STPProcessBO(){
            super();
        }

        public STPProcessBO(int userTransRefId, String message){
            this.userTransRefId = userTransRefId;
            this.message = message;
        }
	
        public int getUserTransRefId() {
		return userTransRefId;
	}
	public void setUserTransRefId(int userTransRefId) {
		this.userTransRefId = userTransRefId;
	}
	public int getSTPId() {
		return STPId;
	}
	public void setSTPId(int STPId) {
		this.STPId = STPId;
	}
	public String getAcknowledge() {
		return acknowledge;
	}
	public void setAcknowledge(String acknowledge) {
		this.acknowledge = acknowledge;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	public int getSuccessRecords() {
		return successRecords;
	}
	public void setSuccessRecords(int successRecords) {
		this.successRecords = successRecords;
	}
	public int getErrorRecords() {
		return errorRecords;
	}
	public void setErrorRecords(int errorRecords) {
		this.errorRecords = errorRecords;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRecordNumber() {
		return recordNumber;
	}
	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ArrayList<STPProcessBO> getArrSTPProcessBO() {
		return arrSTPProcessBO;
	}
	public void setArrSTPProcessBO(ArrayList<STPProcessBO> arrSTPProcessBO) {
		this.arrSTPProcessBO = arrSTPProcessBO;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getUploadDateTime() {
		return uploadDateTime;
	}
	public void setUploadDateTime(String uploadDateTime) {
		this.uploadDateTime = uploadDateTime;
	}
	public boolean isStpProcessFlag() {
		return stpProcessFlag;
	}
	public void setStpProcessFlag(boolean stpProcessFlag) {
		this.stpProcessFlag = stpProcessFlag;
	}
	
	
	
}
