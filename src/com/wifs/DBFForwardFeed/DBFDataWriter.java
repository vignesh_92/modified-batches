package com.wifs.DBFForwardFeed; 

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.linuxense.javadbf.DBFWriter;

public class DBFDataWriter {
	public static Logger logger = Logger.getLogger("DBFDataWriter.class");

	public void writeKarvyDBF(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//long startTime = System.currentTimeMillis();

		int intMainBORowSize		=	0;
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		FatcaDocBO objFatcaDocBO	=	new FatcaDocBO();
		ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>(); 
		
		intMainBORowSize		=	arryTransactionMainBO.size();

		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
			try{
				Object rowData[] = new Object[fieldSize];
				objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
				objTransactionBO		=	objTransactionMainBO.getTransactionBO();
				objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
				objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
				//logger.info("karvy file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
				rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
				rowData[1] = "ARN-69583";//objTransactionBO.getBrokerCode(); // Broker Code
				rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
				rowData[3] = objInvestorDetailsBO.getUserCode(); // User Code
				rowData[4] = objTransactionBO.getUserTransactionNo(); // Transaction No
				rowData[5] = objTransactionBO.getApplNo(); // Application No
				rowData[6] =  ConstansBO.NullCheck(objTransactionBO.getFolioNo()); // Folio No
				rowData[7] =  ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No()); // Cheque degit No
				rowData[8] = objTransactionBO.getTransactionType();
				rowData[9] = objTransactionBO.getSchemeCode();
				rowData[10] = objInvestorDetailsBO.getFirstName();
				rowData[11] = objInvestorDetailsBO.getJointName1();
				rowData[12] = objInvestorDetailsBO.getJointName2();
				rowData[13] = objInvestorDetailsBO.getAddress1();
				rowData[14] = objInvestorDetailsBO.getAddress2();
				rowData[15] = objInvestorDetailsBO.getAddress3();
				rowData[16] = objInvestorDetailsBO.getCity();
				rowData[17] = objInvestorDetailsBO.getPinCode();
				rowData[18] = objInvestorDetailsBO.getPhoneOffice();
				rowData[19] = objInvestorDetailsBO.getMobileNo();
				rowData[20] = objTransactionBO.getTransactionDate().getDateFirstView();
				rowData[21] = objTransactionBO.getTransactionTime();
				if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
				{
					rowData[22] = "";
					rowData[23] = "";
				}
				else
				{
					rowData[22] = String.valueOf(objTransactionBO.getUnits());
					rowData[23] = String.valueOf(objTransactionBO.getAmount());
				}
				rowData[24] = objAccountDetailsBO.getClos_Ac_Ch();
				rowData[25] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
				rowData[26] = objInvestorDetailsBO.getGuardianName();
				rowData[27] = objInvestorDetailsBO.getTaxNo();
				rowData[28] = objInvestorDetailsBO.getPhoneRes();
				rowData[29] = objInvestorDetailsBO.getFaxOff();
				rowData[30] = objInvestorDetailsBO.getFaxRes();
				rowData[31] = objInvestorDetailsBO.getEmail();
				rowData[32] = objAccountDetailsBO.getAccountNo();
				rowData[33] = objAccountDetailsBO.getAccountType();
				rowData[34] = objAccountDetailsBO.getBankName();
				rowData[35] = objAccountDetailsBO.getBranchName();
				rowData[36] = objAccountDetailsBO.getBankCity();
				rowData[37] = objAccountDetailsBO.getReinv_Tag();
				rowData[38] = objAccountDetailsBO.getHoldingNature();
				rowData[39] = objInvestorDetailsBO.getOccupationCode();
				rowData[40] = objAccountDetailsBO.getTaxStatus();
				rowData[41] = objTransactionBO.getPaymentId()+" "+objAccountDetailsBO.getRemarks();
				rowData[42] = objInvestorDetailsBO.getState();
				rowData[43] = objInvestorDetailsBO.getPanHolder2();
				rowData[44] = objInvestorDetailsBO.getPanHolder3();
				rowData[45] = objInvestorDetailsBO.getGuardianPanNo();
				rowData[46] = objInvestorDetailsBO.getLocationCode();
				rowData[47] = objAccountDetailsBO.getUINNo();
				rowData[48] = "";//objAccountDetailsBO.getForm6061();
				rowData[49] = "";//objAccountDetailsBO.getForm6061J1();
				rowData[50] = "";//objAccountDetailsBO.getForm6061J2();
				rowData[51] = objAccountDetailsBO.getPaymentMechanism();
				rowData[52] = objAccountDetailsBO.getRTGS_CD();
				rowData[53] = objAccountDetailsBO.getNEFT_CD();
				rowData[54] = objAccountDetailsBO.getMICR_CD();
				rowData[55] = objAccountDetailsBO.getDepBankName();
				rowData[56] = objAccountDetailsBO.getDepAccountNo();
				rowData[57] = objAccountDetailsBO.getDepDate().getDateFirstView();
				rowData[58] = objAccountDetailsBO.getDepReferenceNo();
				rowData[59] = objTransactionBO.getSubTransactionType();
				if(!objTransactionBO.getSipReferenceId().equals("0"))
					rowData[60] = objTransactionBO.getSipReferenceId();
				if(objTransactionBO.getSIP_Reg_Date()!=null)
					rowData[61] = objTransactionBO.getSIP_Reg_Date().getDateFirstView();
				rowData[62] = objAccountDetailsBO.getNomineeName();
				rowData[63] = objAccountDetailsBO.getNomineeRelation();
				rowData[64] = objInvestorDetailsBO.getKycFlag();
				// ---- New Fields ----
				rowData[65] = "N"; //POA_STAT
				rowData[66] = "W"; //MOD_TRXN
				rowData[67] = "Y"; //SIGN_VF
				rowData[68] = objInvestorDetailsBO.getInvestorId(); //CUST_ID
				rowData[69] = objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo(); // LOG_WT
				rowData[70] = ""; //LOG_PE
				rowData[71] = objTransactionBO.getDpId(); //DPID
				rowData[72] = objTransactionBO.getClientCode(); //ClientID
				if((objAccountDetailsBO.isNriInvestor())&&(objTransactionBO.getTransactionType().equals("P")))
					rowData[73] = objAccountDetailsBO.getAccountNo();//NRI_SOF
				else
					rowData[73] = ""; //NRI_SOF
				
				rowData[74] = objTransactionBO.getEuinId(); // EUIN
				rowData[75] = objTransactionBO.getEuinOpted(); // EUIN_OPT
				
				arrFatcaDocBO	=	objInvestorDetailsBO.getArrFatcaDocBO();
				
				for(int fat = 0; fat<arrFatcaDocBO.size(); fat++){
					objFatcaDocBO	=	arrFatcaDocBO.get(fat);
					if(objFatcaDocBO.getInvestorRelationship().equals("H")){
						rowData[76] = String.valueOf(objFatcaDocBO.getAnnualIncomeCode());//INCSLAB
						//rowData[77] = objTransactionBO.getNetworth_j1();NET_WOR_
						//rowData[78] = objTransactionBO.getNetworthDate_j1();NETWOR_DT
						//---rowData[0] = objFatcaDocBO.getPoliticallyExpPer();
					}else if(objFatcaDocBO.getInvestorRelationship().equals("F")){
						rowData[79] = String.valueOf(objFatcaDocBO.getAnnualIncomeCode());//INCSLAB_J1
						//rowData[80] = objTransactionBO.getNetworth_j2();NET_WOR_J1
						//rowData[81] = objTransactionBO.getNetworthDate_j2();NETDATE_J1
						rowData[82] = objFatcaDocBO.getPoliticallyExpPer();//PEP_J1
					}else if(objFatcaDocBO.getInvestorRelationship().equals("S")){
						rowData[83] = String.valueOf(objFatcaDocBO.getAnnualIncomeCode());//INCSLAB_J2
						//rowData[84] = objTransactionBO.getNetworth_j3();NET_WOR_J2
						//rowData[85] = objTransactionBO.getNetworthDate_j3();NETDATE_J2
						rowData[86] = objFatcaDocBO.getPoliticallyExpPer();//PEP_J2
					}else if(objFatcaDocBO.getInvestorRelationship().equals("T")){
						rowData[87] = String.valueOf(objFatcaDocBO.getAnnualIncomeCode());//INCSLAB_J3
						//rowData[88] = objTransactionBO.getNetworth();NET_WOR_J3
						//rowData[89] = objTransactionBO.getNetworthDate();NETDATE_J3
						rowData[90] = objFatcaDocBO.getPoliticallyExpPer();//PEP_J3
					}else if(objFatcaDocBO.getInvestorRelationship().equals("G")){
						rowData[91] =  String.valueOf(objFatcaDocBO.getAnnualIncomeCode());//INCSLAB_GR
						//rowData[92] = objTransactionBO.getNetworth_gr();NET_WOR_GR
						//rowData[93] = objTransactionBO.getNetworthDate_gr();NETDATE_GR
						rowData[94] = objFatcaDocBO.getPoliticallyExpPer();//PEP_GR
					}
				}
				
				rowData[95] = objTransactionBO.getForex_MCS();
				rowData[96] = objTransactionBO.getGAME_GABLE();
				rowData[97] = objTransactionBO.getLS_ML_PA();
				
				if(objTransactionBO.getSipStartDate() !=null){
					rowData[98] = objTransactionBO.getSipStartDate().getDateFirstView();
					rowData[100] = new Double(objTransactionBO.getSipDate());
				}
				if(objTransactionBO.getSipEndDate() != null)
					rowData[99] = objTransactionBO.getSipEndDate().getDateFirstView();
				
				
				rowData[101] = objTransactionBO.getSipAmount();
				rowData[102] = objInvestorDetailsBO.getNRIAddress1();
				rowData[103] = objInvestorDetailsBO.getNRIAddress2();
				rowData[104] = objInvestorDetailsBO.getNRIAddress3();
				rowData[105] = objInvestorDetailsBO.getNRICity();
				rowData[106] = objInvestorDetailsBO.getNRIState();
				rowData[107] = objInvestorDetailsBO.getNRICountry();
				rowData[108] = objInvestorDetailsBO.getNRIPinCode();
				rowData[109] = objAccountDetailsBO.getNomineeName2();
				rowData[110] = objAccountDetailsBO.getNomineeRelation2();
				rowData[111] = objAccountDetailsBO.getNomineeName3();
				rowData[112] = objAccountDetailsBO.getNomineeRelation3();
				rowData[113] = new Double(objAccountDetailsBO.getNomineePercent());
				rowData[114] = new Double(objAccountDetailsBO.getNomineePercent1());
				rowData[115] = new Double(objAccountDetailsBO.getNomineePercent2());
				//EMP_CODE
				//SUB_ARN
				rowData[118] = objInvestorDetailsBO.getFatcaFlag();//FATCA_FLAG
				/*Dummy1
				Dummy2
				Dummy3*/
				if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
					rowData[122] = new Double(objInvestorDetailsBO.getcKYCRefId1());//Dummy4
				
				if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
					rowData[123] = new Double(objInvestorDetailsBO.getcKYCRefId2());//Dummy5
				
				if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
					rowData[124] = new Double(objInvestorDetailsBO.getcKYCRefId3());//Dummy6
				
				if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
					rowData[125] = new Double(objInvestorDetailsBO.getcKYCRefIdG());//Dummy7
				
				if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
					rowData[126] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();//Dummy8
				
				if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
					rowData[127] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();//Dummy9
				
				if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
					rowData[128] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();//Dummy10
				//Dummy11
				writer.addRecord(rowData);
				
				//Based on the UserTransactionNo Update the ForwardFeed - date
				setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
				setPreparedFeedUpdate.addBatch();
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		setPreparedFeedUpdate.clearBatch();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : KarvyBatchUpdate" + totalTime);
	}
	
	/*
	 * ArrayList<TransactionMainBO> - Values
	 * 
	 * 
	 */ 
	public void writeCampsDBF(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		//logger.info(" Camps DBF Begin ");
		int intMainBORowSize		=	0;
		String remarksCAMS			=	"";
		String paidThrough			=	"";
	
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size(); 
		
	
		
		//logger.info(intMainBORowSize+" ======= ");
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[fieldSize];
				objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
				
				if(objTransactionMainBO!=null)
				{
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
					objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
					//logger.info("Camps file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
					
					/*
					 * for CAMS remarks
						PGID HDFC    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID TPSL    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID DD       |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID ECS     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						3262 NEFT   |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						PGID SW     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID Red     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
					 */
					paidThrough	=	objTransactionBO.getPaidThru();
					if(paidThrough.equalsIgnoreCase("NEFT"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"    |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if((paidThrough.equals(""))&&(objTransactionBO.getTransactionType().equals("SI")||objTransactionBO.getTransactionType().equals("SO")))
						remarksCAMS	=	"66 SW"+"        |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equals("")&&(objTransactionBO.getTransactionType().equals("R")))
						remarksCAMS	=	"77 Red"+"       |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equalsIgnoreCase("DD"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" DD      |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equalsIgnoreCase("ECS"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" ECS     |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else
						remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"    |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
	
					rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
					rowData[1] = "WEALTH";//objTransactionBO.getBrokerCode(); // Broker Code
					rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
					rowData[3] = objInvestorDetailsBO.getUserCode(); // User code
					rowData[4] = new Double(objTransactionBO.getUserTransactionNo()); // Transaction No
					rowData[5] = objTransactionBO.getApplNo(); // Application No
					rowData[6] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo()); // Folio No				
					rowData[7] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No()); // Cheque degit No
					rowData[8] = objTransactionBO.getTransactionType();
					rowData[9] = objTransactionBO.getSchemeCode();
					rowData[10] = objInvestorDetailsBO.getFirstName();
					rowData[11] = objInvestorDetailsBO.getJointName1();
					rowData[12] = objInvestorDetailsBO.getJointName2();
					rowData[13] = objInvestorDetailsBO.getAddress1();
					rowData[14] = objInvestorDetailsBO.getAddress2();
					rowData[15] = objInvestorDetailsBO.getAddress3();
					rowData[16] = objInvestorDetailsBO.getCity();
					rowData[17] = objInvestorDetailsBO.getPinCode();
					rowData[18] = objInvestorDetailsBO.getPhoneOffice();
					rowData[19] = (Date)objTransactionBO.getTransactionDate().getDateFirstView();
					rowData[20] = objTransactionBO.getTransactionTime();
					//logger.info("Data Writer : "+(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))+" == "+objAccountDetailsBO.getClos_Ac_Ch());
					if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
					{
						rowData[21] = new Double(0.0d);
						rowData[22] = new Double(0.0d);
					}
					else
					{
						rowData[21] = objTransactionBO.getUnits();
						rowData[22] = objTransactionBO.getAmount().doubleValue();
					}
					rowData[23] = objAccountDetailsBO.getClos_Ac_Ch();
					try{
					rowData[24] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
					}catch(Exception e){}
					rowData[25] = objInvestorDetailsBO.getGuardianName();
					rowData[26] = objInvestorDetailsBO.getTaxNo();
					rowData[27] = objInvestorDetailsBO.getPhoneRes();
					rowData[28] = objInvestorDetailsBO.getFaxOff();
					rowData[29] = objInvestorDetailsBO.getFaxRes();
					rowData[30] = objInvestorDetailsBO.getEmail();
					rowData[31] = objAccountDetailsBO.getAccountNo();
					rowData[32] = objAccountDetailsBO.getAccountType();
					rowData[33] = objAccountDetailsBO.getBankName();
					rowData[34] = objAccountDetailsBO.getBranchName();
					rowData[35] = objAccountDetailsBO.getBankCity();
					rowData[36] = objAccountDetailsBO.getReinv_Tag();
					rowData[37] = objAccountDetailsBO.getHoldingNature();
					rowData[38] = objInvestorDetailsBO.getOccupationCode();
					rowData[39] = objAccountDetailsBO.getTaxStatus();
					
					// ---- New Fields ----
					rowData[40] = remarksCAMS;
					
					if((objInvestorDetailsBO.getState().equals(""))&&(objInvestorDetailsBO.getNRIInvestor()==1))
						rowData[41] = "OV";
					else
						rowData[41] = objInvestorDetailsBO.getState();
					
					rowData[42] = objTransactionBO.getSubTransactionType();
					rowData[43] = objAccountDetailsBO.getPaymentMechanism();
					
					if(!objAccountDetailsBO.getMICR_CD().equals(""))
						rowData[44] = new Double(objAccountDetailsBO.getMICR_CD());
					rowData[45] = objAccountDetailsBO.getBank_Code();
					
					if(!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals(""))
						rowData[46] = new Double(objAccountDetailsBO.getALT_Folio());
					
					rowData[47] = objAccountDetailsBO.getALT_Broker();
					rowData[48] = objInvestorDetailsBO.getLocationCode();
					rowData[49] = objAccountDetailsBO.getPaymentMechanism();
					rowData[50] = objTransactionBO.getPricing();
					rowData[51] = objInvestorDetailsBO.getPanHolder2();
					rowData[52] = objInvestorDetailsBO.getPanHolder3();
					rowData[53] = objAccountDetailsBO.getNomineeName();
					rowData[54] = objAccountDetailsBO.getNomineeRelation(); 
					rowData[55] = objInvestorDetailsBO.getGuardianPanNo();
					rowData[56] = objAccountDetailsBO.getInstrmno();
					rowData[57] = objAccountDetailsBO.getUINNo();
					rowData[58] = objInvestorDetailsBO.getValid_Pan();
					rowData[59] = objInvestorDetailsBO.getGValid_Pan();
					rowData[60] = objInvestorDetailsBO.getJH1ValidPan();
					rowData[61]	= objInvestorDetailsBO.getJH2ValidPan();
					/*if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
						rowData[62] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();
					
					if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
						rowData[63] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();
					
					if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
						rowData[64] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();*/
					
					rowData[65] = "";
					rowData[66] = "";//objAccountDetailsBO.getSipReferenceNo();
					if(objTransactionBO.getSIP_Reg_Date()!=null)
						rowData[67] = objTransactionBO.getSIP_Reg_Date().getDateFirstView(); 
					rowData[68] = objAccountDetailsBO.getFirst_Hldr_Min();
					rowData[69] = objAccountDetailsBO.getJH1_Min();
					rowData[70] = objAccountDetailsBO.getJH2_Min();
					rowData[71] = objAccountDetailsBO.getGuardian_Min();
					rowData[72] = objAccountDetailsBO.getNEFT_CD();
					rowData[73] = objAccountDetailsBO.getRTGS_CD();
					rowData[74] = "E";//EMAIL_ACST
					rowData[75] = objInvestorDetailsBO.getMobileNo();
					rowData[76] = objTransactionBO.getDpId(); // Dip_Id
					// ---- New Fields ----
					rowData[77] = "N"; // POA_Type
					rowData[78] = "W"; // Trxn Mode
					rowData[79] = "Y"; // Trxn_sign_ver
					
					if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
					{
						rowData[80] = "NRI Ad: "+objInvestorDetailsBO.getNRIAddress1();
						rowData[81] = objInvestorDetailsBO.getNRIAddress2();
						rowData[82] = objInvestorDetailsBO.getNRIAddress3();
						
						if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
							rowData[83] = objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState();
						else
							rowData[83] = objInvestorDetailsBO.getNRICity();
						
						rowData[84] = "";//objInvestorDetailsBO.getNRIState();
						rowData[85] = objInvestorDetailsBO.getNRICountry();
						rowData[86] = objInvestorDetailsBO.getPinCode();
						rowData[88] = ""; // Nom2_Name
						rowData[89] = ""; // Nom2_Relationship
						//rowData[90] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
						rowData[91] = ""; // Nom3_Name
						rowData[92] = ""; // Nom3_Relationship
						//rowData[93] = new Double(0.0d); // Nom3_Applicable -  Number -  5,2
						rowData[94] = "Y"; // Check_Flag
						rowData[97] = "Y"; // FIRC_STA 
	
					}
					else
						rowData[94] = "N"; // Check_Flag
					
					if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
						rowData[87] = new Double(objAccountDetailsBO.getNomineePercent()); //Nom1_Applicable -	Number -  5,0
					
					if(objTransactionBO.getTransactionType().equals("P"))
					{
						rowData[95] = "N"; // TH_P_PYT ( Third Party Payment)
						rowData[96] = objInvestorDetailsBO.getKycFlag(); // KYC
					}
					else
					{
						rowData[95] = ""; // TH_P_PYT ( Third Party Payment)
						rowData[96] = objInvestorDetailsBO.getKycFlag(); // KYC
						rowData[97] = ""; // FIRC_STA 
					}
					
					
					if((objTransactionBO.getSipStartDate() !=null)&&(objTransactionBO.getSipEndDate() != null)){
						rowData[98] = new Double(objTransactionBO.getSipReferenceId());
						rowData[99] = new Double(objTransactionBO.getNoOfInstalment());
						rowData[100] = objTransactionBO.getSipFrequency();
						rowData[101] = objTransactionBO.getSipStartDate().getDateFirstView();
						rowData[102] = objTransactionBO.getSipEndDate().getDateFirstView();
						rowData[103] = new Double(objTransactionBO.getPaidInstallments());
					}
					
					rowData[113] = "N"; // First Holder PAN Exempt
					rowData[114] = "N"; // JH1 PAN Exempt
					rowData[115] = "N"; // JH2 PAN Exempt
					rowData[116] = "N"; // Guardian PAN Exempt
					
					rowData[125] = objTransactionBO.getEuinOpted();
					rowData[126] = objTransactionBO.getEuinId();
					rowData[127] = objAccountDetailsBO.getNomineeOpted();
					rowData[128] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
				
					rowData[129] = "Y";
					
					if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
						rowData[131] = new Double(objInvestorDetailsBO.getcKYCRefId1());//Dummy4
					
					if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
						rowData[132] = new Double(objInvestorDetailsBO.getcKYCRefId2());//Dummy5
					
					if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
						rowData[133] = new Double(objInvestorDetailsBO.getcKYCRefId3());//Dummy6
					
					if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
						rowData[134] = new Double(objInvestorDetailsBO.getcKYCRefIdG());//Dummy7
					
					if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
						rowData[135] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();//Dummy8
					
					if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
						rowData[136] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();//Dummy9
					
					if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
						rowData[137] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();//Dummy10
					
					
					
				/*	
				 * rowData[107] = objInvestorDetailsBO.getKycFlag2();//KYC_Type_Second Holder
					rowData[110] = objInvestorDetailsBO.getKycFlag3();//KYC_Type_Third Holder
					rowData[111] = objInvestorDetailsBO.getKycFlagG();//KYC_Type_Gaurdian
					
				 	rowData[136] = new Double(objInvestorDetailsBO.getcKYCRefId1());
					rowData[137] = new Double(objInvestorDetailsBO.getcKYCRefId2());
					rowData[138] = new Double(objInvestorDetailsBO.getcKYCRefId3());
					rowData[139] = new Double(objInvestorDetailsBO.getcKYCRefIdG());*/
					
					/*if(objInvestorDetailsBO.getKycFlag().equals("C"))
						rowData[140] = "N";
					else
						rowData[140] = "Y";
					
					if(objInvestorDetailsBO.getKycFlag2().equals("C"))
						rowData[141] = "N";
					else
						rowData[141] = "Y";
					
					if(objInvestorDetailsBO.getKycFlag3().equals("C"))
						rowData[142] = "N";
					else
						rowData[142] = "Y";
					
					if(objInvestorDetailsBO.getKycFlagG().equals("C"))
						rowData[143] = "N";
					else
						rowData[143] = "Y";*/
					
					//for (Object i : rowData) {
					//	pipe.append(DELIMETER).append(i);
					 //  DELIMETER = "|";
			//	}
					writer.addRecord(rowData);
					//Based on the UserTransactionNo Update the ForwardFeed - date
					setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
					setPreparedFeedUpdate.addBatch();
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : CAMSBatchUpdate = " + totalTime);
		setPreparedFeedUpdate.clearBatch();
	}
	
	public void writeCampsTXT(ArrayList<TransactionMainBO> arryTransactionMainBO, PreparedStatement setPreparedFeedUpdate, String fileName) throws Exception
	{
		 	int intMainBORowSize		=	0;
			String remarksCAMS			=	"";
			String paidThrough			=	"";
			String DELIMETER = "|";
			StringBuffer pipe = new StringBuffer();
		
			TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
			InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
			AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
			TransactionBO objTransactionBO			=	new TransactionBO();
			
			intMainBORowSize		=	arryTransactionMainBO.size(); 
		 	try {
		 		
		 		File file = new File(fileName);
		 	    OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file, true),"ASCII");
		        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		        
				//logger.info(intMainBORowSize+" ======= ");
				for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
				{
					try{
						String text	=	"";
						objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
						
						if(objTransactionMainBO!=null)
						{
							pipe = new StringBuffer();
							objTransactionBO		=	objTransactionMainBO.getTransactionBO();
							objTransactionBO		=	objTransactionMainBO.getTransactionBO();
							objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
							objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
							//logger.info("Camps file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
							
							paidThrough	=	objTransactionBO.getPaidThru();
							if(paidThrough.equalsIgnoreCase("NEFT"))
								remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if((paidThrough.equals(""))&&(objTransactionBO.getTransactionType().equals("SI")||objTransactionBO.getTransactionType().equals("SO")))
								remarksCAMS	=	"66 SW"+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if(paidThrough.equals("")&&(objTransactionBO.getTransactionType().equals("R")))
								remarksCAMS	=	"77 Red"+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if(paidThrough.equalsIgnoreCase("DD"))
								remarksCAMS	=	objTransactionBO.getPaymentId()+" DD-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if(paidThrough.equalsIgnoreCase("ECS"))
								remarksCAMS	=	objTransactionBO.getPaymentId()+" ECS-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else
								remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
		
							pipe.append(objTransactionBO.getAmcCode()).append(DELIMETER)//1
							.append("WEALTH").append(DELIMETER).//2
							append(objInvestorDetailsBO.getSubBrokerCode()).append(DELIMETER)//3
							.append(objInvestorDetailsBO.getUserCode()).append(DELIMETER).//4
							append(objTransactionBO.getUserTransactionNo()).append(DELIMETER)//5
							.append(objTransactionBO.getApplNo()).append(DELIMETER).//6
							append(ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo())).append(DELIMETER)//7
							.append(ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())).append(DELIMETER)//8
							.append(objTransactionBO.getTransactionType()).append(DELIMETER)//9
							.append(objTransactionBO.getSchemeCode()).append(DELIMETER);//10
							
							text	=	(objInvestorDetailsBO.getFirstName().length()>=70)? objInvestorDetailsBO.getFirstName().substring(0,70) : objInvestorDetailsBO.getFirstName();
							pipe.append(text).append(DELIMETER);//11
							
							text	=	(objInvestorDetailsBO.getJointName1().length()>=35)? objInvestorDetailsBO.getJointName1().substring(0,35) : objInvestorDetailsBO.getJointName1();
							pipe.append(text).append(DELIMETER);//12
							
							text	=	(objInvestorDetailsBO.getJointName2().length()>=35)? objInvestorDetailsBO.getJointName2().substring(0,35) : objInvestorDetailsBO.getJointName2();
							pipe.append(text).append(DELIMETER);//13
							
							text	=	(objInvestorDetailsBO.getAddress1().length()>=40)? objInvestorDetailsBO.getAddress1().substring(0,40) : objInvestorDetailsBO.getAddress1();
							pipe.append(text).append(DELIMETER);//14
							
							text	=	(objInvestorDetailsBO.getAddress2().length()>=40)? objInvestorDetailsBO.getAddress2().substring(0,40) : objInvestorDetailsBO.getAddress2();
							pipe.append(text).append(DELIMETER);//15
							
							text	=	(objInvestorDetailsBO.getAddress3().length()>=40)? objInvestorDetailsBO.getAddress3().substring(0,40) : objInvestorDetailsBO.getAddress3();
							pipe.append(text).append(DELIMETER);//16
							
							text	=	(objInvestorDetailsBO.getCity().length()>=35)? objInvestorDetailsBO.getCity().substring(0,35) : objInvestorDetailsBO.getCity();
							pipe.append(text).append(DELIMETER);//17
							
							text	=	(objInvestorDetailsBO.getPinCode().length()>=15)? objInvestorDetailsBO.getPinCode().substring(0,15) : objInvestorDetailsBO.getPinCode();
							pipe.append(text).append(DELIMETER);//18
							
							pipe.append(objInvestorDetailsBO.getPhoneOffice()).append(DELIMETER)//19
							.append(objTransactionBO.getTransactionDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//20
							.append(objTransactionBO.getTransactionTime()).append(DELIMETER);//21
							
							if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
							{
								pipe.append(new Double(0.00000000d)).append(DELIMETER)//22
								.append(new Double(0.00000000d)).append(DELIMETER);//23
							}
							else
							{
								pipe.append(ConstansBO.getEightDecimal(objTransactionBO.getUnits())).append(DELIMETER)//22
								.append(ConstansBO.getEightDecimal(objTransactionBO.getAmount().doubleValue())).append(DELIMETER);//23
							}
							pipe.append(objAccountDetailsBO.getClos_Ac_Ch()).append(DELIMETER);//24
							
							try{
								pipe.append(objInvestorDetailsBO.getDateOfBirth().getMonthFirstViewRequiredFormat()).append(DELIMETER);//25
							}catch(Exception e){
								pipe.append("").append(DELIMETER);//25
							}
							
							text	=	(objInvestorDetailsBO.getGuardianName().length()>=120)? objInvestorDetailsBO.getGuardianName().substring(0,120) : objInvestorDetailsBO.getGuardianName();
							pipe.append(text).append(DELIMETER);//26
							
							pipe.append(objInvestorDetailsBO.getTaxNo()).append(DELIMETER)//27
							.append(objInvestorDetailsBO.getPhoneRes()).append(DELIMETER)//28
							.append(objInvestorDetailsBO.getFaxOff()).append(DELIMETER)//29
							.append(objInvestorDetailsBO.getFaxRes()).append(DELIMETER);//30
							
							text	=	(objInvestorDetailsBO.getEmail().length()>=128)? objInvestorDetailsBO.getEmail().substring(0,128) : objInvestorDetailsBO.getEmail();
							pipe.append(text).append(DELIMETER);//31
							
							pipe.append( objAccountDetailsBO.getAccountNo()).append(DELIMETER)//32
							.append(objAccountDetailsBO.getAccountType()).append(DELIMETER);//33
							
							text	=	(objAccountDetailsBO.getBankName().length()>=40)? objAccountDetailsBO.getBankName().substring(0,40) : objAccountDetailsBO.getBankName();
							pipe.append(text).append(DELIMETER);//34
							
							text	=	(objAccountDetailsBO.getBranchName().length()>=40)? objAccountDetailsBO.getBranchName().substring(0,40) : objAccountDetailsBO.getBranchName();
							pipe.append(text).append(DELIMETER);//35
							
							text	=	(objAccountDetailsBO.getBankCity().length()>=35)? objAccountDetailsBO.getBankCity().substring(0,35) : objAccountDetailsBO.getBankCity();
							pipe.append(text).append(DELIMETER);//36
							
							pipe.append(objAccountDetailsBO.getReinv_Tag()).append(DELIMETER)//37
							.append(objAccountDetailsBO.getHoldingNature()).append(DELIMETER)//38
							.append(objInvestorDetailsBO.getOccupationCode()).append(DELIMETER)//39
							.append(objAccountDetailsBO.getTaxStatus()).append(DELIMETER);//40
							
							text	=	(remarksCAMS.length()>=254)? remarksCAMS.substring(0,254) : remarksCAMS;
							pipe.append(text).append(DELIMETER);//41
							
							if((objInvestorDetailsBO.getState().equals(""))&&(objInvestorDetailsBO.getNRIInvestor()==1))
								pipe.append("OV").append(DELIMETER);//42
							else
								pipe.append(objInvestorDetailsBO.getState()).append(DELIMETER);//42
							
							pipe.append(objTransactionBO.getSubTransactionType()).append(DELIMETER)//43
							.append(objAccountDetailsBO.getPaymentMechanism()).append(DELIMETER);//44
							
							if(!objAccountDetailsBO.getMICR_CD().equals(""))
								pipe.append(objAccountDetailsBO.getMICR_CD()).append(DELIMETER);//45
							else
								pipe.append("").append(DELIMETER);//45
							
							pipe.append(objAccountDetailsBO.getBank_Code()).append(DELIMETER);//46
							
							if(!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals(""))
								pipe.append(objAccountDetailsBO.getALT_Folio()).append(DELIMETER);//47
							else
								pipe.append("").append(DELIMETER);//47
							
							pipe.append(objAccountDetailsBO.getALT_Broker()).append(DELIMETER);//48
							
							text	=	(objInvestorDetailsBO.getLocationCode().length()>=10)? objInvestorDetailsBO.getLocationCode().substring(0,10) : objInvestorDetailsBO.getLocationCode();
							pipe.append(text).append(DELIMETER);//49
							
							pipe.append(objAccountDetailsBO.getPaymentMechanism()).append(DELIMETER)//50
							.append(objTransactionBO.getPricing()).append(DELIMETER)//51
							.append(objInvestorDetailsBO.getPanHolder2()).append(DELIMETER)//52
							.append(objInvestorDetailsBO.getPanHolder3()).append(DELIMETER);//53
							
							text	=	(objAccountDetailsBO.getNomineeName().length()>=40)? objAccountDetailsBO.getNomineeName().substring(0,40) : objAccountDetailsBO.getNomineeName();
							pipe.append(text).append(DELIMETER);//54
							
							text	=	(objAccountDetailsBO.getNomineeRelation().length()>=40)? objAccountDetailsBO.getNomineeRelation().substring(0,40) : objAccountDetailsBO.getNomineeRelation();
							pipe.append(text).append(DELIMETER);//55
							
							pipe.append(objInvestorDetailsBO.getGuardianPanNo()).append(DELIMETER)//56
							.append(objAccountDetailsBO.getInstrmno()).append(DELIMETER)//57
							.append(objAccountDetailsBO.getUINNo()).append(DELIMETER)//58
							.append(objInvestorDetailsBO.getValid_Pan()).append(DELIMETER)//59
							.append(objInvestorDetailsBO.getGValid_Pan()).append(DELIMETER)//60
							.append(objInvestorDetailsBO.getJH1ValidPan()).append(DELIMETER)//61
							.append(objInvestorDetailsBO.getJH2ValidPan()).append(DELIMETER)//62
							//.append(objAccountDetailsBO.getForm6061())
							.append(DELIMETER)//63
							//.append(objAccountDetailsBO.getForm6061J1())
							.append(DELIMETER)//64
							//.append(objAccountDetailsBO.getForm6061J2())
							.append(DELIMETER)//65
							//.append(objAccountDetailsBO.getGF6061RE())
							.append(DELIMETER)//66
							.append("").append(DELIMETER);//67
					
							try{
								if(objTransactionBO.getSIP_Reg_Date() !=null)
									pipe.append(objTransactionBO.getSIP_Reg_Date().getMonthFirstViewRequiredFormat()).append(DELIMETER); //68
								else
									pipe.append("").append(DELIMETER);//68
							}catch(Exception e){
								pipe.append("").append(DELIMETER);//68
							}
							pipe.append(objAccountDetailsBO.getFirst_Hldr_Min()).append(DELIMETER)//69
							.append(objAccountDetailsBO.getJH1_Min()).append(DELIMETER)//70
							.append(objAccountDetailsBO.getJH2_Min()).append(DELIMETER)//71
							.append(objAccountDetailsBO.getGuardian_Min()).append(DELIMETER)//72
							.append(objAccountDetailsBO.getNEFT_CD()).append(DELIMETER)//73
							.append(objAccountDetailsBO.getRTGS_CD()).append(DELIMETER)//74
							.append("E").append(DELIMETER)//75
							.append(objInvestorDetailsBO.getMobileNo()).append(DELIMETER)//76
							.append(objTransactionBO.getDpId()).append(DELIMETER)//77
							.append("N").append(DELIMETER)//78
							.append("W").append(DELIMETER)//79
							.append("Y").append(DELIMETER);//80
							
							if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
							{
								text	=	(objInvestorDetailsBO.getNRIAddress1().length()>=40)? objInvestorDetailsBO.getNRIAddress1().substring(0,40) : objInvestorDetailsBO.getNRIAddress1();
								pipe.append("NRI Ad: "+text).append(DELIMETER);//81
								
								text	=	(objInvestorDetailsBO.getNRIAddress2().length()>=40)? objInvestorDetailsBO.getNRIAddress2().substring(0,40) : objInvestorDetailsBO.getNRIAddress2();
								pipe.append(text).append(DELIMETER);//82
								
								text	=	(objInvestorDetailsBO.getNRIAddress3().length()>=40)? objInvestorDetailsBO.getNRIAddress3().substring(0,40) : objInvestorDetailsBO.getNRIAddress3();
								pipe.append(text).append(DELIMETER);//83
								
								if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
									text	=	((objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()).length()>=35)? (objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()).substring(0,35) : (objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState());
								else
									text	=	((objInvestorDetailsBO.getNRICity()).length()>=35)? (objInvestorDetailsBO.getNRICity()).substring(0,35) : (objInvestorDetailsBO.getNRICity());
										
								pipe.append(text.trim()).append(DELIMETER)//84
								.append(DELIMETER);//85
								
								text	=	(objInvestorDetailsBO.getNRICountry().length()>=35)? objInvestorDetailsBO.getNRICountry().substring(0,35) : objInvestorDetailsBO.getNRICountry();
								pipe.append(text.trim()).append(DELIMETER);//86
								
								pipe.append(objInvestorDetailsBO.getPinCode()).append(DELIMETER);//87
								
								if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
									pipe.append(objAccountDetailsBO.getNomineePercent()).append(DELIMETER);//88
								else 
									pipe.append("").append(DELIMETER);//88
								
								pipe.append("").append(DELIMETER)//89
								.append("").append(DELIMETER)//90
								.append("").append(DELIMETER)//91
								.append("").append(DELIMETER)//92
								.append("").append(DELIMETER)//93
								.append("").append(DELIMETER)//94
								.append("Y").append(DELIMETER);//95
								if(objTransactionBO.getTransactionType().equals("P"))
							    {
									pipe.append("N").append(DELIMETER)//96
									.append(objInvestorDetailsBO.getKycFlag()).append(DELIMETER);//97
								
							    }
								else
								{
									pipe.append("").append(DELIMETER)//96
									.append(objInvestorDetailsBO.getKycFlag()).append(DELIMETER);//97
							    }
								pipe.append("Y").append(DELIMETER); //98
							}
							else
							{
								pipe.append("").append(DELIMETER)//81
								.append("").append(DELIMETER)//82
								.append("").append(DELIMETER)//83
								.append("").append(DELIMETER)//84
								.append("").append(DELIMETER)//85
								.append("").append(DELIMETER)//86
								.append("").append(DELIMETER);//87
								if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
									pipe.append(objAccountDetailsBO.getNomineePercent()).append(DELIMETER);//88
								else
									pipe.append("").append(DELIMETER);//88
								
								pipe.append("").append(DELIMETER)//89
									.append("").append(DELIMETER)//90
									.append("").append(DELIMETER)//91
									.append("").append(DELIMETER)//92
									.append("").append(DELIMETER)//93
									.append("").append(DELIMETER)//94
								    .append("N").append(DELIMETER);//95
								if(objTransactionBO.getTransactionType().equals("P"))
								{
									pipe.append("N").append(DELIMETER)//96
								  	.append("Y").append(DELIMETER);//97
							
								}
								else
								{
							    	 pipe.append("").append(DELIMETER)//96
							    	  	.append("Y").append(DELIMETER);//97
								
								}
								pipe.append("").append(DELIMETER);//98
							}
							
							//if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
								//pipe.append(objAccountDetailsBO.getNomineePercent()).append(DELIMETER);
							//else
								//pipe.append("").append(DELIMETER);
							
							/*if(objTransactionBO.getTransactionType().equals("P"))
							{
								pipe.append("N").append(DELIMETER)
								.append("Y").append(DELIMETER)
								.append("").append(DELIMETER);
							}
							else
							{
								pipe.append("").append(DELIMETER)
								.append("Y").append(DELIMETER)
								.append("").append(DELIMETER); 
							}*/

							try{
								if((objTransactionBO.getSipStartDate() !=null)&&(objTransactionBO.getSipEndDate() != null)){
									pipe.append(objTransactionBO.getSipReferenceId()).append(DELIMETER)//99
									.append(objTransactionBO.getNoOfInstalment()).append(DELIMETER)//100
									.append("OM").append(DELIMETER)//101
									.append(objTransactionBO.getSipStartDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//102
									.append(objTransactionBO.getSipEndDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//103
									.append(objTransactionBO.getPaidInstallments()).append(DELIMETER);//104
								}
								else
									pipe.append("").append(DELIMETER)//99
									.append("").append(DELIMETER)//100
									.append("").append(DELIMETER)//101
									.append("").append(DELIMETER)//102
									.append("").append(DELIMETER)//103
									.append("").append(DELIMETER);//104
							}catch(Exception e){
								pipe.append("").append(DELIMETER)//99
								.append("").append(DELIMETER)//100
								.append("").append(DELIMETER)//101
								.append("").append(DELIMETER)//102
								.append("").append(DELIMETER)//103
								.append("").append(DELIMETER);//104
							}
							
							pipe.append("").append(DELIMETER)//105
							.append("").append(DELIMETER)//106
							.append("").append(DELIMETER)//107
							.append("").append(DELIMETER)//108
							.append("").append(DELIMETER)//109
							.append("").append(DELIMETER)//110
							.append("").append(DELIMETER)//111
							.append("").append(DELIMETER)//112
							.append("").append(DELIMETER)//113
							.append("N").append(DELIMETER)//114
							.append("N").append(DELIMETER)//115
							.append("N").append(DELIMETER)//116
							.append("N").append(DELIMETER)//117
							.append("").append(DELIMETER)//118
							.append("").append(DELIMETER)//119
							.append("").append(DELIMETER)//120
							.append("").append(DELIMETER)//121
							.append("").append(DELIMETER)//122
							.append("").append(DELIMETER)//123
							.append("").append(DELIMETER)//124
							.append("").append(DELIMETER)//125
							.append(objTransactionBO.getEuinOpted()).append(DELIMETER)//126
							.append(objTransactionBO.getEuinId()).append(DELIMETER)//127
							.append(objAccountDetailsBO.getNomineeOpted()).append(DELIMETER)//128
							.append(objInvestorDetailsBO.getSubBrokerCode()).append(DELIMETER)//129
							.append("Y").append(DELIMETER) //BankMandat 130 -- Bala default value
							.append(DELIMETER); //TO_SCHEME 131
							
							if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId1()).append(DELIMETER);//FHLD_CKYC //132
							else
								pipe.append(DELIMETER);//Dummy4 // 132
							
							if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId2()).append(DELIMETER);//SHLD_CKYC //133
							else
								pipe.append(DELIMETER);//133
							
							if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId3()).append(DELIMETER);//THLD_CKYC // 134
							else
								pipe.append(DELIMETER);//134
								
							if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefIdG()).append(DELIMETER);//GURD_CKYC // 135
							else
								pipe.append(DELIMETER);//135
						
							if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
								pipe.append(objInvestorDetailsBO.getDateOfBirth2().getMonthFirstViewRequiredFormat()).append(DELIMETER);//136
							else
								pipe.append(DELIMETER); // 136
							
							if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
								pipe.append(objInvestorDetailsBO.getDateOfBirth3().getMonthFirstViewRequiredFormat()).append(DELIMETER);//137
							else
								pipe.append(DELIMETER); //137
							
							if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
								pipe.append(objInvestorDetailsBO.getDateOfBirthG().getMonthFirstViewRequiredFormat()).append(DELIMETER);//138
							else
								pipe.append(DELIMETER); //138
							
							
							pipe.append("\r\n");
							bufferedWriter.write(pipe.toString());
							//bufferedWriter.newLine();
	
							//Based on the UserTransactionNo Update the ForwardFeed - date
							setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
							setPreparedFeedUpdate.addBatch();
						}
					}catch(Exception e){
						logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
					}
				}
                                
				if(bufferedWriter != null) bufferedWriter.flush();
		        if(bufferedWriter != null) bufferedWriter.close();
		 		if(fileWriter != null) fileWriter.close();

		 		logger.info("File Created successfully : "+fileName);
                                
                            int insertedRows[] = setPreparedFeedUpdate.executeBatch();
                            logger.info("AMC:" + objTransactionBO.getAmcCode() + " Rows:" + insertedRows.length);
                            //long totalTime = System.currentTimeMillis() - startTime;
                            //logger.info("TIMER : CAMSBatchUpdate = " + totalTime);
                            setPreparedFeedUpdate.clearBatch();
                
		 	} catch (Exception e1) {
		 	  logger.info("Exp while file Creation : "+e1);
		 	}
			//int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
			//logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
			//setPreparedFeedUpdate.clearBatch();
	}
	/*
	 * ArrayList<TransactionMainBO> - Values
	 * 
	 * 
	 */ 
	public void writeSumdramDBF(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		int intMainBORowSize		=	0;
		String remarksCAMS			=	"";
		String paidThrough			=	"";
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size();
		
		//logger.info(intMainBORowSize+" ======= ");
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[fieldSize];
				objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
				
				if(objTransactionMainBO!=null)
				{
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
					objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
					//logger.info("Sundram file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
					
					/*
					 	for Sundram remarks
						PGID HDFC    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID TPSL    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID DD       |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID ECS     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						3262 NEFT   |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						PGID SW     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID Red     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
					 */
					paidThrough	=	objTransactionBO.getPaidThru();
					if(paidThrough.equalsIgnoreCase("NEFT"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"    |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if((paidThrough.equals(""))&&(objTransactionBO.getTransactionType().equals("SI")||objTransactionBO.getTransactionType().equals("SO")))
						remarksCAMS	=	"66 SW"+"        |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equals("")&&(objTransactionBO.getTransactionType().equals("R")))
						remarksCAMS	=	"77 Red"+"       |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equalsIgnoreCase("DD"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" DD      |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equalsIgnoreCase("ECS"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" ECS     |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else
						remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"    |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
	
					rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
					rowData[1] = "WEALTH";//objTransactionBO.getBrokerCode(); // Broker Code
					rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
					rowData[3] = objInvestorDetailsBO.getUserCode(); // User code
					rowData[4] = new Double(objTransactionBO.getUserTransactionNo()); // Transaction No
					rowData[5] = objTransactionBO.getApplNo(); // Application No
					rowData[6] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo()); // Folio No				
					rowData[7] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No()); // Cheque degit No
					rowData[8] = objTransactionBO.getTransactionType();
					rowData[9] = objTransactionBO.getSchemeCode();
					rowData[10] = objInvestorDetailsBO.getFirstName();
					rowData[11] = objInvestorDetailsBO.getJointName1();
					rowData[12] = objInvestorDetailsBO.getJointName2();
					rowData[13] = objInvestorDetailsBO.getAddress1();
					rowData[14] = objInvestorDetailsBO.getAddress2();
					rowData[15] = objInvestorDetailsBO.getAddress3();
					rowData[16] = objInvestorDetailsBO.getCity();
					rowData[17] = objInvestorDetailsBO.getPinCode();
					rowData[18] = objInvestorDetailsBO.getPhoneOffice();
					rowData[19] = (Date)objTransactionBO.getTransactionDate().getDateFirstView();
					rowData[20] = objTransactionBO.getTransactionTime();
					//logger.info("Data Writer : "+(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))+" == "+objAccountDetailsBO.getClos_Ac_Ch());
					if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
					{
						rowData[21] = new Double(0.0d);
						rowData[22] = new Double(0.0d);
					}
					else
					{
						rowData[21] = objTransactionBO.getUnits();
						rowData[22] = objTransactionBO.getAmount().doubleValue();
					}
					rowData[23] = objAccountDetailsBO.getClos_Ac_Ch();
					rowData[24] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
					rowData[25] = objInvestorDetailsBO.getGuardianName();
					rowData[26] = objInvestorDetailsBO.getTaxNo();
					rowData[27] = objInvestorDetailsBO.getPhoneRes();
					rowData[28] = objInvestorDetailsBO.getFaxOff();
					rowData[29] = objInvestorDetailsBO.getFaxRes();
					rowData[30] = objInvestorDetailsBO.getEmail();
					rowData[31] = objAccountDetailsBO.getAccountNo();
					rowData[32] = objAccountDetailsBO.getAccountType();
					rowData[33] = objAccountDetailsBO.getBankName();
					rowData[34] = objAccountDetailsBO.getBranchName();
					rowData[35] = objAccountDetailsBO.getBankCity();
					rowData[36] = objAccountDetailsBO.getReinv_Tag();
					rowData[37] = objAccountDetailsBO.getHoldingNature();
					rowData[38] = objInvestorDetailsBO.getOccupationCode();
					rowData[39] = objAccountDetailsBO.getTaxStatus();
					
					// ---- New Fields ----
					rowData[40] = remarksCAMS;
					
					rowData[41] = objInvestorDetailsBO.getState();
					rowData[42] = objTransactionBO.getSubTransactionType();
					rowData[43] = objAccountDetailsBO.getPaymentMechanism();
					
					if(!objAccountDetailsBO.getMICR_CD().equals(""))
						rowData[44] = new Double(objAccountDetailsBO.getMICR_CD());
					rowData[45] = objAccountDetailsBO.getBank_Code();
					
					if(!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals(""))
						rowData[46] = new Double(objAccountDetailsBO.getALT_Folio());
					
					rowData[47] = objAccountDetailsBO.getALT_Broker();
					rowData[48] = objInvestorDetailsBO.getLocationCode();
					rowData[49] = objAccountDetailsBO.getPaymentMechanism();
					rowData[50] = objTransactionBO.getPricing();
					rowData[51] = objInvestorDetailsBO.getPanHolder2();
					rowData[52] = objInvestorDetailsBO.getPanHolder3();
					rowData[53] = objAccountDetailsBO.getNomineeName();
					rowData[54] = objAccountDetailsBO.getNomineeRelation(); 
					rowData[55] = objInvestorDetailsBO.getGuardianPanNo();
					rowData[56] = objAccountDetailsBO.getInstrmno();
					rowData[57] = objAccountDetailsBO.getUINNo();
					rowData[58] = objInvestorDetailsBO.getValid_Pan();
					rowData[59] = objInvestorDetailsBO.getGValid_Pan();
					rowData[60] = objInvestorDetailsBO.getJH1ValidPan();
					rowData[61]	= objInvestorDetailsBO.getJH2ValidPan();
					
					if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
						rowData[62] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();
					
					if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
						rowData[63] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();
					
					if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
						rowData[64] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();
					
					rowData[65] = "";//objAccountDetailsBO.getGF6061RE();
					rowData[66] = "";//objAccountDetailsBO.getSipReferenceNo();
					if(objTransactionBO.getSIP_Reg_Date()!=null)
						rowData[67] = objTransactionBO.getSIP_Reg_Date().getDateFirstView(); 
					rowData[68] = objAccountDetailsBO.getFirst_Hldr_Min();
					rowData[69] = objAccountDetailsBO.getJH1_Min();
					rowData[70] = objAccountDetailsBO.getJH2_Min();
					rowData[71] = objAccountDetailsBO.getGuardian_Min();
					rowData[72] = objAccountDetailsBO.getNEFT_CD();
					rowData[73] = objAccountDetailsBO.getRTGS_CD();
					rowData[74] = "E";//EMAIL_ACST
					rowData[75] = objInvestorDetailsBO.getMobileNo();
					rowData[76] = objTransactionBO.getDpId(); // Dip_Id
					// ---- New Fields ----
					rowData[77] = "N"; // POA_Type
					rowData[78] = "W"; // Trxn Mode
					rowData[79] = "Y"; // Trxn_sign_ver
					
					if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
					{
						rowData[80] = "NRI Ad: "+objInvestorDetailsBO.getNRIAddress1();
						rowData[81] = objInvestorDetailsBO.getNRIAddress2();
						rowData[82] = objInvestorDetailsBO.getNRIAddress3();
						
						if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
							rowData[83] = objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState();
						else
							rowData[83] = objInvestorDetailsBO.getNRICity();
						
						rowData[84] = "";//objInvestorDetailsBO.getNRIState();
						rowData[85] = objInvestorDetailsBO.getNRICountry();
						rowData[86] = objInvestorDetailsBO.getPinCode();
						rowData[88] = ""; // Nom2_Name
						rowData[89] = ""; // Nom2_Relationship
						//rowData[90] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
						rowData[91] = ""; // Nom3_Name
						rowData[92] = ""; // Nom3_Relationship
						//rowData[93] = new Double(0.0d); // Nom3_Applicable -  Number -  5,2
						rowData[94] = "Y"; // Check_Flag
						rowData[97] = "Y"; // FIRC_STA 
	
					}
					else
						rowData[94] = "N"; // Check_Flag
					
					if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
						rowData[87] = new Double(objAccountDetailsBO.getNomineePercent()); //Nom1_Applicable -	Number -  5,0
					
					if(objTransactionBO.getTransactionType().equals("P"))
					{
						rowData[95] = "N"; // TH_P_PYT ( Third Party Payment)
						rowData[96] = objInvestorDetailsBO.getKycFlag(); // KYC
					}
					else
					{
						rowData[95] = ""; // TH_P_PYT ( Third Party Payment)
						rowData[96] =objInvestorDetailsBO.getKycFlag(); // KYC
						rowData[97] = ""; // FIRC_STA 
					}
					
					if((objTransactionBO.getSipStartDate() !=null)&&(objTransactionBO.getSipEndDate() != null)){
						rowData[98] = new Double(objTransactionBO.getSipReferenceId());
						rowData[99] = new Double(objTransactionBO.getNoOfInstalment());
						rowData[100] = objTransactionBO.getSipFrequency();
						rowData[101] = objTransactionBO.getSipStartDate().getDateFirstView();
						rowData[102] = objTransactionBO.getSipEndDate().getDateFirstView();
						rowData[103] = new Double(objTransactionBO.getPaidInstallments());
					}
					
					rowData[107] =  objInvestorDetailsBO.getKycFlag2();
					rowData[110] =  objInvestorDetailsBO.getKycFlag3();
					rowData[111] =  objInvestorDetailsBO.getKycFlagG();
					
					rowData[113] = "N"; // First Holder PAN Exempt
					rowData[114] = "N"; // JH1 PAN Exempt
					rowData[115] = "N"; // JH2 PAN Exempt
					rowData[116] = "N"; // Guardian PAN Exempt
					
					rowData[125] = objTransactionBO.getEuinOpted();
					rowData[126] = objTransactionBO.getEuinId();
					//Nomination not opted
					rowData[128] = "WEALTH";
					
					if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
						rowData[136] = new Double(objInvestorDetailsBO.getcKYCRefId1());
					
					if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
						rowData[137] = new Double(objInvestorDetailsBO.getcKYCRefId2());
					
					if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
						rowData[138] = new Double(objInvestorDetailsBO.getcKYCRefId3());
					
					if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
						rowData[139] = new Double(objInvestorDetailsBO.getcKYCRefIdG());

					if(objInvestorDetailsBO.getKycFlag().equals("C"))
						rowData[140] = "N";
					else
						rowData[140] = "Y";
					
					if(objInvestorDetailsBO.getKycFlag2().equals("C"))
						rowData[141] = "N";
					else
						rowData[141] = "Y";
					
					if(objInvestorDetailsBO.getKycFlag3().equals("C"))
						rowData[142] = "N";
					else
						rowData[142] = "Y";
					
					if(objInvestorDetailsBO.getKycFlagG().equals("C"))
						rowData[143] = "N";
					else
						rowData[143] = "Y";
					
					writer.addRecord(rowData);
					
					//Based on the UserTransactionNo Update the ForwardFeed - date
					setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
					setPreparedFeedUpdate.addBatch();
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : SudramBatchUpdate = " + totalTime);
		setPreparedFeedUpdate.clearBatch();
	}
/*	public void writeFranklinDBF(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//logger.info(" Franklin DBF Begin ");
		int intMainBORowSize		=	0;
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size();
		
		//logger.info(intMainBORowSize+" ======= ");
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			
			Object rowData[] = new Object[fieldSize];
			objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
			
			if(objTransactionMainBO!=null)
			{
				objTransactionBO		=	objTransactionMainBO.getTransactionBO();
				objTransactionBO		=	objTransactionMainBO.getTransactionBO();
				objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
				objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
				//logger.info("Franklin file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
				rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
				rowData[1] = "WEALTH"; // Broker Code
				rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
				rowData[3] = objInvestorDetailsBO.getUserCode(); // User code
				rowData[4] = objTransactionBO.getUserTransactionNo(); // Transaction No
				rowData[5] = objTransactionBO.getApplNo(); // Application No
				rowData[6] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo()); // Folio No				
				rowData[7] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No()); // Cheque degit No
				rowData[8] = objTransactionBO.getTransactionType();
				rowData[9] = objTransactionBO.getSchemeCode();
				rowData[10] = objInvestorDetailsBO.getFirstName();
				rowData[11] = objInvestorDetailsBO.getJointName1();
				rowData[12] = objInvestorDetailsBO.getJointName2();
				rowData[13] = objInvestorDetailsBO.getAddress1();
				rowData[14] = objInvestorDetailsBO.getAddress2();
				rowData[15] = objInvestorDetailsBO.getAddress3();
				rowData[16] = objInvestorDetailsBO.getCity();
				rowData[17] = objInvestorDetailsBO.getPinCode();
				rowData[18] = objInvestorDetailsBO.getPhoneOffice();
				rowData[19] = (Date)objTransactionBO.getTransactionDate().getDateFirstView();
				rowData[20] = objTransactionBO.getTransactionTime();
				if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
				{
					rowData[21] = new Double(0.0d);
					rowData[22] = new Double(0.0d);
				}
				else
				{
					rowData[21] = objTransactionBO.getUnits();
					rowData[22] = objTransactionBO.getAmount().doubleValue();
				}
				rowData[23] = objAccountDetailsBO.getClos_Ac_Ch();
				rowData[24] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
				rowData[25] = objInvestorDetailsBO.getGuardianName();
				rowData[26] = objInvestorDetailsBO.getTaxNo();
				rowData[27] = objInvestorDetailsBO.getPhoneRes();
				rowData[28] = objInvestorDetailsBO.getFaxOff();
				rowData[29] = objInvestorDetailsBO.getFaxRes();
				rowData[30] = objInvestorDetailsBO.getEmail();
				rowData[31] = objAccountDetailsBO.getAccountNo();
				rowData[32] = objAccountDetailsBO.getAccountType();
				rowData[33] = objAccountDetailsBO.getBankName();
				rowData[34] = objAccountDetailsBO.getBranchName();
				rowData[35] = objAccountDetailsBO.getBankCity();
				rowData[36] = objAccountDetailsBO.getReinv_Tag();
				rowData[37] = objAccountDetailsBO.getHoldingNature();
				rowData[38] = objInvestorDetailsBO.getOccupationCode();
				rowData[39] = objAccountDetailsBO.getTaxStatus();
				rowData[40] = objTransactionBO.getPaymentId()+" "+objAccountDetailsBO.getRemarks();
				rowData[41] = objInvestorDetailsBO.getState();
				rowData[42] = objAccountDetailsBO.getSub_TRXN_T(); 
				rowData[43] = "DC"; // DIV_PY_MEC - default value
				if(!objAccountDetailsBO.getMICR_CD().equals(""))
					rowData[44] = new Double(objAccountDetailsBO.getMICR_CD());
				rowData[45] = objAccountDetailsBO.getBank_Code();
				rowData[46] = objAccountDetailsBO.getALT_Folio();
				rowData[47] = objAccountDetailsBO.getALT_Broker();
				rowData[48] = objInvestorDetailsBO.getLocationCode();
				rowData[49] = "DC"; // RED_PY_MEC - default value
				rowData[50] = objTransactionBO.getPricing();
				rowData[51] = objInvestorDetailsBO.getPanHolder2();
				rowData[52] = objInvestorDetailsBO.getPanHolder3();
				rowData[53] = objAccountDetailsBO.getNomineeName();
				rowData[54] = objAccountDetailsBO.getNomineeRelation();
				rowData[55] = objInvestorDetailsBO.getGuardianPanNo();
				rowData[56] = objAccountDetailsBO.getInstrmno();
				rowData[57] = objAccountDetailsBO.getUINNo();
				rowData[58] = objInvestorDetailsBO.getValid_Pan();
				rowData[59] = objInvestorDetailsBO.getGValid_Pan();
				rowData[60] = objInvestorDetailsBO.getJH1ValidPan();
				rowData[61]	= objInvestorDetailsBO.getJH2ValidPan();
				rowData[62] = "";//FORM6061RE
				rowData[63] = "";//JH1F6061RE
				rowData[64] = "";//JH2F6061RE
				rowData[65] = "";//GF6061RE
				if(objAccountDetailsBO.getSIP_Reg_Date()!=null)
					rowData[66] = objAccountDetailsBO.getSIP_Reg_Date().getDateFirstView();
				rowData[67] = objAccountDetailsBO.getFirst_Hldr_Min();
				rowData[68] = objAccountDetailsBO.getJH1_Min();
				rowData[69] = objAccountDetailsBO.getJH2_Min();
				rowData[70] = objAccountDetailsBO.getGuardian_Min();
				rowData[71] = objAccountDetailsBO.getNEFT_CD();
				rowData[72] = objAccountDetailsBO.getRTGS_CD();
				// ---- New Fields ----
				rowData[73] = objInvestorDetailsBO.getKycFlag(); // KYC Flag
				rowData[74] = "N"; // POA State
				rowData[75] = "W"; // Mode Trxn.
				rowData[76] = "N"; // Sign
				rowData[77] = objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo(); // Log
				
				
				// ---- New Fields ----
				if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
				{
					rowData[78] = "NRI Ad: "+objInvestorDetailsBO.getNRIAddress1();
					rowData[79] = objInvestorDetailsBO.getNRIAddress2();
					rowData[80] = objInvestorDetailsBO.getNRIAddress3();
					rowData[81] = objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState();
					rowData[82] = "";//objInvestorDetailsBO.getNRIState();
					rowData[83] = objInvestorDetailsBO.getNRICountry();
					rowData[84] = objInvestorDetailsBO.getPinCode();
					
					//rowData[85] = new Double(0.0d); // Nom1_Applicable -  Number -  5,2
					rowData[86] = ""; // Nom2_Name
					rowData[87] = ""; // Nom2_Relationship
					
					//rowData[88] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
					rowData[89] = ""; // Nom3_Name
					rowData[90] = ""; // Nom3_Relationship
					//rowData[91] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
					rowData[92] = "Y"; // Check_Flag
					rowData[95] = "Y"; // FIRC_STA 

				}
				else
					rowData[92] = "N"; // Check_Flag
				
				if(objTransactionBO.getTransactionType().equals("P"))
				{
					rowData[93] = "N"; // TH_P_PYT ( Third Party Payment)
					rowData[94] = "Y"; // KYC
				}
				else
				{
					rowData[93] = ""; // TH_P_PYT ( Third Party Payment)
					rowData[94] = "Y"; // KYC
					rowData[95] = ""; // FIRC_STA 
				}
				
				if(!objTransactionBO.getPaidInstallments().equals("")){
					rowData[96] = new Double(objTransactionBO.getPaidInstallments());
					rowData[97] = new Double(objTransactionBO.getNoOfInstalment());
				}
				
				rowData[99] = objTransactionBO.getEuinOpted(); // EUIN_OPT
				rowData[100] = objTransactionBO.getEuinId(); // EUIN
				
				
				writer.addRecord(rowData);
				
				//Based on the UserTransactionNo Update the ForwardFeed - date
				setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
				setPreparedFeedUpdate.addBatch();
			}
		}
		int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		setPreparedFeedUpdate.clearBatch();
	}*/
	public void writeFranklinDBF(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		//logger.info(" Franklin DBF Begin ");
		int intMainBORowSize		=	0;
		String webLog	=	"";
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size();
		
		//logger.info(intMainBORowSize+" ======= ");
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[fieldSize];
				objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
				
				if(objTransactionMainBO!=null)
				{
					try{
						objTransactionBO		=	objTransactionMainBO.getTransactionBO();
						objTransactionBO		=	objTransactionMainBO.getTransactionBO();
						objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
						objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
						//logger.info("Franklin file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
						rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
						rowData[1] = "WEALTH"; // Broker Code
						rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
						rowData[3] = objInvestorDetailsBO.getUserCode(); // User code
						rowData[4] = objTransactionBO.getUserTransactionNo(); // Transaction No
						rowData[5] = objTransactionBO.getApplNo(); // Application No
						rowData[6] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo()); // Folio No				
						rowData[7] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No()); // Cheque degit No
						rowData[8] = objTransactionBO.getTransactionType();
						rowData[9] = objTransactionBO.getSchemeCode();
						rowData[10] = objInvestorDetailsBO.getFirstName();
						rowData[11] = objInvestorDetailsBO.getJointName1();
						rowData[12] = objInvestorDetailsBO.getJointName2();
						rowData[13] = objInvestorDetailsBO.getAddress1();
						rowData[14] = objInvestorDetailsBO.getAddress2();
						rowData[15] = objInvestorDetailsBO.getAddress3();
						rowData[16] = objInvestorDetailsBO.getCity();
						rowData[17] = objInvestorDetailsBO.getPinCode();
						rowData[18] = objInvestorDetailsBO.getPhoneOffice();
						rowData[19] = (Date)objTransactionBO.getTransactionDate().getDateFirstView();
						rowData[20] = objTransactionBO.getTransactionTime();
						if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
						{
							rowData[21] = new Double(0.0d);
							rowData[22] = new Double(0.0d);
						}
						else
						{
							rowData[21] = objTransactionBO.getUnits();
							rowData[22] = objTransactionBO.getAmount().doubleValue();
						}
						rowData[23] = objAccountDetailsBO.getClos_Ac_Ch();
						rowData[24] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
						rowData[25] = objInvestorDetailsBO.getGuardianName();
						rowData[26] = objInvestorDetailsBO.getTaxNo();
						rowData[27] = objInvestorDetailsBO.getPhoneRes();
						rowData[28] = objInvestorDetailsBO.getFaxOff();
						rowData[29] = objInvestorDetailsBO.getFaxRes();
						rowData[30] = objInvestorDetailsBO.getEmail();
						rowData[31] = objAccountDetailsBO.getAccountNo();
						rowData[32] = objAccountDetailsBO.getAccountType();
						rowData[33] = objAccountDetailsBO.getBankName();
						rowData[34] = objAccountDetailsBO.getBranchName();
						rowData[35] = objAccountDetailsBO.getBankCity();
						rowData[36] = objAccountDetailsBO.getReinv_Tag();
						rowData[37] = objAccountDetailsBO.getHoldingNature();
						rowData[38] = objInvestorDetailsBO.getOccupationCode();
						rowData[39] = objAccountDetailsBO.getTaxStatus();
						rowData[40] = objTransactionBO.getPaymentId()+" "+objAccountDetailsBO.getRemarks();
						rowData[41] = objInvestorDetailsBO.getState();
						rowData[42] = objTransactionBO.getSubTransactionType(); 
						rowData[43] = "DC"; // DIV_PY_MEC - default value
						if(!objAccountDetailsBO.getMICR_CD().equals(""))
							rowData[44] = new Double(objAccountDetailsBO.getMICR_CD());
						rowData[45] = objAccountDetailsBO.getBank_Code();
						rowData[46] = objAccountDetailsBO.getALT_Folio();
						rowData[47] = objAccountDetailsBO.getALT_Broker();
						rowData[48] = objInvestorDetailsBO.getLocationCode();
						rowData[49] = "DC"; // RED_PY_MEC - default value
						rowData[50] = objTransactionBO.getPricing();
						rowData[51] = objInvestorDetailsBO.getPanHolder2();
						rowData[52] = objInvestorDetailsBO.getPanHolder3();
						rowData[53] = objAccountDetailsBO.getNomineeName();
						rowData[54] = objAccountDetailsBO.getNomineeRelation();
						rowData[55] = objInvestorDetailsBO.getGuardianPanNo();
						rowData[56] = objAccountDetailsBO.getInstrmno();
						rowData[57] = objAccountDetailsBO.getUINNo();
						rowData[58] = objInvestorDetailsBO.getValid_Pan();
						rowData[59] = objInvestorDetailsBO.getGValid_Pan();
						rowData[60] = objInvestorDetailsBO.getJH1ValidPan();
						rowData[61]	= objInvestorDetailsBO.getJH2ValidPan();
						rowData[62] = "";//FORM6061RE
						rowData[63] = "";//JH1F6061RE
						rowData[64] = "";//JH2F6061RE
						rowData[65] = "";//GF6061RE
						if(objTransactionBO.getSIP_Reg_Date()!=null)
							rowData[66] = objTransactionBO.getSIP_Reg_Date().getDateFirstView();
						rowData[67] = objAccountDetailsBO.getFirst_Hldr_Min();
						rowData[68] = objAccountDetailsBO.getJH1_Min();
						rowData[69] = objAccountDetailsBO.getJH2_Min();
						rowData[70] = objAccountDetailsBO.getGuardian_Min();
						rowData[71] = objAccountDetailsBO.getNEFT_CD();
						rowData[72] = objAccountDetailsBO.getRTGS_CD();
						// ---- New Fields ----
						rowData[73] = objInvestorDetailsBO.getKycFlag(); // KYC Flag
						rowData[74] = "N"; // POA State
						rowData[75] = "W"; // Mode Trxn.
						rowData[76] = "N"; // Sign
						
						webLog	=	"Web Log: "+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+                   
						objTransactionBO.getTransactionTime()+"$$"+                                                      
						objTransactionBO.getUserTransactionNo()+"$"+                                                      
						objTransactionBO.getIPAddress()+
						"$$$$$$$$$$$$$$$$$$";                                                      
	
						if(objTransactionBO.getTransactionType().equals("P"))
							webLog	+=	"PB$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"; // Log
						else
							webLog	+=	"$$$$"; // Log
						rowData[77] = 	webLog;
						
						// ---- New Fields ----
						if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
						{
							rowData[78] = "NRI Ad: "+objInvestorDetailsBO.getNRIAddress1();
							rowData[79] = objInvestorDetailsBO.getNRIAddress2();
							rowData[80] = objInvestorDetailsBO.getNRIAddress3();
							
							if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
								rowData[81] = objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState();
							else
								rowData[81] = objInvestorDetailsBO.getNRICity();
							
							rowData[82] = "";//objInvestorDetailsBO.getNRIState();
							rowData[83] = objInvestorDetailsBO.getNRICountry();
							rowData[84] = objInvestorDetailsBO.getPinCode();
							
							//rowData[85] = new Double(0.0d); // Nom1_Applicable -  Number -  5,2
							rowData[86] = ""; // Nom2_Name
							rowData[87] = ""; // Nom2_Relationship
							
							//rowData[88] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
							rowData[89] = ""; // Nom3_Name
							rowData[90] = ""; // Nom3_Relationship
							//rowData[91] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
							rowData[92] = "Y"; // Check_Flag
							rowData[95] = "Y"; // FIRC_STA 
						}
						else
							rowData[92] = "N"; // Check_Flag
						
						if(objTransactionBO.getTransactionType().equals("P"))
						{
							rowData[93] = "N"; // TH_P_PYT ( Third Party Payment)
							rowData[94] = objInvestorDetailsBO.getKycFlag(); // KYC
							rowData[102] = "Y"; // Che_copy
						}
						else
						{
							rowData[93] = ""; // TH_P_PYT ( Third Party Payment)
							rowData[94] = objInvestorDetailsBO.getKycFlag(); // KYC
							rowData[95] = ""; // FIRC_STA 
						}
						
						if(!objTransactionBO.getPaidInstallments().equals("0")){
							rowData[96] =  new Double(objTransactionBO.getPaidInstallments()); // INSTL_NO
							rowData[97] =  new Double(objTransactionBO.getNoOfInstalment()); // TOTAL_INST
						}
						
						rowData[99] = objTransactionBO.getEuinOpted(); // EUIN_OPT
						rowData[100] = objTransactionBO.getEuinId(); // EUIN
						rowData[101] = new Double(objTransactionBO.getSipReferenceId());
						rowData[102] = objTransactionBO.getFreshInvestor(); // Fres_Exi
//						Che_copy
						rowData[104] = objInvestorDetailsBO.getMobileNo(); // Mob_no
	
						if(objAccountDetailsBO.getNomineeName().equals(""))
							rowData[105] = "N"; // Nomi_avbl
						else
							rowData[105] = "Y"; // Nomi_avbl
						
						if((objInvestorDetailsBO.getIrType().equals("OE"))&&(objTransactionBO.getTransactionType().equals("P"))){
							rowData[77] = 	"Web Log: "+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+                   
											objTransactionBO.getTransactionTime()+"$$"+                                                      
											objTransactionBO.getUserTransactionNo()+"$"+                                                      
											objTransactionBO.getIPAddress()+"$"+                                                              
											objTransactionBO.getCreatedUser()+"$"+                                  		                          
											objInvestorDetailsBO.getTcVersion()+"$"+                                                              
											"USCAN$Y$"+                                           
											objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+                                       
											objTransactionBO.getTransactionTime()+"$"+                                                        
											"TC$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+                                                                  
											"IAG$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+                                                                    
											"SD$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+                                                      
											"PB$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$";
	
							rowData[107] = "CVL KRA"; // KRA_agency
						}
						rowData[106] = "Y"; // KRA_Verified
						rowData[108] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker code
						
						if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
							rowData[109] = new Double(objInvestorDetailsBO.getcKYCRefId1());
						
						if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
							rowData[110] = new Double(objInvestorDetailsBO.getcKYCRefId2());
						
						if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
							rowData[111] = new Double(objInvestorDetailsBO.getcKYCRefId3());
						
						if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
							rowData[112] = new Double(objInvestorDetailsBO.getcKYCRefIdG());
						
						if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
							rowData[113] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();
						
						if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
							rowData[114] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();
						
						if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
							rowData[115] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();
						
						rowData[116] = new Double(objInvestorDetailsBO.getaAdhaarNo_fhld());
						rowData[117] = new Double(objInvestorDetailsBO.getaAdhaarNo_shld());
						rowData[118] = new Double(objInvestorDetailsBO.getaAdhaarNo_thld());
						rowData[119] = new Double(objInvestorDetailsBO.getaAdhaarNo_gurd());
						
						if((!objInvestorDetailsBO.getaAdhaarNo_fhld().equals("0") )||(!objInvestorDetailsBO.getaAdhaarNo_shld().equals("0"))||(!objInvestorDetailsBO.getaAdhaarNo_thld().equals("0"))||(!objInvestorDetailsBO.getaAdhaarNo_gurd().equals("0")))
							rowData[120] = "O";
						
						//if(!objAccountDetailsBO.getSipReferenceNo().equals("0"))
							//rowData[108] = new Double(objAccountDetailsBO.getSipReferenceNo());
	
						writer.addRecord(rowData);
						
						//Based on the UserTransactionNo Update the ForwardFeed - date
						setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
						setPreparedFeedUpdate.addBatch();
					}catch(Exception e){
						logger.info("Exp:row "+e);
					}
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : FranklinBatchUpdate = " + totalTime);
		
		setPreparedFeedUpdate.clearBatch();
	}
	public void writeFranklinTXT(ArrayList<TransactionMainBO> arryTransactionMainBO, String fileName) throws Exception
	{
		logger.info(" Franklin txt Begin ");
		int intMainBORowSize		=	0;
		String webLog	=	"";
		String DELIMETER = "|";
		StringBuffer pipe = new StringBuffer();
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size();
		try {
			//logger.info("Size:"+intMainBORowSize+" Arr:"+arryTransactionMainBO+" fileName:"+fileName);
	 		File file = new File(fileName);
	 	    OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file, true),"ASCII");
	        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	        //logger.info("file success");
			for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
			{
				try{
					objTransactionMainBO	=	new TransactionMainBO();
					pipe = new StringBuffer();
					String text	=	"";
					try{
						objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
						//logger.info("array success"+objTransactionMainBO+" row:"+DBFRow);
						if(objTransactionMainBO	!=	null)
						{
							objTransactionBO		=	objTransactionMainBO.getTransactionBO();
							objTransactionBO		=	objTransactionMainBO.getTransactionBO();
							objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
							objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
							//logger.info("Franklin file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
							//logger.info("BOS success");
							pipe	=	pipe.append(objTransactionBO.getAmcCode()).append(DELIMETER)//0
							.append("WEALTH").append(DELIMETER)//1
							.append(objInvestorDetailsBO.getSubBrokerCode()).append(DELIMETER) //2 Subbroker Code
							.append(objInvestorDetailsBO.getUserCode()).append(DELIMETER) //3 User code
							.append(objTransactionBO.getUserTransactionNo()).append(DELIMETER) //4 Transaction No
							.append(objTransactionBO.getUserTransactionNo()).append(DELIMETER) //5 Application No;Fill the "USR_TXN_NO" data in "Appl_No"
							.append(ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo())).append(DELIMETER) //6 Folio No				
							.append(ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())).append(DELIMETER) //7 Cheque degit No
							.append(objTransactionBO.getTransactionType()).append(DELIMETER)//8
							.append(objTransactionBO.getSchemeCode()).append(DELIMETER);//9
	
							text	= 	ConstansBO.NullCheck((objInvestorDetailsBO.getFirstName().trim().length()>=70)? objInvestorDetailsBO.getFirstName().substring(0,70) : objInvestorDetailsBO.getFirstName());
							pipe	=	pipe.append(text).append(DELIMETER);//10
							
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getJointName1().trim().length()>=35)? objInvestorDetailsBO.getJointName1().substring(0,35) : objInvestorDetailsBO.getJointName1());
							pipe	=	pipe.append(text).append(DELIMETER);//11
							
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getJointName2().trim().length()>=35)? objInvestorDetailsBO.getJointName2().substring(0,35) : objInvestorDetailsBO.getJointName2());
							pipe	=	pipe.append(text).append(DELIMETER);//12
							try{
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getAddress1().trim().length()>=40)? objInvestorDetailsBO.getAddress1().substring(0,40) : objInvestorDetailsBO.getAddress1());
							pipe	=	pipe.append(text).append(DELIMETER);//13
							
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getAddress2().trim().length()>=40)? objInvestorDetailsBO.getAddress2().substring(0,40) : objInvestorDetailsBO.getAddress2());
							pipe	=	pipe.append(text).append(DELIMETER);//14
							
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getAddress3().trim().length()>=40)? objInvestorDetailsBO.getAddress3().substring(0,40) : objInvestorDetailsBO.getAddress3());
							pipe	=	pipe.append(text).append(DELIMETER);//15
							}catch(Exception e){logger.info("1 block"+e);}
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getCity().trim().length()>=35)? objInvestorDetailsBO.getCity().substring(0,35) : objInvestorDetailsBO.getCity());
							pipe	=	pipe.append(text).append(DELIMETER);//16
							
							pipe.append(objInvestorDetailsBO.getPinCode()).append(DELIMETER)//17
							.append(objInvestorDetailsBO.getPhoneOffice()).append(DELIMETER)//18
							.append(objTransactionBO.getTransactionDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//19
							.append(objTransactionBO.getTransactionTime()).append(DELIMETER);//20
							
							if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
							{
								pipe.append(new Double(0.00000000d)).append(DELIMETER)//21
								.append(new Double(0.00000000d)).append(DELIMETER);//22
							}
							else
							{
								pipe.append(ConstansBO.getEightDecimal(objTransactionBO.getUnits())).append(DELIMETER)//21
								.append(ConstansBO.getEightDecimal(objTransactionBO.getAmount().doubleValue())).append(DELIMETER);//22
							}
							pipe.append(objAccountDetailsBO.getClos_Ac_Ch()).append(DELIMETER)//23
							.append(objInvestorDetailsBO.getDateOfBirth().getMonthFirstViewRequiredFormat()).append(DELIMETER);//24
							
							text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getGuardianName().trim().length()>=35)? objInvestorDetailsBO.getGuardianName().substring(0,35) : objInvestorDetailsBO.getGuardianName());
							pipe	=	pipe.append(text).append(DELIMETER);//25
							
							pipe.append(objInvestorDetailsBO.getTaxNo()).append(DELIMETER)//26
							.append(objInvestorDetailsBO.getPhoneRes()).append(DELIMETER)//27
							.append(objInvestorDetailsBO.getFaxOff()).append(DELIMETER)//28
							.append(objInvestorDetailsBO.getFaxRes()).append(DELIMETER)//29
							.append(objInvestorDetailsBO.getEmail()).append(DELIMETER)//30
							.append(objAccountDetailsBO.getAccountNo()).append(DELIMETER)//31
							.append(objAccountDetailsBO.getAccountType()).append(DELIMETER);//32
							try{
							text	=	ConstansBO.NullCheck((objAccountDetailsBO.getBankName().trim().length()>=40)? objAccountDetailsBO.getBankName().substring(0,40) : objAccountDetailsBO.getBankName());
							pipe	=	pipe.append(text).append(DELIMETER);//33
							
							text	=	ConstansBO.NullCheck((objAccountDetailsBO.getBranchName().trim().length()>=40)? objAccountDetailsBO.getBranchName().substring(0,40) : objAccountDetailsBO.getBranchName());
							pipe	=	pipe.append(text).append(DELIMETER);//34
							}catch(Exception e){logger.info("2nd block"+e);}
							text	=	ConstansBO.NullCheck((objAccountDetailsBO.getBankCity().trim().length()>=35)? objAccountDetailsBO.getBankCity().substring(0,35) : objAccountDetailsBO.getBankCity());
							pipe	=	pipe.append(text).append(DELIMETER);//35
							
							pipe.append(objAccountDetailsBO.getReinv_Tag()).append(DELIMETER)//36
							.append(objAccountDetailsBO.getHoldingNature()).append(DELIMETER)//37
							.append(objInvestorDetailsBO.getOccupationCode()).append(DELIMETER)//38
							.append(objAccountDetailsBO.getTaxStatus()).append(DELIMETER)//38
							.append(objTransactionBO.getPaymentId()+" "+objAccountDetailsBO.getRemarks()).append(DELIMETER)//40
							.append(objInvestorDetailsBO.getState()).append(DELIMETER)//41
							.append(objTransactionBO.getSubTransactionType()).append(DELIMETER)//42 
							.append("DC").append(DELIMETER); //43 DIV_PY_MEC - default value
							
							if(!objAccountDetailsBO.getMICR_CD().equals(""))
								text	=	objAccountDetailsBO.getMICR_CD()+DELIMETER;
							else
								text	=	""+DELIMETER;
							pipe	=	pipe.append(text)//44
							
							.append(objAccountDetailsBO.getBank_Code()).append(DELIMETER)//45
							.append(objAccountDetailsBO.getALT_Folio()).append(DELIMETER)//46
							.append(objAccountDetailsBO.getALT_Broker()).append(DELIMETER)//47
							.append(objInvestorDetailsBO.getLocationCode()).append(DELIMETER)//48
							.append("DC").append(DELIMETER) //49 RED_PY_MEC - default value
							.append(objTransactionBO.getPricing()).append(DELIMETER)//50
							.append(objInvestorDetailsBO.getPanHolder2()).append(DELIMETER)//51
							.append(objInvestorDetailsBO.getPanHolder3()).append(DELIMETER);//52
							try{
							text	=	ConstansBO.NullCheck((objAccountDetailsBO.getNomineeName().trim().length()>=40)? objAccountDetailsBO.getNomineeName().substring(0,40) : objAccountDetailsBO.getNomineeName());
							pipe	=	pipe.append(text).append(DELIMETER);//53
							
							text	=	ConstansBO.NullCheck((objAccountDetailsBO.getNomineeRelation().trim().length()>=40)? objAccountDetailsBO.getNomineeRelation().substring(0,40) : objAccountDetailsBO.getNomineeRelation());
							pipe	=	pipe.append(text).append(DELIMETER);//54
							}catch(Exception e){logger.info("3rd block"+e);}
							pipe.append(objInvestorDetailsBO.getGuardianPanNo()).append(DELIMETER)//55
							.append(objAccountDetailsBO.getInstrmno()).append(DELIMETER)//56
							.append(objAccountDetailsBO.getUINNo()).append(DELIMETER)//57
							.append(objInvestorDetailsBO.getValid_Pan()).append(DELIMETER)//58
							.append(objInvestorDetailsBO.getGValid_Pan()).append(DELIMETER)//59
							.append(objInvestorDetailsBO.getJH1ValidPan()).append(DELIMETER)//60
							.append(objInvestorDetailsBO.getJH2ValidPan()).append(DELIMETER)//61
							.append("").append(DELIMETER)//62 FORM6061RE
							.append("").append(DELIMETER)//63 JH1F6061RE
							.append("").append(DELIMETER)//64 JH2F6061RE
							.append("").append(DELIMETER);//65 GF6061RE
							
							try{
								if(objTransactionBO.getSIP_Reg_Date()!=null)
									text	=	(objTransactionBO.getSIP_Reg_Date().getMonthFirstViewRequiredFormat()).toString()+DELIMETER;
								else 
									text	=	DELIMETER;
							}catch(Exception e){text	=	DELIMETER;}
							pipe	=	pipe.append(text);//66
							
							pipe	=	pipe.append(objAccountDetailsBO.getFirst_Hldr_Min()).append(DELIMETER)//67
							.append(objAccountDetailsBO.getJH1_Min()).append(DELIMETER)//68
							.append(objAccountDetailsBO.getJH2_Min()).append(DELIMETER)//69
							.append(objAccountDetailsBO.getGuardian_Min()).append(DELIMETER)//70
							.append(objAccountDetailsBO.getNEFT_CD()).append(DELIMETER)//71
							.append(objAccountDetailsBO.getRTGS_CD()).append(DELIMETER)//72
							.append(objInvestorDetailsBO.getKycFlag()).append(DELIMETER) // 73 KYC Flag
							.append("N").append(DELIMETER) // 74 POA State
							.append("W").append(DELIMETER) // 75 Mode Trxn.
							.append("N").append(DELIMETER); // 76 Sign
							
							webLog	=	"Web Log: "+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+                   
							objTransactionBO.getTransactionTime()+"$$"+                                                      
							objTransactionBO.getUserTransactionNo()+"$"+                                                      
							objTransactionBO.getIPAddress()+
							"$$$$$$$$$$$$$$$$$$";                                                      
			
							if(objTransactionBO.getTransactionType().equals("P"))
								webLog	+=	"PB$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"; // Log
							else
								webLog	+=	"$$$$"; // Log
							
							if((objInvestorDetailsBO.getIrType().equals("OE"))&&(objTransactionBO.getTransactionType().equals("P"))){
								webLog = 	"Web Log: "+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+                   
												objTransactionBO.getTransactionTime()+"$$"+                                                      
												objTransactionBO.getUserTransactionNo()+"$"+                                                      
												objTransactionBO.getIPAddress()+"$"+                                                              
												objTransactionBO.getCreatedUser()+"$"+                                  		                          
												objInvestorDetailsBO.getTcVersion()+"$"+                                                              
												"USCAN$Y$"+                                           
												objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+                                       
												objTransactionBO.getTransactionTime()+"$"+                                                        
												"TC$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+                                                                  
												"IAG$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+                                                                    
												"SD$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+                                                      
												"PB$Y$"+objTransactionBO.getCreatedDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$";
							}
							
							text	=	(webLog.length()>=255)? webLog.substring(0,255) : webLog;
							pipe	=	pipe.append(text).append(DELIMETER);// 77
	
							// ---- New Fields ----
							if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
							{
								text	=	ConstansBO.NullCheck((("NRI Ad: "+objInvestorDetailsBO.getNRIAddress1()).trim().length()>=40)? ("NRI Ad: "+objInvestorDetailsBO.getNRIAddress1()).substring(0,40) : "NRI Ad: "+objInvestorDetailsBO.getNRIAddress1());
								pipe	=	pipe.append(text).append(DELIMETER);//78
								
								text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getNRIAddress2().trim().length()>=40)? objInvestorDetailsBO.getNRIAddress2().substring(0,40) : objInvestorDetailsBO.getNRIAddress2());
								pipe	=	pipe.append(text).append(DELIMETER);//79
								
								text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getNRIAddress3().trim().length()>=40)? objInvestorDetailsBO.getNRIAddress3().substring(0,40) : objInvestorDetailsBO.getNRIAddress3());
								pipe	=	pipe.append(text).append(DELIMETER);//80

								if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
									text	=	ConstansBO.NullCheck(((objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()).trim().length()>=35)? (objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()).substring(0,35) : (objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()));
								else
									text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getNRICity().trim().length()>=35)? (objInvestorDetailsBO.getNRICity()).substring(0,35) : (objInvestorDetailsBO.getNRICity()));
								
								pipe	=	pipe.append(text).append(DELIMETER);//81
								
								pipe	=	pipe.append(DELIMETER) //82 objInvestorDetailsBO.getNRIState();
								.append(objInvestorDetailsBO.getNRICountry()).append(DELIMETER);//83
								
								text	=	ConstansBO.NullCheck((objInvestorDetailsBO.getPinCode().trim().length()>=6)? objInvestorDetailsBO.getPinCode().substring(0,6) : objInvestorDetailsBO.getPinCode());
								
								pipe.append(text).append(DELIMETER)//84
								.append(DELIMETER) //85 new Double(0.0d); // Nom1_Applicable -  Number -  5,2
								.append(DELIMETER) //86 Nom2_Name
								.append(DELIMETER)// 87 Nom2_Relationship
								.append(DELIMETER) //88 Nom2_Applicable -  Number -  5,2
								.append(DELIMETER)//89 Nom3_Name
								.append(DELIMETER) //90  Nom3_Relationship
								.append(DELIMETER)//91 Nom2_Applicable -  Number -  5,2
								.append("Y").append(DELIMETER); //92 Check_Flag
								
								if(objTransactionBO.getTransactionType().equals("P"))
								{
									text	=	"N"+DELIMETER+ //93 TH_P_PYT ( Third Party Payment)
									"Y"+DELIMETER+ //94 KYC
									"Y"+DELIMETER; //95 FIRC_STA 
								}else{
									text	=	""+DELIMETER+ //93 TH_P_PYT ( Third Party Payment)
									"Y"+DELIMETER+ //94 KYC
									DELIMETER; //95 FIRC_STA 
								}
								pipe	=	pipe.append(text);
								
							}
							else{
								text	= DELIMETER+//78 NRIAddress1
								DELIMETER+ //79 NRIAddress2 
								DELIMETER+ //80 NRIAddress3 
								DELIMETER+ //81 city 
								DELIMETER+ //82 state
								DELIMETER+ //83 country
								DELIMETER+ //84 pincode
								DELIMETER+ //85 Nom1_Applicable 
								DELIMETER+ //86 Nom2_Name
								DELIMETER+ //87 Nom2_Relationship 
								DELIMETER+ //88 Nom2_Applicable
								DELIMETER+ //89 Nom3_Name 
								DELIMETER+ //90 Nom3_Relationship 
								DELIMETER+ //91 Nom2_Applicable
								"N"+DELIMETER; //92 Check_Flag
								pipe.append(text);
								if(objTransactionBO.getTransactionType().equals("P"))
								{
									text	=	"N"+DELIMETER+ //93 TH_P_PYT ( Third Party Payment)
									"Y"+DELIMETER+//94 KYC
									DELIMETER; //95 FIRC_STA
								}else{
									text	=	""+DELIMETER+ //93 TH_P_PYT ( Third Party Payment)
									"Y"+DELIMETER+ //94 KYC
									DELIMETER; //95 FIRC_STA 
								}
								pipe.append(text);
							}
							
							if(!objTransactionBO.getPaidInstallments().equals("0"))
								pipe.append(objTransactionBO.getPaidInstallments()).append(DELIMETER);//96
                                                        else
                                                            pipe.append(DELIMETER);//96
							
							pipe.append(DELIMETER)//97
							.append(DELIMETER)//98
							.append(objTransactionBO.getEuinOpted()).append(DELIMETER) //99 EUIN_OPT
							.append(objTransactionBO.getEuinId()).append(DELIMETER) //100 EUIN
                                                        .append(objInvestorDetailsBO.getSubBrokerCode()).append(DELIMETER); //108 Subbroker code
							
                                                        if(!objTransactionBO.getNoOfInstalment().equals("0"))
                                                            pipe.append(objTransactionBO.getNoOfInstalment()).append(DELIMETER); //101 
                                                        else
                                                            pipe.append(DELIMETER);
                                                        
							pipe.append(objTransactionBO.getFreshInvestor()).append(DELIMETER); //102 Fres_Exi
							
							if(objTransactionBO.getTransactionType().equals("P"))
							{
								pipe.append("Y").append(DELIMETER); //103 Che_copy
							}else{
								pipe.append(DELIMETER); //103 Che_copy 
							}
							pipe.append(objInvestorDetailsBO.getMobileNo()).append(DELIMETER); //104 Mob_no
			
							if(objAccountDetailsBO.getNomineeName().equals(""))
								pipe.append("N").append(DELIMETER); //105 Nomi_avbl
							else
								pipe.append("Y").append(DELIMETER); //105 Nomi_avbl
							
							pipe.append("Y").append(DELIMETER); //106 KRA_Verified
							if((objInvestorDetailsBO.getIrType().equals("OE"))&&(objTransactionBO.getTransactionType().equals("P"))){
								pipe	=	pipe.append("CVL KRA").append(DELIMETER);//107 KRA_agency
							}else{
								pipe	=	pipe.append(DELIMETER);//107 KRA_agency
							}
							
							if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId1()).append(DELIMETER); //109 FHLD_CKYC
							else
								pipe.append(DELIMETER); //109 FHLD_CKYC
							
							if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId2()).append(DELIMETER); //110 SHLD_CKYC
							else
								pipe.append(DELIMETER); //110 SHLD_CKYC
							
							if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId3()).append(DELIMETER); //111 THLD_CKYC
							else
								pipe.append(DELIMETER); // 111 THLD_CKYC
							
							if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefIdG()).append(DELIMETER); // 112 GURD_CKYC
							else
								pipe.append(DELIMETER); //112 GURD_CKYC
							
							if(objInvestorDetailsBO.getDateOfBirth2() != null)
								pipe.append(objInvestorDetailsBO.getDateOfBirth2().getMonthFirstViewRequiredFormat()).append(DELIMETER); // 113 JH1 DOB
							else
								pipe.append(DELIMETER); // 113 JH1 DOB
							
							if(objInvestorDetailsBO.getDateOfBirth3() != null)
								pipe.append(objInvestorDetailsBO.getDateOfBirth3().getMonthFirstViewRequiredFormat()).append(DELIMETER); // 114 JH2 DOB
							else
								pipe.append(DELIMETER); // 114 JH2 DOB
							
							if(objInvestorDetailsBO.getDateOfBirthG() != null)
								pipe.append(objInvestorDetailsBO.getDateOfBirthG().getMonthFirstViewRequiredFormat()).append(DELIMETER); // 115 GUARDIAN DOB
							else
								pipe.append(DELIMETER);// 115 GUARDIAN DOB
							
							pipe.append(objInvestorDetailsBO.getaAdhaarNo_fhld()).append(DELIMETER) // 116 FHLD_AADHAAR
							.append(objInvestorDetailsBO.getaAdhaarNo_shld()).append(DELIMETER) // 117 SHLD_AADHAAR
							.append(objInvestorDetailsBO.getaAdhaarNo_thld()).append(DELIMETER) // 118 THLD_AADHAAR
							.append(objInvestorDetailsBO.getaAdhaarNo_gurd()).append(DELIMETER); // 119 GURD_AADHAAR
							
							if((!objInvestorDetailsBO.getaAdhaarNo_fhld().equals("0") )||(!objInvestorDetailsBO.getaAdhaarNo_shld().equals("0"))||(!objInvestorDetailsBO.getaAdhaarNo_thld().equals("0"))||(!objInvestorDetailsBO.getaAdhaarNo_gurd().equals("0")))
								pipe.append("O"); // 120 AADHAAR_STATUS
							else
								pipe.append(""); // 120 AADHAAR_STATUS
							
							//pipe.append(DELIMETER);
							
							/*if(!objAccountDetailsBO.getSipReferenceNo().equals("0"))
								pipe.append(objAccountDetailsBO.getSipReferenceNo());*/
							
							pipe.append("\r\n");
							bufferedWriter.write(pipe.toString());
							//logger.info("*** row success ***"+DBFRow);
						}
					}catch(Exception e){
						logger.info("Exp:"+DBFRow+" row "+e);
					}
					//logger.info("File Created successfully : "+fileName+" & TotalRows:"+DBFRow);
				}catch(Exception e){
					logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
				}
			}
			
			if(bufferedWriter != null) bufferedWriter.flush();
	        if(bufferedWriter != null) bufferedWriter.close();
	 		if(fileWriter != null) fileWriter.close();
	 		
	 	} catch (Exception e1) {
	 	  logger.info("Exp while file Creation : "+e1);
	 	}
	}
	public void writeDSPLDBF(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		//logger.info(" DSPL DBF Begin ");
		int intMainBORowSize		=	0;
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size();
		
		
		
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[fieldSize];
				objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
				
				if(objTransactionMainBO!=null)
				{
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
					objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
					//logger.info("DSPL file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
					rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
					rowData[1] = "WEALTH";//objTransactionBO.getBrokerCode(); // Broker Code
					rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
					rowData[3] = objInvestorDetailsBO.getUserCode(); // User Code
					rowData[4] = objTransactionBO.getUserTransactionNo(); // Transaction No
					rowData[5] = objTransactionBO.getApplNo(); // Application No
					rowData[6] = objTransactionBO.getFolioNo(); // Folio No
					rowData[7] = objTransactionBO.getCk_Dig_No(); // Cheque degit No
					rowData[8] = objTransactionBO.getTransactionType();
					rowData[9] = objTransactionBO.getSchemeCode();
					rowData[10] = objInvestorDetailsBO.getFirstName();
					rowData[11] = objInvestorDetailsBO.getJointName1();
					rowData[12] = objInvestorDetailsBO.getJointName2();
					rowData[13] = objInvestorDetailsBO.getAddress1();
					rowData[14] = objInvestorDetailsBO.getAddress2();
					rowData[15] = objInvestorDetailsBO.getAddress3();
					rowData[16] = objInvestorDetailsBO.getCity();
					rowData[17] = objInvestorDetailsBO.getPinCode();
					rowData[18] = objInvestorDetailsBO.getPhoneOffice();
					rowData[19] = (Date)objTransactionBO.getTransactionDate().getDateFirstView();
					rowData[20] = objTransactionBO.getTransactionTime();
					if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
					{
						rowData[21] = new Double(0);
						rowData[22] = new Double(0);
					}
					else
					{
						rowData[21] = objTransactionBO.getUnits();
						rowData[22] = objTransactionBO.getAmount().doubleValue();
					}
					rowData[23] = objAccountDetailsBO.getClos_Ac_Ch();
					rowData[24] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
					rowData[25] = objInvestorDetailsBO.getGuardianName();
					rowData[26] = objInvestorDetailsBO.getTaxNo();
					rowData[27] = objInvestorDetailsBO.getPhoneRes();
					rowData[28] = objInvestorDetailsBO.getFaxOff();
					rowData[29] = objInvestorDetailsBO.getFaxRes();
					rowData[30] = objInvestorDetailsBO.getEmail();
					rowData[31] = objAccountDetailsBO.getAccountNo();
					rowData[32] = objAccountDetailsBO.getAccountType();
					rowData[33] = objAccountDetailsBO.getBankName();
					rowData[34] = objAccountDetailsBO.getBranchName();
					rowData[35] = objAccountDetailsBO.getBankCity();
					rowData[36] = objAccountDetailsBO.getReinv_Tag();
					rowData[37] = objAccountDetailsBO.getHoldingNature();
					rowData[38] = objInvestorDetailsBO.getOccupationCode();
					rowData[39] = objAccountDetailsBO.getTaxStatus();
					rowData[40] = objTransactionBO.getPaymentId()+" "+objAccountDetailsBO.getRemarks();
					rowData[41] = objInvestorDetailsBO.getState();
					rowData[42] = objTransactionBO.getSubTransactionType();
					rowData[43] = objAccountDetailsBO.getPaymentMechanism();
					if(!objAccountDetailsBO.getMICR_CD().equals(""))
						rowData[44] = new Double(objAccountDetailsBO.getMICR_CD());
					rowData[45] = objAccountDetailsBO.getBank_Code();
					rowData[46] = objAccountDetailsBO.getALT_Folio();
					rowData[47] = objAccountDetailsBO.getALT_Broker();
					rowData[48] = objInvestorDetailsBO.getLocationCode();
					rowData[49] = objAccountDetailsBO.getPaymentMechanism();
					rowData[50] = objTransactionBO.getPricing();
					rowData[51] = objInvestorDetailsBO.getPanHolder2();
					rowData[52] = objInvestorDetailsBO.getPanHolder3();
					rowData[53] = objAccountDetailsBO.getNomineeName();
					rowData[54] = objAccountDetailsBO.getNomineeRelation(); 
					rowData[55] = objInvestorDetailsBO.getGuardianPanNo();
					rowData[56] = objAccountDetailsBO.getInstrmno();
					rowData[57] = objAccountDetailsBO.getUINNo();
					rowData[58] = objInvestorDetailsBO.getValid_Pan();
					rowData[59] = objInvestorDetailsBO.getGValid_Pan();
					rowData[60] = objInvestorDetailsBO.getJH1ValidPan();
					rowData[61] = objInvestorDetailsBO.getJH2ValidPan();
					rowData[62] = "";//objAccountDetailsBO.getForm6061();
					rowData[63] = "";//objAccountDetailsBO.getForm6061J1();
					rowData[64] = "";//objAccountDetailsBO.getForm6061J2();
					rowData[65] = "";//objAccountDetailsBO.getGF6061RE();
					rowData[66] = "";//objAccountDetailsBO.getSipReferenceNo();
					if(objTransactionBO.getSIP_Reg_Date()!=null)
						rowData[67] = objTransactionBO.getSIP_Reg_Date().getDateFirstView();
					rowData[68] = objAccountDetailsBO.getFirst_Hldr_Min();
					rowData[69] = objAccountDetailsBO.getJH1_Min();
					rowData[70] = objAccountDetailsBO.getJH2_Min();
					rowData[71] = objAccountDetailsBO.getGuardian_Min();
					// ---- New Fields ----
					/*rowData[72] = objAccountDetailsBO.getNEFT_CD(); //NEFT_CODE
					rowData[73] = objAccountDetailsBO.getRTGS_CD();
					rowData[74] = "E"; //EMAIL_ACST
					rowData[75] = objInvestorDetailsBO.getMobileNo(); //MOBILE_NO
					rowData[76] = ""; //DP_ID
					rowData[77] = "N"; //POA_type
					rowData[78] = "W"; //Trxn Mode
					rowData[79] = ""; //Trxn Mode*/
	
					writer.addRecord(rowData);
					
					//Based on the UserTransactionNo Update the ForwardFeed - date
					setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
					setPreparedFeedUpdate.addBatch();
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : DSPBatchUpdate " + totalTime);
		
		setPreparedFeedUpdate.clearBatch();
	}
	public void writeKarvySIPDBF(int rtCode, ArrayList<SIPInfoBO> arrySIPInfoBO, DBFWriter writer) throws Exception 
	{
		//long startTime = System.currentTimeMillis();
		
		int intMainBORowSize		=	0;
		SIPInfoBO objSIPInfoBO	=	new SIPInfoBO();
		intMainBORowSize		=	arrySIPInfoBO.size();
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[19];
				objSIPInfoBO	=	arrySIPInfoBO.get(DBFRow);
				
				if(objSIPInfoBO!=null)
				{
					rowData[0] = objSIPInfoBO.getFundCode(); 
					rowData[1] = objSIPInfoBO.getSchemeCode();
					rowData[2] = objSIPInfoBO.getFolioNumber();
					rowData[3] = objSIPInfoBO.getInvestorName();
					rowData[4] = objSIPInfoBO.getFrequency();
					rowData[5] = objSIPInfoBO.getSipType(); // SIP Types
					rowData[6] = (Date)objSIPInfoBO.getStartMonth().getDateFirstView();				
					rowData[7] = (Date)objSIPInfoBO.getEndMonth().getDateFirstView();
					rowData[8] = (Date)objSIPInfoBO.getStartMonth().getDateFirstView();
					rowData[9] = new Double(String.valueOf(objSIPInfoBO.getNoOfInstalments()));
					rowData[10] = new Double(objSIPInfoBO.getAmount());
					if(rtCode==6)
						rowData[11] = "WEALTH";
					else
						rowData[11] = "ARN-69583";
					rowData[12] = objSIPInfoBO.getSubBrokerCode();
					rowData[13] = new Double(objSIPInfoBO.getSIPRefNo());
					rowData[14] = objSIPInfoBO.getAccountNo();
					rowData[15] = objSIPInfoBO.getBankName();
					rowData[16] = objSIPInfoBO.getAccountType();
					rowData[17] = objSIPInfoBO.getInvestorBankName();
					rowData[18] = objSIPInfoBO.getRemarks();
					writer.addRecord(rowData);
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : KarvyBatchUpdate = " + totalTime);
	}
	public void writeKarvyNewFolioDBF(ArrayList<NewFolioBO> arryNewFolioBO, DBFWriter writer) throws Exception 
	{
		//long startTime = System.currentTimeMillis();
		
		int intMainBORowSize		=	0;
		NewFolioBO objNewFolioBO	=	new NewFolioBO();
		intMainBORowSize		=	arryNewFolioBO.size();
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
				try{
				Object rowData[] = new Object[16];
				objNewFolioBO	=	arryNewFolioBO.get(DBFRow);
				
				if(objNewFolioBO!=null)
				{
					rowData[0] = String.valueOf(objNewFolioBO.getBatchNo());
					rowData[1] = String.valueOf(objNewFolioBO.getSlNo());
					rowData[2] = "ARN69583"+objNewFolioBO.getPanNo(); 
					rowData[3] = "";
					rowData[4] = "ARN-69583";
					rowData[5] = "ARN-69583";
					rowData[6] = "";				
					rowData[7] = "";
					rowData[8] = objNewFolioBO.getInvestorName();
					rowData[9] = "1";
					rowData[10] = "N";
					rowData[11] = "ON";
					rowData[12] = "AOFPOAPANKYC";
					rowData[13] = objNewFolioBO.getPanNo();
					rowData[14] = String.valueOf(objNewFolioBO.getInvestorId());
					rowData[15] = "N";
					writer.addRecord(rowData);
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : KarvyNewFolioFileWritings= " + totalTime);
	}
	public void writeCAMSNewFolioDBF(ArrayList<NewFolioBO> arryNewFolioBO, DBFWriter writer) throws Exception 
	{
		//long startTime = System.currentTimeMillis();
		
		int intMainBORowSize		=	0;
		NewFolioBO objNewFolioBO	=	new NewFolioBO();
		intMainBORowSize		=	arryNewFolioBO.size();
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[5];
				objNewFolioBO	=	arryNewFolioBO.get(DBFRow);
				
				if(objNewFolioBO!=null)
				{
					rowData[0] = "ARN-69583";
					rowData[1] = objNewFolioBO.getPanNo();
					rowData[2] = objNewFolioBO.getInvestorName(); 
					rowData[3] = "ALL";
					rowData[4] = "ARN-69583$"+objNewFolioBO.getPanNo()+"$ALL";
					writer.addRecord(rowData);
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : CAMSNewFolioFileWritings = " + totalTime);
	}
	public Hashtable<String, TransactionMainBO> writeFatcaDBFFile(int dbfColumnSize, int rtCode, ArrayList<TransactionMainBO> arryTransactionFatcaBO, DBFWriter writer, Hashtable<String, TransactionMainBO> hashFATCANotavailable, Connection objConnection) throws Exception 
	{
		//long startTime = System.currentTimeMillis();

		String passivePan			=	"";
		int uboDeclarationSize		=	1;
		//boolean fileFlag			=	false;
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		//AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO		=	new TransactionBO();
		FatcaUBODeclarationBO objFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
		FatcaDocBO objFatcaDocBO	=	new FatcaDocBO();
		DBFFilesDAO objDBFFilesDAO	=	new DBFFilesDAO();
		ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
		ArrayList<FatcaUBODeclarationBO> arrFatcaUBODeclarationActiveBO	=	new ArrayList<FatcaUBODeclarationBO>();
		ArrayList<FatcaUBODeclarationBO> arrFatcaUBODeclarationPassiveBO	=	new ArrayList<FatcaUBODeclarationBO>();
		Hashtable<String, String> addedPans	=	new Hashtable<String, String>();
		
 		for(int DBFRow=0;DBFRow<arryTransactionFatcaBO.size();DBFRow++)
		{
			objTransactionMainBO	=	new TransactionMainBO();
			objInvestorDetailsBO	=	new InvestorDetailsBO();
			objTransactionBO		=	new TransactionBO();
			arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
			try{
				objTransactionMainBO	=	arryTransactionFatcaBO.get(DBFRow);
				objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
				
				//objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
				objTransactionBO		=	objTransactionMainBO.getTransactionBO();
				
				arrFatcaDocBO			=	objInvestorDetailsBO.getArrFatcaDocBO();
				for(int fat=0;fat<arrFatcaDocBO.size();fat++)
				{
					uboDeclarationSize		=	1;
					objFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
					objFatcaDocBO			=	new FatcaDocBO();
					arrFatcaUBODeclarationActiveBO	=	new ArrayList<FatcaUBODeclarationBO>();
					arrFatcaUBODeclarationPassiveBO	=	new ArrayList<FatcaUBODeclarationBO>();
					
					objFatcaDocBO			=	arrFatcaDocBO.get(fat);
					if(objFatcaDocBO.getFatcaNotAvailablePan().equals("")){
						
						if((!addedPans.containsKey(objFatcaDocBO.getPan())) && (!objFatcaDocBO.getPan().equals(""))) {
							addedPans.put(objFatcaDocBO.getPan(), "Y");
							//fileFlag			=	true;
							
							objFatcaUBODeclarationBO	=	objDBFFilesDAO.getFatcaUBODeclarations(objFatcaDocBO.getPan(), objConnection);
							arrFatcaUBODeclarationActiveBO	=	objDBFFilesDAO.getFatcaUBODeclarationsActive(objFatcaUBODeclarationBO, objConnection);
								
							if(objFatcaUBODeclarationBO.getNFFE_CATG_Flag().equals("P"))
								arrFatcaUBODeclarationPassiveBO	=	objDBFFilesDAO.getFatcaUBODeclarationsPassive(rtCode, objFatcaUBODeclarationBO, objConnection);
							
							if(arrFatcaUBODeclarationActiveBO.size()==0)
								uboDeclarationSize		=	1;
							else
								uboDeclarationSize	=	arrFatcaUBODeclarationActiveBO.size();
							
							for(int ubo=0;ubo<uboDeclarationSize;ubo++){//by ubo wise data
								
								passivePan	=	"";
								Object rowData[] = new Object[dbfColumnSize];
								rowData[0] = objFatcaDocBO.getPan();//Investor pan
								//PEKRN
								rowData[2] = objFatcaDocBO.getInvestorName();// Investor name
								
								//if(objFatcaDocBO.getTaxStatusFlag().equals("IN"))
									rowData[3] = (Date)objFatcaDocBO.getDateOfBirth().getDateFirstView();
								//4	FR_NAME	7	Character	70	N	Father Name if PAN is not provided.
								//5	SP_NAME	7	Character	70	N	Spouse Name if PAN and Father name is not provided.
								rowData[6] = String.valueOf(objFatcaDocBO.getTaxStatus());
								if (objFatcaDocBO.getTaxStatusFlag().equals("IN"))//This is to indicated data sources whether it is obtained Electronically or through physical request; Values - 'E' / 'P'
									 rowData[7] = "E";
								 else
									 rowData[7] = "P";
								//if Individual then Residential 2 if nonindividual then Business 3 else Res. or Business 1
								if(objFatcaDocBO.getTaxStatusFlag().equals("IN"))//8	ADDR_TYPE	9	Character	1	Y	Address Type needs to be mentioned as per KYC Data. Code 1 to 5 needs to be populated.
									rowData[8] = "2";
								else if (objFatcaDocBO.getTaxStatusFlag().equals("NONIN"))
									rowData[8] = "3";
								else
									rowData[8] = "1";
									
								rowData[9] = objFatcaDocBO.getCountryOfBirthCode();
								rowData[10] = objFatcaDocBO.getCountryOfBirthCode();
								
								rowData[11] = objFatcaDocBO.getCountryOfBirthCode();
								rowData[12] = objFatcaDocBO.getPan();
								rowData[13] = "C";
								
								for(int i=0; i<objFatcaDocBO.getArrFatcaDocBO().size(); i++){
									if(i==0){
										rowData[11] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxResidencyCode();
										rowData[12] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxPayerIdentificationNo();
										rowData[13] = objFatcaDocBO.getArrFatcaDocBO().get(i).getIdentificationType();
									}else if(i==1){
										rowData[14] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxResidencyCode();
										rowData[15] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxPayerIdentificationNo();
										rowData[16] = objFatcaDocBO.getArrFatcaDocBO().get(i).getIdentificationType();
									}else if(i==2){
										rowData[17] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxResidencyCode();
										rowData[19] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxPayerIdentificationNo();
										rowData[19] = objFatcaDocBO.getArrFatcaDocBO().get(i).getIdentificationType();
									}else if(i==3){
										rowData[20] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxResidencyCode();
										rowData[21] = objFatcaDocBO.getArrFatcaDocBO().get(i).getTaxPayerIdentificationNo();
										rowData[22] = objFatcaDocBO.getArrFatcaDocBO().get(i).getIdentificationType();
									}
								}
								
								rowData[23] = objFatcaDocBO.getSourceOfWealthCode();
								rowData[24] = "04";//24	CORP_SERVS	10	Character	2	Y
								rowData[25] = String.valueOf(objFatcaDocBO.getAnnualIncomeCode());//INCSLAB
								
								if(new Double(String.valueOf(objFatcaUBODeclarationBO.getNetWorthAmount()))>0)
									rowData[26] = new Double(String.valueOf(objFatcaUBODeclarationBO.getNetWorthAmount()));//26	NET_WORTH	9	Character	20	Y	Net-Worth in Rs. From account opening form. If App Income code has given this field is not mandatory
								//27	NW_DATE	7	Date		Y	Net-Worth date From account opening form. Mandatory if Net Worth filled,
								if(objFatcaUBODeclarationBO.getNetWorthDate() != null)
									rowData[27] = (Date)objFatcaUBODeclarationBO.getNetWorthDate().getDateFirstView();
								
                                                                if(objFatcaDocBO.getPoliticallyExpPer().equalsIgnoreCase("no"))
                                                                    rowData[28] = "N";
                                                                else
                                                                    rowData[28] = objFatcaDocBO.getPoliticallyExpPer();
                                                                
								rowData[29] = objFatcaDocBO.getOccupationCode();
								rowData[30] = objFatcaDocBO.getOccupationTypeCode();

								rowData[31] = objFatcaUBODeclarationBO.getEntityExemptionCode();
								if (objFatcaDocBO.getTaxStatusFlag().equals("NONIN")){
									
									if(objFatcaUBODeclarationBO.getEntityExemptionCode().equals(""))//EXEMP_CODE please map as �N� by default for non individual.
										rowData[31] = "N";
										
									if(objFatcaUBODeclarationBO.getNFEsType().equals(""))
										rowData[32] = "NA";
									else
										rowData[32] = objFatcaUBODeclarationBO.getNFEsType();
									
									if(objFatcaUBODeclarationBO.getNFEs_GIIN_SP_Entity().equals(""))
										rowData[34] = "NA";
									else
										rowData[34] = objFatcaUBODeclarationBO.getNFEs_GIIN_SP_Entity();
									
									if((objFatcaUBODeclarationBO.getREPTCExchangeName()+objFatcaUBODeclarationBO.getPTCStockExchange()).length()>0)
										rowData[41] = objFatcaUBODeclarationBO.getREPTCExchangeName()+objFatcaUBODeclarationBO.getPTCStockExchange();
									else
										rowData[41] = "O";
								}
								
								
								rowData[33] = objFatcaUBODeclarationBO.getNFEs_GIINCode();
								
								
								rowData[35] = objFatcaUBODeclarationBO.getNFEs_GIIN_NA_Flag();
								rowData[36] = objFatcaUBODeclarationBO.getNFEs_GIIN_NA_Subcategory();//GIIN_EXEMC
								rowData[37] = objFatcaUBODeclarationBO.getNFFE_CATG_Flag();
								rowData[38] = objFatcaUBODeclarationBO.getActiveNFESubCategory();
								rowData[39] = objFatcaUBODeclarationBO.getActiveNFEBusiness()+objFatcaUBODeclarationBO.getPassiveNFEBusiness();
								rowData[40] = objFatcaUBODeclarationBO.getREPTCRelation();
								
								rowData[42] = objFatcaUBODeclarationBO.getUboApplicable();
								
								if(( objFatcaUBODeclarationBO.getUboApplicable().equals("Y"))&&(arrFatcaUBODeclarationActiveBO.size()>0)){
									
									rowData[43] = String.valueOf(arrFatcaUBODeclarationActiveBO.size());
									rowData[44] = arrFatcaUBODeclarationActiveBO.get(ubo).getName();
									rowData[46] = arrFatcaUBODeclarationActiveBO.get(ubo).getCountryCode();
									rowData[47] = arrFatcaUBODeclarationActiveBO.get(ubo).getAddress1();
									rowData[48] = arrFatcaUBODeclarationActiveBO.get(ubo).getAddress2();
									rowData[49] = arrFatcaUBODeclarationActiveBO.get(ubo).getAddress3();
									rowData[50] = objFatcaUBODeclarationBO.getUboCity();
									rowData[51] = arrFatcaUBODeclarationActiveBO.get(ubo).getPin();
									rowData[52] = arrFatcaUBODeclarationActiveBO.get(ubo).getState();
									rowData[53] = arrFatcaUBODeclarationActiveBO.get(ubo).getCountryCode();
									rowData[54] = arrFatcaUBODeclarationActiveBO.get(ubo).getAddressType();
									rowData[55] = arrFatcaUBODeclarationActiveBO.get(ubo).getAcountryCode();
									rowData[56] = arrFatcaUBODeclarationActiveBO.get(ubo).getTaxIdNo();
									rowData[57] = arrFatcaUBODeclarationActiveBO.get(ubo).getTaxIdType();
									rowData[58] = arrFatcaUBODeclarationActiveBO.get(ubo).getCountryCode();
									
									rowData[66] = arrFatcaUBODeclarationActiveBO.get(ubo).getUboCode();
									
									try{
										passivePan	=	ConstansBO.NullCheck(arrFatcaUBODeclarationPassiveBO.get(ubo).getUboPan());
										if(!passivePan.equals("")){
											rowData[45] = arrFatcaUBODeclarationPassiveBO.get(ubo).getUboPan();
											
											if(arrFatcaUBODeclarationPassiveBO.get(ubo).getDob() != null)
												rowData[59] = (Date)arrFatcaUBODeclarationPassiveBO.get(ubo).getDob().getDateFirstView();
											rowData[60] = ConstansBO.NullCheck(arrFatcaUBODeclarationPassiveBO.get(ubo).getGender());
											rowData[61] = ConstansBO.NullCheck(arrFatcaUBODeclarationPassiveBO.get(ubo).getFatherName());
											rowData[62] = ConstansBO.NullCheck(arrFatcaUBODeclarationPassiveBO.get(ubo).getOccupationCode());
											rowData[63] = ConstansBO.NullCheck(arrFatcaUBODeclarationPassiveBO.get(ubo).getOccupationType());
										}
									}catch(Exception e){	passivePan	=	"";			}
									
									rowData[64] = "";//Tele
									rowData[65] = "";//mobile
									rowData[67] = "";//UBO_HOL_PC
									
								}
								if (objFatcaDocBO.getTaxStatusFlag().equals("IN")){ //IF indv then 'N' others 'Y'
									rowData[68] = "N";//SDF_FLAG
									rowData[69] = "N";//UBO_DF
								}
								else{
									rowData[68] = "Y";//SDF_FLAG
									rowData[69] = "Y";//UBO_DF
								}
									
								rowData[70] = "";//AADHAAR_RP
								rowData[71] = "N";
								
								if(objTransactionBO.getFatcaIPAddress().equals(""))
									objTransactionBO.setFatcaIPAddress("72.32.69.244");
								
								if(objTransactionBO.getFatcaDate() != null){
									rowData[72] = objTransactionBO.getFatcaIPAddress()+"#"+objTransactionBO.getFatcaDate().getDateMonthView().replace("-","")+";"+objTransactionBO.getFatcaTime();
								}else{
									rowData[72] = objTransactionBO.getFatcaIPAddress()+"#"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+";"+objTransactionBO.getTransactionTime();
								}
								
								writer.addRecord(rowData);
							}
						}
					}else{
						hashFATCANotavailable.put(objFatcaDocBO.getFatcaNotAvailablePan(), objTransactionMainBO);
					}
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : FATCA File Writing " + totalTime);
		return hashFATCANotavailable;
	}
	public void writeCKYCDBFFile(int dbfColumnSize, DBFWriter writer, Hashtable<String,InvestorDetailsBO> hashCKCDetails) throws Exception 
	{
		int DBFRow	=	0;
		
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		Set<String> set = hashCKCDetails.keySet(); 
	    Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {
		    String	strKeyValue = itr.next();
			objInvestorDetailsBO	=	new InvestorDetailsBO();
			objInvestorDetailsBO	=	hashCKCDetails.get(strKeyValue);
			try{
				if(objInvestorDetailsBO.getTaxNo().length() == 10){
					DBFRow++;
					Object rowData[] = new Object[dbfColumnSize];
					rowData[0] = objInvestorDetailsBO.getTaxNo();//Investor pan
					//PEKRN
					rowData[2] = objInvestorDetailsBO.getSalutation();
					rowData[3] = objInvestorDetailsBO.getFirstName();
					
					rowData[11] = objInvestorDetailsBO.getMotherName();
					rowData[14] = objInvestorDetailsBO.getTaxStatus();
					rowData[15] = objInvestorDetailsBO.getOccupationCode();
					rowData[16] = "02";
					rowData[19] = objInvestorDetailsBO.getPlaceOfBirth();
					rowData[20] = objInvestorDetailsBO.getCountryOfBirthCode();
					rowData[21] = objInvestorDetailsBO.getAddress1();
					rowData[22] = objInvestorDetailsBO.getAddress2();
					rowData[23] = objInvestorDetailsBO.getAddress3();
					rowData[24] = objInvestorDetailsBO.getCity();
					rowData[25] = objInvestorDetailsBO.getDistrict();
					rowData[26] = objInvestorDetailsBO.getState();
					rowData[27] = objInvestorDetailsBO.getCountry();
					rowData[28] = objInvestorDetailsBO.getPinCode();
					rowData[29] = "02";
					rowData[30] = "01";
					if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
						rowData[31] = new Double(objInvestorDetailsBO.getcKYCRefId1());
						
					writer.addRecord(rowData);
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
	}
        
        public void writeCampsDBF2(int fieldSize, ArrayList<TransactionMainBO> arryTransactionMainBO, DBFWriter writer, PreparedStatement setPreparedFeedUpdate) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		//logger.info(" Camps DBF Begin ");
		int intMainBORowSize		=	0;
		String remarksCAMS			=	"";
		String paidThrough			=	"";
	
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		TransactionBO objTransactionBO			=	new TransactionBO();
		
		intMainBORowSize		=	arryTransactionMainBO.size(); 
		
	
		
		//logger.info(intMainBORowSize+" ======= ");
		for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
		{
			try{
				Object rowData[] = new Object[fieldSize];
				objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
				
				if(objTransactionMainBO!=null)
				{
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objTransactionBO		=	objTransactionMainBO.getTransactionBO();
					objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
					objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
					//logger.info("Camps file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
					
					/*
					 * for CAMS remarks
						PGID HDFC    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID TPSL    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID DD       |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID ECS     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						3262 NEFT   |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						PGID SW     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID Red     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
					 */
					paidThrough	=	objTransactionBO.getPaidThru();
					if(paidThrough.equalsIgnoreCase("NEFT"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"    |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if((paidThrough.equals(""))&&(objTransactionBO.getTransactionType().equals("SI")||objTransactionBO.getTransactionType().equals("SO")))
						remarksCAMS	=	"66 SW"+"        |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equals("")&&(objTransactionBO.getTransactionType().equals("R")))
						remarksCAMS	=	"77 Red"+"       |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equalsIgnoreCase("DD"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" DD      |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else if(paidThrough.equalsIgnoreCase("ECS"))
						remarksCAMS	=	objTransactionBO.getPaymentId()+" ECS     |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
					else
						remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"    |"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
	
					rowData[0] = objTransactionBO.getAmcCode(); //AMCCode
					rowData[1] = "WEALTH";//objTransactionBO.getBrokerCode(); // Broker Code
					rowData[2] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
					rowData[3] = objInvestorDetailsBO.getUserCode(); // User code
					rowData[4] = new Double(objTransactionBO.getUserTransactionNo()); // Transaction No
					rowData[5] = objTransactionBO.getApplNo(); // Application No
					rowData[6] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo()); // Folio No				
					rowData[7] = ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No()); // Cheque degit No
					rowData[8] = objTransactionBO.getTransactionType();
					rowData[9] = objTransactionBO.getSchemeCode();
					rowData[10] = objInvestorDetailsBO.getFirstName();
					rowData[11] = objInvestorDetailsBO.getJointName1();
					rowData[12] = objInvestorDetailsBO.getJointName2();
					rowData[13] = objInvestorDetailsBO.getAddress1();
					rowData[14] = objInvestorDetailsBO.getAddress2();
					rowData[15] = objInvestorDetailsBO.getAddress3();
					rowData[16] = objInvestorDetailsBO.getCity();
					rowData[17] = objInvestorDetailsBO.getPinCode();
					rowData[18] = objInvestorDetailsBO.getPhoneOffice();
					rowData[19] = (Date)objTransactionBO.getTransactionDate().getDateFirstView();
					rowData[20] = objTransactionBO.getTransactionTime();
					//logger.info("Data Writer : "+(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))+" == "+objAccountDetailsBO.getClos_Ac_Ch());
					if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
					{
						rowData[21] = new Double(0.0d);
						rowData[22] = new Double(0.0d);
					}
					else
					{
						rowData[21] = objTransactionBO.getUnits();
						rowData[22] = objTransactionBO.getAmount().doubleValue();
					}
					rowData[23] = objAccountDetailsBO.getClos_Ac_Ch();
					try{
					rowData[24] = (Date)objInvestorDetailsBO.getDateOfBirth().getDateFirstView();
					}catch(Exception e){}
					rowData[25] = objInvestorDetailsBO.getGuardianName();
					rowData[26] = objInvestorDetailsBO.getTaxNo();
					rowData[27] = objInvestorDetailsBO.getPhoneRes();
					rowData[28] = objInvestorDetailsBO.getFaxOff();
					rowData[29] = objInvestorDetailsBO.getFaxRes();
					rowData[30] = objInvestorDetailsBO.getEmail();
					rowData[31] = objAccountDetailsBO.getAccountNo();
					rowData[32] = objAccountDetailsBO.getAccountType();
					rowData[33] = objAccountDetailsBO.getBankName();
					rowData[34] = objAccountDetailsBO.getBranchName();
					rowData[35] = objAccountDetailsBO.getBankCity();
					rowData[36] = objAccountDetailsBO.getReinv_Tag();
					rowData[37] = objAccountDetailsBO.getHoldingNature();
					rowData[38] = objInvestorDetailsBO.getOccupationCode();
					rowData[39] = objAccountDetailsBO.getTaxStatus();
					
					// ---- New Fields ----
					rowData[40] = remarksCAMS;
					
					if((objInvestorDetailsBO.getState().equals(""))&&(objInvestorDetailsBO.getNRIInvestor()==1))
						rowData[41] = "OV";
					else
						rowData[41] = objInvestorDetailsBO.getState();
					
					rowData[42] = objTransactionBO.getSubTransactionType();
					rowData[43] = objAccountDetailsBO.getPaymentMechanism();
					
					if(!objAccountDetailsBO.getMICR_CD().equals(""))
						rowData[44] = new Double(objAccountDetailsBO.getMICR_CD());
					rowData[45] = objAccountDetailsBO.getBank_Code();
					
					if(!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals(""))
						rowData[46] = new Double(objAccountDetailsBO.getALT_Folio());
					
					rowData[47] = objAccountDetailsBO.getALT_Broker();
					rowData[48] = objInvestorDetailsBO.getLocationCode();
					rowData[49] = objAccountDetailsBO.getPaymentMechanism();
					rowData[50] = objTransactionBO.getPricing();
					rowData[51] = objInvestorDetailsBO.getPanHolder2();
					rowData[52] = objInvestorDetailsBO.getPanHolder3();
					rowData[53] = objAccountDetailsBO.getNomineeName();
					rowData[54] = objAccountDetailsBO.getNomineeRelation(); 
					rowData[55] = objInvestorDetailsBO.getGuardianPanNo();
					rowData[56] = objAccountDetailsBO.getInstrmno();
					rowData[57] = objAccountDetailsBO.getUINNo();
					rowData[58] = objInvestorDetailsBO.getValid_Pan();
					rowData[59] = objInvestorDetailsBO.getGValid_Pan();
					rowData[60] = objInvestorDetailsBO.getJH1ValidPan();
					rowData[61]	= objInvestorDetailsBO.getJH2ValidPan();
					/*if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
						rowData[62] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();
					
					if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
						rowData[63] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();
					
					if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
						rowData[64] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();*/
					
					rowData[65] = "";
					rowData[66] = "";//objAccountDetailsBO.getSipReferenceNo();
					if(objTransactionBO.getSIP_Reg_Date()!=null)
						rowData[67] = objTransactionBO.getSIP_Reg_Date().getDateFirstView(); 
					rowData[68] = objAccountDetailsBO.getFirst_Hldr_Min();
					rowData[69] = objAccountDetailsBO.getJH1_Min();
					rowData[70] = objAccountDetailsBO.getJH2_Min();
					rowData[71] = objAccountDetailsBO.getGuardian_Min();
					rowData[72] = objAccountDetailsBO.getNEFT_CD();
					rowData[73] = objAccountDetailsBO.getRTGS_CD();
					rowData[74] = "E";//EMAIL_ACST
					rowData[75] = objInvestorDetailsBO.getMobileNo();
					rowData[76] = objTransactionBO.getDpId(); // Dip_Id
					// ---- New Fields ----
					rowData[77] = "N"; // POA_Type
					rowData[78] = "W"; // Trxn Mode
					rowData[79] = "Y"; // Trxn_sign_ver
					
					if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
					{
						rowData[80] = "NRI Ad: "+objInvestorDetailsBO.getNRIAddress1();
						rowData[81] = objInvestorDetailsBO.getNRIAddress2();
						rowData[82] = objInvestorDetailsBO.getNRIAddress3();
						
						if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
							rowData[83] = objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState();
						else
							rowData[83] = objInvestorDetailsBO.getNRICity();
						
						rowData[84] = "";//objInvestorDetailsBO.getNRIState();
						rowData[85] = objInvestorDetailsBO.getNRICountry();
						rowData[86] = objInvestorDetailsBO.getPinCode();
						rowData[88] = ""; // Nom2_Name
						rowData[89] = ""; // Nom2_Relationship
						//rowData[90] = new Double(0.0d); // Nom2_Applicable -  Number -  5,2
						rowData[91] = ""; // Nom3_Name
						rowData[92] = ""; // Nom3_Relationship
						//rowData[93] = new Double(0.0d); // Nom3_Applicable -  Number -  5,2
						rowData[94] = "Y"; // Check_Flag
						rowData[97] = "Y"; // FIRC_STA 
	
					}
					else
						rowData[94] = "N"; // Check_Flag
					
					if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
						rowData[87] = new Double(objAccountDetailsBO.getNomineePercent()); //Nom1_Applicable -	Number -  5,0
					
					if(objTransactionBO.getTransactionType().equals("P"))
					{
						rowData[95] = "N"; // TH_P_PYT ( Third Party Payment)
						rowData[96] = objInvestorDetailsBO.getKycFlag(); // KYC
					}
					else
					{
						rowData[95] = ""; // TH_P_PYT ( Third Party Payment)
						rowData[96] = objInvestorDetailsBO.getKycFlag(); // KYC
						rowData[97] = ""; // FIRC_STA 
					}
					
					
					if((objTransactionBO.getSipStartDate() !=null)&&(objTransactionBO.getSipEndDate() != null)){
						rowData[98] = new Double(objTransactionBO.getSipReferenceId());
						rowData[99] = new Double(objTransactionBO.getNoOfInstalment());
						rowData[100] = objTransactionBO.getSipFrequency();
						rowData[101] = objTransactionBO.getSipStartDate().getDateFirstView();
						rowData[102] = objTransactionBO.getSipEndDate().getDateFirstView();
						rowData[103] = new Double(objTransactionBO.getPaidInstallments());
					}
					
					rowData[113] = "N"; // First Holder PAN Exempt
					rowData[114] = "N"; // JH1 PAN Exempt
					rowData[115] = "N"; // JH2 PAN Exempt
					rowData[116] = "N"; // Guardian PAN Exempt
					
					rowData[125] = objTransactionBO.getEuinOpted();
					rowData[126] = objTransactionBO.getEuinId();
					rowData[127] = objAccountDetailsBO.getNomineeOpted();
					rowData[128] = objInvestorDetailsBO.getSubBrokerCode(); // Subbroker Code
				
					rowData[129] = "Y";
					
					if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
						rowData[131] = new Double(objInvestorDetailsBO.getcKYCRefId1());//Dummy4
					
					if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
						rowData[132] = new Double(objInvestorDetailsBO.getcKYCRefId2());//Dummy5
					
					if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
						rowData[133] = new Double(objInvestorDetailsBO.getcKYCRefId3());//Dummy6
					
					if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
						rowData[134] = new Double(objInvestorDetailsBO.getcKYCRefIdG());//Dummy7
					
					if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
						rowData[135] = (Date)objInvestorDetailsBO.getDateOfBirth2().getDateFirstView();//Dummy8
					
					if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
						rowData[136] = (Date)objInvestorDetailsBO.getDateOfBirth3().getDateFirstView();//Dummy9
					
					if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
						rowData[137] = (Date)objInvestorDetailsBO.getDateOfBirthG().getDateFirstView();//Dummy10
					
					
					
				/*	
				 * rowData[107] = objInvestorDetailsBO.getKycFlag2();//KYC_Type_Second Holder
					rowData[110] = objInvestorDetailsBO.getKycFlag3();//KYC_Type_Third Holder
					rowData[111] = objInvestorDetailsBO.getKycFlagG();//KYC_Type_Gaurdian
					
				 	rowData[136] = new Double(objInvestorDetailsBO.getcKYCRefId1());
					rowData[137] = new Double(objInvestorDetailsBO.getcKYCRefId2());
					rowData[138] = new Double(objInvestorDetailsBO.getcKYCRefId3());
					rowData[139] = new Double(objInvestorDetailsBO.getcKYCRefIdG());*/
					
					/*if(objInvestorDetailsBO.getKycFlag().equals("C"))
						rowData[140] = "N";
					else
						rowData[140] = "Y";
					
					if(objInvestorDetailsBO.getKycFlag2().equals("C"))
						rowData[141] = "N";
					else
						rowData[141] = "Y";
					
					if(objInvestorDetailsBO.getKycFlag3().equals("C"))
						rowData[142] = "N";
					else
						rowData[142] = "Y";
					
					if(objInvestorDetailsBO.getKycFlagG().equals("C"))
						rowData[143] = "N";
					else
						rowData[143] = "Y";*/
					
					//for (Object i : rowData) {
					//	pipe.append(DELIMETER).append(i);
					 //  DELIMETER = "|";
			//	}
					writer.addRecord(rowData);
					//Based on the UserTransactionNo Update the ForwardFeed - date
					//setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
					//setPreparedFeedUpdate.addBatch();
				}
			}catch(Exception e){
				logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
			}
		}
		//int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
		//logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : CAMSBatchUpdate = " + totalTime);
		//setPreparedFeedUpdate.clearBatch();
	}
        
    public void writeCampsTXT2(ArrayList<TransactionMainBO> arryTransactionMainBO, PreparedStatement setPreparedFeedUpdate, String fileName) throws Exception
	{
		 	int intMainBORowSize		=	0;
			String remarksCAMS			=	"";
			String paidThrough			=	"";
			String DELIMETER = "|";
			StringBuffer pipe = new StringBuffer();
		
			TransactionMainBO objTransactionMainBO	=	new TransactionMainBO();
			InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
			AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
			TransactionBO objTransactionBO			=	new TransactionBO();
			
			intMainBORowSize		=	arryTransactionMainBO.size(); 
		 	try {
		 		
		 		File file = new File(fileName);
		 	    OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(file, true),"ASCII");
		        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		        
				//logger.info(intMainBORowSize+" ======= ");
				for(int DBFRow=0;DBFRow<intMainBORowSize;DBFRow++)
				{
					try{
						String text	=	"";
						objTransactionMainBO	=	arryTransactionMainBO.get(DBFRow);
						
						if(objTransactionMainBO!=null)
						{
							pipe = new StringBuffer();
							objTransactionBO		=	objTransactionMainBO.getTransactionBO();
							objTransactionBO		=	objTransactionMainBO.getTransactionBO();
							objInvestorDetailsBO	=	objTransactionMainBO.getInvestorDetailsBO();
							objAccountDetailsBO		=	objTransactionMainBO.getAccountDetailsBO();
							//logger.info("Camps file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
							
							paidThrough	=	objTransactionBO.getPaidThru();
							if(paidThrough.equalsIgnoreCase("NEFT"))
								remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if((paidThrough.equals(""))&&(objTransactionBO.getTransactionType().equals("SI")||objTransactionBO.getTransactionType().equals("SO")))
								remarksCAMS	=	"66 SW"+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if(paidThrough.equals("")&&(objTransactionBO.getTransactionType().equals("R")))
								remarksCAMS	=	"77 Red"+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$72.32.69.244$72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if(paidThrough.equalsIgnoreCase("DD"))
								remarksCAMS	=	objTransactionBO.getPaymentId()+" DD-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else if(paidThrough.equalsIgnoreCase("ECS"))
								remarksCAMS	=	objTransactionBO.getPaymentId()+" ECS-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
							else
								remarksCAMS	=	objTransactionBO.getPaymentId()+" "+paidThrough+"-"+objTransactionBO.getTransactionDate().getDateMonthView().replace("-","")+"$"+objTransactionBO.getTransactionTime()+"$"+objTransactionBO.getIPAddress()+"$"+"72.32.69.244$"+objTransactionBO.getUserTransactionNo();
		
							pipe.append(objTransactionBO.getAmcCode()).append(DELIMETER)//1
							.append("WEALTH").append(DELIMETER).//2
							append(objInvestorDetailsBO.getSubBrokerCode()).append(DELIMETER)//3
							.append(objInvestorDetailsBO.getUserCode()).append(DELIMETER).//4
							append(objTransactionBO.getUserTransactionNo()).append(DELIMETER)//5
							.append(objTransactionBO.getApplNo()).append(DELIMETER).//6
							append(ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo())).append(DELIMETER)//7
							.append(ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())).append(DELIMETER)//8
							.append(objTransactionBO.getTransactionType()).append(DELIMETER)//9
							.append(objTransactionBO.getSchemeCode()).append(DELIMETER);//10
							
							text	=	(objInvestorDetailsBO.getFirstName().length()>=70)? objInvestorDetailsBO.getFirstName().substring(0,70) : objInvestorDetailsBO.getFirstName();
							pipe.append(text).append(DELIMETER);//11
							
							text	=	(objInvestorDetailsBO.getJointName1().length()>=35)? objInvestorDetailsBO.getJointName1().substring(0,35) : objInvestorDetailsBO.getJointName1();
							pipe.append(text).append(DELIMETER);//12
							
							text	=	(objInvestorDetailsBO.getJointName2().length()>=35)? objInvestorDetailsBO.getJointName2().substring(0,35) : objInvestorDetailsBO.getJointName2();
							pipe.append(text).append(DELIMETER);//13
							
							text	=	(objInvestorDetailsBO.getAddress1().length()>=40)? objInvestorDetailsBO.getAddress1().substring(0,40) : objInvestorDetailsBO.getAddress1();
							pipe.append(text).append(DELIMETER);//14
							
							text	=	(objInvestorDetailsBO.getAddress2().length()>=40)? objInvestorDetailsBO.getAddress2().substring(0,40) : objInvestorDetailsBO.getAddress2();
							pipe.append(text).append(DELIMETER);//15
							
							text	=	(objInvestorDetailsBO.getAddress3().length()>=40)? objInvestorDetailsBO.getAddress3().substring(0,40) : objInvestorDetailsBO.getAddress3();
							pipe.append(text).append(DELIMETER);//16
							
							text	=	(objInvestorDetailsBO.getCity().length()>=35)? objInvestorDetailsBO.getCity().substring(0,35) : objInvestorDetailsBO.getCity();
							pipe.append(text).append(DELIMETER);//17
							
							text	=	(objInvestorDetailsBO.getPinCode().length()>=15)? objInvestorDetailsBO.getPinCode().substring(0,15) : objInvestorDetailsBO.getPinCode();
							pipe.append(text).append(DELIMETER);//18
							
							pipe.append(objInvestorDetailsBO.getPhoneOffice()).append(DELIMETER)//19
							.append(objTransactionBO.getTransactionDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//20
							.append(objTransactionBO.getTransactionTime()).append(DELIMETER);//21
							
							if(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))
							{
								pipe.append(new Double(0.00000000d)).append(DELIMETER)//22
								.append(new Double(0.00000000d)).append(DELIMETER);//23
							}
							else
							{
								pipe.append(ConstansBO.getEightDecimal(objTransactionBO.getUnits())).append(DELIMETER)//22
								.append(ConstansBO.getEightDecimal(objTransactionBO.getAmount().doubleValue())).append(DELIMETER);//23
							}
							pipe.append(objAccountDetailsBO.getClos_Ac_Ch()).append(DELIMETER);//24
							
							try{
								pipe.append(objInvestorDetailsBO.getDateOfBirth().getMonthFirstViewRequiredFormat()).append(DELIMETER);//25
							}catch(Exception e){
								pipe.append("").append(DELIMETER);//25
							}
							
							text	=	(objInvestorDetailsBO.getGuardianName().length()>=120)? objInvestorDetailsBO.getGuardianName().substring(0,120) : objInvestorDetailsBO.getGuardianName();
							pipe.append(text).append(DELIMETER);//26
							
							pipe.append(objInvestorDetailsBO.getTaxNo()).append(DELIMETER)//27
							.append(objInvestorDetailsBO.getPhoneRes()).append(DELIMETER)//28
							.append(objInvestorDetailsBO.getFaxOff()).append(DELIMETER)//29
							.append(objInvestorDetailsBO.getFaxRes()).append(DELIMETER);//30
							
							text	=	(objInvestorDetailsBO.getEmail().length()>=128)? objInvestorDetailsBO.getEmail().substring(0,128) : objInvestorDetailsBO.getEmail();
							pipe.append(text).append(DELIMETER);//31
							
							pipe.append( objAccountDetailsBO.getAccountNo()).append(DELIMETER)//32
							.append(objAccountDetailsBO.getAccountType()).append(DELIMETER);//33
							
							text	=	(objAccountDetailsBO.getBankName().length()>=40)? objAccountDetailsBO.getBankName().substring(0,40) : objAccountDetailsBO.getBankName();
							pipe.append(text).append(DELIMETER);//34
							
							text	=	(objAccountDetailsBO.getBranchName().length()>=40)? objAccountDetailsBO.getBranchName().substring(0,40) : objAccountDetailsBO.getBranchName();
							pipe.append(text).append(DELIMETER);//35
							
							text	=	(objAccountDetailsBO.getBankCity().length()>=35)? objAccountDetailsBO.getBankCity().substring(0,35) : objAccountDetailsBO.getBankCity();
							pipe.append(text).append(DELIMETER);//36
							
							pipe.append(objAccountDetailsBO.getReinv_Tag()).append(DELIMETER)//37
							.append(objAccountDetailsBO.getHoldingNature()).append(DELIMETER)//38
							.append(objInvestorDetailsBO.getOccupationCode()).append(DELIMETER)//39
							.append(objAccountDetailsBO.getTaxStatus()).append(DELIMETER);//40
							
							text	=	(remarksCAMS.length()>=254)? remarksCAMS.substring(0,254) : remarksCAMS;
							pipe.append(text).append(DELIMETER);//41
							
							if((objInvestorDetailsBO.getState().equals(""))&&(objInvestorDetailsBO.getNRIInvestor()==1))
								pipe.append("OV").append(DELIMETER);//42
							else
								pipe.append(objInvestorDetailsBO.getState()).append(DELIMETER);//42
							
							pipe.append(objTransactionBO.getSubTransactionType()).append(DELIMETER)//43
							.append(objAccountDetailsBO.getPaymentMechanism()).append(DELIMETER);//44
							
							if(!objAccountDetailsBO.getMICR_CD().equals(""))
								pipe.append(objAccountDetailsBO.getMICR_CD()).append(DELIMETER);//45
							else
								pipe.append("").append(DELIMETER);//45
							
							pipe.append(objAccountDetailsBO.getBank_Code()).append(DELIMETER);//46
							
							if(!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals(""))
								pipe.append(objAccountDetailsBO.getALT_Folio()).append(DELIMETER);//47
							else
								pipe.append("").append(DELIMETER);//47
							
							pipe.append(objAccountDetailsBO.getALT_Broker()).append(DELIMETER);//48
							
							text	=	(objInvestorDetailsBO.getLocationCode().length()>=10)? objInvestorDetailsBO.getLocationCode().substring(0,10) : objInvestorDetailsBO.getLocationCode();
							pipe.append(text).append(DELIMETER);//49
							
							pipe.append(objAccountDetailsBO.getPaymentMechanism()).append(DELIMETER)//50
							.append(objTransactionBO.getPricing()).append(DELIMETER)//51
							.append(objInvestorDetailsBO.getPanHolder2()).append(DELIMETER)//52
							.append(objInvestorDetailsBO.getPanHolder3()).append(DELIMETER);//53
							
							text	=	(objAccountDetailsBO.getNomineeName().length()>=40)? objAccountDetailsBO.getNomineeName().substring(0,40) : objAccountDetailsBO.getNomineeName();
							pipe.append(text).append(DELIMETER);//54
							
							text	=	(objAccountDetailsBO.getNomineeRelation().length()>=40)? objAccountDetailsBO.getNomineeRelation().substring(0,40) : objAccountDetailsBO.getNomineeRelation();
							pipe.append(text).append(DELIMETER);//55
							
							pipe.append(objInvestorDetailsBO.getGuardianPanNo()).append(DELIMETER)//56
							.append(objAccountDetailsBO.getInstrmno()).append(DELIMETER)//57
							.append(objAccountDetailsBO.getUINNo()).append(DELIMETER)//58
							.append(objInvestorDetailsBO.getValid_Pan()).append(DELIMETER)//59
							.append(objInvestorDetailsBO.getGValid_Pan()).append(DELIMETER)//60
							.append(objInvestorDetailsBO.getJH1ValidPan()).append(DELIMETER)//61
							.append(objInvestorDetailsBO.getJH2ValidPan()).append(DELIMETER)//62
							//.append(objAccountDetailsBO.getForm6061())
							.append(DELIMETER)//63
							//.append(objAccountDetailsBO.getForm6061J1())
							.append(DELIMETER)//64
							//.append(objAccountDetailsBO.getForm6061J2())
							.append(DELIMETER)//65
							//.append(objAccountDetailsBO.getGF6061RE())
							.append(DELIMETER)//66
							.append("").append(DELIMETER);//67
					
							try{
								if(objTransactionBO.getSIP_Reg_Date() !=null)
									pipe.append(objTransactionBO.getSIP_Reg_Date().getMonthFirstViewRequiredFormat()).append(DELIMETER); //68
								else
									pipe.append("").append(DELIMETER);//68
							}catch(Exception e){
								pipe.append("").append(DELIMETER);//68
							}
							pipe.append(objAccountDetailsBO.getFirst_Hldr_Min()).append(DELIMETER)//69
							.append(objAccountDetailsBO.getJH1_Min()).append(DELIMETER)//70
							.append(objAccountDetailsBO.getJH2_Min()).append(DELIMETER)//71
							.append(objAccountDetailsBO.getGuardian_Min()).append(DELIMETER)//72
							.append(objAccountDetailsBO.getNEFT_CD()).append(DELIMETER)//73
							.append(objAccountDetailsBO.getRTGS_CD()).append(DELIMETER)//74
							.append("E").append(DELIMETER)//75
							.append(objInvestorDetailsBO.getMobileNo()).append(DELIMETER)//76
							.append(objTransactionBO.getDpId()).append(DELIMETER)//77
							.append("N").append(DELIMETER)//78
							.append("W").append(DELIMETER)//79
							.append("Y").append(DELIMETER);//80
							
							if(objInvestorDetailsBO.getNRIInvestor()==1)// For NRI Investor send NRI Addr. detials
							{
								text	=	(objInvestorDetailsBO.getNRIAddress1().length()>=40)? objInvestorDetailsBO.getNRIAddress1().substring(0,40) : objInvestorDetailsBO.getNRIAddress1();
								pipe.append("NRI Ad: "+text).append(DELIMETER);//81
								
								text	=	(objInvestorDetailsBO.getNRIAddress2().length()>=40)? objInvestorDetailsBO.getNRIAddress2().substring(0,40) : objInvestorDetailsBO.getNRIAddress2();
								pipe.append(text).append(DELIMETER);//82
								
								text	=	(objInvestorDetailsBO.getNRIAddress3().length()>=40)? objInvestorDetailsBO.getNRIAddress3().substring(0,40) : objInvestorDetailsBO.getNRIAddress3();
								pipe.append(text).append(DELIMETER);//83
								
								if(!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState()))
									text	=	((objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()).length()>=35)? (objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState()).substring(0,35) : (objInvestorDetailsBO.getNRICity()+" "+objInvestorDetailsBO.getNRIState());
								else
									text	=	((objInvestorDetailsBO.getNRICity()).length()>=35)? (objInvestorDetailsBO.getNRICity()).substring(0,35) : (objInvestorDetailsBO.getNRICity());
										
								pipe.append(text.trim()).append(DELIMETER)//84
								.append(DELIMETER);//85
								
								text	=	(objInvestorDetailsBO.getNRICountry().length()>=35)? objInvestorDetailsBO.getNRICountry().substring(0,35) : objInvestorDetailsBO.getNRICountry();
								pipe.append(text.trim()).append(DELIMETER);//86
								
								pipe.append(objInvestorDetailsBO.getPinCode()).append(DELIMETER);//87
								
								if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
									pipe.append(objAccountDetailsBO.getNomineePercent()).append(DELIMETER);//88
								else 
									pipe.append("").append(DELIMETER);//88
								
								pipe.append("").append(DELIMETER)//89
								.append("").append(DELIMETER)//90
								.append("").append(DELIMETER)//91
								.append("").append(DELIMETER)//92
								.append("").append(DELIMETER)//93
								.append("").append(DELIMETER)//94
								.append("Y").append(DELIMETER);//95
								if(objTransactionBO.getTransactionType().equals("P"))
							    {
									pipe.append("N").append(DELIMETER)//96
									.append(objInvestorDetailsBO.getKycFlag()).append(DELIMETER);//97
								
							    }
								else
								{
									pipe.append("").append(DELIMETER)//96
									.append(objInvestorDetailsBO.getKycFlag()).append(DELIMETER);//97
							    }
								pipe.append("Y").append(DELIMETER); //98
							}
							else
							{
								pipe.append("").append(DELIMETER)//81
								.append("").append(DELIMETER)//82
								.append("").append(DELIMETER)//83
								.append("").append(DELIMETER)//84
								.append("").append(DELIMETER)//85
								.append("").append(DELIMETER)//86
								.append("").append(DELIMETER);//87
								if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
									pipe.append(objAccountDetailsBO.getNomineePercent()).append(DELIMETER);//88
								else
									pipe.append("").append(DELIMETER);//88
								
								pipe.append("").append(DELIMETER)//89
									.append("").append(DELIMETER)//90
									.append("").append(DELIMETER)//91
									.append("").append(DELIMETER)//92
									.append("").append(DELIMETER)//93
									.append("").append(DELIMETER)//94
								    .append("N").append(DELIMETER);//95
								if(objTransactionBO.getTransactionType().equals("P"))
								{
									pipe.append("N").append(DELIMETER)//96
								  	.append("Y").append(DELIMETER);//97
							
								}
								else
								{
							    	 pipe.append("").append(DELIMETER)//96
							    	  	.append("Y").append(DELIMETER);//97
								
								}
								pipe.append("").append(DELIMETER);//98
							}
							
							//if(objAccountDetailsBO.getNomineePercent()>0) // if Nominee is available then 100% goes to that nominee
								//pipe.append(objAccountDetailsBO.getNomineePercent()).append(DELIMETER);
							//else
								//pipe.append("").append(DELIMETER);
							
							/*if(objTransactionBO.getTransactionType().equals("P"))
							{
								pipe.append("N").append(DELIMETER)
								.append("Y").append(DELIMETER)
								.append("").append(DELIMETER);
							}
							else
							{
								pipe.append("").append(DELIMETER)
								.append("Y").append(DELIMETER)
								.append("").append(DELIMETER); 
							}*/

							try{
								if((objTransactionBO.getSipStartDate() !=null)&&(objTransactionBO.getSipEndDate() != null)){
									pipe.append(objTransactionBO.getSipReferenceId()).append(DELIMETER)//99
									.append(objTransactionBO.getNoOfInstalment()).append(DELIMETER)//100
									.append("OM").append(DELIMETER)//101
									.append(objTransactionBO.getSipStartDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//102
									.append(objTransactionBO.getSipEndDate().getMonthFirstViewRequiredFormat()).append(DELIMETER)//103
									.append(objTransactionBO.getPaidInstallments()).append(DELIMETER);//104
								}
								else
									pipe.append("").append(DELIMETER)//99
									.append("").append(DELIMETER)//100
									.append("").append(DELIMETER)//101
									.append("").append(DELIMETER)//102
									.append("").append(DELIMETER)//103
									.append("").append(DELIMETER);//104
							}catch(Exception e){
								pipe.append("").append(DELIMETER)//99
								.append("").append(DELIMETER)//100
								.append("").append(DELIMETER)//101
								.append("").append(DELIMETER)//102
								.append("").append(DELIMETER)//103
								.append("").append(DELIMETER);//104
							}
							
							pipe.append("").append(DELIMETER)//105
							.append("").append(DELIMETER)//106
							.append("").append(DELIMETER)//107
							.append("").append(DELIMETER)//108
							.append("").append(DELIMETER)//109
							.append("").append(DELIMETER)//110
							.append("").append(DELIMETER)//111
							.append("").append(DELIMETER)//112
							.append("").append(DELIMETER)//113
							.append("N").append(DELIMETER)//114
							.append("N").append(DELIMETER)//115
							.append("N").append(DELIMETER)//116
							.append("N").append(DELIMETER)//117
							.append("").append(DELIMETER)//118
							.append("").append(DELIMETER)//119
							.append("").append(DELIMETER)//120
							.append("").append(DELIMETER)//121
							.append("").append(DELIMETER)//122
							.append("").append(DELIMETER)//123
							.append("").append(DELIMETER)//124
							.append("").append(DELIMETER)//125
							.append(objTransactionBO.getEuinOpted()).append(DELIMETER)//126
							.append(objTransactionBO.getEuinId()).append(DELIMETER)//127
							.append(objAccountDetailsBO.getNomineeOpted()).append(DELIMETER)//128
							.append(objInvestorDetailsBO.getSubBrokerCode()).append(DELIMETER)//129
							.append("Y").append(DELIMETER) //BankMandat 130 -- Bala default value
							.append(DELIMETER); //TO_SCHEME 131
							
							if(!objInvestorDetailsBO.getcKYCRefId1().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId1()).append(DELIMETER);//FHLD_CKYC //132
							else
								pipe.append(DELIMETER);//Dummy4 // 132
							
							if(!objInvestorDetailsBO.getcKYCRefId2().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId2()).append(DELIMETER);//SHLD_CKYC //133
							else
								pipe.append(DELIMETER);//133
							
							if(!objInvestorDetailsBO.getcKYCRefId3().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefId3()).append(DELIMETER);//THLD_CKYC // 134
							else
								pipe.append(DELIMETER);//134
								
							if(!objInvestorDetailsBO.getcKYCRefIdG().equals(""))
								pipe.append(objInvestorDetailsBO.getcKYCRefIdG()).append(DELIMETER);//GURD_CKYC // 135
							else
								pipe.append(DELIMETER);//135
						
							if(objInvestorDetailsBO.getDateOfBirth2() !=  null)
								pipe.append(objInvestorDetailsBO.getDateOfBirth2().getMonthFirstViewRequiredFormat()).append(DELIMETER);//136
							else
								pipe.append(DELIMETER); // 136
							
							if(objInvestorDetailsBO.getDateOfBirth3() !=  null)
								pipe.append(objInvestorDetailsBO.getDateOfBirth3().getMonthFirstViewRequiredFormat()).append(DELIMETER);//137
							else
								pipe.append(DELIMETER); //137
							
							if(objInvestorDetailsBO.getDateOfBirthG() !=  null)
								pipe.append(objInvestorDetailsBO.getDateOfBirthG().getMonthFirstViewRequiredFormat()).append(DELIMETER);//138
							else
								pipe.append(DELIMETER); //138
							
							
							pipe.append("\r\n");
							bufferedWriter.write(pipe.toString());
							//bufferedWriter.newLine();
	
							//Based on the UserTransactionNo Update the ForwardFeed - date
				//			setPreparedFeedUpdate.setString(1, objTransactionBO.getUserTransactionNo());
				//			setPreparedFeedUpdate.addBatch();
						}
					}catch(Exception e){
						logger.info("Exp while file writing.. row:"+DBFRow+" exp:"+e);
					}
				}
                                
				if(bufferedWriter != null) bufferedWriter.flush();
		        if(bufferedWriter != null) bufferedWriter.close();
		 		if(fileWriter != null) fileWriter.close();

		 		//logger.info("File Created successfully : "+fileName);
                                
                            //int insertedRows[] = setPreparedFeedUpdate.executeBatch();
                            //logger.info("AMC:" + objTransactionBO.getAmcCode() + " Rows:" + insertedRows.length);
                            //long totalTime = System.currentTimeMillis() - startTime;
                            //logger.info("TIMER : CAMSBatchUpdate = " + totalTime);
                            //setPreparedFeedUpdate.clearBatch();
                
		 	} catch (Exception e1) {
		 	  logger.info("Exp while file Creation : "+e1);
		 	}
			//int insertedRows[]	=	setPreparedFeedUpdate.executeBatch();
			//logger.info("AMC:"+objTransactionBO.getAmcCode()+" Rows:"+insertedRows.length);
			//setPreparedFeedUpdate.clearBatch();
	}
        
        
}