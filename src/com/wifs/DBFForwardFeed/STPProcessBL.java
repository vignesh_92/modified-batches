package com.wifs.DBFForwardFeed;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.wifs.utilities.PropertiesConstant;
import com.wifs.utilities.PropertyFileReader;
import com.wifs.utilities.SendMail;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Scanner;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class STPProcessBL {
	
	public static Logger logger=Logger.getLogger(new Object().getClass().getName());
        public static BackgroundThreadProcess bkThreadProc = new BackgroundThreadProcess();

	public STPProcessBO FranklinSTPProcess(int rowSize, String sourcePath, String targetPath, String stpURL, StringBody stpUser, StringBody stpPswd) throws Exception {
	//public static void main(String[] args) throws Exception {

		//logger.info("Batch Started...");
		String responseAsString	=	"";
		String fileNameForText  	=	"";
		String fileNameForXML  	=	"";
		
		//String stpURL = "https://online.franklintempletonindia.com/cpartnersstp/ResponseUpload.asp";
		//String sourcePath		=	"C:\\FT";
		//String targetPath		=	"C:\\WIFS_LOGS\\JBoss\\wifs\\STPDBFForwardFeed";
		//StringBody stpUser			=	new StringBody("fundsindiastp0314");
		//StringBody stpPswd			=	new StringBody("W6$0qqNx");
		//int rowSize				=	66;
		
		STPProcessBO objSTPProcessBO	=	new STPProcessBO();
		STPProcessBL objSTPProcessBL	=	new STPProcessBL();
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(stpURL);
        try {
            if(!(new File(targetPath+File.separator)).exists()){
    			new File(targetPath+File.separator).mkdirs();
    		}
    		
    		File objFile	= 	new File(sourcePath+File.separator+"FTText");	 
    		String listOfFiles[] 	= objFile.list();
    		
    		for(int i=0;i<listOfFiles.length;i++)
    		{
    			if(listOfFiles[i].startsWith("FTMF")){
    				fileNameForText = listOfFiles[i];
    				fileNameForXML	= fileNameForText.replace(".txt", ".xml");
    			}
    		}
    	    
    	    logger.info("File target Path:"+targetPath+File.separator+fileNameForXML);
    	    logger.info("URL Path:"+stpURL);
    	    logger.info("File source Path:"+sourcePath+File.separator+"FTText"+File.separator+fileNameForText);
    	    
            FileBody input = new FileBody(new File(sourcePath+File.separator+"FTText"+File.separator+fileNameForText));
			//StringBody FUNDIND1 = new StringBody("FUNDIND1");
            //StringBody Password = new StringBody("Welcome123");

            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("File1", input);
            reqEntity.addPart("UserId", stpUser);
            reqEntity.addPart("Password", stpPswd);

            httppost.setEntity(reqEntity);

            logger.info("executing request " + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            responseAsString = EntityUtils.toString(response.getEntity());

            try{
	        	objSTPProcessBL.FranklinXMLResponse(responseAsString, targetPath+File.separator+fileNameForXML);
	        	objSTPProcessBO	=	objSTPProcessBL.FranklinXMLParsing(rowSize,targetPath+File.separator, fileNameForXML);
	        	objSTPProcessBO.setStpProcessFlag(true);
	        }catch(Exception e){
	        	//logger.info("Exp in XML file creation:"+e);
	        	objSTPProcessBO.setStpProcessFlag(false);
	        }
	        
            logger.info("----------------"+responseAsString+"------------------------");
            logger.info(response.getStatusLine());
            if (resEntity != null) {
                logger.info("Response content length: " + resEntity.getContentLength());
            }
            EntityUtils.consume(resEntity);
        } catch(Exception e){
        	logger.error("EXP:"+e);
        	objSTPProcessBO.setStpProcessFlag(false);
        } finally {
            try { 
            	httpclient.getConnectionManager().shutdown(); 
            	httppost.releaseConnection();
            } catch (Exception ignore) {
            	objSTPProcessBO.setStpProcessFlag(false);
            }
        }
		
        //XML Mail
        try{
        	logger.info("XML Mail begin");
        	ConstansBO objConstansBO	=	new ConstansBO("");
        	SendMail objSendMail	=	new SendMail();
        	
        	if(objSTPProcessBO.isStpProcessFlag()){
        		objSendMail.sendReportFile(targetPath+File.separator+fileNameForXML,sourcePath+File.separator+"FTText"+File.separator+fileNameForText, "FT XML Resut file"+objConstansBO.getTodayDate("MM/dd/yyyy"),"Uploaded success. "+objConstansBO.getTodayDate("MM/dd/yyyy hh:mm:ss a zzz"));
        		logger.info("XML Mail success");
        	}
        	else{
        		logger.info("XML Mail Fails");
        		objSendMail.sendReportFile("","", "Failed FT STP "+objConstansBO.getTodayDate("MM/dd/yyyy"),"Failed FT STP"+objConstansBO.getTodayDate("MM/dd/yyyy hh:mm:ss a zzz"));
        	}
        }catch(Exception e){
        	logger.error("Exp:"+e);
        	objSTPProcessBO.setStpProcessFlag(false);
        }
		//String stpURL = "https://ft.karvy.com/cpartnersstp/ResponseUpload.asp";
		
		//String path = System.getProperty("user.dir");
		//String sourceDir	=	path+File.separator+"FeedFile"+File.separator;
		
	    //String sourcePath = sourceDir+"FTMF0014000120140122061910.txt";
	    //String targetPath	=	sourceDir+"FTMF0014000120140122061910.xml";
		
	   /* 
	    PostMethod post = new PostMethod(stpURL);
	    
		try {
	        
			post.setRequestEntity(new InputStreamRequestEntity(new FileInputStream(input), input.length()));
	        post.setRequestHeader("Content-Type","text/plain");
	        post.setRequestHeader("enctype", "multipart/form-data");
	        post.setRequestHeader("Content-Disposition","form-data; name=\"File1\"; filename=\""+fileNameForText+"\"");
	        post.setParameter("UserID","FUNDIND1");
			post.setParameter("Password","Welcome123");

			ArrayList<NameValuePair> postParameters;
		    httpclient = new DefaultHttpClient();
		    httppost = new HttpPost("your login link");


		    postParameters = new ArrayList<NameValuePair>();
		    postParameters.add(new BasicNameValuePair("param1", "param1_value"));
		    postParameters.add(new BasicNameValuePair("param2", "param2_value"));

		    httppost.setEntity(new UrlEncodedFormEntity(postParameters));

		    HttpResponse response = httpclient.execute(httppost);

			HttpClient httpclient = new HttpClient();

	        int result = httpclient.executeMethod(post);
	        logger.info("Status code: "+result);
	        logger.info("FileName:"+fileNameForText);
	        //logger.info(post.getAuthenticationRealm());
	        //logger.info(" *** Response body *** ");
	        //logger.info(post.getResponseBodyAsString());
	        
	        responseXMLString	=	post.getResponseBodyAsString();
	        
	        
	        
	        logger.info("*** File Response Succes ***");
	    } catch (Exception e) {
	    	//logger.info("Exp:"+e);
	    } */
	    return objSTPProcessBO;
	}
	
	public void FranklinXMLResponse(String xmlString, String targetPath) throws Exception {
		
		PrintWriter xmlOut = null;
		File fXmlFile = new File(targetPath);
		xmlOut = new PrintWriter(fXmlFile);
		xmlOut.print(xmlString);
		xmlOut.close(); 
	}
	public STPProcessBO FranklinXMLParsing(int totalRecords, String targetDir, String fileName) throws Exception{
		
		String errMessage	=	"";
		ConstansBO objConstansBO	=	new ConstansBO("");
		STPProcessBO objSTPProcessBO	=	new STPProcessBO();
		STPProcessBO objSTPProcessBOMain	=	new STPProcessBO();
		ArrayList<STPProcessBO> arrSTPProcessBO	=	new ArrayList<STPProcessBO>();
		
		try {
			File ft = new File(targetDir+fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(ft);
			doc.getDocumentElement().normalize();

			//logger.info("Post method parameters: UserID:FUNDIND1    &&  Password:Welcome123   && FileName:"+fileName);
			//logger.info("root of xml file: " + doc.getDocumentElement().getNodeName());
			
			NodeList status = doc.getElementsByTagName("Status");
			
			if(status.getLength()>0){
				if(status.getLength()==1){
					for (int i = 0; i < status.getLength(); i++) {
						Node node = status.item(i);
		
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							Element element = (Element) node;
							try{
								errMessage	=	ConstansBO.NullCheck(getValue("ErrMsg", element));
							}catch(Exception e){
								errMessage	=	"";
							}
							if(!errMessage.equals("")){
								objSTPProcessBOMain.setErrorMessage(errMessage);
								objSTPProcessBOMain.setUploadDateTime(objConstansBO.getTodayDate("MM/dd/yyyy"));
								objSTPProcessBOMain.setFileName(fileName);
								objSTPProcessBOMain.setTotalRecords(totalRecords);
								objSTPProcessBOMain.setErrorRecords(totalRecords);
								logger.info("ErrMsg: " + getValue("ErrMsg", element));
							}else{
								objSTPProcessBOMain.setAcknowledge(getValue("ack", element));
								objSTPProcessBOMain.setUploadDateTime(getValue("Upload-DateTime", element));
								objSTPProcessBOMain.setFileName(getValue("FileName", element));
								objSTPProcessBOMain.setTotalRecords(Integer.parseInt(getValue("Total-records", element)));
								objSTPProcessBOMain.setSuccessRecords(Integer.parseInt(getValue("Success-records", element)));
								objSTPProcessBOMain.setErrorRecords(Integer.parseInt(getValue("Error-records", element)));
								
								
								logger.info("ack: " + getValue("ack", element));
								logger.info("Upload-DateTime: " + getValue("Upload-DateTime", element));
								logger.info("FileName: " + getValue("FileName", element));
								logger.info("Total-records: " + getValue("Total-records", element));
								logger.info("Success-records: " + getValue("Success-records", element));
								logger.info("Error-records: " + getValue("Error-records", element));
							}
						}
					 }
				}
			}
			
			NodeList result = doc.getElementsByTagName("result");
			
			if(result.getLength()>0){

				for (int i = 0; i < result.getLength(); i++) {
					Node node = result.item(i);
					objSTPProcessBO	=	new STPProcessBO();
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						
						objSTPProcessBO.setCode(getValue("code", element));
						objSTPProcessBO.setRecordNumber(getValue("recordnumber", element));
						objSTPProcessBO.setMessage(getValue("message", element));
						arrSTPProcessBO.add(objSTPProcessBO);
						
						logger.info("Code: " + getValue("code", element));
						logger.info("RecordNumber: " + getValue("recordnumber", element));
						logger.info("Message: " + getValue("message", element));
					 }
				}
				objSTPProcessBOMain.setArrSTPProcessBO(arrSTPProcessBO);
			}
			objSTPProcessBO.setStpProcessFlag(true);
		 } catch (Exception ex) {
			 logger.error("Exp:"+ex);
			 objSTPProcessBO.setStpProcessFlag(false);
		 }
		 return objSTPProcessBOMain;
	}
	
	private static String getValue(String tag, Element element) {
		NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
	}
	public static String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	      CharacterData cd = (CharacterData) child;
	      return cd.getData();
	    }
	    return "";
	  }

	/**
	 * Direct upload forward feed files (Text file) to CAMS server
	 * @param uploadUserId
	 * @param uploadFolderPath
	 */
	public void uploadFiles_ToCAMS (int uploadUserId, String uploadFolderPath) {
	
		logger.info("## -- START - Upload to CAMS server -- #");
		//boolean returnFlag = false;

		try {
			
			boolean checkPoint_1 = false;
			
			//Validations
			if (uploadFolderPath == null || uploadFolderPath.trim().length() < 1
					|| !new File(uploadFolderPath).isDirectory())
				throw new Exception ("Given folder path in valid");
			
			logger.info("Upload Folder Path:" + uploadFolderPath);
			
			PropertyFileReader propertyFileReader = new PropertyFileReader();
			Properties properties =	propertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);
			
			String url = properties.getProperty("CAMS.DIRECT.FEED.URL");
			StringBody auth_UserId = new StringBody(properties.getProperty("CAMS.DIRECT.FEED.LOGIN")), 
					auth_Password = new StringBody(properties.getProperty("CAMS.DIRECT.FEED.PASSWORD"));

				File uploadFolder = new File(uploadFolderPath);
	    		String listOfFiles[] = uploadFolder.list();
	    		
	    		for(String fileName : listOfFiles){

	    			CloseableHttpClient httpclient = HttpClients.createDefault();
    				HttpPost httpPost = new HttpPost(url);
    				
    				String uploadFilePath = uploadFolderPath + File.separator + fileName;
    				File uploadFile = new File(uploadFilePath);
    				
    				if (!uploadFile.isDirectory()){    				
    				
    					logger.info("Upload File Path:" + uploadFilePath);
    				
    				StringBuffer responseAsString = new StringBuffer();
	    			try {
	    	    
		            FileBody input = new FileBody(uploadFile);
		            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		            reqEntity.addPart("Usrcode", auth_UserId);
		            reqEntity.addPart("pass", auth_Password);
		            reqEntity.addPart("Uploadfile", input);
		            reqEntity.addPart("mailreq", new StringBody("Y"));
		            reqEntity.addPart("remarks", new StringBody("TESTING - Direct upload to CAMS server"));
		            httpPost.setEntity(reqEntity);
	
		            logger.info("Executing request " + httpPost.getRequestLine());
		            
		            HttpResponse response = httpclient.execute(httpPost);
		            checkPoint_1 = true;
		            
		            HttpEntity resEntity = response.getEntity();
		            
		            BufferedReader rd = new BufferedReader(new InputStreamReader(resEntity.getContent()));
				    String line = "";
				    while ((line = rd.readLine()) != null) {
				    	responseAsString.append(line);
				    }
				    
				    logger.info("RESPONSE Code : " + response.getStatusLine().getStatusCode());

		            EntityUtils.consume(resEntity);

	    			} catch(Exception iblEx2){
	    				
	    				//To roll back all changes
	    				if (!checkPoint_1)
	    					responseAsString.append("<result><code>-1</code><message>"+ PropertiesConstant.CAMS_STP_FAIL_MESSAGE_01 +"</message></result>");
	    				
	    				logger.info("iBL - DIRECT DBF TO CAMS. $ FILE: "+uploadFilePath+" # EX: "+iblEx2);
	    				
	    	        } finally {
	    	            try { 
	    	            	httpclient.close(); 
	    	            	httpPost.releaseConnection();
	    	            } catch (Exception ignore) {
	    	            	
	    	            }
	    	        }
	    			
	    			logger.info("RESPONSE: "+responseAsString);
	    			
	    			if (responseAsString.length() > 0){
		            try{
		            	doResponseProcess_ForwardFeed(uploadUserId, responseAsString.toString(), uploadFile);

			        }catch(Exception iblEx1){
			        	logger.info("iBL - RESPONSE UPDATE. EX: "+iblEx1);
			        }
	    			}
    				}
	    		}
                        logger.info("Starting the background thread for CAMS insert");
                        new BackgroundThreadProcess().startCAMSInsertThread(bkThreadProc.hashTransactionMainBO, bkThreadProc.failedList);

		}
		catch (Exception blEx){
			logger.error("BL - DIRECT DBF TO CAMS. $ PATH: "+uploadFolderPath+" # EX: "+blEx);
		}
		
		logger.info("## -- END - Upload to CAMS server -- #");
		
	//return returnFlag;	
	}
	
	/**
         *  Process the direct upload to CAMS server's response & Update DB about forward feed status. 
         * @param uploadUserId
         * @param response
         * @param uploadedFile
         * @throws Exception 
         */
	public void doResponseProcess_ForwardFeed (int uploadUserId, String response, File uploadedFile) throws Exception {
		
		logger.info("## -- START - Process CAMS server response -- #");
                BackgroundThreadProcess backgroundThread = new BackgroundThreadProcess();

		
		try {

			if (!uploadedFile.isFile())
				throw new Exception ("Insufficient input value");

                        boolean isRequestProcessed = false, /*Is uploaded request to server processed*/
                                isRequestFailed_InFundsIndia = false /*Is upload request no-response from/not reached CAMS server*/ ;
			NodeList records_result = null;

            try {
                //Parse response XML & Check if error is available
            	logger.info("P 1 : Find 'Error-records' tag");
            	
                DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    			InputSource is = new InputSource();
    			is.setCharacterStream(new StringReader(response));
                Document document = dBuilder.parse(is);
                document.getDocumentElement().normalize();
                //'Error-records' will be zero, if no error
	            NodeList errorRecords_NodeList = document.getElementsByTagName("Error-records");
	            Element errorRecords_Element = (Element) errorRecords_NodeList.item(0);
	            String errorRecords = getCharacterDataFromElement(errorRecords_Element);
	            records_result = document.getElementsByTagName("result");
	            //if (errorRecords != null && errorRecords.trim().length() > 0)
	            if (Integer.valueOf(errorRecords) > -1)
	            	isRequestProcessed = true;
            }catch (Exception iblEx0){
                logger.info("I-BL Request not processed # EX: "+iblEx0);
                isRequestProcessed = false;//If no 'Error-records' tag, all feed is failed.
                
                //Parse response XML & Check if code (-1) is available, mean customized FundsIndia failed request.
                logger.info("P 1.1 : Find code (-1)");
                try{
            	DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    			InputSource is = new InputSource();
    			is.setCharacterStream(new StringReader(response));
                Document document = dBuilder.parse(is);
                document.getDocumentElement().normalize();
                //'Error-records' will be zero, if no error
	            NodeList codeRecords_NodeList = document.getElementsByTagName("code");
	            Element codeRecords_Element = (Element) codeRecords_NodeList.item(0);
	            String codeRecords = getCharacterDataFromElement(codeRecords_Element);
	            //if (codeRecords != null && codeRecords.trim().length() > 0)
	            if (Integer.valueOf(codeRecords) == -1)
	            	isRequestFailed_InFundsIndia = true;
                }catch (Exception iblEx01){
                logger.info("I-BL REALLY SOMTHING WENT WRONG :( #EX: "+iblEx01);
                }
            }

            logger.info("P 2 : isRequestProcessed: "+ isRequestProcessed);
            
            //if (isRevertAllFeed || isErrorRecords){
            
                ArrayList<STPProcessBO> feedSuccessList = new ArrayList<STPProcessBO>();
            	ArrayList<STPProcessBO> feedFailedList = new ArrayList<STPProcessBO>();
            	
            	//Load User transaction reference number
            	   Hashtable<Integer, Integer> userTransRefId_Hash = new Hashtable<Integer, Integer>();
                Scanner sc = new Scanner(uploadedFile);
                int rowCount = 0;
                while (sc.hasNextLine()) {
                    rowCount++;
                    try {
                        String line = sc.nextLine();
                        String[] line_split = line.split("\\|");
                        userTransRefId_Hash.put(rowCount, Integer.valueOf(line_split[4]));// 4th is user transaction number column
                    } catch (Exception iBlEx1) {
                        logger.info("iBL1 - LOAD USER TRANS REF ID # EX: " + iBlEx1);

                    }
                }
                logger.info("All User transaction ref id loaded in HASH. Size: "+userTransRefId_Hash.size());
                
                //Segregate feed failed/success user transaction reference id
                if (isRequestProcessed) {
                logger.info("P 3 : Load failed & success list");    			
    			if(records_result.getLength()>0){

    				for (int i = 0; i < records_result.getLength(); i++) {
    					Node node = records_result.item(i);

    					if (node.getNodeType() == Node.ELEMENT_NODE) {
    						Element element = (Element) node;
    						
    						String code = getValue("code", element);
                                                String message = getValue("message", element);
    						
    						try {
                                                    int recordNumber = Integer.valueOf(getValue("recordnumber", element));
                                                    int userTransRefId = userTransRefId_Hash.get(recordNumber);
                                                    STPProcessBO stpBO = new STPProcessBO(userTransRefId, message);
                                                    
                                                    //If code == 0 success
                                                    if (Integer.valueOf(code) == 0)
                                                        feedSuccessList.add(stpBO);
                                                    //Else Fail
                                                    else {
                                                        feedFailedList.add(stpBO);

    	    						logger.info("Code: " + code);
    	    						logger.info("Record: " + recordNumber +"-"+ userTransRefId);
    	    						logger.info("Message: " + message);	
                                                    }
    						
    						}catch (Exception iBlEx2){
    							logger.info("iBL2 - PARSING USER TRANS REF ID # EX: "+iBlEx2);
    						}
    					 }
    				}
    			}
                }
                //Total feed is failed
                else {
                    logger.info("P 3.1 : Load all in failed list");
                    
                    //Intimation mail to operations for, Upload failed in FundsIndia server itself.
                    if (isRequestFailed_InFundsIndia) {

                        Runnable mailRunnable = new Runnable() {
                            public void run() {
                                try{
                                SendMail sendMail = new SendMail();

                                sendMail.SendDirectUploadStatusMail(false, "CAMS");
                                }catch (Exception iBlEx){
                                    logger.info("I-BL - Direct upload status(Fail) mail. EX:"+iBlEx);
                                }
                            }
                        };
                        Thread mailThread = new Thread(mailRunnable);
                        mailThread.start();
                    }
                    
                    String message = (isRequestFailed_InFundsIndia
                                        ?PropertiesConstant.CAMS_STP_FAIL_MESSAGE_01
                                        :"Unknow");
                    
                    ArrayList<Integer> feedFailed_UserTransRefID_List = new ArrayList<Integer>(userTransRefId_Hash.values());//Since entire feed file upload is fail 
                    for (int userTransRefId : feedFailed_UserTransRefID_List){
                        feedFailedList.add(new STPProcessBO(userTransRefId, message));
                    }
                    
                }
                
    			logger.info("Feed success LIST. Size: "+feedSuccessList.size());
                        logger.info("Feed fail LIST. Size Success: "+feedFailedList.size());
                        DBFFilesDAO dbfFilesDAO = new DBFFilesDAO();
    			dbfFilesDAO.updateSTPForwardFeedStatus(uploadUserId, feedSuccessList, feedFailedList);
                        for (STPProcessBO item : feedFailedList)
                         if (!bkThreadProc.failedList.containsKey(item.getUserTransRefId()))
                             bkThreadProc.failedList.put(item.getUserTransRefId(), item.getMessage());
                        for (STPProcessBO item : feedSuccessList)
                            if (bkThreadProc.failedList.containsKey(item.getUserTransRefId()))
                                bkThreadProc.failedList.remove(item.getUserTransRefId()); // This is done so that success feeds are not reflected in failed list in the iterations
		}
		catch (Exception blEx) {
			logger.error("BL - FORWARD FEED RESPONSE PROCESS $ RESPONSE: "+ response +"EX: "+blEx);
		}
		logger.info("## -- END - Process CAMS server response -- #");
	}

        /**
         * Generate direct upload (STP) failed status excel, from given transaction main BO
         * @param excelFilePath
         * @param transactionMainBO_Hash
         * @return 
         */
        public boolean generateDirectUploadFailedExcel(String excelFilePath, Hashtable<String, ArrayList<TransactionMainBO>> transactionMainBO_Hash)
	{
            logger.info("## -- START -Generate direct upload Failed status Excel -- #");
            boolean returnFlag = false;
            
            if (ConstansBO.isEmpty(excelFilePath) || transactionMainBO_Hash == null || transactionMainBO_Hash.size() < 1)
                return returnFlag;
                                
		try {
            logger.info("P 1 : Create excel");
			int rowCount = 0;
			HSSFWorkbook wb = new HSSFWorkbook();
			HSSFSheet sheet = wb.createSheet();
                        
                        //Excel title
			HSSFRow  row =	sheet.createRow(rowCount);
			HSSFCell cell = row.createCell(0);
			cell.setCellValue(new HSSFRichTextString("RT Code"));
			HSSFCell cell1 = row.createCell(1);
			cell1.setCellValue(new HSSFRichTextString("AMC"));
			HSSFCell cell2 = row.createCell(2);
			cell2.setCellValue(new HSSFRichTextString("Scheme Code"));
			HSSFCell cell3 = row.createCell(3);
			cell3.setCellValue(new HSSFRichTextString("User Transaction No."));
			HSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(new HSSFRichTextString("Remarks"));
                        
                        Enumeration<String> hashKeys = transactionMainBO_Hash.keys();
            logger.info("P 2 : Write excel data");
                    while (hashKeys.hasMoreElements()) {
                        try{
                        String key = hashKeys.nextElement(); 
                        String rtCode = key.split("@")[0];// Main BO Hash key, value will be like RTCode@AMCCode
                        ArrayList<TransactionMainBO> transactionMainBO_List = transactionMainBO_Hash.get(key);

                        if (transactionMainBO_List != null
                                && transactionMainBO_List.size() > 0) {
            logger.info("P 3 : Data from LIST SZ: "+transactionMainBO_List.size());
			for(TransactionMainBO transactionMainBO : transactionMainBO_List){
                            ++rowCount;
                                row = sheet.createRow(rowCount);
				cell = row.createCell(0);
				cell.setCellValue(new HSSFRichTextString(rtCode));
				cell1 = row.createCell(1);
				cell1.setCellValue(new HSSFRichTextString(transactionMainBO.getTransactionBO().getAmcCode()));
				cell2 = row.createCell(2);
				cell2.setCellValue(new HSSFRichTextString(transactionMainBO.getTransactionBO().getSchemeCode()));
				cell3 = row.createCell(3);
				cell3.setCellValue(new HSSFRichTextString(transactionMainBO.getTransactionBO().getUserTransactionNo()));
				cell4 = row.createCell(4);
				cell4.setCellValue(new HSSFRichTextString(transactionMainBO.getTransactionBO().getDirectUploadRemarks()));
                                }
                        }
                        }catch (Exception iblEx0){
                            logger.info("I-BL Transaction BO to excel. # EX: "+iblEx0);
                        }
                        
			}
            logger.info("P 4 : Loaded excel data");
                    //Write as file
                    FileOutputStream fos = new FileOutputStream(excelFilePath);
                    wb.write(fos);
                    fos.close();
                    returnFlag = true;
		}catch(Exception blEx){
			logger.error("BL - Direct upload (STP) failed transaction excel $ EXCEL: "+ excelFilePath +" # EX: "+blEx);
		}
                logger.info("## -- END -Generate direct upload Failed status Excel -- #");
		return returnFlag;
	}
        
        /**
         * Generate excel for the total details of direct upload to CAMS
         * @param excelFilePath
         * @param reportBO_List
         * @return 
         */
        public boolean generateDirectUploadStatusExcel(String excelFilePath, ArrayList<ReportBO> reportBO_List){
            logger.info("## -- START -Generate direct upload All status Excel -- #");
            boolean returnFlag = false;
            
            if (ConstansBO.isEmpty(excelFilePath) || reportBO_List == null || reportBO_List.size() < 1)
                return returnFlag;
                                
		try {
            logger.info("P 1 : Create excel");
                    int rowCount = 0;
                    HSSFWorkbook wb = new HSSFWorkbook();
                    HSSFSheet sheet = wb.createSheet();

                    //Excel title
                    HSSFRow  row =	sheet.createRow(rowCount);
                    HSSFCell cell = row.createCell(0);
                    cell.setCellValue(new HSSFRichTextString("RT Code"));
                    HSSFCell cell1 = row.createCell(1);
                    cell1.setCellValue(new HSSFRichTextString("AMC"));
                    HSSFCell cell2 = row.createCell(2);
                    cell2.setCellValue(new HSSFRichTextString("Success"));
                    HSSFCell cell3 = row.createCell(3);
                    cell3.setCellValue(new HSSFRichTextString("Failed"));
                    HSSFCell cell4 = row.createCell(4);
                    cell4.setCellValue(new HSSFRichTextString("Count"));
                        
            logger.info("P 2 : Write excel data");
            int successCount_Total = 0,
                    failedCount_Total = 0, 
                    transactionCount_Total = 0;
                    for(ReportBO reportBO : reportBO_List){
                        try {
                        ++rowCount;
                        int successCount = reportBO.getSuccessCount(),
                                failedCount = reportBO.getFailedCount(),
                                transactionCount = successCount + failedCount;
                            row = sheet.createRow(rowCount);
                            cell = row.createCell(0);
                            cell.setCellValue(new HSSFRichTextString(String.valueOf(reportBO.getRtCode())));
                            cell1 = row.createCell(1);
                            cell1.setCellValue(new HSSFRichTextString(reportBO.getAmcCode()));
                            cell2 = row.createCell(2);
                            cell2.setCellValue(new HSSFRichTextString(String.valueOf(successCount)));
                            cell3 = row.createCell(3);
                            cell3.setCellValue(new HSSFRichTextString(String.valueOf(failedCount)));
                            cell4 = row.createCell(4);
                            cell4.setCellValue(new HSSFRichTextString(String.valueOf(transactionCount)));
                            
                            successCount_Total += successCount;
                            failedCount_Total += failedCount;
                            transactionCount_Total += transactionCount;
                            
                        }catch (Exception iblEx0){
                            logger.info("I-BL Report BO to excel. # EX: "+iblEx0);
                        }
                        
			}
                    
                    //Last row to display totals
                    HSSFRow  row_TotalRow = sheet.createRow(++rowCount);
                    HSSFCell cell_TotalRow = row_TotalRow.createCell(0);
                    cell_TotalRow.setCellValue(new HSSFRichTextString("Total"));
                    HSSFCell cell2_TotalRow = row_TotalRow.createCell(2);
                    cell2_TotalRow.setCellValue(new HSSFRichTextString(String.valueOf(successCount_Total)));
                    HSSFCell cell3_TotalRow = row_TotalRow.createCell(3);
                    cell3_TotalRow.setCellValue(new HSSFRichTextString(String.valueOf(failedCount_Total)));
                    HSSFCell cell4_TotalRow = row_TotalRow.createCell(4);
                    cell4_TotalRow.setCellValue(new HSSFRichTextString(String.valueOf(transactionCount_Total)));
                    
            logger.info("P 4 : Loaded excel data");
                    //Write as file
                    FileOutputStream fos = new FileOutputStream(excelFilePath);
                    wb.write(fos);
                    fos.close();
                    returnFlag = true;
		}catch(Exception blEx){
			logger.error("BL - Direct upload (STP) All transaction status excel $ EXCEL: "+ excelFilePath +" # EX: "+blEx);
		}
                logger.info("## -- END -Generate direct upload All status Excel -- #");
		return returnFlag;
	}
}