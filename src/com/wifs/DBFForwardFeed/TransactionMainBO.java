package com.wifs.DBFForwardFeed;

import java.util.Hashtable;

public class TransactionMainBO {
	
	private String bankAccountNo	=	"";
	private String bankName			=	"";
	private String amount			=	"";
	
	
	TransactionBO objTransactionBO	=	new TransactionBO();
	InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
	AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
        ReportBO reportBO = new ReportBO(0, "Empty", 0, 0);
	
	
	
	public void setTransactionBO(TransactionBO objTransactionBO){this.objTransactionBO=objTransactionBO;}
	public void setInvestorDetailsBO(InvestorDetailsBO objInvestorDetailsBO){this.objInvestorDetailsBO=objInvestorDetailsBO;}
	public void setAccountDetailsBO(AccountDetailsBO objAccountDetailsBO){this.objAccountDetailsBO=objAccountDetailsBO;}
	
	
	public TransactionBO getTransactionBO(){return (this.objTransactionBO);}
	public InvestorDetailsBO getInvestorDetailsBO(){return (this.objInvestorDetailsBO);}
	public AccountDetailsBO getAccountDetailsBO(){return (this.objAccountDetailsBO);}
	public String getBankAccountNo() {
		return bankAccountNo;
	}
	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
}


