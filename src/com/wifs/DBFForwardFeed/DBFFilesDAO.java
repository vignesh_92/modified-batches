package com.wifs.DBFForwardFeed;

import java.math.BigDecimal; 
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.wifs.dao.dbconnection.WIFSSQLCon;
import com.wifs.utilities.PropertyFileReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.apache.poi.hdf.model.util.NumberFormatter;

public class DBFFilesDAO {
	public static Logger logger = Logger.getLogger("DBFFilesDAO.class");
	/*
	 * To Update the Current Date and time in ForwaredFeed -InvestorTransactionTrack table
	 */
	public PreparedStatement setPreparedFeedUpdate(int userId, Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = 	" Update InvestorTransactionTrack set ForwardFeed = DATEADD(MI, 330, GETDATE())" +
					" , ModifiedUser = "+userId+", ModifiedDate = DATEADD(MI, 330, GETDATE()) where UserTransRefID = ? ";  
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	
	/**
	 * Tables - InvestorTransaction T, AFT_VRO_Mapping V, InvestorBasicDetail D, InvestorAddress A, InvestorBankInfo 
	 * Returns Hashtable<Integer, ArrayList<TransactionMainBO>> 
	 * Condition - In InvestorTransaction table Transaction Status is Success called 'PS' or TC and 77-RED,66-SW
	 * 
	 * <br /><br />
	 * 
	 * [Senthil - 10112016 Comment: Old method re-written to handle direct upload to CAMS / KARVY Server]
	 * @param doProcess - To do process 1)Get data & upload to CAMS / KARVY server directly 2) Get CAMS / KARVY failed transaction
	 */
	public DBFFileMainBO getInvestorTransaction(String liquidScheme, String searchKey, String searchAMC, String bankVerified, Properties properties, Connection objConnection, String doProcess) throws Exception  
	{
		boolean [] whosProcess = DBFFilesBL.whosProcess(doProcess);
                boolean isCAMS_DirectUpload = whosProcess[0], 
                        isCAMS_DirectUpload_FailedDBF = whosProcess[1], 
                        isCAMS_DirectUpload_AllDBF = whosProcess[2],
                        isKARVY_DirectUpload = whosProcess[3];
		
		long startTime = System.currentTimeMillis();
		
		//Connection objConnectionNetMagic	=	null;
		String reportContent	=	"";
		String IDFCSchemeFromProperty	=	"";
		String MiraeAssetSchemeFromProperty	=	"";
		String stpSchemeCode	=	"";
		String sipInsureTransaction	=	"";
		int startsUserinfoMailId	=	0;
		String investorMailId		=	"";
		String dummyUsers			=	"";
		String forwardFeedExist		=	"";
		String searchCondition = "", sqlQuery_Append_IT_Select = "", 
                        sqlQuery_Append_ITT_Select = "	AND CK.ForwardFeed IS NULL ";
		String strInvestorMailId[];
		String IDFCSchemes[];
		ArrayList<String> IDFCSchemeCode			=	new ArrayList<String>();
		String MiraeSchemes[];
		ArrayList<String> MiraeSchemeCode			=	new ArrayList<String>();
		ArrayList<String> sip2kSchemes = new ArrayList<String>();
		String min2kSchemes[];
		String propDividentTypeAMC	=	"";
		String arrDividentTypeAMC[];
		String euinNRIAmc	=	"";
		ArrayList<String> byDividentTypeAMCsCode			=	new ArrayList<String>();
		Hashtable<String, TransactionBO> STPSchemes	=	new Hashtable<String, TransactionBO>();
		Hashtable<String, String> hashAltFolio	=	new Hashtable<String, String>();
		Hashtable<String, NewFolioBO> hashNewFolioBO=	new Hashtable<String, NewFolioBO>();
		ConstansBO objCreatedDate	=	new ConstansBO("");
		ArrayList<Integer> arrInvestorMailId	=	new ArrayList<Integer>();
		try{
			
     		PropertyFileReader generalProperties = new PropertyFileReader();
			Properties gnProperties = generalProperties.getGeneralConfig();   
			
			try{
				euinNRIAmc	=	ConstansBO.NullCheck(properties.getProperty("euinNRIAmc"));
				logger.info("NRI investor euin eliminate amc list : "+euinNRIAmc);
			}catch(Exception e){
				logger.error("NRI investor euin eliminate amc exp:"+e);
			}
			
			try{
	        	String sipMin2kSchemes = ConstansBO.NullCheck(gnProperties.getProperty("sipMinmum2kSchemes"));
	        	logger.info("SIP min 2k scheme list : "+sipMin2kSchemes);
	        	if(!sipMin2kSchemes.equals(""))//IDFC Schemes subtrxn type as S
				{
	        		min2kSchemes = sipMin2kSchemes.split("[,]");
					for(int i=0;i<min2kSchemes.length;i++)
					{
						sip2kSchemes.add(min2kSchemes[i]);
					}
				}
			}catch(Exception e){
				logger.error("Exp in sipMin2kSchemes property reader:"+e);
			}
			
			try{
				IDFCSchemeFromProperty	=	ConstansBO.NullCheck(properties.getProperty("SwitchSchemeCode"));
				logger.info("IDFC trxn.sub type schemes : "+IDFCSchemeFromProperty);
				if(!IDFCSchemeFromProperty.equals(""))//IDFC Schemes subtrxn type as S
				{
					IDFCSchemes	=	IDFCSchemeFromProperty.split("[,]");
					for(int i=0;i<IDFCSchemes.length;i++)
					{
						IDFCSchemeCode.add(IDFCSchemes[i]);
					}
				}
			}catch(Exception e){
				logger.error("Exp in SwitchSchemeCode property reader:"+e);
			}
			
			try{
				MiraeAssetSchemeFromProperty	=	ConstansBO.NullCheck(properties.getProperty("MiraeAssetScheme"));
				logger.info("Mirae Asset schemes : "+MiraeAssetSchemeFromProperty);
				if(!MiraeAssetSchemeFromProperty.equals(""))//IDFC Schemes subtrxn type as S
				{
					MiraeSchemes	=	MiraeAssetSchemeFromProperty.split("[,]");
					for(int i=0;i<MiraeSchemes.length;i++)
					{
						MiraeSchemeCode.add(MiraeSchemes[i]);
					}
				}
			}catch(Exception e){
				logger.error("Exp in MiraeAssetSchemeFromProperty property reader:"+e);
			}
			
			try{
				investorMailId			=	ConstansBO.NullCheck(properties.getProperty("investorMailId"));
				if(!investorMailId.equals(""))
				{
					logger.info("InvestorMailId send for existing user : "+investorMailId);
					strInvestorMailId		=	investorMailId.split("[,]");
					for(int i=0;i<strInvestorMailId.length;i++)
					{
						arrInvestorMailId.add(Integer.parseInt(strInvestorMailId[i].trim()));
					}
				}
			}catch(Exception e){
				logger.error("Exp in investorMailId property reader:"+e);
			}
			
			//For divident type amc's
			try{
				propDividentTypeAMC	=	ConstansBO.NullCheck(properties.getProperty("DividentTypeByAMCCode"));
				
				if(!propDividentTypeAMC.equals(""))
				{
					arrDividentTypeAMC	=	propDividentTypeAMC.split("[,]");
					for(int i=0;i<arrDividentTypeAMC.length;i++)
					{
						byDividentTypeAMCsCode.add(arrDividentTypeAMC[i]);
					}
				}
			}catch(Exception e){
				logger.error("Exp in DividentTypeByAMCCode property reader:"+e);
			}
			
			
			dummyUsers	=	ConstansBO.NullCheck(properties.getProperty("dummyUsers"));
			logger.info("dummy users : "+dummyUsers);
			
			startsUserinfoMailId	=	ConstansBO.NullCheckINT(Integer.parseInt(properties.getProperty("userInfoMailId").trim().toString()));
			logger.info("UserInfo mail id : "+startsUserinfoMailId);
			
			stpSchemeCode	=	properties.getProperty("STPSchemeCode");
			logger.info("STP SchemeCode : "+stpSchemeCode);
			
			
		}catch(Exception e){ logger.error("Exception in getting shms. & mail"+e);		 }
		
		ConstansBO objConstansBO	=	new ConstansBO("");
		String strRTFileCode		=	"";
		int intRTCode				=	0;
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		DBFFileMainBO	objDBFFileMainBO	=	new DBFFileMainBO();
		Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
		//Hashtable<String, ArrayList<TransactionMainBO>> hashMISReportTransactionBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
		//Hashtable<String, ArrayList<TransactionMainBO>> hashMFUpfrontTrialTransactionBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
		Hashtable<String, ArrayList<TransactionMainBO>> hashKarvyTransactionMainSBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
		Hashtable<String, ArrayList<TransactionMainBO>> hashirTransactionMainSBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
		Hashtable<String, ArrayList<SIPInfoBO>> hashSIPInfoBO	=	new Hashtable<String, ArrayList<SIPInfoBO>>();
		Hashtable<Integer, ArrayList<TransactionMainBO>> hashTransactionFatcaBO	=	new Hashtable<Integer, ArrayList<TransactionMainBO>>();
		Hashtable<String,InvestorDetailsBO> hashCKCDetails	=	new Hashtable<String,InvestorDetailsBO>();
		
		ArrayList<TransactionMainBO> arryTransactionMainBO	=	new ArrayList<TransactionMainBO>();
		//ArrayList<TransactionMainBO> arryMISReportTransactionBO	=	new ArrayList<TransactionMainBO>();
		//ArrayList<TransactionMainBO> arryMFUpfrontTrialTransactionBO	=	new ArrayList<TransactionMainBO>();
		ArrayList<TransactionMainBO> arryKarvyTransactionMainSBO	=	new ArrayList<TransactionMainBO>();
		ArrayList<TransactionMainBO> arryTransactionFatcaBO	=	new ArrayList<TransactionMainBO>();
		//ArrayList<TransactionMainBO> arryirTransactionMainSBO	=	new ArrayList<TransactionMainBO>();
		ArrayList<SIPInfoBO> arrySIPInfoBO	=	new ArrayList<SIPInfoBO>();
        ArrayList<ReportBO> reportBO_List = new ArrayList<ReportBO>();
		
		TransactionMainBO objTransactionMainBO	=	new TransactionMainBO(); 
		TransactionBO objTransactionBO			=	new TransactionBO();
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		AccountDetailsBO objAccountDetailsBO	=	new AccountDetailsBO();
		AddtionalInfoBO objAddtionalInfoBO		=	new AddtionalInfoBO();
		DBFFilesDAO	objDBFFilesDAO				=	new DBFFilesDAO();
		SIPInfoBO objSIPInfoBO					=	new SIPInfoBO();

		PreparedStatement objPreparedStatement	=	null;
		
		// Prepared statement for Additional Information like Joint Details, Gaurdian Details from InvestorAdditionalInfo - table
		PreparedStatement objAddtionalPSM	=	null;
		objAddtionalPSM	=	objDBFFilesDAO.getPreparedAddtionalInfo(objConnection);
		
		// Prepared statement for Basic details, Address, Bank details based on the Investor Id - ByFolio
		PreparedStatement objAccountsInfoPSMFolio	=	null;
		objAccountsInfoPSMFolio	=	objDBFFilesDAO.getPreparedAccountsInfoByFolio(objConnection);
		
		// Prepared statement for Basic details, Address, Bank details based on the Investor Id
		PreparedStatement objAccountsInfoPSMByTransRefId	=	null;
		objAccountsInfoPSMByTransRefId	=	objDBFFilesDAO.getPreparedAccountsInfoByTransRefId(objConnection);
		
		// Prepared Statement for AMCMasterCodes - Based on the mapping and lookupcode to get the ReferenceCode
		PreparedStatement objAMCMasterCodesPSTM	=	null;
		objAMCMasterCodesPSTM	=	objDBFFilesDAO.getPreparedAMCMasterCodes(objConnection);
		
		// Prepared Statement for AMCBank Master based on the AFT-SchemeCode, AMCCode, RTCode
		PreparedStatement objAMCBankPSTM	=	null;
		objAMCBankPSTM	=	objDBFFilesDAO.getPreparedAMCBank(objConnection);
		
		//Prepared Statement for AMCCode from AFT_Schemedetails table
		//PreparedStatement objAMCCodePSTM	=	null;
		//objAMCCodePSTM	=	objDBFFilesDAO.getPreparedAMCCode(objConnection);
		
		//PreparedStatement objSIPBankDetails	=	null;
		//objSIPBankDetails	=	objDBFFilesDAO.getPreparedSIPBankDetails(objConnection);
		
		PreparedStatement objExistFolioNoByAMC	=	null;
		objExistFolioNoByAMC	=	objDBFFilesDAO.getPreparedStmtExistFolioNoByAMC(objConnection);
		
		PreparedStatement objExistFolioNoByAMCRMF	=	null;
		objExistFolioNoByAMCRMF	=	objDBFFilesDAO.getPreparedStmtExistFolioNoByAMCRMF(objConnection);

		//PreparedStatement objCampsLocationByPicode	=	null;
		//objCampsLocationByPicode	=	objDBFFilesDAO.getPreparedStmtCampsLocationByPicode(objConnection);
		
		PreparedStatement stmtIDFCSchemeCode	=	null;
		stmtIDFCSchemeCode	=	objDBFFilesDAO.getPreparedStmtIDFCSchemeCode(objConnection);
		
		PreparedStatement stmtSIPMin2KSchemeCode	=	null;
		stmtSIPMin2KSchemeCode	=	objDBFFilesDAO.getPreparedStmtSIPMin2KSchemes(objConnection);

		PreparedStatement stmtSwitchScheme	=	null;
		stmtSwitchScheme	=	objDBFFilesDAO.getPreparedStmtSTPSchemeCodeTemp(stpSchemeCode, objConnection);
		
		PreparedStatement objPreparedStmtFTBankName	=	null;
		objPreparedStmtFTBankName	=	objDBFFilesDAO.getPreparedStmtFranklinBankName(objConnection);
		
		//PreparedStatement pstmtFranklinSIScheme	=	null;
		//pstmtFranklinSIScheme	=	objDBFFilesDAO.getPreparedStmtFranklinSIScheme(objConnection);
		
		PreparedStatement objBankInfoByUserTransNo	=	null;
		objBankInfoByUserTransNo	=	objDBFFilesDAO.getPreparedBankInfoByUserTransId(objConnection);
		
		PreparedStatement objBankInfoByFolio	=	null;
		objBankInfoByFolio	=	objDBFFilesDAO.getPreparedBankInfoByFolio(objConnection);
		
		PreparedStatement objHolidayDate	=	null;
		objHolidayDate	=	objDBFFilesDAO.getPreparedStmtHoliday(objConnection);
		
		PreparedStatement objAFTCodeByDividendType	=	null;
		objAFTCodeByDividendType	=	objDBFFilesDAO.getPreparedStmtAFTCodeByDividendType(objConnection);
		
		PreparedStatement objNRIAddress		=	null;
		objNRIAddress	=	objDBFFilesDAO.getPreparedStmtNRIAddress(objConnection);
		
		PreparedStatement objPortfolioId	=	null;
		objPortfolioId	=	objDBFFilesDAO.getPreparedStmtPortfolioId(objConnection);
		
		PreparedStatement objFatcaOccupationCodes	=	null;
		objFatcaOccupationCodes	=	objDBFFilesDAO.getPreparedStmtFatcaOccupationCodes(objConnection);
		
		PreparedStatement objTaxStatusCodes	=	null;
		objTaxStatusCodes	=	objDBFFilesDAO.getPreparedStmtTaxStatusCodes(objConnection);

		PreparedStatement objSubBrokerCode		=	null;
		objSubBrokerCode	=	objDBFFilesDAO.getPreparedStmtSubBrokerCode(objConnection);

		String fDate			=	objConstansBO.getYesterDate(objHolidayDate); 
		String tDate			=	objConstansBO.getTodayDate("yyyy-MM-dd");
		
		PreparedStatement objAltFolio	=	null;
		objAltFolio	=	objDBFFilesDAO.getPreparedStmtAltFolio(fDate, tDate, objConnection, objHolidayDate);

		PreparedStatement objFolioCloseValidation	=	null;
		objFolioCloseValidation	=	objDBFFilesDAO.getPreparedStmtFolioCloseValidation(objConnection);
		
		PreparedStatement objUTISIPRegistration	=	null;
		objUTISIPRegistration	=	objDBFFilesDAO.getPreparedStmtUTISIPRegistration(objConnection);

		PreparedStatement objAMCNominee		=	null;
		objAMCNominee	=	objDBFFilesDAO.getPreparedStmtAMCNominee(objConnection);
		
		PreparedStatement objNoOfInstallmentsForFT		=	null;
		objNoOfInstallmentsForFT	=	objDBFFilesDAO.getPreparedStmtNoOfInstallmentsForFT(objConnection);
		
		PreparedStatement objEuinValidate		=	null;
		objEuinValidate	=	objDBFFilesDAO.getPreparedStmtEUINValidate(objConnection);
		
		PreparedStatement objPreparedStatementFatco	=	null;
		objPreparedStatementFatco	=	objDBFFilesDAO.getPreparedStmtInvestorCitizenship(objConnection);
		
		CallableStatement objPreparedStatementSIPInstallmentsPaid	=	null;
		objPreparedStatementSIPInstallmentsPaid	=	objDBFFilesDAO.pstmtSIPOpenMandateNoOfInstallmentsPaid(objConnection);
		
		PreparedStatement objPreparedStatementSIPRegistrationFlag	=	null;
		objPreparedStatementSIPRegistrationFlag	=	objDBFFilesDAO.getPreparedStmtSIPRegistrationFlag(objConnection);
		
		PreparedStatement pstmtAlertSIPRegistration	=	null;
		pstmtAlertSIPRegistration	=	objDBFFilesDAO.getPreparedStmtAlertSIPRegistration(objConnection);

		PreparedStatement pstmtSTPSwitchSubTransType	=	null;
		pstmtSTPSwitchSubTransType	=	objDBFFilesDAO.getPreparedStmtSTPSwitchSubTransType(objConnection);
		
		PreparedStatement pstmtVTPSwitchSubTransType	=	null;
		pstmtVTPSwitchSubTransType	=	objDBFFilesDAO.getPreparedStmtVTPSwitchSubTransType(objConnection);
		
		PreparedStatement pstmtSIPDatesByAMC	=	null;
		pstmtSIPDatesByAMC	=	objDBFFilesDAO.getPreparedStmtSIPDatesByAMC(objConnection);
                
                PreparedStatement pstmtAMCSIPStartDates =       null;
                pstmtAMCSIPStartDates   =   objDBFFilesDAO.getPreparedStmtAMCSIPStartDates(objConnection);
		
		/*try{
			
			WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
			objConnectionNetMagic = objWIFSSQLCon.getNetMagicWifsSQLConnect();			
		}catch(Exception e){
			logger.error("Exp:"+e);
		}*/
		
		PreparedStatement objOTPResponseByPANAadhaarNo	=	null;
		objOTPResponseByPANAadhaarNo	=	objDBFFilesDAO.getPreparedStmtOTPResponseByPANAadhaarNo(objConnection);
		
		PreparedStatement objPreparedStatementCKYCDetails	=	null;
		objPreparedStatementCKYCDetails	=	objDBFFilesDAO.getPreparedStmtCKYCDetails(objConnection);
		
		PreparedStatement objPreparedStatementCKYCMasterCodes	=	null;
		objPreparedStatementCKYCMasterCodes	=	objDBFFilesDAO.getPreparedCKYCMasterCodes(objConnection);
		
                PreparedStatement objPreparedStatementDPId	=	null;
		objPreparedStatementDPId	=	objDBFFilesDAO.getPreparedStmtDPId(objConnection);
		
		
		//PreparedStatement objPreparedStatementFolioExist	=	null;
		//objPreparedStatementFolioExist	=	objDBFFilesDAO.getPreparedStmtFolioSchemeExist(objConnection);
		
		reportContent	=	objDBFFilesDAO.dailyReport(fDate,tDate,objConnection);

		hashAltFolio	=	objDBFFilesDAO.getHashAltFolio(objAltFolio);
		
		logger.info("ALL prepare stmt executed");
		
		ResultSet objResultSet	=	null;
		String SQLQuery			=	""; 
		
		String searchByPaymentId	=	"";
		String searchByLiquidScheme	=	"";
		String searchByAMCName		=	"";
		String lqSchemeSearch		=	"";
		
		 if(!searchKey.equals(""))
			 searchByPaymentId	=	" and T.PaymentID in("+searchKey+")";
		 
		 if(!liquidScheme.equals(""))
			 searchByLiquidScheme	=	 " join AFT_SCHEME_DETAILS D on D.SCHEMECODE = T.SchemeCode and D.CLASSCODE=24 ";
		 else
			 searchByLiquidScheme	=	 " left join AFT_SCHEME_DETAILS D on T.SchemeCode = D.SCHEMECODE and D.CLASSCODE=24 ";
		 
		 if(!searchAMC.equals("")){//For AMC wise feed generation LQ scheme no needs to sent
			 searchByAMCName	=	" and WS.AMC_CODE in ( "+searchAMC+ ") and T.Transtype in('PUR','PURA','SO','SI') " ;
			 lqSchemeSearch		=	" and (SD.CLASSCODE <> 24 or T.TransType in('SI','SO')) ";
		 }
		 
		 searchCondition	=	" and convert(varchar(20),T.TransactionDate,120) > convert(varchar(20),'"+fDate+" 14:00:00',120) and convert(varchar(20),T.TransactionDate,120) <= convert(varchar(20),'"+tDate+" 14:00:00',120) ";
		 if(!searchAMC.equals(""))
			 searchCondition	+=	" and T.UserTransRefID not in(Select distinct UserTransRefID From InvestorTransaction T1"
			 		+ " Inner Join AFT_SCHEME_DETAILS D1 on D1.SCHEMECODE = T1.SchemeCode and D1.CLASSCODE = 24 and D1.AMC_CODE in ( "+searchAMC+ ")"
			 		+ " where T1.Transtype in('PUR','PURA','SO','SI')  and T1.Active = 'A' and ( (T1.TransactionStatus = 'PS') or ( (T1.TransactionStatus = 'TC') and (T1.PaymentID in(77,66,54)))) and"
			 		+ " convert(varchar(20),T1.TransactionDate,120) > convert(varchar(20),'"+fDate+" 14:00:00',120) and convert(varchar(20),T1.TransactionDate,120) <= convert(varchar(20),'"+tDate+" 14:00:00',120) )";
		 
		 if (isCAMS_DirectUpload)
			 sqlQuery_Append_IT_Select = " AND V.RTCode = 1 ";
                 else if (isCAMS_DirectUpload_FailedDBF) {
                     sqlQuery_Append_IT_Select = " AND V.RTCode = 1 ";
                     sqlQuery_Append_ITT_Select = "	AND CK.DirectUpload_Status = 'Fail' AND CK.ForwardFeed IS NOT NULL ";
                 }
                 else if (isCAMS_DirectUpload_AllDBF) {
                     sqlQuery_Append_IT_Select = " AND V.RTCode = 1 ";
                     sqlQuery_Append_ITT_Select = "	AND CK.DirectUpload_Status IS NOT NULL AND CK.ForwardFeed IS NOT NULL ";
                 }
                 else if (isKARVY_DirectUpload)
			 sqlQuery_Append_IT_Select = " AND V.RTCode = 2 ";
		 
		SQLQuery = " Select distinct V.RTAMCCode, V.AFT_Scheme_Code, V.AMCSchemeCode,T.UserTransRefID, T.FolioNumber, T.TransType, T.SchemeCode, T.SwitchOption, D.SCHEMECODE as LiquidShm, SD.ClassCode, "+ 
				   " T.TransMode, T.HoldingProfileID, V.RTCode, T.TransactionDate, T.TransactionTime, T.Units, T.Amount, T.Comments, T.DividendType, T.PaymentID, " +
				   " S.UserTransRefID as SIPUserTrnsRefId, S.Frequency, S.EcsDate, S.StartDate, S.EndDate, DATEADD(MONTH,S.NoOfInstalment,T.TransactionDate) as SIPEndDate, S.NoOfInstalment, S.Amount as SIPAmount, S.SIPID,"+
				   " DATEADD(MI, 330, GETDATE()) as SIPINSStartDate, DATEADD(MONTH,(S.NoOfInstalment-1),DATEADD(MI, 330, GETDATE())) as SIPINSEndDate," +
				   " CK.PaidThru, CK.IPAddress, WS.SCHEME_NAME, AM.Fund, AM.ADD3, AM.FUND_SNAME, T.ITCValue, CK.ForwardFeed, WS.AMC_CODE, " +
				   " I.UserTransRefID as ISIPUserTransId, I.fromDate as isipFromDate, I.toDate as isipToDate, datediff(MONTH,I.fromDate, I.toDate) as isipInstallments, sipDay as isipDay, iSIPId," +
				   " CK.DPID, CK.clientcode, CK.EuinId, T.CreatedUser, T.CreatedDate, B.FatcaIPAddress, B.FatcaDate,  CONVERT(char(8), B.FatcaDate, 114) as FatcaTime, CK.subtranstype, CK.DirectUpload_Status, CK.DirectUpload_Remarks, B.InvestorID "+ 
				   " from InvestorTransaction T  "+
				   " inner join InvestorAdditionalInfo A on A.HoldingProfileID = T.HoldingProfileID and A.RelationshipID = 'F'"+
				   " inner join InvestorBasicDetail B on B.InvestorID = A.PrimaryInvestorID and B.AllowTransact = 'A' and B.InvestmentLimit <> 4999 "+  
				   " inner join UserInfo U on U.userId = B.UserID and U.Flag = 'A' and U.verified = 'true' and U.UserId not in ("+dummyUsers+")" +
				   " inner join AFT_VRO_Mapping V  on T.SchemeCode=V.AFT_Scheme_Code and V.AMCSchemeCode is not null  "
				   + sqlQuery_Append_IT_Select + 
				   " inner join WealthSchemeMaster WS on WS.SCHEMECODE = T.schemeCode " +searchByAMCName+
				   " inner join WealthAMCMaster AM on AM.AMC_CODE = WS.AMC_CODE"+
				   " inner join AFT_SCHEME_DETAILS SD on SD.SCHEMECODE = T.SchemeCode " +lqSchemeSearch+
				   " inner join InvestorTransactionTrack CK on CK.UserTransRefID = T.UserTransRefID "
                                    + sqlQuery_Append_ITT_Select + 
				   " left join iSIPBirla I on T.UserTransRefID = I.UserTransRefID "+ 
				   " left join SIPDetails S on T.TransType ='PUR' and T.UserTransRefID = S.UserTransRefID" +searchByLiquidScheme +
				   " where T.Active = 'A'  and ( (T.TransactionStatus = 'PS') or ( (T.TransactionStatus = 'TC') and (T.PaymentID in(77,66,54))))"+ 
				   searchByPaymentId+ searchCondition+
				   //" and TransactionDate between '"+fDate+" 2:00:00 PM' and '"+tDate+" 2:00:00 PM' " +
				   //" and T.HoldingProfileID not in(select distinct HoldingProfileID from InvestorHoldingProfile where Userid in) " +
				   //" and T.PaymentID=72837 " +
				   //" and T.UserTransRefID in(149240,149239,149238,149237,149236,149235,149225,149224,149223,149222,149258,149259,149260) "+ 
				   " order by V.RTAMCCode, V.RTCode, T.UserTransRefID,S.UserTransRefID, T.TransType, T.CreatedUser"; 
		
		
		
		logger.info(SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		
		long startTime1 = System.currentTimeMillis();
		objResultSet = objPreparedStatement.executeQuery();
		long totalTime1 = System.currentTimeMillis() - startTime1;
		logger.info("TIMER : ####TIMER:Total time for trxn fetching query = " + totalTime1);
		
		int row = 0, directUpload_SuccessCount = 0, directUpload_FailedCount = 0;
		String strAMCSchemeCode	=	"";
		String strAFTSchemeCode	=	"";
                String rtAMCCode = "", rtAMCCode_Temp = null;
		String strAMCCode		=	"";
		String strTransType		=	"";
		String strSIPEnabled	=	"";
		String iSIPEnabled		=	"";
		String strFolioNo		=	"";
		String strAccClosing	=	"";
		String strLiquidShm		=	"";
		String euinId			=	"";
		int intHoldingProfile	=	0;
		int intPortfolioNo		=	0;
		int classCode			=	0;
		String strFolioNoCamps[];
		//String fatcaFlag		=	"";
                String sipStartDate             =       "";
		String sipEndDate               =	"";
		int sipDates			=	0;
                ReportBO reportBO = null;

		while(objResultSet.next()) {
			
			row	=	row+1;
			logger.info("*************************** Row Begin : "+row+"*************************************************");
			long startTime2 = System.currentTimeMillis();
			try{
				strAMCSchemeCode	=	""; 
				strAFTSchemeCode	=	"";
				strAMCCode			=	"";
				strTransType		=	"";
				strSIPEnabled		=	"";
				iSIPEnabled			=	"";
				forwardFeedExist	=	"";
				euinId				=	"";
                                sipStartDate            =       "";
                                sipEndDate              =	"";
                                sipDates                =	0;
				//fatcaFlag			=	"N";
				
				objTransactionBO		=	new TransactionBO();
				objInvestorDetailsBO	=	new InvestorDetailsBO();
				objAccountDetailsBO		=	new AccountDetailsBO();
				objAddtionalInfoBO		=	new AddtionalInfoBO();
				objTransactionMainBO	=	new TransactionMainBO();
				objSIPInfoBO			=	new SIPInfoBO();
				
                                rtAMCCode = objResultSet.getString("RTAMCCode").trim();
				classCode			=	objResultSet.getInt("ClassCode");
				intRTCode			=	objResultSet.getInt("RTCode");
				strAFTSchemeCode	=	objResultSet.getString("AFT_Scheme_Code").trim();
				strTransType		=	ConstansBO.NullCheck(objResultSet.getString("TransType").trim());
				intHoldingProfile	=	objResultSet.getInt("HoldingProfileID");
				strLiquidShm		=	ConstansBO.NullCheck(objResultSet.getString("LiquidShm")).trim();
				strAccClosing		=	ConstansBO.NullCheck(objResultSet.getString("SwitchOption")).trim();
				strAMCCode			=	ConstansBO.NullCheck(objResultSet.getString("AMC_CODE")).trim(); //objDBFFilesDAO.getAMCCode(strAFTSchemeCode, objAMCCodePSTM); // get AMCCode from AFT_Seheme_Details
				strSIPEnabled		=	ConstansBO.NullCheck(objResultSet.getString("SIPUserTrnsRefId"));
				iSIPEnabled			=	ConstansBO.NullCheck(objResultSet.getString("iSIPUserTransId"));
				forwardFeedExist	=	ConstansBO.NullCheck(objResultSet.getString("ForwardFeed"));
				euinId				=	ConstansBO.NullCheck(objResultSet.getString("EuinId"));
				sipInsureTransaction	=	ConstansBO.NullCheck(objResultSet.getString("subtranstype"));
				objAccountDetailsBO.setReinv_Tag(ConstansBO.NullCheck(objResultSet.getString("DividendType")));  //Reinv_Tag
				
				objTransactionBO.setAmcCode(rtAMCCode);  //AmcCode
				objTransactionBO.setUserTransactionNo(ConstansBO.NullCheck(objResultSet.getString("UserTransRefID")));  //UserTransactionNo
				objTransactionBO.setCreatedUser(objResultSet.getInt("CreatedUser"));
				
				objCreatedDate	=	new ConstansBO(objResultSet.getString("CreatedDate"));
				objTransactionBO.setCreatedDate(objCreatedDate);
				
				//For ELSS Scheme Div type should be Payout
				if((classCode == 8)&&(objAccountDetailsBO.getReinv_Tag().equals("Y"))){
					objAccountDetailsBO.setReinv_Tag("N");
					logger.info("DivType changed from Y to N : classcode:"+classCode+" div:"+objAccountDetailsBO.getReinv_Tag());
				}
				
				/* AMCScheme Code as per dividend type  */ 
				if((byDividentTypeAMCsCode.contains(objTransactionBO.getAmcCode()))&&(objAccountDetailsBO.getReinv_Tag().equals("N")))
					strAMCSchemeCode	=	objDBFFilesDAO.getAFTCodeByDividendType(strAFTSchemeCode, objTransactionBO.getAmcCode(), intRTCode, objAccountDetailsBO.getReinv_Tag(), objAFTCodeByDividendType);
				
				if(strAMCSchemeCode.equals(""))
					strAMCSchemeCode	=	objResultSet.getString("AMCSchemeCode").trim();
				
				objSIPInfoBO.setSIPTransaction(false);
				
				objTransactionBO.setExistForwardFeed(forwardFeedExist);
				objTransactionBO.setSchemeCode(strAMCSchemeCode);  //SchemeCode
				objTransactionBO.setAftSchemeCode(Integer.parseInt(strAFTSchemeCode));
				
				objTransactionBO.setRtCode(intRTCode);
				
				objTransactionBO.setApplNo("");  //ApplNo
				
				objInvestorDetailsBO.setBrokerCode("");  //BrokerCode
				objInvestorDetailsBO.setUserCode("WIFS");  //UserCode 
				
                                if(Integer.parseInt(strAFTSchemeCode) == 38252)
                                    objDBFFilesDAO.getDPId(objResultSet.getInt("InvestorId"), objTransactionBO, objPreparedStatementDPId);
                                else{
        			    objTransactionBO.setDpId(ConstansBO.NullCheck(objResultSet.getString("DPID")));
                                    objTransactionBO.setClientCode(ConstansBO.NullCheck(objResultSet.getString("clientcode")));
                                }
				objTransactionBO.setIPAddress(ConstansBO.NullCheck(objResultSet.getString("IPAddress")));
				objTransactionBO.setSchemeName(ConstansBO.NullCheck(objResultSet.getString("SCHEME_NAME")));
				objTransactionBO.setAmcName(ConstansBO.NullCheck(objResultSet.getString("Fund")));
				objTransactionBO.setAmcAddress(ConstansBO.NullCheck(objResultSet.getString("ADD3")));
				objTransactionBO.setAmcShortName(ConstansBO.NullCheck(objResultSet.getString("FUND_SNAME")));
				objTransactionBO.setITCValue(ConstansBO.NullCheck(objResultSet.getString("ITCValue")));
				objTransactionBO.setFatcaIPAddress(ConstansBO.NullCheck(objResultSet.getString("FatcaIPAddress")));
				if(objResultSet.getString("FatcaDate") != null){
					objCreatedDate	=	new ConstansBO(objResultSet.getString("FatcaDate"));
					objTransactionBO.setFatcaTime(ConstansBO.NullCheck(objResultSet.getString("FatcaTime")));
				}
				else{
					objCreatedDate	=	new ConstansBO(objResultSet.getString("TransactionDate"));
					objTransactionBO.setFatcaTime(ConstansBO.NullCheck(objResultSet.getString("TransactionTime")));
				}
				objTransactionBO.setFatcaDate(objCreatedDate);
				
				strFolioNo	=	ConstansBO.NullCheck(objResultSet.getString("FolioNumber"));
				
				logger.info("UserTrxn.No.Starts ========== "+objTransactionBO.getUserTransactionNo()+" ======= ");
				
				intPortfolioNo		=	objDBFFilesDAO.getPortfolioId(objTransactionBO.getUserTransactionNo(), strTransType, objPortfolioId);
				//before old folio selection needs to empty 0 values
				if(strFolioNo.equals("0")){
					strFolioNo	=	"";
					logger.info("User select new folio as 0 "); 
				}
				if(isCAMS_DirectUpload_FailedDBF || isCAMS_DirectUpload_AllDBF || forwardFeedExist.equals(""))
				{
					//If folioNo is empty then get the corresponding folioNo from position table on holdingProfile,PortfolioNo,AMCCode and AFTSchemeCode otherwise existing folioNo will be updated
					if(strFolioNo.equals(""))
					{
						/* For RMF existing folio don't have the same scheme code	 **/
						if((objTransactionBO.getAmcCode().equals("RMF"))&&(!strSIPEnabled.equals("")))
							strFolioNo	=	objDBFFilesDAO.getExistFolioNoByAMC(objTransactionBO.getAmcCode(), strSIPEnabled, intHoldingProfile, intPortfolioNo, strAMCCode, strAFTSchemeCode, objExistFolioNoByAMCRMF);
						//else if((strTransType.equals("SI"))&&(intRTCode==6))
						//	logger.info("Franklin SI Scheme, validate the same holdings having SI trxn.");
						else
							strFolioNo	=	objDBFFilesDAO.getExistFolioNoByAMC(objTransactionBO.getAmcCode(), strSIPEnabled, intHoldingProfile, intPortfolioNo, strAMCCode, strAFTSchemeCode, objExistFolioNoByAMC);
					}
					/*//No need SI trxn folio no for Franklin
					if((strTransType.equals("SI"))&&(intRTCode==6))
					{
						boolean franklinSISchemeAvailable	=	false;
						franklinSISchemeAvailable	=	objDBFFilesDAO.FranklinSIScheme(intHoldingProfile, intPortfolioNo, strAFTSchemeCode, strFolioNo, pstmtFranklinSIScheme);
						if(!franklinSISchemeAvailable)
							strFolioNo	=	"";
					}*/
					//Folio needs to empty
					if(strFolioNo.equals("-"))// New Folio 
					{
						strFolioNo	=	"";
						logger.info("User select new folio as - ");
					} 
					objTransactionBO.setFolioNo(strFolioNo);  //FolioNo
					
					// Split the folio for CAMS & Franklin
					if(((intRTCode==1)||(intRTCode==14)||(intRTCode==6))&&(!strFolioNo.equals("")))
					{
						strFolioNoCamps	=	strFolioNo.split("[/]");
						//If Franklin Folio should be concadination of AMCSchemeCode
						/*if(intRTCode==6)
							objTransactionBO.setCk_Dig_FolioNo(strAMCSchemeCode+""+ConstansBO.NullCheck(strFolioNoCamps[0]));
						else*/
						objTransactionBO.setCk_Dig_FolioNo(ConstansBO.NullCheck(strFolioNoCamps[0]));

						if(strFolioNoCamps.length>1)
							objTransactionBO.setCk_Dig_No(ConstansBO.NullCheck(strFolioNoCamps[1]));
					}
					else if(((intRTCode==1)||(intRTCode==14))&&(strFolioNo.equals("")))
					{
						String altFolioId	=	"";
						try
						{
							altFolioId	=	hashAltFolio.get(intHoldingProfile+"_"+intRTCode+"_"+objTransactionBO.getAmcCode());
							logger.info("**** ALT FOLIO for :::::::: "+intHoldingProfile+"_"+intRTCode+"_"+objTransactionBO.getAmcCode()+"  ==== Holding Id : "+altFolioId);
						}catch(Exception e){altFolioId	=	"";}
						objAccountDetailsBO.setALT_Folio(ConstansBO.NullCheck(altFolioId));
					}
					
					objInvestorDetailsBO.setFatcaFlag("Y");// Send fatca feed for all 31Dec2015
					/*if(!strFolioNo.equals("")){//For FATCA Validate new folio or not
						fatcaFlag	=	getFolioSchemeExist(strFolioNo, Integer.parseInt(strAFTSchemeCode), objPreparedStatementFolioExist);
						objInvestorDetailsBO.setFatcaFlag(fatcaFlag);
					}else if(strFolioNo.equals(""))
						objInvestorDetailsBO.setFatcaFlag("Y");*/
				}
				//For FT new folio needs to sent details
				/*if(intRTCode==6 && strFolioNo.equals(""))
				{
					ftNewFolioUserTransRefId	+=	objTransactionBO.getUserTransactionNo()+",";
				}*/
				
				if((strTransType.equals("SI"))||(strTransType.equals("SO")))
					strLiquidShm	=	"";
				// Setting the Transaction date and time. if time >14 hrs then trxndate+1 else trxndate
				 if(!liquidScheme.equals(""))
					 objTransactionBO = objDBFFilesDAO.setLiquidTransactionDateTimeFormat(fDate, tDate, strLiquidShm,objTransactionBO, objResultSet.getString("TransactionDate"), objResultSet.getString("TransactionTime"));
				 else
					 objTransactionBO = objDBFFilesDAO.setTransactionDateTimeFormat(fDate, tDate, objTransactionBO, objResultSet.getString("TransactionDate"), objResultSet.getString("TransactionTime"));
				
				objTransactionBO.setUnits(ConstansBO.NullCheckDBL(objResultSet.getDouble("Units")));  //Units
				objTransactionBO.setAmount(new BigDecimal(ConstansBO.NullCheckDBL(objResultSet.getDouble("Amount"))));  //Amount
				
				
				if(isCAMS_DirectUpload_FailedDBF || isCAMS_DirectUpload_AllDBF || forwardFeedExist.equals(""))
				{
						
					// For adding the Additional information to TransactionMainBO - Based on the HoldingProfileId
					objDBFFilesDAO.getAddtionalInfo(intRTCode, strAMCCode, objInvestorDetailsBO, objTransactionBO, hashNewFolioBO, intHoldingProfile, objAddtionalPSM, objPreparedStatementFatco, objFatcaOccupationCodes, objTaxStatusCodes, objOTPResponseByPANAadhaarNo);
					
					//Getting CKYC details
					if((!objInvestorDetailsBO.getTaxNo().equals(""))&&(intRTCode==14)&&(objTransactionBO.getFolioNo().equals(""))&&(!hashCKCDetails.containsKey(objInvestorDetailsBO.getTaxNo()))){
						InvestorDetailsBO tempInvestorDetailsBO	=	new InvestorDetailsBO();
						tempInvestorDetailsBO	=	getCKYCDetails(objTransactionBO.getUserTransactionNo(), objPreparedStatementCKYCDetails);
						if(tempInvestorDetailsBO != null){
							
							tempInvestorDetailsBO	=	getCKYCMasterCodes(tempInvestorDetailsBO, objPreparedStatementCKYCMasterCodes);
							
							hashCKCDetails.put(objInvestorDetailsBO.getTaxNo(), tempInvestorDetailsBO);
						}
					}
					
					// Based on the schemeCode DepBanNmae, DepAccuntNo and DepDate should be Transaction Date
					if((strTransType.equals("PUR"))||(strTransType.equals("PURA"))) // For Redemption, Switch no need for deposit bank Name
					{
						objAddtionalInfoBO	=	objDBFFilesDAO.getPreparedAMCBank(strAMCCode, intRTCode, strAFTSchemeCode,objAMCBankPSTM);
						if(!objAddtionalInfoBO.equals(null))
						{
							objAccountDetailsBO.setDepBankName(objAddtionalInfoBO.getAMCBankName());  //DepBankName
							objAccountDetailsBO.setDepAccountNo(objAddtionalInfoBO.getAMCAccountNo());  //DepAccountNo
							objAccountDetailsBO.setDepDate(objTransactionBO.getTransactionDate());  //DepDate
							objAccountDetailsBO.setDepReferenceNo(objTransactionBO.getUserTransactionNo());  //DepReferenceNo
						}
						objTransactionBO.setPaymentId(ConstansBO.NullCheck(objResultSet.getString("PaymentID")));
					}
					
					// Adding Address Info and Basic Info details based on the InvestoID By folio or userTransRefid
					if(objTransactionBO.getFolioNo().equals(""))
					{
						//long startAcTime = System.currentTimeMillis();
						objDBFFilesDAO.getAccountsInfo(objTransactionBO.getFolioNo(), objTransactionBO.getUserTransactionNo(), strAMCCode, startsUserinfoMailId, arrInvestorMailId, intRTCode, hashNewFolioBO, objInvestorDetailsBO.getInvestorId(), objInvestorDetailsBO, objAccountDetailsBO, objTransactionMainBO, objAccountsInfoPSMByTransRefId, objPreparedStmtFTBankName, objSubBrokerCode, objAMCNominee);
						//long totalAcTime = System.currentTimeMillis() - startAcTime;
						//logger.info("TIMER : getByUserTrans = " + totalAcTime);
					}else{
						//long startFolioTime = System.currentTimeMillis();
						objDBFFilesDAO.getAccountsInfo(objTransactionBO.getFolioNo(), objTransactionBO.getFolioNo(), strAMCCode, startsUserinfoMailId, arrInvestorMailId, intRTCode, hashNewFolioBO, objInvestorDetailsBO.getInvestorId(), objInvestorDetailsBO, objAccountDetailsBO, objTransactionMainBO, objAccountsInfoPSMFolio, objPreparedStmtFTBankName, objSubBrokerCode, objAMCNominee);
						//long totalFoloTime = System.currentTimeMillis() - startFolioTime;
						//logger.info("TIMER : getByFolio = " + totalFoloTime);
					}
					
					//For not eligible trxn enliminate
					if( (!objInvestorDetailsBO.getIrType().equals("OE") && (objInvestorDetailsBO.getKycFlag().equals("C")||objInvestorDetailsBO.getKycFlag().equals("Y")||objInvestorDetailsBO.getKycFlag().equals("K")) ) || (((bankVerified.equals("Y"))||(objInvestorDetailsBO.getBankVerified().equals("true")))&& (objInvestorDetailsBO.getIrType().equals("OE")||objInvestorDetailsBO.getKycFlag().equalsIgnoreCase("E"))))
					{
						// NRI investor NRI address should send CAMS feed.
						if(objInvestorDetailsBO.getNRIInvestor()==1){
							objDBFFilesDAO.getNRIAddress(objInvestorDetailsBO, objNRIAddress);
						}
						
						// Adding Bank Info details based on the InvestoID and folio or usertransRefid
						if(objTransactionBO.getFolioNo().equals(""))
						{
							//long startBankTime = System.currentTimeMillis();
							objDBFFilesDAO.getBankInfo(intRTCode, objTransactionBO.getUserTransactionNo(), objInvestorDetailsBO, objAccountDetailsBO, objTransactionMainBO, objBankInfoByUserTransNo, objPreparedStmtFTBankName);
							logger.info("New folio, By UserTransNo : "+objTransactionBO.getUserTransactionNo()+" Bank Details starts");
							//long totalAcTime = System.currentTimeMillis() - startBankTime;
							//logger.info("TIMER : new Folio = " + totalAcTime);
						}
						else
						{
							//long startBankTime = System.currentTimeMillis();
							objDBFFilesDAO.getBankInfo(intRTCode, objTransactionBO.getFolioNo(), objInvestorDetailsBO, objAccountDetailsBO, objTransactionMainBO, objBankInfoByFolio, objPreparedStmtFTBankName);
							logger.info("Old folio, By Existing folio : "+objTransactionBO.getFolioNo()+" Bank Details starts");
							//long totalAcTime = System.currentTimeMillis() - startBankTime;
							//logger.info("TIMER : existing = " + totalAcTime);
						}
						
						// Gettring the All AMCRTMaster Reference Values
						objDBFFilesDAO.getAMCMasterCodes(strTransType, strAFTSchemeCode, strAMCCode, intRTCode, objInvestorDetailsBO, objAccountDetailsBO, objTransactionBO, objAMCMasterCodesPSTM);
						
						if((!euinId.equals(""))&&(getEUINValidate(euinId, objEuinValidate)))
						{
							if((objInvestorDetailsBO.getNRIInvestor()==1) && (objInvestorDetailsBO.getUsaCanadaInvestor().equals("Y")) &&(euinNRIAmc.contains(strAMCCode))){
								logger.info("NRI investor AMC:"+strAMCCode+" euin blank, euinId:"+euinId);
							}else{
								objTransactionBO.setEuinId(euinId);
								objTransactionBO.setEuinOpted("Y");
							}
						}
						/*else{
							//Getting EUINID for RT-2 by default ARN EUIN ID
							if((!objTransactionMainBO.getInvestorDetailsBO().getSubBrokerCode().equals(""))&&(intRTCode==2))
								objDBFFilesDAO.getEuinId(objInvestorDetailsBO.getSubBrokerCode(), objTransactionBO , objEuinId);
							
						}*/
						
						//For FT No of installments needs to add
						if (intRTCode==6){
							objDBFFilesDAO.getNoOfInstallmentsForFT(objTransactionBO.getUserTransactionNo(), objTransactionBO, objNoOfInstallmentsForFT);
						}
						
						/*
						 * For UTI SIP installment
						 */
						if((intRTCode==2)&&(objTransactionBO.getAmcCode().equals("UTI"))&&(strSIPEnabled.equals(""))&&(iSIPEnabled.equals(""))){
							logger.info("UTI AMC sub trxn type");
							UTISIPRegistration(objTransactionBO.getAftSchemeCode(), objTransactionBO.getUserTransactionNo(), objTransactionBO, objUTISIPRegistration);
						}else if(iSIPEnabled.equals("") && strSIPEnabled.equals("") && IDFCSchemeCode.contains(strAFTSchemeCode)&&(strTransType.equals("PUR")||strTransType.equals("PURA"))) { //static code for Scheme Restriction
							logger.info("IDFCScheme code sub trxn type:"+strAFTSchemeCode);
							objTransactionBO	=	objDBFFilesDAO.getIDFCSchemeCode(intHoldingProfile, intPortfolioNo, strAFTSchemeCode, strFolioNo, objTransactionBO, stmtIDFCSchemeCode);
						}else if((strSIPEnabled.equals(""))&&(iSIPEnabled.equals(""))) 	{
							objTransactionBO.setSubTransactionType("Normal");  //Sub_TRXN_T
							//objAccountDetailsBO.setSipReferenceNo("");  //SipReferenceNo
							
							//static code for SIP min 2k schemes
							if((sip2kSchemes.contains(strAFTSchemeCode)&&((strTransType.equals("PUR"))||(strTransType.equals("PURA"))))){
								objTransactionBO	=	objDBFFilesDAO.getSIPMin2kScheme(objTransactionBO.getUserTransactionNo(), objTransactionBO, stmtSIPMin2KSchemeCode);
								logger.info("///////////////----------/////////////////////-----------///////////-//-/-/-/-/-/-");
							}else if(( ( (strAMCCode.trim().equals("400024")) || MiraeSchemeCode.contains(strAFTSchemeCode) )&&((strTransType.equals("PUR"))||(strTransType.equals("PURA"))))||(sipInsureTransaction.equals("INSSIP"))){
								SIPRegistrationFlag(intRTCode, strAMCCode, objAccountDetailsBO, objTransactionBO, objPreparedStatementSIPRegistrationFlag, pstmtAlertSIPRegistration, pstmtAMCSIPStartDates, objPreparedStatementSIPInstallmentsPaid);
							}
						}
						else if((!strSIPEnabled.equals(""))||(!iSIPEnabled.equals("")))	{
							/*if(objTransactionBO.getAmcCode().equals("RMF"))
							{
								objTransactionBO.setCk_Dig_No("");
								objTransactionBO.setFolioNo("");
								objTransactionBO.setCk_Dig_FolioNo("");
							}*/
							objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
							objTransactionBO.setSipReferenceId(String.valueOf(ConstansBO.NullCheckINT(objResultSet.getInt("SIPID"))));  //SipReferenceNo//
							objSIPInfoBO.setStartMonth(objTransactionBO.getTransactionDate());	//	startMonth
                                                        if(!iSIPEnabled.equals(""))
                                                            objSIPInfoBO.setEndMonth(new ConstansBO(objResultSet.getString("isipToDate")));	//	trxn.date+noofinstallments = endMonth
                                                        else
                                                            objSIPInfoBO.setEndMonth(new ConstansBO(objResultSet.getString("SIPEndDate"))); // endMonth
                                                        
							objTransactionBO.setSipFrequency("OM"); // only karvy we added so hot coded.. while sip insure needs to change.
							
							//Registration date should be transaction date
							objTransactionBO.setSIP_Reg_Date(objTransactionBO.getTransactionDate());
							
							if((intRTCode==1)||(intRTCode==2)||(intRTCode==6)||(intRTCode==14))// getting sip information as mentioned amcs
							{
								if((intRTCode==2)||(intRTCode==6))// Only FT & Karvy we need to sent separate file.
									objSIPInfoBO.setSIPTransaction(true);
								
								//objSIPInfoBO	=	objDBFFilesDAO.getSIPBankDetails(objInvestorDetailsBO.getInvestorId(), objResultSet.getInt("SIPBankId"), objSIPInfoBO, objSIPBankDetails);
								objSIPInfoBO.setFolioNumber(strFolioNo);
								objSIPInfoBO.setFundCode(objTransactionBO.getAmcCode());	// Fund
								objSIPInfoBO.setSchemeCode(strAMCSchemeCode);	//	SchemeCode
								objSIPInfoBO.setOption(objAccountDetailsBO.getReinv_Tag());	//	option
								objSIPInfoBO.setInvestorName(objInvestorDetailsBO.getFirstName());
								
								if(sipInsureTransaction.equals("INSSIP")){//For sip insure
									
									sipEndDate	=	objResultSet.getString("SIPINSEndDate");
									sipEndDate	=	sipEndDate.substring(0,8);
									sipEndDate	=	sipEndDate+(objResultSet.getInt("EcsDate")+1); 
                                                                        
                                                                        sipStartDate    =       objDBFFilesDAO.getAMCSIPStartDates(Integer.parseInt(strAMCCode), (objResultSet.getInt("EcsDate")+1), objResultSet.getString("SIPINSStartDate"), pstmtAMCSIPStartDates);
                                                                        if(!sipStartDate.equals(""))
                                                                            objSIPInfoBO.setStartMonth(new ConstansBO(sipStartDate));	//	startMonth
                                                                        else
                                                                            objSIPInfoBO.setStartMonth(new ConstansBO(objResultSet.getString("SIPINSStartDate")));	//	startMonth
									
									objSIPInfoBO.setEndMonth(new ConstansBO(sipEndDate));	//	endMonth
									objSIPInfoBO.setNoOfInstalments(objResultSet.getInt("NoOfInstalment"));	//	noOfInstalments
									objSIPInfoBO.setSIPRefNo(objResultSet.getString("SIPID"));
									objSIPInfoBO.setFrequency("M");	//	frequency
									objSIPInfoBO.setSipType("I");
									objTransactionBO.setSipDate(objResultSet.getInt("EcsDate")+1);
									objTransactionBO.setSipFrequency("M"); // only karvy SIP Insure
									objTransactionBO.setSIP_Reg_Date(new ConstansBO(objResultSet.getString("SIPINSStartDate")));
									
									if(intRTCode==1){ //For CAMS
										objTransactionBO.setSubTransactionType("U"); 
										objSIPInfoBO.setFrequency("OM");
										objTransactionBO.setSipFrequency("OM"); 
									}
									else
										objTransactionBO.setSubTransactionType("I");
									
								}else if(!strSIPEnabled.equals("")){ // sip details
									
									objSIPInfoBO.setNoOfInstalments(objResultSet.getInt("NoOfInstalment"));	//	noOfInstalments
									if(objSIPInfoBO.getNoOfInstalments()==0)//	trxn.date+noofinstallments = endMonth
										sipEndDate	=	objResultSet.getString("EndDate");
									else
										sipEndDate	=	objResultSet.getString("SIPEndDate");
									
									if(( intRTCode == 1 )||( intRTCode == 2 ))
										sipDates	=	getSIPDatesByAMC(Integer.parseInt(strAMCCode), objResultSet.getInt("EcsDate"),pstmtSIPDatesByAMC);
									
                                                                        if(sipDates == 0) //If no sip dates available then by default investor sip date **
										sipDates	=	objResultSet.getInt("EcsDate");
									
									sipEndDate	=	sipEndDate.substring(0,8); 
									sipEndDate	=	sipEndDate+sipDates;
									objSIPInfoBO.setEndMonth(new ConstansBO(sipEndDate));	//	endMonth
                                                                        
                                                                        sipStartDate    =       objDBFFilesDAO.getAMCSIPStartDates(Integer.parseInt(strAMCCode), sipDates, objResultSet.getString("SIPINSStartDate"), pstmtAMCSIPStartDates);
                                                                        if(!sipStartDate.equals(""))
                                                                            objSIPInfoBO.setStartMonth(new ConstansBO(sipStartDate));
									
									objSIPInfoBO.setFrequency(objResultSet.getString("Frequency"));	//	frequency
									//objSIPInfoBO.setEndMonth(new ConstansBO(objResultSet.getString("SIPEndDate")));	
									
									objSIPInfoBO.setSIPRefNo(objResultSet.getString("SIPID"));
									objTransactionBO.setSipDate(sipDates);
									//For alert SIP if installments as '0' then end date should be sip table enddate, installments value default as 12 
									if(objSIPInfoBO.getNoOfInstalments()==0){
										objSIPInfoBO.setNoOfInstalments(12);	//	noOfInstalments
										objTransactionBO.setNoOfInstalment("12");
										objTransactionBO.setPaidInstallments("1");
									}
								} 
								else if(!iSIPEnabled.equals(""))// for Isip details UTI
								{
									objSIPInfoBO.setFrequency("M");	//	frequency
									objSIPInfoBO.setEndMonth(new ConstansBO(objResultSet.getString("isipToDate")));	//	trxn.date+noofinstallments = endMonth
									objSIPInfoBO.setNoOfInstalments(objResultSet.getInt("isipInstallments"));	//	noOfInstalments
									objSIPInfoBO.setSIPRefNo(objResultSet.getString("iSIPId"));
								}
								
								//start date should be transaction date
								//objSIPInfoBO.setStartMonth(new ConstansBO(objResultSet.getString("StartDate")));	//	startMonth
								
								//objSIPInfoBO.setAmount(objResultSet.getDouble("SIPAmount"));	//	amount ** get from SIP table
								objSIPInfoBO.setAmount(objTransactionBO.getAmount().doubleValue());	//	amount ** trxn. amount as sip amount
								//objSIPInfoBO.setApplicationNo("");	//	applicationNo
								//objSIPInfoBO.setNomineeName(objAccountDetailsBO.getNomineeName());	//	
								//objSIPInfoBO.setNomineeRelation(objAccountDetailsBO.getNomineeRelation());	//	nomineeRelation
								objSIPInfoBO.setPan(objInvestorDetailsBO.getTaxNo());
								objSIPInfoBO.setDocumentCategory("");
								//objSIPInfoBO.setDocumentRefNo("");
								objSIPInfoBO.setRemarks(objTransactionBO.getPaymentId());	//	remarks
								objSIPInfoBO.setSubBrokerCode(objInvestorDetailsBO.getSubBrokerCode());
								objSIPInfoBO.setDepAccountNo(objAddtionalInfoBO.getAMCAccountNo());
								objSIPInfoBO.setDepBankName(objAddtionalInfoBO.getAMCBankName());
								objSIPInfoBO.setAccountType(objAccountDetailsBO.getAccountType());
								objSIPInfoBO.setInvestorBankName(objAccountDetailsBO.getBankName()+", "+objAccountDetailsBO.getBranchName());
								objSIPInfoBO.setAccountNo(objAccountDetailsBO.getAccountNo());
								objSIPInfoBO.setBankName(objAccountDetailsBO.getBankName());
								
								Calendar calendar = Calendar.getInstance(); 
								calendar.add(Calendar.DATE,+30);
								objSIPInfoBO.setSIPDueDate(new ConstansBO(String.valueOf(formatter.format(calendar.getTime()))));
								
								objTransactionBO.setNoOfInstalment(String.valueOf(objSIPInfoBO.getNoOfInstalments()));
								objTransactionBO.setPaidInstallments("1");
								objTransactionBO.setSipStartDate(objSIPInfoBO.getStartMonth());
								objTransactionBO.setSipEndDate(objSIPInfoBO.getEndMonth());
								objTransactionBO.setSipAmount(objTransactionBO.getAmount().doubleValue());
								
							}
						}
							
						if(strAccClosing.equals("ALL"))
						{
							logger.info(":::::::RED or SWITCH Acc. Closer Starts:::::");
							objDBFFilesDAO.FolioCloseValidation(intHoldingProfile, Integer.parseInt(strAFTSchemeCode), intPortfolioNo, objTransactionBO, objAccountDetailsBO, objFolioCloseValidation);
						}
						
						// Static code for STP trxn sub-transactin type as 'S'
						if((strTransType.equals("SI"))||(strTransType.equals("SO")))
						{
							try{
								//For STP switch schemes we need to sent stp reg details 12Nov2016									
								if(STPSchemes.containsKey(objTransactionBO.getUserTransactionNo())){
									logger.info("STPSchemeCode Sub trxn. type as 'S'.UserTransRefId : "+objTransactionBO.getUserTransactionNo()+" Type : "+strTransType);
									TransactionBO tempobjTransactionBO	=	new TransactionBO();
									tempobjTransactionBO	=	STPSchemes.get(objTransactionBO.getUserTransactionNo());
									
									objTransactionBO.setSubTransactionType(tempobjTransactionBO.getSubTransactionType());
									objTransactionBO.setSipReferenceId(tempobjTransactionBO.getSipReferenceId()); 
									objTransactionBO.setSIP_Reg_Date(tempobjTransactionBO.getSIP_Reg_Date());
									objTransactionBO.setNoOfInstalment(tempobjTransactionBO.getNoOfInstalment());
									objTransactionBO.setPaidInstallments(tempobjTransactionBO.getPaidInstallments());
									objTransactionBO.setSipStartDate(tempobjTransactionBO.getSipStartDate());
									objTransactionBO.setSipEndDate(tempobjTransactionBO.getSipEndDate());
									objTransactionBO.setSipAmount(tempobjTransactionBO.getSipAmount());
									objTransactionBO.setSipDate(tempobjTransactionBO.getSipDate());
									objTransactionBO.setSipFrequency("OM");
								}
								else
								{
									objDBFFilesDAO.getSwitchScheme(objTransactionBO.getUserTransactionNo(), objAccountDetailsBO, objTransactionBO, STPSchemes, stmtSwitchScheme, pstmtSTPSwitchSubTransType, pstmtVTPSwitchSubTransType);
								}
							}catch(Exception e){	}
						}
					}else{ // Else for bank not verified OE user 
						logger.info("*************************** OE TRANSACTION / EKY TRANSACTION, BANK NOT VERIFIED!... SO FEED NOT GENERATED *************************** ");
					}
				}
				
				if(strFolioNo.equals("")&& objInvestorDetailsBO.getIrType().equals("OE"))
					objTransactionBO.setFreshInvestor("F");
				else if(strFolioNo.equals(""))
					objTransactionBO.setFreshInvestor("P");
				else
					objTransactionBO.setFreshInvestor("E");
				
				objAccountDetailsBO.setRemarks(ConstansBO.NullCheck(objResultSet.getString("PaidThru")));  //Remarks
				objTransactionBO.setPaidThru(ConstansBO.NullCheck(objResultSet.getString("PaidThru"))); // Paid through
                                
                                String directUploadStatus = objResultSet.getString("DirectUpload_Status");
                                objTransactionBO.setDirectUploadStatus(directUploadStatus);
                                objTransactionBO.setDirectUploadRemarks(objResultSet.getString("DirectUpload_Remarks"));
				
				objAccountDetailsBO.setUINNo("");  //UINNo
				//objAccountDetailsBO.setForm6061("");  //Form6061
				//objAccountDetailsBO.setForm6061J1("");  //Form6061J1
				//objAccountDetailsBO.setForm6061J2("");  //Form6061J2
				
				objTransactionMainBO.setTransactionBO(objTransactionBO);
				objTransactionMainBO.setInvestorDetailsBO(objInvestorDetailsBO);
				objTransactionMainBO.setAccountDetailsBO(objAccountDetailsBO);
				
				//For DBF File generation, IrType should not be OE or If it is OE then Bank verified flag in DB should be True or by manual bankverified should be Y
				// Then DBF file should generate...
				
				arryTransactionMainBO	=	new ArrayList<TransactionMainBO>();
				//arryMISReportTransactionBO	=	new ArrayList<TransactionMainBO>();
				//arryMFUpfrontTrialTransactionBO	=	new ArrayList<TransactionMainBO>();
				arryKarvyTransactionMainSBO	=	new ArrayList<TransactionMainBO>();
				//arryirTransactionMainSBO	=	new ArrayList<TransactionMainBO>();
				arrySIPInfoBO			=	new ArrayList<SIPInfoBO>();
				arryTransactionFatcaBO	=	new ArrayList<TransactionMainBO>();
				
				if( (!objInvestorDetailsBO.getIrType().equals("OE") && (objInvestorDetailsBO.getKycFlag().equals("C")||objInvestorDetailsBO.getKycFlag().equals("Y")||objInvestorDetailsBO.getKycFlag().equals("K") ) ) || (((bankVerified.equals("Y"))||(objInvestorDetailsBO.getBankVerified().equals("true")))&& (objInvestorDetailsBO.getIrType().equals("OE")||objInvestorDetailsBO.getKycFlag().equalsIgnoreCase("E"))))
				{
					if(((strTransType.equals("PUR")||strTransType.equals("PURA"))&&(objTransactionBO.getAmount().doubleValue()>0))
							||strAccClosing.equals("ALL")||strTransType.equals("SI")
							||((strTransType.equals("SO")||strTransType.equals("RED"))&&(objTransactionBO.getAmount().doubleValue()!=0||objTransactionBO.getUnits()!=0))){
						//For karvy seperate file for SubTransaction
						strRTFileCode		=	intRTCode+"@"+objTransactionBO.getAmcCode(); // For key value of Hashtable and file name
	
						if((intRTCode==2)&&(objTransactionBO.getSubTransactionType().equals("S")||objTransactionBO.getSubTransactionType().equals("I"))&&(objTransactionBO.getExistForwardFeed().equals("") || isCAMS_DirectUpload_FailedDBF || isCAMS_DirectUpload_AllDBF))	{
							if(hashKarvyTransactionMainSBO.containsKey(strRTFileCode)){
								arryKarvyTransactionMainSBO	=	hashKarvyTransactionMainSBO.get(strRTFileCode);
								arryKarvyTransactionMainSBO.add(objTransactionMainBO);
								hashKarvyTransactionMainSBO.put(strRTFileCode, arryKarvyTransactionMainSBO);
							}else{
								arryKarvyTransactionMainSBO.add(objTransactionMainBO);
								hashKarvyTransactionMainSBO.put(strRTFileCode, arryKarvyTransactionMainSBO);
							}
						}  else	{
							if(isCAMS_DirectUpload_FailedDBF || isCAMS_DirectUpload_AllDBF || objTransactionBO.getExistForwardFeed().equals("")){
								
								if(hashTransactionMainBO.containsKey(strRTFileCode)){
									arryTransactionMainBO	=	hashTransactionMainBO.get(strRTFileCode);
									arryTransactionMainBO.add(objTransactionMainBO);
									hashTransactionMainBO.put(strRTFileCode, arryTransactionMainBO);
								}else{
									arryTransactionMainBO.add(objTransactionMainBO);
									hashTransactionMainBO.put(strRTFileCode, arryTransactionMainBO);
								}
							}
						}
	
						/*if((strTransType.equals("PUR"))||(strTransType.equals("PURA"))){//&&(objTransactionBO.getExistForwardFeed().equals("")))
							
							if(hashMISReportTransactionBO.containsKey(strRTFileCode)){
								arryMISReportTransactionBO	=	hashMISReportTransactionBO.get(strRTFileCode);
								arryMISReportTransactionBO.add(objTransactionMainBO);
								hashMISReportTransactionBO.put(strRTFileCode, arryMISReportTransactionBO);
							}else{
								arryMISReportTransactionBO.add(objTransactionMainBO);
								hashMISReportTransactionBO.put(strRTFileCode, arryMISReportTransactionBO);
							}
						}*/
						
						if((objSIPInfoBO.isSIPTransaction())&&(isCAMS_DirectUpload_FailedDBF || isCAMS_DirectUpload_AllDBF || objTransactionBO.getExistForwardFeed().equals(""))){
							
							if(hashSIPInfoBO.containsKey(strRTFileCode)){
								arrySIPInfoBO	=	hashSIPInfoBO.get(strRTFileCode);
								arrySIPInfoBO.add(objSIPInfoBO);
								hashSIPInfoBO.put(strRTFileCode, arrySIPInfoBO);
							}else{
								arrySIPInfoBO.add(objSIPInfoBO);
								hashSIPInfoBO.put(strRTFileCode, arrySIPInfoBO);
							}
						}
						//Fatca files by RT wise
						//if(((strTransType.equals("PUR"))||(strTransType.equals("PURA")))&&(objInvestorDetailsBO.getFatcaFlag().equals("Y"))&&(objTransactionBO.getExistForwardFeed().equals(""))  ){
						if(isCAMS_DirectUpload_FailedDBF || isCAMS_DirectUpload_AllDBF || objTransactionBO.getExistForwardFeed().equals("") || objTransactionBO.getExistForwardFeed() == null){
							if(hashTransactionFatcaBO.containsKey(intRTCode)){
								arryTransactionFatcaBO	=	hashTransactionFatcaBO.get(intRTCode);
								arryTransactionFatcaBO.add(objTransactionMainBO);
								hashTransactionFatcaBO.put(intRTCode, arryTransactionFatcaBO);
							}else{
								arryTransactionFatcaBO.add(objTransactionMainBO);
								hashTransactionFatcaBO.put(intRTCode, arryTransactionFatcaBO);
							}
						}
					}else
						logger.error("*************************** TRANSACTION AMOUNT/UNITS ARE ZERO VALUE!... SO FEED NOT GENERATED *************************** ");
				}
                                
                                //For report of all CAMS direct upload transaction (isCAMS_DirectUpload_AllDBF)
                                if (!rtAMCCode.equals(rtAMCCode_Temp)){
                                        if (!ConstansBO.isEmpty(rtAMCCode_Temp)){
                                            reportBO = new ReportBO(intRTCode, rtAMCCode_Temp, directUpload_SuccessCount, directUpload_FailedCount);
                                            reportBO_List.add(reportBO);
                                            rtAMCCode_Temp = null;directUpload_SuccessCount = directUpload_FailedCount = 0;//Reset
                                        }
                                }
                                rtAMCCode_Temp = rtAMCCode;
                                if (directUploadStatus != null){
                                    if (directUploadStatus.trim().equalsIgnoreCase("YES"))
                                        directUpload_SuccessCount++;
                                    else if (directUploadStatus.trim().equalsIgnoreCase("FAIL"))
                                        directUpload_FailedCount++;
                                }
                                        
                        
			}catch(Exception e){	logger.info(" Exceptin in Resultset blog. "+e);	}
			long totalTime2 = System.currentTimeMillis() - startTime2;
			logger.info("*************************** Row End : "+row+"####TIMER:"+totalTime2+"*************************************************");
                        
		}
                
                if (!ConstansBO.isEmpty(rtAMCCode_Temp)){
                    reportBO = new ReportBO(intRTCode, rtAMCCode_Temp, directUpload_SuccessCount, directUpload_FailedCount);
                    reportBO_List.add(reportBO);
                    rtAMCCode_Temp = null;directUpload_SuccessCount = directUpload_FailedCount = 0;//Reset
                }
		
		objDBFFileMainBO.setHashTransactionMainBO(hashTransactionMainBO);
		objDBFFileMainBO.setHashKarvyTransactionMainSBO(hashKarvyTransactionMainSBO);
		objDBFFileMainBO.setHashirTransactionMainSBO(hashirTransactionMainSBO);
		objDBFFileMainBO.setHashSIPInfoBO(hashSIPInfoBO);
		//objDBFFileMainBO.setHashMISReportTransactionBO(hashMISReportTransactionBO);
		objDBFFileMainBO.setHashTransactionFatcaBO(hashTransactionFatcaBO);
		//objDBFFileMainBO.setHashMFUpfrontTrialTransactionBO(hashMFUpfrontTrialTransactionBO);
		objDBFFileMainBO.setHashNewFolioBO(hashNewFolioBO);
		objDBFFileMainBO.setHashCKCDetails(hashCKCDetails);
		objDBFFileMainBO.setFDate(fDate);
		objDBFFileMainBO.setTDate(tDate);
		//objDBFFileMainBO.setFtNewFolioUserTransRefId(ftNewFolioUserTransRefId);
		reportContent	+=	"<br />No. of Transactions "+"&#9;: "+row+"</pre>";
		
		objDBFFileMainBO.setDailyReportContent(reportContent);
                objDBFFileMainBO.setReportBO_List(reportBO_List);
		
		if(objPreparedStatement	!=	null)objPreparedStatement.close();
		if(objAddtionalPSM	!=	null) objAddtionalPSM.close();
		if(objAccountsInfoPSMFolio	!=	null) objAccountsInfoPSMFolio.close();
		if(objAccountsInfoPSMByTransRefId	!=	null) objAccountsInfoPSMByTransRefId.close();
		if(objAMCMasterCodesPSTM	!=	null) objAMCMasterCodesPSTM.close();
		if(objAMCBankPSTM	!=	null) objAMCBankPSTM.close();
		if(objExistFolioNoByAMC	!=	null) objExistFolioNoByAMC.close();
		if(objExistFolioNoByAMCRMF	!=	null) objExistFolioNoByAMCRMF.close();
		if(stmtIDFCSchemeCode	!=	null) stmtIDFCSchemeCode.close();
		if(stmtSwitchScheme	!=	null) stmtSwitchScheme.close();
		if(objPreparedStmtFTBankName	!=	null) objPreparedStmtFTBankName.close();
		if(objBankInfoByUserTransNo	!=	null) objBankInfoByUserTransNo.close();
		if(objBankInfoByFolio	!=	null) objBankInfoByFolio.close();
		if(objHolidayDate	!=	null) objHolidayDate.close();
		if(objAFTCodeByDividendType	!=	null) objAFTCodeByDividendType.close();
		if(objNRIAddress	!=	null) objNRIAddress.close();
		if(objSubBrokerCode	!=	null) objSubBrokerCode.close();
		if(objAltFolio	!=	null) objAltFolio.close();
		if(objFolioCloseValidation!=null) objFolioCloseValidation.close();
		if(objPortfolioId!=null) objPortfolioId.close();
		if(objUTISIPRegistration != null) objUTISIPRegistration.close();
		if(objAMCNominee != null) objAMCNominee.close();
		if(objNoOfInstallmentsForFT != null) objNoOfInstallmentsForFT.close();
		if(objEuinValidate!=null) objEuinValidate.close();
		if(objPreparedStatementFatco != null) objPreparedStatementFatco.close();
		if(objFatcaOccupationCodes != null) objFatcaOccupationCodes.close();
		if(objTaxStatusCodes != null) objTaxStatusCodes.close();
		if(objPreparedStatementSIPRegistrationFlag != null) objPreparedStatementSIPRegistrationFlag.close();
		if(objPreparedStatementSIPInstallmentsPaid != null) objPreparedStatementSIPInstallmentsPaid.close();
		if(pstmtSTPSwitchSubTransType != null) pstmtSTPSwitchSubTransType.close();
		if(pstmtVTPSwitchSubTransType != null) pstmtVTPSwitchSubTransType.close();
		if(pstmtAlertSIPRegistration != null) pstmtAlertSIPRegistration.close();
		if(pstmtSIPDatesByAMC != null) pstmtSIPDatesByAMC.close();
                if(pstmtAMCSIPStartDates != null) pstmtAMCSIPStartDates.close();
		if(objOTPResponseByPANAadhaarNo != null) objOTPResponseByPANAadhaarNo.close();
		if(objPreparedStatementCKYCDetails != null) objPreparedStatementCKYCDetails.close();
		if(objPreparedStatementCKYCMasterCodes != null) objPreparedStatementCKYCMasterCodes.close();
                if(objPreparedStatementDPId != null) objPreparedStatementDPId.close();
		if(objResultSet != null) objResultSet.close();
		//if(objConnectionNetMagic != null) objConnectionNetMagic.close();
		
		logger.info("Investor transaction Success");
		long totalTime = System.currentTimeMillis() - startTime;
		logger.info("####TIMER : getInvestorTransaction = " + totalTime);
		return objDBFFileMainBO;
	}
	public PreparedStatement getPreparedBankInfoByFolio(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select B.accountNumber, B.bankAccountType, case when L.lookUpId=999 then B.bankName else L.lookUpDescription end as BankName," +
				" replace(B.branchAddress,CHAR(13)+CHAR(10),'') as branchAddress, B.bankCity, Upper(B.NEFTCode) as NEFTCode, B.micrCode, Upper(B.IFSCCode) as IFSCCode, L.lookUpId" +
				" from InvestorBankInfo B " +
				" inner join Lookup L on L.lookUpType ='BANKLIST' and  B.BANKLOOKUPID = L.lookUpId" +
				" where B.userBankId = isnull((Select UserBankId from FOLIO_BANK_INFO where FolioNumber = ? and Active=1 ),1)" +
				" and B.InvestorType='MF' and B.Investor_ID = ? ";
		
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public PreparedStatement getPreparedBankInfoByUserTransId(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select B.accountNumber, B.bankAccountType, case when L.lookUpId=999 then B.bankName else L.lookUpDescription end as BankName," +
				" replace(B.branchAddress,CHAR(13)+CHAR(10),'') as branchAddress, B.bankCity, Upper(B.NEFTCode) as NEFTCode, B.micrCode, Upper(B.IFSCCode) as IFSCCode, L.lookUpId" +
				" from InvestorBankInfo B " +
				" inner join Lookup L on L.lookUpType ='BANKLIST' and  B.BANKLOOKUPID = L.lookUpId" +
				" where B.userBankId = isnull((Select UserBankId from TRANSACTION_BANK_INFO where UserTransRefId = ? ),1)" +
				" and B.InvestorType='MF' and B.Investor_ID = ? ";
		
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public TransactionMainBO getBankInfo(int intRTCode, String folioUserTrans, InvestorDetailsBO objInvestorDetailsBO, AccountDetailsBO objAccountDetailsBO, TransactionMainBO objTransactionMainBO, PreparedStatement objPreparedStatement, PreparedStatement objPreparedStmtFTBankName) throws Exception
	{
		logger.info("*** BankInfo Begin : Folio or UserTransId "+folioUserTrans+" InvestorId "+objInvestorDetailsBO.getInvestorId()+" ***");
		String ftBankName		=	"";
		ResultSet objResultSet 		= 	null;
		
		objPreparedStatement.setString(1, folioUserTrans);
		objPreparedStatement.setString(2, objInvestorDetailsBO.getInvestorId());
		DBFFilesDAO	objDBFFilesDAO	=	new DBFFilesDAO();
		
		//long startTime = System.currentTimeMillis();
		objResultSet	=	objPreparedStatement.executeQuery();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : RSTgetBankInfo = " + totalTime);
		if(objResultSet.next())
		{
			//objInvestorDetailsBO.setModeOfHolding(objResultSet.getString("ModeOfHolding"));
			if((ConstansBO.NullCheck(objResultSet.getString("NEFTCode")).trim().equals(""))||(ConstansBO.NullCheck(objResultSet.getString("IFSCCode")).trim().equals(""))){//IFSC or NEFT should not empty
				objAccountDetailsBO.setNEFT_CD(ConstansBO.NullCheck(objResultSet.getString("NEFTCode")).trim()+ConstansBO.NullCheck(objResultSet.getString("IFSCCode")).trim());  //NEFT_CD
				objAccountDetailsBO.setRTGS_CD(ConstansBO.NullCheck(objResultSet.getString("NEFTCode")).trim()+ConstansBO.NullCheck(objResultSet.getString("IFSCCode")).trim());  // RTGS Code
			}else{
				objAccountDetailsBO.setNEFT_CD(ConstansBO.NullCheck(objResultSet.getString("NEFTCode")).trim());  //NEFT_CD
				objAccountDetailsBO.setRTGS_CD(ConstansBO.NullCheck(objResultSet.getString("IFSCCode")).trim());  // RTGS Code
			}
			
			objAccountDetailsBO.setAccountNo(objResultSet.getString("accountNumber").trim());  //AccountNo
			objAccountDetailsBO.setAccountType(objResultSet.getString("bankAccountType").trim());
			objAccountDetailsBO.setBranchName(objResultSet.getString("branchAddress").trim());  //BranchName
			objAccountDetailsBO.setBankCity(objResultSet.getString("bankCity").trim());  //BankCity
			objAccountDetailsBO.setBankLookUpId(objResultSet.getString("lookUpId").trim());  //BankLookUpId
			
			if(ConstansBO.containsOnlyNumbers(objResultSet.getString("micrCode").trim())) 
				objAccountDetailsBO.setMICR_CD(ConstansBO.NullCheck(objResultSet.getString("micrCode")).trim());  //MICR_CD
			
			if(intRTCode==6)
			{
				ftBankName	=	objDBFFilesDAO.getFranklinBankName(objAccountDetailsBO.getBankLookUpId(), objPreparedStmtFTBankName);
				if(!ftBankName.equals(""))
					objAccountDetailsBO.setBankName(ftBankName);  //BankName
				else
					objAccountDetailsBO.setBankName(objResultSet.getString("BankName").trim());   //BankName
			}
			else
			{
				objAccountDetailsBO.setBankName(objResultSet.getString("BankName").trim());   //BankName
			}
			
			//Hot coded if ING bank the Kotak MH bank
			if(objAccountDetailsBO.getBankLookUpId().equals("344"))
				objAccountDetailsBO.setBankName("Kotak Mahindra Bank Ltd");

			if(objAccountDetailsBO.getAccountType().equals("3")||objAccountDetailsBO.getAccountType().equals("4"))
				objAccountDetailsBO.setNriInvestor(true);
			
			objTransactionMainBO.setAccountDetailsBO(objAccountDetailsBO);
			objTransactionMainBO.setInvestorDetailsBO(objInvestorDetailsBO);
		}
		if(objResultSet != null) objResultSet.close();
		logger.info("*** BankInfo Success : Folio or UserTransId "+folioUserTrans+" InvestorId "+objInvestorDetailsBO.getInvestorId()+" ***");
		//totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : getBankInfo = " + totalTime);
		return objTransactionMainBO; 
	}
	/*
	 * PreparedStatement for AccountDetails
	 */
	public PreparedStatement getPreparedAccountsInfoByFolio(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct U.userId, U.Advisorid, N.NomineeName, N.nomineeRelation, N.DateofBirth, D.InvestorId, D.PAN, D.IsMinor, D.IsNRI, D.DOB, D.Occupation, E.InvestorId as OEInvestor, D.isBankVerified, B.TaxStatus as NRITaxStatus, D.TaxStatus as TaxStatus , A.Address1, A.Address2, A.Email as InvestorEmail, " +
				" A.City, A.PINCODE, A.Mobile, U.MobileNumber, A.WorkPhone, A.HomePhone, U.Email, A.Location, A.State, D.KYC,  isnull(D.TC_Version,'1.0') as TC_Version, EK.PAN as EkycPan, D.irType,  D.CKYCNumber " +
				" from InvestorBasicDetail D " +
				" inner join InvestorBankInfo B on B.Investor_ID = D.InvestorID and B.InvestorType='MF' and B.userBankId = isnull((Select UserBankId from FOLIO_BANK_INFO where FolioNumber = ? and Active=1 ),1) " +
				" inner join UserInfo U on U.userId = D.UserID " +
				" inner join InvestorAddress A on D.InvestorID = A.InvestorID  and A.AddressType = A.MailingAddress and A.InvestorType = 'MF'"+
				" left join EKYCInfo EK on D.Investorid = EK.Investorid and EK.Flag = 'A' and EK.Status = 'Y' " +
				" left join OE_AMC_Transaction E on D.InvestorID = E.investorId and E.flag = 'A' and E.AMCCode = ?" +
				" left join InvestorNominationDetails N on D.InvestorID = N.InvestorID and N.Flag='Y' where D.InvestorID = ? ";
		
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	/*
	 * PreparedStatement for AccountDetails
	 */
	public PreparedStatement getPreparedAccountsInfoByTransRefId(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct U.userId, U.Advisorid, N.NomineeName, N.nomineeRelation, N.DateofBirth, D.InvestorId, D.PAN, D.IsMinor, D.IsNRI, D.DOB, D.Occupation, E.InvestorId as OEInvestor, D.isBankVerified, B.TaxStatus as NRITaxStatus, D.TaxStatus as TaxStatus, A.Address1, A.Address2, A.Email as InvestorEmail, " +
				" A.City, A.PINCODE, A.Mobile, U.MobileNumber, A.WorkPhone, A.HomePhone, U.Email, A.Location, A.State, D.KYC, isnull(D.TC_Version,'1.0') as TC_Version, EK.PAN as EkycPan, D.irType, D.CKYCNumber " +
				" from InvestorBasicDetail D " +
				" inner join InvestorBankInfo B on B.Investor_ID = D.InvestorID and B.InvestorType='MF' and B.userBankId = isnull((Select UserBankId from TRANSACTION_BANK_INFO where UserTransRefId = ? ),1) " +
				" inner join UserInfo U on U.userId = D.UserID " +
				" inner join InvestorAddress A on D.InvestorID = A.InvestorID  and A.AddressType = A.MailingAddress and A.InvestorType = 'MF' " +
				" left join EKYCInfo EK on D.Investorid = EK.Investorid and EK.Flag = 'A' and EK.Status = 'Y' " + 
				" left join OE_AMC_Transaction E on D.InvestorID = E.investorId and E.flag = 'A' and E.AMCCode = ?" +
				" left join InvestorNominationDetails N on D.InvestorID = N.InvestorID  and N.Flag='Y' where D.InvestorID = ? ";
		
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	/*
	 * Getting the Investor Basic Details, Address, BankInfo - Based the Investor Id
	 */
	public TransactionMainBO getAccountsInfo(String folioNumber, String folioOrUserTransRefId, String aftAmcCode, int startsUserinfoMailId, ArrayList<Integer> arrInvestorMailId, int intRTCode, Hashtable<String, NewFolioBO> hashNewFolioBO, String strInvestorId, InvestorDetailsBO objInvestorDetailsBO,AccountDetailsBO objAccountDetailsBO, TransactionMainBO objTransactionMainBO, PreparedStatement objPreparedStatement, PreparedStatement objPreparedStmtFTBankName, PreparedStatement objSubBrokerCode, PreparedStatement objAMCNominee) throws Exception
	{
		logger.info("*** AccountInfo Begin : InvestorId "+strInvestorId+" ***");
		String strState			=	"";
		int userId				=	0;
		int advisorId			=	0;
		String eMailId			=	"";
		String nomineeName		=	"";
		int IsMinor				=	0;
		ResultSet objResultSet 		= 	null;
		String subBrokerCode	=	"";
		boolean amcNominee		=	false;
		String irType			=	"";
		String ekycFlag			=	"";
		String cKYCNumber		=	"";
		String tcVerson = "";
		String[] tcVersonParts;
		
		NewFolioBO objNewFolioBO	=	new NewFolioBO();
		DBFFilesDAO objDBFFilesDAO	=	new DBFFilesDAO();
		
		objPreparedStatement.setString(1, folioOrUserTransRefId); 
		objPreparedStatement.setString(2, aftAmcCode);
		objPreparedStatement.setString(3, strInvestorId); 
		//long startTime = System.currentTimeMillis();
		objResultSet	=	objPreparedStatement.executeQuery();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : RSTgetAccountsInfo = " + totalTime);
		if(objResultSet.next())
		{
			userId		=	objResultSet.getInt("userId");
			strState	=	ConstansBO.NullCheck(objResultSet.getString("State")).trim();
			eMailId		=	ConstansBO.NullCheck(objResultSet.getString("InvestorEmail"));
			IsMinor		=	objResultSet.getInt("IsMinor");
			advisorId	=	ConstansBO.NullCheckINT(objResultSet.getInt("Advisorid"));
			irType		=	ConstansBO.NullCheck(objResultSet.getString("irType"));
			ekycFlag	=	ConstansBO.NullCheck(objResultSet.getString("EkycPan"));
			cKYCNumber	=	ConstansBO.NullCheck(objResultSet.getString("CKYCNumber"));
			
			//objInvestorDetailsBO.setNRICountry(ConstansBO.NullCheck(objResultSet.getString("Country")));
			objInvestorDetailsBO.setNRIInvestor(objResultSet.getInt("IsNRI"));
			objInvestorDetailsBO.setInvestorId(objResultSet.getString("InvestorId"));
			objInvestorDetailsBO.setAddress1(objResultSet.getString("Address1").replace("|","").trim());  //Address1
			objInvestorDetailsBO.setBankVerified(ConstansBO.NullCheck(objResultSet.getString("isBankVerified")));
			objInvestorDetailsBO.setIrType(irType);
			
			if(irType.equals(""))
				objInvestorDetailsBO.setIrType("ON");
			
			if(intRTCode == 14)
				objInvestorDetailsBO.setKycFlag("K");
			else
				objInvestorDetailsBO.setKycFlag("Y");
			
			if((!ekycFlag.equals(""))&&(objInvestorDetailsBO.getIrType().equalsIgnoreCase("ON"))&&(intRTCode == 2)){
				objInvestorDetailsBO.setKycFlag("E");
			}//else if(!cKYCNumber.equals("")) // Not implemented in R&T side
				//objInvestorDetailsBO.setKycFlag("C");
			
			if(folioNumber.equals("")&&( (!objInvestorDetailsBO.getIrType().equals("OE") && (objInvestorDetailsBO.getKycFlag().equals("K") || objInvestorDetailsBO.getKycFlag().equals("Y")) ) || ((objInvestorDetailsBO.getBankVerified().equals("true"))&& (objInvestorDetailsBO.getIrType().equals("OE")||objInvestorDetailsBO.getKycFlag().equalsIgnoreCase("E"))))){
				String keyValue	=	intRTCode+"@"+ConstansBO.NullCheck(objResultSet.getString("pan"))+""+objResultSet.getString("InvestorId");
				if(irType.equals("OE")){
					if(hashNewFolioBO.containsKey(keyValue)){
						objNewFolioBO	=	new NewFolioBO(); 
						objNewFolioBO	=	hashNewFolioBO.get(keyValue);
						objNewFolioBO.setInvestorType("OE");
						hashNewFolioBO.put( keyValue, objNewFolioBO);
					}
				}else if(objInvestorDetailsBO.getKycFlag().equals("E")){
					if(hashNewFolioBO.containsKey(keyValue)){
						objNewFolioBO	=	new NewFolioBO(); 
						objNewFolioBO	=	hashNewFolioBO.get(keyValue);
						objNewFolioBO.setInvestorType("EKyc");
						hashNewFolioBO.put( keyValue, objNewFolioBO);
					}
				}
			}
			
			
			tcVerson =	ConstansBO.NullCheck(objResultSet.getString("TC_Version"));
			if(tcVerson.contains(":")){
				tcVersonParts	=	tcVerson.split(":");
				tcVerson	=	tcVersonParts[0];
			}
			objInvestorDetailsBO.setTcVersion(tcVerson);

			//If address1 >40 then split to address2 remaining strings
			String add2	=	"";
			if(objInvestorDetailsBO.getAddress1().length()>=40){	//Address3
				try{
					String add1			=	"";
					int commaSeperator	=	0;
					String addressString	=	objInvestorDetailsBO.getAddress1();
					
					commaSeperator	=	addressString.lastIndexOf(",", 40);
					if(commaSeperator<=0){
						commaSeperator	=	addressString.lastIndexOf(" ", 40);
						if(commaSeperator<=0)
							commaSeperator	=	40;
					}
					add1		=	addressString.substring(0, commaSeperator);
					add1		=	add1.concat(",");
					objInvestorDetailsBO.setAddress1(add1);
					add2		=	addressString.substring((commaSeperator+1),addressString.length());
				}catch(Exception e){
					logger.info(" ***** Exception in Address1 Column ***** "+e);
				}
			}
			
			if(add2.length()>0)
				add2	=	add2+" "+ConstansBO.NullCheck(objResultSet.getString("Address2")).replace("|","");
			else
				add2	=	ConstansBO.NullCheck(objResultSet.getString("Address2")).replace("|","");
			
			//Address2
			//If address2 >40 then split to address3 remaining strings
			if(add2.length()>=40){		//Address3
				try{
					String addressString	=	add2;
					String add3	=	"";
					int commaSeperator	=	0;
					
					commaSeperator	=	addressString.lastIndexOf(",", 40);
					if(commaSeperator<=0){
						commaSeperator	=	addressString.lastIndexOf(" ", 40);
						if(commaSeperator<=0)
							commaSeperator	=	40;
					}
					add2		=	addressString.substring(0, commaSeperator);
					add2		=	add2.concat(",");
					objInvestorDetailsBO.setAddress2(add2);
					add3		=	addressString.substring((commaSeperator+1),addressString.length());
					objInvestorDetailsBO.setAddress3(add3);
				}catch(Exception e){
					logger.info(" ****** Exception in Address2 Column ***** "+e);
				}
			}
			else
				objInvestorDetailsBO.setAddress2(add2); 
			
			
			objInvestorDetailsBO.setCity(ConstansBO.NullCheck(objResultSet.getString("City")).trim());  //City
			objInvestorDetailsBO.setPinCode(objResultSet.getString("PINCODE").trim());  //PinCode
			objInvestorDetailsBO.setPhoneOffice(ConstansBO.NullCheck(objResultSet.getString("WorkPhone")).trim());  //PhoneOffice
			objInvestorDetailsBO.setDateOfBirth(new ConstansBO(objResultSet.getString("DOB").trim()));  //DateOfBirth
			objInvestorDetailsBO.setPhoneRes(ConstansBO.NullCheck(objResultSet.getString("HomePhone")).trim());  //PhoneRes
			objInvestorDetailsBO.setFaxOff("");  //FaxOff
			objInvestorDetailsBO.setFaxRes("");  //FaxRes
			objInvestorDetailsBO.setOccupation(objResultSet.getString("Occupation").trim());
			
			if(((startsUserinfoMailId<userId)||(arrInvestorMailId.contains(userId)))&&(!eMailId.equals("")))
				objInvestorDetailsBO.setEmail(eMailId);  //Investor Email
			else
				objInvestorDetailsBO.setEmail(ConstansBO.NullCheck(objResultSet.getString("Email")).trim());  //Email
			
			if(!ConstansBO.NullCheck(objResultSet.getString("Mobile")).equals(""))
				objInvestorDetailsBO.setMobileNo(ConstansBO.NullCheck(objResultSet.getString("Mobile")));  //MobileNo
			else
				objInvestorDetailsBO.setMobileNo(ConstansBO.NullCheck(objResultSet.getString("MobileNumber")));  //MobileNo
			
			objInvestorDetailsBO.setStateWIFS(strState);  //State
			
			objInvestorDetailsBO.setLocation(ConstansBO.NullCheck(objResultSet.getString("Location")).trim());  //Location
			/*Blocked 30-Oct-2012
			if(intRTCode==2)
			{
				objInvestorDetailsBO.setLocation(ConstansBO.NullCheck(objResultSet.getString("Location")).trim());  //Location
			}
			else
			{
				strLocationCode	=	objDBFFilesDAO.getCampsLocationByPicode(objInvestorDetailsBO.getPinCode(), objCampsLocationByPicode);
				objInvestorDetailsBO.setLocation(strLocationCode);
			}*/
			if(objInvestorDetailsBO.getTaxNo().equals("") ||(IsMinor==1))
				objInvestorDetailsBO.setTaxStatus("2");
			else{
				
				if(objInvestorDetailsBO.getNRIInvestor()==1)
					objInvestorDetailsBO.setTaxStatus(objResultSet.getString("NRITaxStatus"));
				else
					objInvestorDetailsBO.setTaxStatus(objResultSet.getString("TaxStatus"));
			}
			
			if(IsMinor==0){//For minor no need to send nominee mail at 28Aug2015
				//For OE Investor Nominee should not sent for the corresponding amc's
				if(!ConstansBO.NullCheck(objResultSet.getString("OEInvestor")).equals("")){
					objInvestorDetailsBO.setIrType("OE");
					
					amcNominee	=	getAMCNominee(aftAmcCode, objAMCNominee); 
					if(amcNominee)
						nomineeName	=	ConstansBO.NullCheck(objResultSet.getString("NomineeName"));
				}else
					nomineeName	=	ConstansBO.NullCheck(objResultSet.getString("NomineeName"));
			}
			
			
			if((!nomineeName.equals(""))&&((nomineeName.replace(" ", "").equalsIgnoreCase(objInvestorDetailsBO.getFirstName().replace(" ", "")))||nomineeName.replace(" ", "").equalsIgnoreCase(objInvestorDetailsBO.getJointName1().replace(" ", ""))||nomineeName.replace(" ", "").equalsIgnoreCase(objInvestorDetailsBO.getJointName2().replace(" ", ""))))
			{
				if((intRTCode==1)&&(folioNumber.equals(""))) 
					objAccountDetailsBO.setNomineeOpted("Y");
				else if(folioNumber.equals(""))
					objAccountDetailsBO.setNomineeOpted("N");

				objAccountDetailsBO.setNomineeName("");
				objAccountDetailsBO.setNomineeRelation("");
			}
			else
			{
				if((nomineeName.equals(""))&&(folioNumber.equals(""))&&((intRTCode==1) || (intRTCode == 2))) {
					objAccountDetailsBO.setNomineeOpted("Y");
					objAccountDetailsBO.setNomineeName("");
				}else{
					objAccountDetailsBO.setNomineeName(nomineeName);
					objAccountDetailsBO.setNomineePercent(100);
					
					if(!nomineeName.equals(""))
						objAccountDetailsBO.setNomineeRelation(ConstansBO.NullCheck(objResultSet.getString("nomineeRelation")).trim());
					
					if(folioNumber.equals(""))
						objAccountDetailsBO.setNomineeOpted("N");
				}
			}
			//AccountDetailsBO Details// By default kyc as Y modified at 28Aug2015
			/*intKYC	=	objResultSet.getInt("KYC");
			if(intKYC==0)
				objAccountDetailsBO.setKYC_Flag("N");
			else
				objAccountDetailsBO.setKYC_Flag("Y");*/
			
			/*
			 * Date : 11Aug2012
			 * If SubbrokerCode is available for that transaction then we will send that code else if it is HDFC then subbroker code as Chennai else empty 
			 */
			
			if(advisorId>0){
				subBrokerCode	=	objDBFFilesDAO.getSubBrokerCode(advisorId, objSubBrokerCode);
			}
			if(!subBrokerCode.equals(""))
				objInvestorDetailsBO.setSubBrokerCode(subBrokerCode);  //SubBrokerCode as per Transaction holding's advisor id 
			//else if((intRTCode==1)&& (amcCode.equals("H")))
				//objInvestorDetailsBO.setSubBrokerCode("CHENNAI");  //SubBrokerCode as per HDFC Trxn.
			else
				objInvestorDetailsBO.setSubBrokerCode("");  //SubBrokerCode
			
			objTransactionMainBO.setAccountDetailsBO(objAccountDetailsBO);
			objTransactionMainBO.setInvestorDetailsBO(objInvestorDetailsBO);
		}
		if(objResultSet != null) objResultSet.close();
		logger.info("*** AccountInfo Success : InvestorId "+strInvestorId+" ***");
		//totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : getAccountsInfo = " + totalTime);
		return objTransactionMainBO; 
	}

	/*
	 * From AMCMasterCode table it returns the Reference Code based on LookupCode, MapID, SCHEMECODE, AMC and RT ";
	 */
	public PreparedStatement getPreparedAMCMasterCodes(Connection objConnection) throws Exception 
	{ 
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		/*
		 * Select trxn.ReferenceCode as TrxnCode, occ.ReferenceCode as Occupation, loc.ReferenceCode as Location, 
			bnk.ReferenceCode as BankAccountType, mdh.ReferenceCode as ModeOfHolding, tax.ReferenceCode as TaxCode, 
			st.ReferenceCode as State, pt.ReferenceCode as PayoutMethod -- A.ReferenceCode 
			from AFT_SCHEME_DETAILS D
			left join  AMCRTMasterCodes trxn on D.AMC_CODE=trxn.AMC and trxn.LookupCode='TrxnCode' and trxn.MapID = 'PUR'
			left join  AMCRTMasterCodes occ on D.AMC_CODE=occ.AMC and occ.LookupCode='Occupation' and occ.MapID = '1'
			left join  AMCRTMasterCodes loc on D.AMC_CODE=loc.AMC and loc.LookupCode='Location' and loc.MapID = 'A13'
			left join  AMCRTMasterCodes bnk on D.AMC_CODE=bnk.AMC and bnk.LookupCode='BankAccountType' and bnk.MapID = '4'
			left join  AMCRTMasterCodes mdh on D.AMC_CODE=mdh.AMC and mdh.LookupCode='ModeOfHolding' and mdh.MapID = '1'
			left join  AMCRTMasterCodes tax on D.AMC_CODE=tax.AMC and tax.LookupCode='TaxCode' and tax.MapID = '10'
			left join  AMCRTMasterCodes st on D.AMC_CODE=st.AMC and st.LookupCode='State' and st.MapID = 'AN'
			left join  AMCRTMasterCodes pt on D.AMC_CODE=pt.AMC and pt.LookupCode='PayoutMethod' and pt.MapID = 'CH1'
			where D.RT_CODE=1 and D.AMC_CODE=400004 and D.SCHEMECODE = 276
		 */
		SQLQuery = " " +
				" Select distinct trxn.ReferenceCode as TrxnCode, occ.ReferenceCode as Occupation, loc.ReferenceCode as Location, "+
				" bnk.ReferenceCode as BankAccountType, mdh.ReferenceCode as ModeOfHolding, tax.ReferenceCode as TaxCode, "+ 
				" st.ReferenceCode as State, pt.ReferenceCode as PayoutMethod, pt.ReferenceCode as PaymentMec "+ 
				" from AFT_SCHEME_DETAILS D "+
				" left join  AMCRTMasterCodes trxn on D.AMC_CODE=trxn.AMC and trxn.LookupCode='TrxnCode' and trxn.MapID = ? and trxn.RT = ? and trxn.AMC = ? "+ 	// 	1 TrxnCode = Purchase Type
				" left join  AMCRTMasterCodes occ on D.AMC_CODE=occ.AMC and occ.LookupCode='Occupation' and occ.MapID = ? and occ.RT = ? and occ.AMC = ? "+	// 	2 Occupation
				" left join  AMCRTMasterCodes loc on D.AMC_CODE=loc.AMC and loc.LookupCode='Location' and loc.MapID = ? and loc.RT = ? and loc.AMC = ? "+		//	3 Location
				" left join  AMCRTMasterCodes bnk on D.AMC_CODE=bnk.AMC and bnk.LookupCode='BankAccountType' and bnk.MapID = ? and bnk.RT = ? and bnk.AMC = ? "+	//	4 BankAccountType
				" left join  AMCRTMasterCodes mdh on D.AMC_CODE=mdh.AMC and mdh.LookupCode='ModeOfHolding' and mdh.MapID = ? and mdh.RT = ? and mdh.AMC = ? "+	//	5 ModeOfHolding
				" left join  AMCRTMasterCodes tax on D.AMC_CODE=tax.AMC and tax.LookupCode='TaxCode' and tax.MapID = ? and tax.RT = ? and tax.AMC = ? "+		//	6 TaxCode
				" left join  AMCRTMasterCodes st on D.AMC_CODE=st.AMC and st.LookupCode='State' and st.MapID = ? and st.RT = ? and st.AMC = ? "+				//	7 State
				" left join  AMCRTMasterCodes pt on D.AMC_CODE=pt.AMC and pt.LookupCode='PayMech' and pt.MapID = ? and pt.RT = ? and pt.AMC = ? "+		//	8 PayMec
				" where D.RT_CODE = ? and D.AMC_CODE = ? and D.SCHEMECODE = ? ";	//	9 RTCode	10 AMCCode 	11 SchemeCode
		
		//logger.info("AMCMasterCodes Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	/*
	 * Returns the Reference code from AMCMasterCode table
	 */
	public boolean getAMCMasterCodes(String strTransType, String strSchemeCode, String strAMCCode, int intRTCode, InvestorDetailsBO objInvestorDetailsBO, AccountDetailsBO objAccountDetailsBO, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception
	{
		if(strTransType.equals("PURA"))
			strTransType	=	"PUR";	
		ResultSet objResultSet 		= 	null;
		objPreparedStatement.setString(1, strTransType);
		objPreparedStatement.setInt(2, intRTCode);
		objPreparedStatement.setString(3, strAMCCode.trim());
		objPreparedStatement.setString(4, objInvestorDetailsBO.getOccupation());
		objPreparedStatement.setInt(5, intRTCode);
		objPreparedStatement.setString(6, strAMCCode.trim());
		objPreparedStatement.setString(7, objInvestorDetailsBO.getLocation().trim());
		objPreparedStatement.setInt(8, intRTCode);
		objPreparedStatement.setString(9, strAMCCode.trim());
		objPreparedStatement.setString(10, objAccountDetailsBO.getAccountType().trim());
		objPreparedStatement.setInt(11, intRTCode);
		objPreparedStatement.setString(12, strAMCCode.trim());
		objPreparedStatement.setString(13, objInvestorDetailsBO.getModeOfHolding().trim());
		objPreparedStatement.setInt(14, intRTCode);
		objPreparedStatement.setString(15, strAMCCode.trim());
		objPreparedStatement.setString(16, objInvestorDetailsBO.getTaxStatus().trim());
		objPreparedStatement.setInt(17, intRTCode);
		objPreparedStatement.setString(18, strAMCCode.trim());
		objPreparedStatement.setString(19, objInvestorDetailsBO.getStateWIFS().trim());
		objPreparedStatement.setInt(20, intRTCode);
		objPreparedStatement.setString(21, strAMCCode.trim());
		objPreparedStatement.setString(22, objAccountDetailsBO.getBankLookUpId().trim());
		objPreparedStatement.setInt(23, intRTCode);
		objPreparedStatement.setString(24, strAMCCode.trim());
		objPreparedStatement.setInt(25, intRTCode);
		objPreparedStatement.setString(26, strAMCCode.trim());
		objPreparedStatement.setString(27, strSchemeCode.trim());
		//long startTime = System.currentTimeMillis();
		objResultSet	=	objPreparedStatement.executeQuery();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : RSTgetAMCMasterCodes = " + totalTime);
		if(objResultSet.next())
		{
			objTransactionBO.setTransactionType(ConstansBO.NullCheck(objResultSet.getString("TrxnCode")).trim());
			objInvestorDetailsBO.setOccupationCode(ConstansBO.NullCheck(objResultSet.getString("Occupation")).trim());
			objInvestorDetailsBO.setLocationCode(ConstansBO.NullCheck(objResultSet.getString("Location")).trim());
			objAccountDetailsBO.setAccountType(ConstansBO.NullCheck(objResultSet.getString("BankAccountType")).trim());
			objAccountDetailsBO.setHoldingNature(ConstansBO.NullCheck(objResultSet.getString("ModeOfHolding")).trim());
			objAccountDetailsBO.setTaxStatus(ConstansBO.NullCheck(objResultSet.getString("TaxCode")).trim());
			objAccountDetailsBO.setPaymentMechanism(ConstansBO.NullCheck(objResultSet.getString("PaymentMec")).trim());
			objInvestorDetailsBO.setState(ConstansBO.NullCheck(objResultSet.getString("State")));
			/*
			    for State not in lookup
				CAMS FF
				if add is overseas address
				leave state as blank and location is "chennai"
				KARVY FF
				if add is overseas address
				leave state as others - 27 and location is "chennai"
				FT FF
				if add is overseas address
				leave state as Unknown State - ZZ and location is "chennai"
				DISPL FF**
				if add is overseas address
				leave state as blank and location is "chennai"
			 */
			if(objInvestorDetailsBO.getState().equals(""))
			{
				String add3	=	objInvestorDetailsBO.getAddress3();
				add3		=	objInvestorDetailsBO.getAddress3().concat(objInvestorDetailsBO.getStateWIFS()).trim();
				objInvestorDetailsBO.setAddress3(add3);
				
				if(intRTCode==1)
				{
					objInvestorDetailsBO.setLocationCode("CHN");
				}
				else if(intRTCode==2)
				{
					objInvestorDetailsBO.setLocationCode("C3");
					objInvestorDetailsBO.setState("27");
				}
				else if(intRTCode==6)
				{
					objInvestorDetailsBO.setLocationCode("M3");
					objInvestorDetailsBO.setState("ZZ");
				}
				else if(intRTCode==7)
				{
					objInvestorDetailsBO.setLocationCode("C3");
				}
			}
			// For CAMS only paymentmechanism empty then send as NEFT by default
			if((intRTCode==1)&&(objAccountDetailsBO.getPaymentMechanism().equals("")))
				objAccountDetailsBO.setPaymentMechanism("NEFT");
			
			//logger.info("objAccountDetailsBO ::::::: "+objAccountDetailsBO.getPaymentMechanism()+" RT "+intRTCode+" AMC "+strAMCCode+" lookUp "+objAccountDetailsBO.getBankLookUpId());
			
		}
		if(objResultSet != null) objResultSet.close();
		logger.info(" *** AMCMaster Codes. Success strTransType : "+strTransType+" SchmCode : "+strSchemeCode+" AMCCode : "+strAMCCode+" RTCode : "+intRTCode+" *** ");
		//totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : getAMCMasterCodes = " + totalTime);
		return true;
	}
	/*public PreparedStatement getPreparedSIPBankDetails(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select case when L.lookUpId=999 then I.bankName else L.lookUpDescription end as BankName, I.accountNumber " +
				" from InvestorBankInfo I " +
				" inner join Lookup L on lookUpType='BANKLIST' and I.BANKLOOKUPID = L.lookUpId " +
				" where I.Investor_ID = ? and I.userBankId = ? and I.InvestorType='MF'"; 
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public SIPInfoBO getSIPBankDetails(String strInvestorId, int intUserBankId, SIPInfoBO objSIPInfoBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet 		= 	null;
		
		objPreparedStatement.setString(1, strInvestorId);
		objPreparedStatement.setInt(2, intUserBankId);
		objResultSet	=	objPreparedStatement.executeQuery();
		if(objResultSet.next())
		{
			//objSIPInfoBO.setAccountNo(objResultSet.getString("accountNumber"));
			objSIPInfoBO.setInvestorBankName(objResultSet.getString("bankName"));
		}
		if(objResultSet != null) objResultSet.close();
		logger.info("SIP BankDetails. InvestorId : "+strInvestorId+" UserBankId : "+intUserBankId+" Results ::: FolioNo. : "+objSIPInfoBO.getFolioNumber()+"BankName : "+objSIPInfoBO.getInvestorBankName());
		return objSIPInfoBO;
	}*/
	
	/*
	 * From AFT_SCHEME_DETAILS table it returns the AMCCode based on the Schemecode
	*/
	/*public PreparedStatement getPreparedAMCCode(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select AMC_Code from AFT_SCHEME_DETAILS where SCHEMECODE = ? "; 
		//logger.info("AMCCode Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	
	 * Returns AMCCode from AFT_SCHEME_DETAILS Table
	public String getAMCCode(String strSchemeCode, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet 		= 	null;
		String strAMCCode	=	"";
		
		objPreparedStatement.setString(1, strSchemeCode);
		objResultSet	=	objPreparedStatement.executeQuery();
		if(objResultSet.next())
		{
			strAMCCode	=	ConstansBO.NullCheck(String.valueOf(objResultSet.getInt("AMC_Code"))).trim();
		}
		else
		{
			strAMCCode	=	"";
		}
		objResultSet.close();
		logger.info("SchmCode : "+strSchemeCode+" ResultAMCCode:::"+strAMCCode);
		return strAMCCode;
	}
	/*
	 * From AMCBankMaster table it returns bankAccNo,BankName based on the Schemecode
	 */
	public PreparedStatement getPreparedAMCBank(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select bankAccNo,BankName from AMCBankMaster where AmcCode=? and RTCode=? and SchemeCode=? "; 
		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	/*
	 * Returns AddtionalInfoBO - BankAccountNo, BankName from AMCBankMaster Table
	 */
	public AddtionalInfoBO getPreparedAMCBank(String strAMCCode, int strRTCode, String strAFTSchemeCode, PreparedStatement objPreparedStatement) throws Exception 
	{
		AddtionalInfoBO	objAddtionalInfoBO	=	new AddtionalInfoBO();
		ResultSet objResultSet 		= 	null;
		
		objPreparedStatement.setString(1, strAMCCode);
		objPreparedStatement.setInt(2, strRTCode);
		objPreparedStatement.setString(3, strAFTSchemeCode);
		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetAMCBank = " + totalTime);
			if(objResultSet.next())
			{
				objAddtionalInfoBO.setAMCAccountNo(ConstansBO.NullCheck(objResultSet.getString("bankAccNo")).trim());
				objAddtionalInfoBO.setAMCBankName(ConstansBO.NullCheck(objResultSet.getString("BankName")).trim());
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getAMCBank = " + totalTime);
		}catch(Exception e){logger.info("Exception in getAMCBank Details. AMCCode: "+strAMCCode+" RTCode : "+strRTCode+" ShmCode : "+strAFTSchemeCode+" Exp "+e);}
		finally{		if(objResultSet != null) objResultSet.close();		}		
		logger.info("DepBank Details. SchmCode : "+strAFTSchemeCode+" AMCCode : "+strAMCCode+" ResultAMCAccNo.:::"+objAddtionalInfoBO.getAMCAccountNo()+" BnkName:::"+objAddtionalInfoBO.getAMCBankName());
		return objAddtionalInfoBO;
	}
	/*
	 * PreparedStatement for JointHolder's,Gardian Names from InvestorAdditionalInfo table
	 */
	public PreparedStatement getPreparedAddtionalInfo(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select Distinct A.AssociatedInvestorID, A.RelationshipID,B.InvestorName,B.PAN, B.DOB, B.InHouseNo, B.IsMinor, B.IsNRI, B.AnnualIncome, B.PoliticalEP, B.taxstatus, B.Occupation, B.CountryOfBirth, B.Nationality, B.CKYCNumber, EK.PAN as EkycPan"
				+ " from InvestorAdditionalInfo A"
				+ " inner join InvestorBasicDetail B  on A.AssociatedInvestorID = B.InvestorID"
				+ " left join EKYCInfo EK on B.Investorid = EK.Investorid and EK.Flag = 'A' and EK.Status = 'Y'"
				+ " where  A.HoldingProfileID = ? order by A.RelationshipID  ";

		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	/*
	 * Returns the AddtionalInfo table - Investor name, Pan no.
	 */
	public InvestorDetailsBO getAddtionalInfo(int rtcode, String strAMCCode, InvestorDetailsBO objInvestorDetailsBO, TransactionBO objTransactionBO, Hashtable<String, NewFolioBO> hashNewFolioBO, int intHoldingProfileId, PreparedStatement objPreparedStatement, PreparedStatement objPreparedStatementFatco, PreparedStatement objFatcaOccupationCodes, PreparedStatement objTaxStatusCodes, PreparedStatement objOTPResponseByPANAadhaarNo) throws Exception
	{
		logger.info("=== Additional Info Begin ==");
		int isMinor 				=	0;
		String panNo				=	"";
		String strRelationShipId	=	"";
		String annualIncomeCode		=	"";
		String politicalEP			=	"";
		//String birthPlace			=	"";
		String occupationCode		=	"";
		String nationality			=	"";
		String countryOfBirth		=	"";
		String aAdhaarNo			=	"0";
		String ekycFlag				=	"";
		int taxStatus				=	0;
		
		NewFolioBO objNewFolioBO	=	new NewFolioBO(); 
		FatcaDocBO objFatcaDocBO	=	new FatcaDocBO();
		DBFFilesBL objDBFFilesBL	=	new DBFFilesBL();
		ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
		
		ResultSet objResultSet 		= 	null;
		objPreparedStatement.setInt(1, intHoldingProfileId);
		//long startTime = System.currentTimeMillis();
		objResultSet	=	objPreparedStatement.executeQuery();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : RSTgetAdditionalInfo = " + totalTime);	
		while(objResultSet.next())
		{
			occupationCode		=	"";
			countryOfBirth		=	"";
			panNo				=	"";
			objNewFolioBO	=	new NewFolioBO();
			objFatcaDocBO	=	new FatcaDocBO();
			
			panNo				=	objResultSet.getString("PAN");
			strRelationShipId	=	objResultSet.getString("RelationshipID").trim();
			taxStatus			=	objResultSet.getInt("taxstatus");
			isMinor	=		objResultSet.getInt("IsMinor");
			nationality		=	objResultSet.getString("Nationality");
			countryOfBirth		=	ConstansBO.NullCheck(objResultSet.getString("CountryOfBirth"));
			ekycFlag	=	ConstansBO.NullCheck(objResultSet.getString("EkycPan"));
			
			objFatcaDocBO.setMinorFlag(isMinor);
			if( (!countryOfBirth.equals("")) && ( ((strRelationShipId.equals("F")) && isMinor == 0 ) || (strRelationShipId.equals("S")) || (strRelationShipId.equals("T")) || (strRelationShipId.equals("G")) ) ){ 
				
				//Only for 1st investor & Guardian
				occupationCode		=	objResultSet.getString("Occupation").trim();
				objFatcaDocBO	=	getFatcaOccupationCodes(objTransactionBO.getRtCode(), occupationCode, nationality, countryOfBirth, objFatcaDocBO, objFatcaOccupationCodes);
				objFatcaDocBO	=	getTaxStatusCodes(objResultSet.getInt("AssociatedInvestorID"), objFatcaDocBO, objTaxStatusCodes);
				
				objFatcaDocBO.setDateOfBirth(new ConstansBO(objResultSet.getString("DOB").trim()));  //DateOfBirth
				
				if(!panNo.startsWith("MPAN-"))
					objFatcaDocBO.setPan(panNo);

				objFatcaDocBO.setInvestorId(objResultSet.getInt("AssociatedInvestorID"));
				objFatcaDocBO.setInvestorName(objResultSet.getString("InvestorName").trim());
				
				
				annualIncomeCode	= 	objDBFFilesBL.getAnnualIncomeCode(objResultSet.getDouble("AnnualIncome"));
				objFatcaDocBO.setAnnualIncomeCode(annualIncomeCode);
				objFatcaDocBO.setAnnualIncome(objResultSet.getDouble("AnnualIncome"));
				
				politicalEP	=	ConstansBO.NullCheck(objResultSet.getString("PoliticalEP"));
				politicalEP	=	politicalEP.replaceAll("NA", "N");
				if(!politicalEP.equals(""))
					objFatcaDocBO.setPoliticallyExpPer(politicalEP);
				else
					objFatcaDocBO.setPoliticallyExpPer("N");
				
				/*if((objTransactionBO.getFolioNo().equals(""))&&(objTransactionBO.getExistForwardFeed().equals("")))
					objFatcaDocBO.setNewFolioFlag(true);*/
				
				if(objTransactionBO.getExistForwardFeed().equals("")){ // if country of birth is available then FATCO doc needs to fill 
					objFatcaDocBO	=	getCitizenship(objTransactionBO.getRtCode(), strAMCCode, objFatcaDocBO, objPreparedStatementFatco);
				}

				//1,2,13,20,21,3,99 -- Indv
				if( (taxStatus == 1)||(taxStatus == 2) || (taxStatus == 3) ||(taxStatus == 13)||(taxStatus == 20)||(taxStatus == 21)||(taxStatus == 99) ){// For individuals
					objFatcaDocBO.setInvestorRelationship(strRelationShipId);
					objFatcaDocBO.setTaxStatusFlag("IN");
				}else{// For nan-individuals 				
					objFatcaDocBO.setTaxStatusFlag("NONIN");
					objFatcaDocBO.setInvestorRelationship("H");
				}
				/*if(((strRelationShipId.equals("F")) || (strRelationShipId.equals("G"))) && ( (investorStatus == 3) || (investorStatus == 7) || (investorStatus == 8))){
					
					objInvestorDetailsBO.setSc_flag("Y");//"Y" for individuals if self declaration provided. Otherwise "N" should be populated. Leave blank for non individuals
					if(objTransactionBO.getRtCode() == 1){
						objInvestorDetailsBO.setFormType("Self declaration");
					}else if(objTransactionBO.getRtCode() == 2){
						
						if(objResultSet.getString("Nationality").trim().equalsIgnoreCase("USA"))
							objInvestorDetailsBO.setFormType("W9");
						else
							objInvestorDetailsBO.setFormType("NOT REQUIRED");
					}
				}else if((objTransactionBO.getRtCode() == 2) || (objTransactionBO.getRtCode() == 6) )//// For nan-individuals
					objInvestorDetailsBO.setFormType("NO FORM");*/
				
				arrFatcaDocBO.add(objFatcaDocBO);
			}else{ //FATCA not available pans
				if(!panNo.startsWith("MPAN-")){
					objFatcaDocBO.setFatcaNotAvailablePan(panNo);
					objFatcaDocBO.setInvestorName(objResultSet.getString("InvestorName").trim());
					arrFatcaDocBO.add(objFatcaDocBO); 
				}
			}
			
			if(strRelationShipId.equals("F"))
			{
				objInvestorDetailsBO.setInvestorId(objResultSet.getString("AssociatedInvestorID").trim());
				objInvestorDetailsBO.setFirstName(objResultSet.getString("InvestorName").trim());
				objInvestorDetailsBO.setValid_Pan("Y");
				objInvestorDetailsBO.setIHNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));

				if(!ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")).equals(""))
					objInvestorDetailsBO.setcKYCRefId1(ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")));
				
				if(isMinor==1) // For Minor no need to send PAN
				{
					objInvestorDetailsBO.setTaxNo(""); 
					objInvestorDetailsBO.setValid_Pan("");
				}
				else
				{
					objInvestorDetailsBO.setTaxNo(objResultSet.getString("PAN"));
					
					if((!ekycFlag.equals(""))&&(rtcode == 6))
						aAdhaarNo	=	getOTPResponseByPANAadhaarNo(objResultSet.getString("PAN"), objInvestorDetailsBO, objOTPResponseByPANAadhaarNo);
					objInvestorDetailsBO.setaAdhaarNo_fhld(aAdhaarNo);

					// Adding the New Folio for file creation
					if((objTransactionBO.getFolioNo().equals(""))&&(objTransactionBO.getExistForwardFeed().equals("")))
					{
						objNewFolioBO.setInvestorName(objInvestorDetailsBO.getFirstName());
						objNewFolioBO.setPanNo(objInvestorDetailsBO.getTaxNo());
						objNewFolioBO.setInvestorId(Integer.parseInt(objInvestorDetailsBO.getInvestorId()));
						
						objNewFolioBO.setRtCode(objTransactionBO.getRtCode());
						objNewFolioBO.setInHouseNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));
						objNewFolioBO.setFolioNo(objTransactionBO.getFolioNo());
						
						hashNewFolioBO.put(objNewFolioBO.getRtCode()+"@"+objNewFolioBO.getPanNo()+objNewFolioBO.getInvestorId(), objNewFolioBO);
					}
				}
				if(objInvestorDetailsBO.getModeOfHolding().equals(""))
					objInvestorDetailsBO.setModeOfHolding("1");

			}
			else if(strRelationShipId.equals("S"))
			{
				objInvestorDetailsBO.setJointName1(objResultSet.getString("InvestorName"));
				objInvestorDetailsBO.setPanHolder2(objResultSet.getString("PAN"));
				objInvestorDetailsBO.setJH1ValidPan("Y");
				objInvestorDetailsBO.setModeOfHolding("3");
				objInvestorDetailsBO.setIHNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));
				
				if(!ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")).equals(""))
					objInvestorDetailsBO.setcKYCRefId2(ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")));
				
				objInvestorDetailsBO.setDateOfBirth2(new ConstansBO(objResultSet.getString("DOB").trim()));  //DateOfBirth
				
				if((!ekycFlag.equals(""))&&(rtcode == 6))
					aAdhaarNo	=	getOTPResponseByPANAadhaarNo(objResultSet.getString("PAN"), objInvestorDetailsBO, objOTPResponseByPANAadhaarNo);
				objInvestorDetailsBO.setaAdhaarNo_shld(aAdhaarNo);

				
				if((rtcode == 14)&&(!String.valueOf(objInvestorDetailsBO.getcKYCRefId2()).equals("0")))
					objInvestorDetailsBO.setKycFlag2("C");
				else if (rtcode == 14)
					objInvestorDetailsBO.setKycFlag2("K");
				
				// Adding the New Folio for file creation
				if((objTransactionBO.getFolioNo().equals(""))&&(objTransactionBO.getExistForwardFeed().equals("")))
				{
					objNewFolioBO.setInvestorName(objInvestorDetailsBO.getJointName1());
					objNewFolioBO.setPanNo(objInvestorDetailsBO.getPanHolder2());
					objNewFolioBO.setInvestorId(objResultSet.getInt("AssociatedInvestorID"));
					objNewFolioBO.setRtCode(objTransactionBO.getRtCode());
					objNewFolioBO.setInHouseNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));
					objNewFolioBO.setFolioNo(objTransactionBO.getFolioNo());
					
					hashNewFolioBO.put(objNewFolioBO.getRtCode()+"@"+objNewFolioBO.getPanNo()+objNewFolioBO.getInvestorId(), objNewFolioBO);
				}
			}
			else if(strRelationShipId.equals("T"))
			{
				objInvestorDetailsBO.setJointName2(objResultSet.getString("InvestorName"));
				objInvestorDetailsBO.setPanHolder3(objResultSet.getString("PAN"));
				objInvestorDetailsBO.setJH2ValidPan("Y");
				objInvestorDetailsBO.setModeOfHolding("3");
				objInvestorDetailsBO.setIHNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));
				
				if(!ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")).equals(""))
					objInvestorDetailsBO.setcKYCRefId3(ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")));
				
				objInvestorDetailsBO.setDateOfBirth3(new ConstansBO(objResultSet.getString("DOB").trim()));  //DateOfBirth
				
				if((!ekycFlag.equals(""))&&(rtcode == 6))
					aAdhaarNo	=	getOTPResponseByPANAadhaarNo(objResultSet.getString("PAN"), objInvestorDetailsBO, objOTPResponseByPANAadhaarNo);
				objInvestorDetailsBO.setaAdhaarNo_thld(aAdhaarNo);
				
				if((rtcode == 14)&&(!String.valueOf(objInvestorDetailsBO.getcKYCRefId3()).equals("0")))
					objInvestorDetailsBO.setKycFlag3("C");
				else if (rtcode == 14)
					objInvestorDetailsBO.setKycFlag3("K");
				// Adding the New Folio for file creation
				if((objTransactionBO.getFolioNo().equals(""))&&(objTransactionBO.getExistForwardFeed().equals("")))
				{
					objNewFolioBO.setInvestorName(objInvestorDetailsBO.getJointName2());
					objNewFolioBO.setPanNo(objInvestorDetailsBO.getPanHolder3());
					objNewFolioBO.setInvestorId(objResultSet.getInt("AssociatedInvestorID"));
					objNewFolioBO.setRtCode(objTransactionBO.getRtCode());
					objNewFolioBO.setInHouseNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));
					objNewFolioBO.setFolioNo(objTransactionBO.getFolioNo());
					
					hashNewFolioBO.put(objNewFolioBO.getRtCode()+"@"+objNewFolioBO.getPanNo()+objNewFolioBO.getInvestorId(), objNewFolioBO);
				}
			}
			else if(strRelationShipId.equals("G"))
			{
				objInvestorDetailsBO.setGuardianName(objResultSet.getString("InvestorName"));
				objInvestorDetailsBO.setGuardianPanNo(objResultSet.getString("PAN"));
				objInvestorDetailsBO.setGValid_Pan("Y");
				objInvestorDetailsBO.setModeOfHolding("1");
				
				if(!ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")).equals(""))
					objInvestorDetailsBO.setcKYCRefIdG(ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")));
				
				objInvestorDetailsBO.setDateOfBirthG(new ConstansBO(objResultSet.getString("DOB").trim()));  //DateOfBirth
				
				if((!ekycFlag.equals(""))&&(rtcode == 6))
					aAdhaarNo	=	getOTPResponseByPANAadhaarNo(objResultSet.getString("PAN"), objInvestorDetailsBO, objOTPResponseByPANAadhaarNo);
				objInvestorDetailsBO.setaAdhaarNo_gurd(aAdhaarNo);
				
				if((rtcode == 14)&&(!String.valueOf(objInvestorDetailsBO.getcKYCRefIdG()).equals("0")))
					objInvestorDetailsBO.setKycFlagG("C");
				else if (rtcode == 14)
					objInvestorDetailsBO.setKycFlagG("K");
				
				// Adding the New Folio for file creation
				if((objTransactionBO.getFolioNo().equals(""))&&(objTransactionBO.getExistForwardFeed().equals("")))
				{
					objNewFolioBO.setInvestorName(objInvestorDetailsBO.getFirstName()+" U/G "+objInvestorDetailsBO.getGuardianName());
					objNewFolioBO.setPanNo(objInvestorDetailsBO.getGuardianPanNo());
					objNewFolioBO.setInvestorId(objResultSet.getInt("AssociatedInvestorID"));
					objNewFolioBO.setRtCode(objTransactionBO.getRtCode());
					objNewFolioBO.setInHouseNo(ConstansBO.NullCheck(objResultSet.getString("InHouseNo")));
					objNewFolioBO.setMinorIHNo(objInvestorDetailsBO.getIHNo());
					objNewFolioBO.setFolioNo(objTransactionBO.getFolioNo());
					
					hashNewFolioBO.put(objNewFolioBO.getRtCode()+"@"+objNewFolioBO.getPanNo()+objNewFolioBO.getInvestorId(), objNewFolioBO);
				}
			}
		}
		objInvestorDetailsBO.setArrFatcaDocBO(arrFatcaDocBO);
		if(objResultSet != null) objResultSet.close();
		//totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : getAdditionalInfo = " + totalTime);
		return objInvestorDetailsBO;
	}
	public PreparedStatement getPreparedStmtInvestorCitizenship(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct C.TaxRefNumber, L.lookUpDescription as taxResidency, isnull(C.TaxReferenceType,'X') as TaxReferenceType,"
				+ " isnull((Select A.RTMapId from  AMCRTMasterCodesFatca A where A.LookupId = C.Citizenship and A.LookupType='Countrycode' and A.RTCode = 1), C.Citizenship) as taxResidencyCode"
				+ " from InvestorCitizenship C"
				+ " inner join InvestorBasicDetail B on B.InvestorID = C.InvestorID"
				+ " inner join lookup L on L.lookUpId = C.Citizenship and L.lookUpType = 'Country'"
				+ " where C.Active = 'A' and C.Investorid = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public FatcaDocBO getCitizenship(int rtCode, String AMCCode, FatcaDocBO objFatcaDocBO,PreparedStatement objPreparedStatement) throws Exception
	{
		String taxRefNumber	=	"";
		logger.info("=== InvestorCitizenship Begin ===");
		FatcaDocBO tempFatcaDocBO	=	new FatcaDocBO();
		ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
		
		ResultSet objResultSet 		= 	null;
		try{
			objPreparedStatement.setInt(1, objFatcaDocBO.getInvestorId());
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetCitizenship = " + totalTime);
			while(objResultSet.next())
			{
				tempFatcaDocBO	=	new FatcaDocBO();
				taxRefNumber	=	ConstansBO.NullCheck(objResultSet.getString("TaxRefNumber"));
				
				if(!taxRefNumber.equals(""))
					tempFatcaDocBO.setTaxPayerIdentificationNo(taxRefNumber);
				else
					tempFatcaDocBO.setTaxPayerIdentificationNo("N/A");
				tempFatcaDocBO.setTaxResidencyCode(ConstansBO.NullCheck(objResultSet.getString("taxResidencyCode")));
				tempFatcaDocBO.setIdentificationType(ConstansBO.NullCheck(objResultSet.getString("TaxReferenceType")));
				arrFatcaDocBO.add(tempFatcaDocBO);
			}
			objFatcaDocBO.setArrFatcaDocBO(arrFatcaDocBO);
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getCitizenship = " + totalTime);
		}catch(Exception e){
		}finally{
			if(objResultSet != null) objResultSet.close();
		}
		return objFatcaDocBO;
	}
	
	/*public PreparedStatement getPreparedStmtFolioSchemeExist(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"select PositionID from PositionsInfo where Active = 'A' and FolioNumber = ? and SchemeCode = ?";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public String getFolioSchemeExist(String folioNumber, int schemeCode, PreparedStatement objPreparedStatement) throws Exception
	{
		logger.info("=== Folio exist validation begin ===");
		String fatcaFlag	=	"N";
		ResultSet objResultSet 		= 	null;
		try{
			objPreparedStatement.setString(1, folioNumber);
			objPreparedStatement.setInt(2, schemeCode);
			long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			long totalTime = System.currentTimeMillis() - startTime;
			logger.info("TIMER : RSTgetFolioSchemeExist = " + totalTime);
			if(objResultSet.next())
			{
				fatcaFlag	=	"Y";
			}
			totalTime = System.currentTimeMillis() - startTime;
			logger.info("TIMER : getFolioSchemeExist = " + totalTime);
		}catch(Exception e){
			logger.error("Exp:"+e);
		}finally{
			if(objResultSet != null) objResultSet.close();
		}
		logger.info("=== Folio exist validation End ===Flo:"+folioNumber+" shm:"+schemeCode+" Flag:"+fatcaFlag);
		return fatcaFlag;
	}*/
	
	public TransactionBO setTransactionDateTimeFormat(String fDate, String tDate, TransactionBO objTransactionBO,String strTransactionDate, String strTransactionTime) throws Exception
	{
		long daysBetween	=	0;
		DateFormat Tempformatter = new SimpleDateFormat("yyyy-MM-dd");
		int intTime	=	Integer.valueOf(strTransactionTime.substring(0,2)); 
		ConstansBO objTrxnDate	=	new ConstansBO(strTransactionDate);

		objTransactionBO.setTransactionDateMIS(objTrxnDate);
		objTransactionBO.setTransactionTimeMIS(strTransactionTime);
		
		Calendar calendar = Calendar.getInstance(); 
		Calendar trxnDate = Calendar.getInstance();
		trxnDate.setTime(objTrxnDate.getDateFirstView());
		
		Calendar fromDate 	= Calendar.getInstance(); 
		Calendar toDate	 	= Calendar.getInstance(); 
		
		fromDate.setTime((Date)Tempformatter.parse(fDate.trim()));
		toDate.setTime((Date)Tempformatter.parse(tDate.trim()));
		
		daysBetween	=	(fromDate.getTime().getTime() - toDate.getTime().getTime())	/ (24 * 3600 * 1000);

		if((String.valueOf(Tempformatter.format(calendar.getTime()))).equals(String.valueOf(Tempformatter.format(trxnDate.getTime()))))
		{
			objTransactionBO.setPricing("F");
			objTransactionBO.setTransactionDate(objTrxnDate);
			objTransactionBO.setTransactionTime(strTransactionTime);
		}
		else if(daysBetween<=-2)
		{
			objTransactionBO.setPricing("F");
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(calendar.getTime()))));
			objTransactionBO.setTransactionTime("10:00:00");	
		}
		else if(intTime>13)
		{
			objTransactionBO.setPricing("F");
			trxnDate.add(Calendar.DATE,+1);
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(trxnDate.getTime()))));
			objTransactionBO.setTransactionTime("10:00:00");
		}
		else
		{
			objTransactionBO.setPricing("F");
			objTransactionBO.setTransactionDate(objTrxnDate);
			objTransactionBO.setTransactionTime(strTransactionTime);
		}
		logger.info("Trxn Date : "+objTransactionBO.getTransactionDate().getDateFirstView()+" trxn time : "+objTransactionBO.getTransactionTime());
		return objTransactionBO;
	}
	public TransactionBO setLiquidTransactionDateTimeFormat(String fDate, String tDate, String strLiquidShm, TransactionBO objTransactionBO,String strTransactionDate, String strTransactionTime) throws Exception 
	{
		DateFormat Tempformatter = new SimpleDateFormat("yyyy-MM-dd");
		int intTime	=	0;
		long daysBetween	=	0;
		try{
			intTime	=	Integer.valueOf(strTransactionTime.substring(0,2));
		}catch(Exception e){}
		int intValidateTime	=	0;
		ConstansBO objTrxnDate	=	new ConstansBO(strTransactionDate);
		Calendar calendar = Calendar.getInstance(); 
		Calendar trxnDate = Calendar.getInstance();
		trxnDate.setTime(objTrxnDate.getDateFirstView());
		
		objTransactionBO.setTransactionDateMIS(objTrxnDate);
		objTransactionBO.setTransactionTimeMIS(strTransactionTime);
		
		Calendar fromDate 	= Calendar.getInstance(); 
		Calendar toDate	 	= Calendar.getInstance(); 
		
		fromDate.setTime((Date)Tempformatter.parse(fDate.trim()));
		toDate.setTime((Date)Tempformatter.parse(tDate.trim()));
		
		daysBetween	=	(fromDate.getTime().getTime() - toDate.getTime().getTime())	/ (24 * 3600 * 1000);
		
		//For Lequid Scheme 11 AM others 2 PM
		if(strLiquidShm.equals(""))
			intValidateTime	=	13;
		else
			intValidateTime	=	10;
		
		
		if(calendar.get(Calendar.DAY_OF_WEEK)==2) 
		{
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(calendar.getTime()))));
			if(String.valueOf(Tempformatter.format(calendar.getTime())).equals(String.valueOf(Tempformatter.format(trxnDate.getTime()))))
			{
				if((intTime<=intValidateTime)&&(!strLiquidShm.equals("")))
				{
					calendar.add(Calendar.DATE,-1); 
					objTransactionBO.setPricing("H");
					objTransactionBO.setTransactionTime("10:00:00");
					objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(calendar.getTime()))));
				}
				else
				{
					objTransactionBO.setTransactionTime(strTransactionTime);
					objTransactionBO.setPricing("F");
				}
			}
			else
			{
				if(strLiquidShm.equals(""))
				{
					objTransactionBO.setTransactionTime(strTransactionTime);
					objTransactionBO.setPricing("F");
				}
				else
				{
					calendar.add(Calendar.DATE,-1);
					objTransactionBO.setPricing("H");
					objTransactionBO.setTransactionTime("10:00:00");
					objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(calendar.getTime()))));
				}
			}
		}
		else if((daysBetween<=-2)&&(!strLiquidShm.equals("")))
		{
			objTransactionBO.setPricing("H");
			calendar.add(Calendar.DATE,-1);
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(calendar.getTime()))));
			objTransactionBO.setTransactionTime("10:00:00");
		}
		else if((daysBetween<=-2)&&(strLiquidShm.equals("")))
		{
			objTransactionBO.setPricing("F");
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(calendar.getTime()))));
			objTransactionBO.setTransactionTime(strTransactionTime);
		}
		else if((intTime>intValidateTime)&&(strLiquidShm.equals("")))
		{
			objTransactionBO.setPricing("F");
			trxnDate.add(Calendar.DATE,+1);
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(trxnDate.getTime()))));
			objTransactionBO.setTransactionTime(strTransactionTime);
		}
		else if((intTime<=intValidateTime)&&(!strLiquidShm.equals("")))
		{
			objTransactionBO.setPricing("H");
			trxnDate.add(Calendar.DATE,-1);
			objTransactionBO.setTransactionDate(new ConstansBO(String.valueOf(Tempformatter.format(trxnDate.getTime()))));
			objTransactionBO.setTransactionTime(strTransactionTime);
		}
		else
		{
			objTransactionBO.setPricing("F");
			objTransactionBO.setTransactionDate(objTrxnDate);
			objTransactionBO.setTransactionTime(strTransactionTime);
		}
		if(strLiquidShm.equals(""))
			objTransactionBO.setPricing("F");
		logger.info("Trxn Date : "+objTransactionBO.getTransactionDate().getDateFirstView()+" trxn time : "+objTransactionBO.getTransactionTime());
		//logger.info("============== final ***"+objTransactionBO.getTransactionDate().getDateFirstView());
		return objTransactionBO;
	}
	/*
	 * Tables FFFieldOrderInfo , FieldInfo
	 * Return type - Hashtable<Integer, ArrayList<FieldInfoBO>>
	 */
	public Hashtable<String, ArrayList<FieldInfoBO>> getFieldInfoTransaction(Connection objConnection) throws Exception 
	{
		logger.info("Field Information Begin ");
		//String RTTypeCode		=	"";
		//String strFeedTypeDup	=	"";
		//String strFeedType	=	"";
		String keyValue			=	"";
		
		FieldInfoBO objFieldInfoBO	=	new FieldInfoBO();
		
		Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO	=	new Hashtable<String, ArrayList<FieldInfoBO>>();
		ArrayList<FieldInfoBO> arryFieldInfoBO	=	new ArrayList<FieldInfoBO>();
		///long startTime = System.currentTimeMillis();
		PreparedStatement objPreparedStatement	=	null;
		ResultSet objResultSet	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select F.RTCode,F.FieldID,F.OrderID,F.Optional,F.Width,F.Decimal,I.FieldName, F.DataType, F.FeedType " +
				//" from FFFieldOrderInfo F, FieldInfo I " +
				" from FFFieldOrderInfo F, FieldInfo I " +
				" where F.FieldID = I.FieldID " +
				" order by F.feedType,F.RTCode,F.OrderID ";
		
		//logger.info(SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		objResultSet = objPreparedStatement.executeQuery();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : RSTgetFieldInfoTransaction = " + totalTime);
		try{
			while (objResultSet.next()) {
				
				objFieldInfoBO	=	new FieldInfoBO();
				arryFieldInfoBO	=	new ArrayList<FieldInfoBO>();
				
				ConstansBO objDateType	=	new ConstansBO("");
				objFieldInfoBO.setRTCode(objResultSet.getString("RTCode").trim()); // RTCode 
				objFieldInfoBO.setFieldId(objResultSet.getInt("FieldID")); // FieldID
				objFieldInfoBO.setOrderId(objResultSet.getInt("OrderID")); // OrderID
				
				objFieldInfoBO.setOptional(ConstansBO.NullCheck(objResultSet.getString("Optional").trim())); // Optional
				objFieldInfoBO.setWidth(ConstansBO.NullCheckINT(objResultSet.getInt("Width"))); // Width
				objFieldInfoBO.setDecimal(ConstansBO.NullCheckINT(objResultSet.getInt("Decimal"))); // Decimal
				objFieldInfoBO.setFieldName(objResultSet.getString("FieldName").trim()); // FieldName
				objDateType.setDataTypeId(objResultSet.getString("DataType").trim());
				objFieldInfoBO.setDataType(objDateType); // DataType
				
				keyValue	=	objResultSet.getString("RTCode").trim()+objResultSet.getString("FeedType");
				
				if(hashFieldInfoBO.containsKey(keyValue)){
					arryFieldInfoBO	=	hashFieldInfoBO.get(keyValue);
					arryFieldInfoBO.add(objFieldInfoBO);
					hashFieldInfoBO.put(keyValue, arryFieldInfoBO);
				}else
				{
					arryFieldInfoBO.add(objFieldInfoBO);
					hashFieldInfoBO.put(keyValue, arryFieldInfoBO);
					//logger.info(keyValue);
				}
			}
		}catch(Exception e){	
			logger.error("Exp. in getFieldInfoTransaction : "+e);	
		}
		finally{
			objPreparedStatement.close();
			objResultSet.close();
		}
		//totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : getFieldInfoTransaction = " + totalTime);
		logger.info("Field Info Success");
		return hashFieldInfoBO;
	}
	/*public Hashtable<String, Integer> getTransactionLot(String fromDate, String toDate, Connection objConnection) throws Exception 
	{
		
		Hashtable<String, Integer> hashMISLot	=	new Hashtable<String, Integer>();
		PreparedStatement objPreparedStatement	=	null;
		ResultSet objResultSet	=	null;
		String SQLQuery	=	"Select distinct PaymentId, LotNo from MISReportUpload where CreatedDate >='"+fromDate+" 14:00:00' and CreatedDate <= '"+toDate+" 14:00:00'";

		//logger.info(SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		objResultSet = objPreparedStatement.executeQuery();
		try{
			while (objResultSet.next()) {
				
				hashMISLot.put(objResultSet.getString("PaymentId").trim(), objResultSet.getInt("LotNo"));
				
			}
		}catch(Exception e){	
			logger.error("Exp. in getTransactionLot : "+e);	
		}
		finally{
			objPreparedStatement.close();
			objResultSet.close();
		}
		logger.info("Transaction Lot Success");
		return hashMISLot;
	}*/
	public PreparedStatement getPreparedStmtExistFolioNoByAMC(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		
		SQLQuery = " Select distinct top 1 isnull(C.FolioNumber,'') as exactFolio, isnull(O.FolioNumber,'') as schemeFolio, P.FolioNumber as amcFolio" +
				" from PositionsInfo P " +
				" inner join PortfolioInfo F on F.PortfolioID = P.PortfolioID and F.PortFolioType not in('SMART','MA') " +
				" inner join AFT_SCHEME_DETAILS S on S.SCHEMECODE = P.SchemeCode" +
				" left join PositionsInfo C on C.Active = 'A' and P.PositionID = C.PositionID and C.SchemeCode = ? and C.PortfolioID = ?" +
				" left join PositionsInfo O on O.Active = 'A' and P.PositionID = O.PositionID and O.SchemeCode = ?" +
				" where P.Active = 'A' and P.HoldingProfileID = ? and S.AMC_CODE = ? and P.TransMode = 1" +
				" and S.RT_CODE = (Select RT_CODE from AFT_SCHEME_DETAILS A where A.SCHEMECODE = ?)" +
				" order by exactFolio desc, schemeFolio desc ";

		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public PreparedStatement getPreparedStmtExistFolioNoByAMCRMF(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		
		SQLQuery = " Select top 1 isnull(C.FolioNumber,'') as exactFolio, isnull(O.FolioNumber,'') as schemeFolio, P.FolioNumber as amcFolio" +
				" from PositionsInfo P " +
				" inner join PortfolioInfo F on F.PortfolioID = P.PortfolioID and F.PortFolioType not in('SMART','MA') " +
				" inner join AFT_SCHEME_DETAILS S on S.SCHEMECODE = P.SchemeCode" +
				" left join PositionsInfo C on C.Active = 'A' and P.PositionID = C.PositionID and C.SchemeCode = ? and C.PortfolioID = ?" +
				" left join PositionsInfo O on O.Active = 'A' and P.PositionID = O.PositionID  and O.SchemeCode = ?" +
				" where P.Active = 'A' and P.HoldingProfileID = ? and S.AMC_CODE = ? and P.TransMode = 1" +
				" and S.RT_CODE = (Select RT_CODE from AFT_SCHEME_DETAILS A where A.SCHEMECODE = ?)" +
				" and P.SchemeCode not in (Select SCHEMECODE from SIPDetails D where D.SchemeCode = ? and D.FolioNumber = P.FolioNumber" +
				" and D.SchemeCode = P.SchemeCode and D.HoldingProfileID = P.HoldingProfileID and D.Active = 0)" +
				" order by exactFolio desc, schemeFolio desc  "; 

		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public String getExistFolioNoByAMC(String RTAMCCode, String strSIPEnabled, int intHoldingProfile, int intPortfolio, String strAMCCode, String strAFTSchemeCode, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		String strFolioNo		=	"";
		
		objPreparedStatement.setString(1, strAFTSchemeCode);
		objPreparedStatement.setInt(2, intPortfolio);
		objPreparedStatement.setString(3, strAFTSchemeCode);
		objPreparedStatement.setInt(4, intHoldingProfile);
		objPreparedStatement.setString(5, strAMCCode);
		objPreparedStatement.setString(6, strAFTSchemeCode);
		
		if((RTAMCCode.equals("RMF"))&&(!strSIPEnabled.equals("")))
		{
			logger.info("RMF Existing Folio ");
			objPreparedStatement.setString(7, strAFTSchemeCode);
		}
		
		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetExistFolioNumberByAMC = " + totalTime);
			if(objResultSet.next())
			{
				strFolioNo	=	ConstansBO.NullCheck(objResultSet.getString("exactFolio"));
				if(strFolioNo.equals(""))
					strFolioNo	=	ConstansBO.NullCheck(objResultSet.getString("schemeFolio"));
				if(strFolioNo.equals(""))
					strFolioNo	=	ConstansBO.NullCheck(objResultSet.getString("amcFolio"));
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getExistFolioNumberByAMC = " + totalTime);
		}catch(Exception e){logger.error("Exception in getFolioNo. AMCCode: "+strAMCCode+" HoldingProfile : "+intHoldingProfile+" PortfolioNO : "+intPortfolio+"ShmCode : "+strAFTSchemeCode+" Exp "+e);}
		finally{		if(objResultSet != null) objResultSet.close();		}
		logger.info("Result in getFolioNo. AMCCode: "+strAMCCode+" HoldingProfile : "+intHoldingProfile+" PortfolioNO : "+intPortfolio+"ShmCode : "+strAFTSchemeCode+" FolioNo : "+strFolioNo);
		return strFolioNo;
	}
	public PreparedStatement getPreparedStmtFranklinBankName(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select BankName from AMCSupportBanks where AmcCode = '400012' and BankCode = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public String getFranklinBankName(String srtBankId, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		String strBankName	=	"";
		
		objPreparedStatement.setString(1, srtBankId);
		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetFranklinBankName = " + totalTime);
			if(objResultSet.next())
			{
				strBankName	=	objResultSet.getString("BankName").trim();
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getFranklinBankName = " + totalTime);
		}catch(Exception e){
			strBankName	=	""; 
			logger.error("Exception in Franklin BankName. BankId : "+srtBankId+" Exp "+e);
		}finally{		if(objResultSet != null) objResultSet.close();		}
		logger.info("Result in Franklin Bank name : "+strBankName+" BankId : "+srtBankId);
		return strBankName;
	}
	
	public PreparedStatement getPreparedStmtSIPDatesByAMC(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select top 1 SIPDate from AMCSIPDates where AMCCode = ? order by abs(? - SIPDate) ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public int getSIPDatesByAMC(int AFTAMCCode, int sipDate, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		int sipDateRes	=	0;      //Return 0 for investor default
		
		objPreparedStatement.setInt(1, AFTAMCCode);
		objPreparedStatement.setInt(2, sipDate);
		try{
			objResultSet	=	objPreparedStatement.executeQuery();
			if(objResultSet.next())
				sipDateRes	=	objResultSet.getInt("SIPDate");
		}catch(Exception e){
			sipDateRes	=	0;
		}finally{		if(objResultSet != null) objResultSet.close();		}
		
		logger.info("SIP recent dates ByAMC : "+AFTAMCCode+" sipDate: "+sipDate+" SIPRsltDate:"+sipDateRes);
		return sipDateRes;
	}
        public PreparedStatement getPreparedStmtAMCSIPStartDates(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select top 1 * from (\n" +
                            "(select CONVERT(VARCHAR(8), DATEADD(MM, 1, ?), 120)+Right('0' + CONVERT(NVARCHAR, SIPDate), 2) as amcSipStartDate from dbo.AMCSIPDates  where AMCCode = ? and SIPDate >= ? )\n" +
                            "union\n" +
                            "select top 1 CONVERT(VARCHAR(8), DATEADD(MM, 2, ?), 120)+Right('0' + CONVERT(NVARCHAR, SIPDate), 2) as amcSipStartDate from AMCSIPDates where AMCCode = ? order by 1\n" +
                            ") as M order by amcSipStartDate ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public String getAMCSIPStartDates(int AFTAMCCode, int sipDate, String transactionDate, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		String sipStartDateRes	=	"";
		
                objPreparedStatement.setString(1, transactionDate);
		objPreparedStatement.setInt(2, AFTAMCCode);
		objPreparedStatement.setInt(3, sipDate);
                objPreparedStatement.setString(4, transactionDate);
                objPreparedStatement.setInt(5, AFTAMCCode);
		try{
			objResultSet	=	objPreparedStatement.executeQuery();
			if(objResultSet.next())
				sipStartDateRes	=	objResultSet.getString("amcSipStartDate");
		}catch(Exception e){
			sipStartDateRes	=	"";
		}finally{		if(objResultSet != null) objResultSet.close();		}
		
		logger.info("SIP start date ByAMC : "+AFTAMCCode+" sipDate: "+sipDate+" SIPRsltDate:"+sipStartDateRes);
		return sipStartDateRes;
	}
	
	
			
	/*public PreparedStatement getPreparedStmtCampsLocationByPicode(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		*
		 * select top 1 * from PinCodeMaster order by abs(1235555-Pincode)
		 *
		SQLQuery = " Select top 1 LookUpCode from PinCodeMaster order by abs(?-Pincode) ";

		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public String getCampsLocationByPicode(String strPincode, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		String strLocationCode	=	"";
		
		objPreparedStatement.setString(1, strPincode);
		try{
			objResultSet	=	objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				strLocationCode	=	objResultSet.getString("LookUpCode");
			}
		}catch(Exception e){
			strLocationCode	=	"C6"; // By default chennai location
			logger.error("Exception in PinCode By LocationCode. PinCode: "+strPincode+" Exp "+e);
		}finally{		if(objResultSet != null) objResultSet.close();		}
		
		logger.info("Result in PinCode By LocationCode. PinCode: "+strPincode+" strLocationCode : "+strLocationCode);
		return strLocationCode;
	}*/
	//Temp
	public PreparedStatement getPreparedStmtSIPMin2KSchemes(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select USERTRANSREFID from SIP_TRANSACTION_TRACK where USERTRANSREFID = ? ";

		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public TransactionBO getSIPMin2kScheme(String userTransRefID, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		
		objPreparedStatement.setString(1, userTransRefID);
		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetSIPMin2kScheme = " + totalTime);
			if(objResultSet.next())
			{
				objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
				objTransactionBO.setSIP_Reg_Date(objTransactionBO.getTransactionDate());
				logger.info("Result in SIP Min 2k Scheme: "+userTransRefID+" objAccountDetailsBO : "+objTransactionBO.getSIP_Reg_Date().getMonthFirstView());  
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getSIPMin2kScheme = " + totalTime);
		}catch(Exception e){logger.error("Exception in SIP Min 2k Scheme: "+userTransRefID+" Exp "+e);}
		finally{	if(objResultSet != null) objResultSet.close();		}
		return objTransactionBO;
	}
	public PreparedStatement getPreparedStmtIDFCSchemeCode(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select StartDate from SIPDetails where HoldingProfileID = ? and PortfolioID = ? and SchemeCode = ? and FolioNumber = ?";

		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public TransactionBO getIDFCSchemeCode(int Holdings, int portfolioId, String SchemeCode, String Folio, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception 
	{
		ResultSet objResultSet	= 	null;
		
		objPreparedStatement.setInt(1, Holdings);
		objPreparedStatement.setInt(2, portfolioId);
		objPreparedStatement.setString(3, SchemeCode);
		objPreparedStatement.setString(4, Folio);
		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetIDFCSchemeCode = " + totalTime);
			objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
			if(objResultSet.next())
			{
				objTransactionBO.setSIP_Reg_Date(new ConstansBO(objResultSet.getString("StartDate")));
				logger.info("Result in IDFCchemeCode Holdings: "+Holdings+" Holdings : "+Holdings+" SchemeCode : "+SchemeCode+" objAccountDetailsBO : "+objTransactionBO.getSIP_Reg_Date().getMonthFirstView());  
			}else{
				objTransactionBO.setSIP_Reg_Date(objTransactionBO.getTransactionDate());
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getIDFCSchemeCode = " + totalTime);
		}catch(Exception e){logger.error("Exception in IDFCchemeCode Holdings: "+Holdings+" Holdings : "+Holdings+" SchemeCode : "+SchemeCode+" Exp "+e);}
		finally{	if(objResultSet != null) objResultSet.close();		}
		return objTransactionBO;
	}
	
	public PreparedStatement getPreparedStmtSTPSwitchSubTransType(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select min(t.NAVDate) as startdate,  dateadd(month, NoOfInstalments, min(t.NAVDate)) as enddate, count(distinct t.NAVDate) as paidIns, s.MonthlyAmount as Amount, s.NoOfInstalments as instmnt, s.STPDate as switchDate"
				+ " From STPDetails s	"
				+ " Join STP_TRANSACTION st on st.STPID = s.STPID"
				+ " Join InvestorTransaction t on t.UserTransRefID = st.UserTransRefID and t.active = 'A' and  t.schemeCode = ? "
				+ " Where s.stpid = ?  group by s.MonthlyAmount, s.NoOfInstalments, s.STPDate Order by 1";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 

	public PreparedStatement getPreparedStmtVTPSwitchSubTransType(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select min(t.NAVDate) as startdate,  dateadd(month, v.NOOFINSTALLMENTS, min(t.NAVDate)) as enddate, count(distinct t.NAVDate) as paidIns, t.Amount, V.NOOFINSTALLMENTS as instmnt, v.VTPDATE as switchDate"
				+ " From VTPSETUP V"
				+ " Join VTP_TRANSACTION vt on vt.VTPID = v.VTPID"
				+ " Join InvestorTransaction t on t.UserTransRefID = vt.USERTRANSREFNO and t.active = 'A' and  t.schemeCode = ? "
				+ " Where v.vtpid = ?  group by t.Amount, V.NOOFINSTALLMENTS,v.VTPDATE order by 1";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 

	public PreparedStatement getPreparedStmtSTPSchemeCodeTemp(String stpSchemeCode, Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select distinct S.STPID as STP, V.VTPID as VTP from InvestorTransaction I"
				+ " left join STP_TRANSACTION S on I.UserTransRefID = S.UserTransRefID"
				+ " left join VTP_TRANSACTION V on I.UserTransRefID = V.USERTRANSREFNO"
				+ " where I.Active = 'A' and I.UserTransRefID = ? and I.SchemeCode in("+stpSchemeCode+") and I.TransType in ('SI', 'SO') and (S.STPID is not null or V.VTPID is not null)";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 

	public AccountDetailsBO getSwitchScheme(String userTransRefId, AccountDetailsBO objAccountDetailsBO, TransactionBO objTransactionBO, Hashtable<String, TransactionBO> STPSchemes, PreparedStatement objPreparedStatement, PreparedStatement pstmtSTPSwitchSubTransType, PreparedStatement pstmtVTPSwitchSubTransType) throws Exception 
	{
		int trxnValidate		=	0;
		ResultSet objResultSet	= 	null;
		ResultSet objResultSetSwitchDetails	= 	null;
		
		objPreparedStatement.setString(1, userTransRefId);

		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetSwitchScheme = " + totalTime);
			if(objResultSet.next())
			{
				trxnValidate	= ConstansBO.NullCheckINT(objResultSet.getInt("STP"));
				if(trxnValidate>0){
					pstmtSTPSwitchSubTransType.setInt(1, objTransactionBO.getAftSchemeCode());
					pstmtSTPSwitchSubTransType.setInt(2, trxnValidate);
					objResultSetSwitchDetails	=	pstmtSTPSwitchSubTransType.executeQuery();
				}else if(trxnValidate==0){
					trxnValidate	= ConstansBO.NullCheckINT(objResultSet.getInt("VTP"));
					if(trxnValidate>0){
						pstmtVTPSwitchSubTransType.setInt(1, objTransactionBO.getAftSchemeCode());
						pstmtVTPSwitchSubTransType.setInt(2, trxnValidate);
						objResultSetSwitchDetails	=	pstmtVTPSwitchSubTransType.executeQuery();
					}
				}
				
				if(trxnValidate>0){
					logger.info("STPSchemeCode Sub trxn. type as 'S'.UserTransRefId : "+objTransactionBO.getUserTransactionNo()+" Type : S");
					if(objResultSetSwitchDetails.next()){

						objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
						objTransactionBO.setSipReferenceId(String.valueOf(trxnValidate)); 
						objTransactionBO.setSIP_Reg_Date(new ConstansBO(objResultSetSwitchDetails.getString("startdate")));
						objTransactionBO.setNoOfInstalment(objResultSetSwitchDetails.getString("instmnt"));
						objTransactionBO.setPaidInstallments(objResultSetSwitchDetails.getString("paidIns"));
						objTransactionBO.setSipStartDate(new ConstansBO(objResultSetSwitchDetails.getString("startdate")));
						objTransactionBO.setSipEndDate(new ConstansBO(objResultSetSwitchDetails.getString("enddate")));
						objTransactionBO.setSipAmount(objResultSetSwitchDetails.getDouble("Amount"));
						objTransactionBO.setSipDate(objResultSetSwitchDetails.getInt("switchDate"));
						objTransactionBO.setSipFrequency("OM");
						
						STPSchemes.put(userTransRefId, objTransactionBO);
						
					}
				}
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getSwitchScheme = " + totalTime);
		}catch(Exception e){logger.error("Result in getSTPSchemeCodeTemp. UserTransRefId : "+userTransRefId+" Exp "+e);}
		finally{	if(objResultSet != null) objResultSet.close();	if(objResultSetSwitchDetails != null)	objResultSetSwitchDetails.close();  		}
		return objAccountDetailsBO;
	}
	public PreparedStatement getPreparedStmtAltFolio(String fDate, String tDate, Connection objConnection, PreparedStatement objHolidayDate) throws Exception
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		/*
		 * 	Select count(*), I.HoldingProfileID, V.RTCode, V.RTAMCCode from InvestorTransaction I join AFT_VRO_Mapping V on V.aft_scheme_code = I.SchemeCode 
			where RTCode=1 and (I.TransactionStatus = 'PS') and I.TransType='PUR' and (I.FolioNumber='' or I.FolioNumber='0' or I.FolioNumber is null) 
			and I.TransactionDate between '"+ConstansBO.getYesterDate()+" 2:00:00 PM' and '"+ConstansBO.getTodayDate()+" 2:00:00 PM' 
			group by I.HoldingProfileID, V.RTCode, V.RTAMCCode having count(*)>1
		*/
		SQLQuery = " Select count(*), I.HoldingProfileID, V.RTCode, V.RTAMCCode " +
				   " from InvestorTransaction I " +
				   " inner join AFT_VRO_Mapping V on V.aft_scheme_code = I.SchemeCode "+ 
				   " where I.Active = 'A' and RTCode= 1 and (I.TransactionStatus = 'PS') and I.TransType='PUR' and (I.FolioNumber='-' or I.FolioNumber='' or I.FolioNumber='0' or I.FolioNumber is null) "+ 
				   //" and I.TransactionDate between '"+fDate+" 2:00:00 PM' and '"+tDate+" 2:00:00 PM' "+
				   " and convert(varchar(20),I.TransactionDate,120) > convert(varchar(20),'"+fDate+" 14:00:00',120) and convert(varchar(20),I.TransactionDate,120) <= convert(varchar(20),'"+tDate+" 14:00:00',120) "+
				   " group by I.HoldingProfileID, V.RTCode, V.RTAMCCode having count(*)>1 "; 

		//logger.info("Query : "+SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public Hashtable<String, String> getHashAltFolio(PreparedStatement objPreparedStatement) throws Exception
	{
		Hashtable<String,String> altFolioHoldingId	=	new Hashtable<String, String>();
		String altHoldingId	=	"";
		ResultSet objResultSet	= 	null;
		try{
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();  
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetHashAltFolio = " + totalTime);
			while(objResultSet.next())
			{
				altHoldingId	=	objResultSet.getString("HoldingProfileID").trim()+"_"+objResultSet.getString("RTCode").trim()+"_"+objResultSet.getString("RTAMCCode").trim();
				altFolioHoldingId.put(altHoldingId, objResultSet.getString("HoldingProfileID").trim()); 
				logger.info(altHoldingId+" ==== ALT FOLIO === "+objResultSet.getString("HoldingProfileID").trim());
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getHashAltFolio = " + totalTime);
			logger.info("ALT Folio Size : "+altFolioHoldingId.size());
		}catch(Exception e){logger.error("Result ingetHashAltFolio Exp : "+e);}
		finally{	if(objResultSet != null) objResultSet.close();		}
		return altFolioHoldingId;
		
	}
	public PreparedStatement getPreparedStmtHoliday(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select DateOfHoliday from HolidayCalendar where DateOfHoliday between ? and ? and Flag='A' ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public boolean getHoliday(String holidayDate, PreparedStatement objPreparedStatement) throws Exception
	{
		boolean	holiday			=	false; 
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, holidayDate+" 00:00:00");
			objPreparedStatement.setString(2, holidayDate+" 23:59:59");
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetHoliday = " + totalTime);
			if(objResultSet.next())
			{
				holiday			=	true;   
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetHoliday = " + totalTime);
		}catch(Exception e){logger.error("Result getHoliday Exp : "+e);	holiday			=	false;}
		finally{	if(objResultSet != null) objResultSet.close();		}
		return holiday;
		
	}
	public PreparedStatement getPreparedStmtFranklinSIScheme(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select T.UserTrnsRefID from InvestorTransaction P " +
				" inner join InvestorTransactByPortfolio T on T.UserTrnsRefID = P.UserTransRefID and T.TransType = P.TransType" +
				" inner join AFT_SCHEME_DETAILS S on P.SchemeCode = S.SCHEMECODE and AMC_CODE='400012' and P.TransType='SI' and P.TransactionStatus='TS' "  +
				" and P.Active = 'A' and P.HoldingProfileID=? and T.PortfolioID=? and P.SCHEMECODE = ? and FolioNumber=? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public boolean FranklinSIScheme(int holdingProfile, int portFolio, String schemeCode, String folioNo, PreparedStatement objPreparedStatement) throws Exception
	{
		boolean	franklinSISchemeAvailable	=	false; 
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, holdingProfile);
			objPreparedStatement.setInt(2, portFolio);
			objPreparedStatement.setString(3, schemeCode);
			objPreparedStatement.setString(4, folioNo);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetFranklinSIScheme = " + totalTime);
			if(objResultSet.next())
			{
				franklinSISchemeAvailable			=	true;   
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getFranklinSIScheme = " + totalTime);
		}catch(Exception e){logger.error("Result FranklinSIScheme Exp : "+e);	
		franklinSISchemeAvailable			=	false;}
		finally{	if(objResultSet != null) objResultSet.close();		}
		return franklinSISchemeAvailable;
		
	}
	
	/*public PreparedStatement getPreparedStmtMFSchemeFee(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select S.FEE, F.FEEMODE from MFSCHEMEFEEDETAILS S " +
				" inner join FEEMASTER F ON F.FEEID = S.FEEID AND PRODUCTCODE='Scheme' AND PRODUCTTYPE='MF' AND FEETYPE=1 " +
				" where ((F.ENDDATE is null and F.STARTDATE<=DATEADD(MI, 330, GETDATE())) or (F.STARTDATE<=DATEADD(MI, 330, GETDATE()) and F.ENDDATE>=DATEADD(MI, 330, GETDATE())))" +
				" and ((S.ENDDATE is null and S.STARTDATE<=DATEADD(MI, 330, GETDATE())) or (S.STARTDATE<=DATEADD(MI, 330, GETDATE()) and S.ENDDATE>=DATEADD(MI, 330, GETDATE())))" +
				" and S.SCHEMECODE = ? and S.ACTIVE = 1 and F.ACTIVE = 1 " +
				" order by S.STARTDATE desc";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public BigDecimal getMFSchemeFee(int schemeCode, BigDecimal transactionAmount, PreparedStatement objPreparedStatement) throws Exception
	{
		double schemeFee	=	0.0d;
		BigDecimal commFee		=	new BigDecimal(0);;
		ResultSet objResultSet	= 	null;
		MFUpfrontTrial objMFUpfrontTrial	=	new MFUpfrontTrial();
		try{
			objPreparedStatement.setInt(1, schemeCode);
			
			objResultSet	=	objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				if(objResultSet.getInt("FEEMODE")==1)
				{
					schemeFee			=	objResultSet.getDouble("FEE");
					
					commFee	=	objMFUpfrontTrial.calculateCOMM(schemeFee, transactionAmount.doubleValue());
				}
				else
				{
					commFee			=	objResultSet.getBigDecimal("FEE");
				}
			}
			else
			{
				logger.info("******* Scheme Code  not mapped. shmCode : "+schemeCode+" ********");
			}
		}catch(Exception e){logger.error("Result FranklinSIScheme Exp : "+e);	
		commFee	=	new BigDecimal(0);
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return  commFee;
		
	}
	
	public PreparedStatement getPreparedStmtMFUpFrontTrialInsert(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " INSERT INTO MFUPFRONTTRIAL (USERTRANSREFID, COMMAMOUNT, FEEID, ACTIVE, CREATEDDATE, CREATEDUSER, MODIFIEDDATE, MODIFIEDUSER)" +
				" VALUES (?, ?, ?, 1, DATEADD(MI, 330, GETDATE()), 1, DATEADD(MI, 330, GETDATE()), 1) ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public void MFUpFrontTrialInsert(String userTransRefId, BigDecimal commAmount, PreparedStatement objPreparedStatement) throws Exception
	{
		try{
			objPreparedStatement.setString(1, userTransRefId.trim());
			objPreparedStatement.setBigDecimal(2, commAmount);
			objPreparedStatement.setInt(3, 1);
			
			objPreparedStatement.executeUpdate();
			
			logger.info("UpfrontTrial amt success. userTransId : "+userTransRefId+" commAmount : "+commAmount);

		}catch(Exception e){logger.error("Result MFUpFrontTrialInsert Exp : "+e);	
			logger.info("UpfrontTrial amt FAILS!!!!. userTransId : "+userTransRefId+" commAmount : "+commAmount);
		}
	}*/

	/*public PreparedStatement getPreparedStmtSchemeClassCode(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select W.WealthSchemeClass, I.TransType, I.Units, I.Amount, C.NAVRS" +
				" from AFT_SCHEME_DETAILS S " +
				" inner join InvestorTransaction I on I.Active = 'A' and I.SchemeCode = S.SCHEMECODE and I.UserTransRefID = ? " +
				" inner join WealthSchemeClass W on S.CLASSCODE = W.AFT_Class_Code " +
				" inner join AFT_CURRENTNAV C on S.SCHEMECODE = C.SCHEMECODE ";
		
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public BigDecimal getSchemeClassCodeAmount(String userTransRefId, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet	= 	null;
		String classCodeSI		=	"";
		String classCodeSO		=	"";
		double amountSO			=	0.0d;
		double unitSO			=	0.0d;
		double NAVSchemeBySO	=	0.0d;
		try{
			objPreparedStatement.setString(1, userTransRefId.trim());
			long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			long totalTime = System.currentTimeMillis() - startTime;
			logger.info("TIMER : RSTgetSchemeClassCodeAmount = " + totalTime);
			while(objResultSet.next())
			{
				String transType	=	objResultSet.getString("TransType").trim();
				if(transType.equals("SI"))
				{
					classCodeSI	=	objResultSet.getString("WealthSchemeClass");
				}
				if(transType.equals("SO"))
				{
					classCodeSO	=	objResultSet.getString("WealthSchemeClass");
					amountSO	=	ConstansBO.NullCheckDBL(objResultSet.getDouble("Amount"))*(-1);
					unitSO		=	ConstansBO.NullCheckDBL(objResultSet.getDouble("Units"))*(-1);
					NAVSchemeBySO	=	ConstansBO.NullCheckDBL(objResultSet.getDouble("NAVRS"));
				}
			}
			
			if(((classCodeSI.equals("EQ"))&&(classCodeSO.equals("EQ")))||(classCodeSI.equals("LQ")))
			{
				amountSO	=	0;
				logger.info("Both SI & SO schemes are EQ or SI schmes is LQ for userTransNo : "+userTransRefId);
			}
			else 
			{
				if(unitSO > 0)
				{
					amountSO	=	unitSO * NAVSchemeBySO;
				}
			}
			logger.info("getSchemeClassCodeAmount success. userTransId : "+userTransRefId+" AmountSO : "+amountSO);
			totalTime = System.currentTimeMillis() - startTime;
			logger.info("TIMER : getSchemeClassCodeAmount = " + totalTime);
		}catch(Exception e){logger.error("Result getSchemeClassCode Exp : "+e);	
			logger.info("getSchemeClassCodeAmount FAILS!!!!. userTransId : "+userTransRefId);
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return new BigDecimal(amountSO);
	}*/
	// PreparedStatement for Auto Number Generation 
	public PreparedStatement PreparedStmtAutoNo(Connection objConnection) throws Exception
	{
		PreparedStatement pstmAutoNumberGenerator	=	null;
		String SQLQuery	=	" SELECT autoNumber FROM AutoNumberGenerator WHERE [type.setString(?  ";
		pstmAutoNumberGenerator = objConnection.prepareStatement(SQLQuery);
		return pstmAutoNumberGenerator;
	}
	public int getAutoNumber(String type, PreparedStatement pstmAutoNumberGenerator) throws Exception {
		int intAutoNumber = 0;
		ResultSet rstAutoNumberGenerator			=	null;
		try {
			pstmAutoNumberGenerator.setString(1, type);
			//long startTime = System.currentTimeMillis();
			rstAutoNumberGenerator = pstmAutoNumberGenerator.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetAutoNumber = " + totalTime);
			if (rstAutoNumberGenerator.next()) {
				intAutoNumber = rstAutoNumberGenerator.getInt("autoNumber");
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getAutoNumber = " + totalTime);
		} catch (Exception errEx) {	logger.info("Exp in AUTO : "+errEx);		}
		finally{	if(rstAutoNumberGenerator != null) rstAutoNumberGenerator.close();		}
		return intAutoNumber;
	}
	// PreparedStatement for Auto Number Update
	public PreparedStatement PreparedStmtUpdateAutoNo(Connection objConnection)throws Exception
	{
		PreparedStatement pstmAutoNumberUpdate		=	null;
		String SQLQuery	=	"UPDATE AutoNumberGenerator SET autoNumber = ? WHERE [type.setString(? ";
		pstmAutoNumberUpdate = objConnection.prepareStatement(SQLQuery);
		return pstmAutoNumberUpdate;
	}
	
	public synchronized int getAutoNumberGenerated(int intAutoNumber, String type, PreparedStatement pstmAutoNumberUpdate) throws Exception {
		try {
			pstmAutoNumberUpdate.setInt(1, intAutoNumber);
			pstmAutoNumberUpdate.setString(2, type);
			//long startTime = System.currentTimeMillis();
			pstmAutoNumberUpdate.executeUpdate();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetAutoNumberGenerated = " + totalTime);
		
		} catch (Exception errEx) { logger.info("Exp in Auto Update : "+errEx);		} 		
		return intAutoNumber;
	}
	
	public PreparedStatement PreparedStmtNewFolioDocInsert(Connection objConnection) throws Exception
	{
		PreparedStatement pstmAutoNumberGenerator	=	null;
		String SQLQuery	=	" INSERT INTO RT_NEWFOLIO_DOC (BATCHNO, SLNO, RTCODE, INVESTORID, INVESTORNAME, PANNO, INHOUSENO, CREATEDDATE, MODIFIEDDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, DATEADD(MI, 330, GETDATE()), DATEADD(MI, 330, GETDATE()))  ";
		pstmAutoNumberGenerator = objConnection.prepareStatement(SQLQuery);
		return pstmAutoNumberGenerator;
	}
	
	public void NewFolioDocInsert(int batchNo, int slno, NewFolioBO objNewFolioBO, PreparedStatement objPreparedStatement) throws Exception
	{
		objPreparedStatement.setInt(1, batchNo);
		objPreparedStatement.setInt(2, slno);
		objPreparedStatement.setInt(3, objNewFolioBO.getRtCode());
		objPreparedStatement.setInt(4, objNewFolioBO.getInvestorId());
		objPreparedStatement.setString(5, objNewFolioBO.getInvestorName());
		objPreparedStatement.setString(6, objNewFolioBO.getPanNo());
		objPreparedStatement.setString(7, objNewFolioBO.getInHouseNo());
		//long startTime = System.currentTimeMillis();
		objPreparedStatement.executeUpdate();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : NEWFolioDocInsert = " + totalTime);
	}
	
	public String dailyReport(String fDate, String tDate, Connection objConnection) throws Exception
	{
		DBFFileMainBO	objDBFFileMainBO	=	new DBFFileMainBO();
		String reportText	=	"";
		PreparedStatement objPreparedStatement	=	null;
		PreparedStatement pstmtMISDataInset		=	null;
		ResultSet objResultSet	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select L.lookUpDescription as TransactionType, T.TransType, sum(case when transtype in('PUR','PURA') then isnull(T.Amount,0) when transtype in('RED','SO') and T.Units<>0 then isnull(T.Units*C.NAVRS,0) else isnull(T.Amount,0) end) as Amount " +
				" from InvestorTransaction T join LookUp L on L.lookUpId = T.TransType and L.lookUpType='TrxnCode'" +
				" join AFT_CURRENTNAV C on C.SCHEMECODE = T.SchemeCode " +
				" where T.Active = 'A' and TransType in('PUR','PURA','RED','SO') and ( (TransactionStatus = 'PS') or ( (TransactionStatus = 'TC') and (PaymentID in(77,66,54))))" +
				//" and TransactionDate between '"+fDate+" 2:00:00 PM' and '"+tDate+" 2:00:00 PM' " +
				" and convert(varchar(20),T.TransactionDate,120) > convert(varchar(20),'"+fDate+" 14:00:00',120) and convert(varchar(20),T.TransactionDate,120) <= convert(varchar(20),'"+tDate+" 14:00:00',120) "+
				" group by L.lookUpDescription, T.TransType";
		
		//logger.info(SQLQuery);
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		//long startTime = System.currentTimeMillis();
		objResultSet = objPreparedStatement.executeQuery();
		//long totalTime = System.currentTimeMillis() - startTime;
		//logger.info("TIMER : RSTdailyReport = " + totalTime);
		try{
			reportText	+="<pre style='font-family: Verdana, Arial, sans serif;'>";
			while (objResultSet.next()) {
				
				reportText	+=	"<br>"+objResultSet.getString("TransactionType")+"&#9;: Rs. "+ConstansBO.getRupeeBigDecimal(objResultSet.getBigDecimal("Amount"), 2);
				
				if(objResultSet.getString("TransType").equals("PUR"))
					objDBFFileMainBO.setPurchase(objResultSet.getDouble("Amount"));
				else if(objResultSet.getString("TransType").equals("PURA"))
					objDBFFileMainBO.setAddPurchase(objResultSet.getDouble("Amount"));
				else if(objResultSet.getString("TransType").equals("RED"))
					objDBFFileMainBO.setRedemption(objResultSet.getDouble("Amount"));
				else if(objResultSet.getString("TransType").equals("SO"))
					objDBFFileMainBO.setSwitchTrxn(objResultSet.getDouble("Amount"));
			}
			
			String reportInset	=	" Insert into FI_MISDATA values (?, ?, ?, ?, DATEADD(MI, 330, GETDATE()), 1, null, DATEADD(MI, 330, GETDATE()), DATEADD(MI, 330, GETDATE())) ";
			
			pstmtMISDataInset	=	objConnection.prepareStatement(reportInset);
			
			pstmtMISDataInset.setDouble(1, objDBFFileMainBO.getPurchase());
			pstmtMISDataInset.setDouble(2, objDBFFileMainBO.getAddPurchase());
			pstmtMISDataInset.setDouble(3, objDBFFileMainBO.getRedemption());
			pstmtMISDataInset.setDouble(4, objDBFFileMainBO.getSwitchTrxn());
			
			pstmtMISDataInset.executeUpdate();
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : dailyReport = " + totalTime);
		}catch(Exception e){	
			logger.error("Exp. in dailyReport : "+e);	
		}
		finally{
			if(objPreparedStatement != null) objPreparedStatement.close();
			if(pstmtMISDataInset != null) pstmtMISDataInset.close();
			if(objResultSet != null) objResultSet.close();
		}
		logger.info("Daily Report Success");
		return reportText;
	
	}
	public PreparedStatement getPreparedStmtAFTCodeByDividendType(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " SELECT AMC_SCHEME_CODE FROM WEALTH_DIVIDEND_MAP where AFT_SCHEME_CODE = ? and AMC_CODE = ? and RT_CODE = ? and DIVIDEND_TYPE = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public String getAFTCodeByDividendType(String schemeCode, String amcCode, int rtCode, String dividendType, PreparedStatement objPreparedStatement) throws Exception
	{
		String amcSchemeCode	=	"";
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, schemeCode);
			objPreparedStatement.setString(2, amcCode);
			objPreparedStatement.setInt(3, rtCode);
			objPreparedStatement.setString(4, dividendType);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetAFTCodeByDividentType = " + totalTime);
			if(objResultSet.next())
			{
				amcSchemeCode	=	objResultSet.getString("AMC_SCHEME_CODE").trim();
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getAFTCodeByDividentType = " + totalTime);
		}catch(Exception e){
			logger.error("Result getAFTCodeByDividendType Exp : "+e);	
			amcSchemeCode	=	"";
		}finally{	if(objResultSet != null) objResultSet.close();	}
		return  amcSchemeCode;
	}
	public PreparedStatement getPreparedStmtNRIAddress(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct A.Address1, A.Address2, A.City, A.state, A.PINCODE , A.Country as CountryCode, con.lookUpDescription as Country" +
				" from InvestorAddress A " +
				" inner join LookUp con on A.Country = con.lookUpId and con.lookUpType='Country' " +
				" where Country !=1 and InvestorID = ? and InvestorType = 'MF' ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public InvestorDetailsBO getNRIAddress(InvestorDetailsBO objInvestorDetailsBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, Integer.parseInt(objInvestorDetailsBO.getInvestorId()));
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetNRIAddress = " + totalTime);
			if(objResultSet.next())
			{
				//If address1 >40 then split to address2 remaining strings
				String address	=	objResultSet.getString("Address1").replace("|","").trim();
				
				if(address.length()>=40){	//Address1
					try{
						int commaSeperator	=	0;
						String addressString	=	address;
						
						commaSeperator	=	addressString.lastIndexOf(",", 40);
						if(commaSeperator<=0){
							commaSeperator	=	addressString.lastIndexOf(" ", 40); 
							if(commaSeperator<=0)
								commaSeperator	=	40;
						}
						address		=	addressString.substring(0, commaSeperator);
						address		=	address.concat(",");
						objInvestorDetailsBO.setNRIAddress1(address);
						address		=	addressString.substring((commaSeperator+1),addressString.length());
					}catch(Exception e){
						logger.info(" ***** Exception in Address1 Column ***** "+e);
					}
				}
				else{
					objInvestorDetailsBO.setNRIAddress1(address);
					address	=	"";
				}
				
				address	+=	objResultSet.getString("Address2").trim();
				if(address.length()>=40){	//Address1
					try{
						int commaSeperator	=	0;
						String addressString	=	address;
						
						commaSeperator	=	addressString.lastIndexOf(",", 40);
						if(commaSeperator<=0){
							commaSeperator	=	addressString.lastIndexOf(" ", 40);
							if(commaSeperator<=0)
								commaSeperator	=	40;
						}
						address		=	addressString.substring(0, commaSeperator);
						address		=	address.concat(",");
						objInvestorDetailsBO.setNRIAddress2(address);
						address		=	addressString.substring((commaSeperator+1),addressString.length());
						objInvestorDetailsBO.setNRIAddress3(address);
						
					}catch(Exception e){
						logger.info(" ***** Exception in Address2 Column ***** "+e);
					}
				}
				else
					objInvestorDetailsBO.setNRIAddress2(address);
				
				objInvestorDetailsBO.setNRICity(objResultSet.getString("City").trim());
				objInvestorDetailsBO.setNRIState(objResultSet.getString("State").trim());
				objInvestorDetailsBO.setNRIPinCode(objResultSet.getString("Pincode").trim());
				objInvestorDetailsBO.setNRICountry(objResultSet.getString("Country").trim());
				
				if(objResultSet.getString("CountryCode").equals("2")||objResultSet.getString("CountryCode").equals("11"))
					objInvestorDetailsBO.setUsaCanadaInvestor("Y");
				
				address		=	objInvestorDetailsBO.getAddress3().concat(objInvestorDetailsBO.getNRIState()).replace("|","").trim();
				objInvestorDetailsBO.setNRIAddress3(address);
			}
			else
			{
				objInvestorDetailsBO.setNRIAddress1(objInvestorDetailsBO.getAddress1());
				objInvestorDetailsBO.setNRIAddress2(objInvestorDetailsBO.getAddress2());
				objInvestorDetailsBO.setNRIAddress3(objInvestorDetailsBO.getAddress3());
				objInvestorDetailsBO.setNRICity(objInvestorDetailsBO.getCity());
				objInvestorDetailsBO.setNRIState(objInvestorDetailsBO.getState());
				objInvestorDetailsBO.setNRIPinCode(objInvestorDetailsBO.getPinCode());
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getNRIAddress = " + totalTime);
		}catch(Exception e){
			logger.error("Result getNRIAddress Exp : "+e);	
		}finally{	if(objResultSet!=null) objResultSet.close();		}
		return  objInvestorDetailsBO;
	}
	public PreparedStatement getPreparedStmtSubBrokerCode(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select ARN from AdvisorBasicInfo where Userid = ? and advUserType ='IN' "; 
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public String getSubBrokerCode(int userId, PreparedStatement objPreparedStatement) throws Exception
	{
		String subBrokerCode	=	"";
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, userId);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetSubBrokerCode = " + totalTime);
			if(objResultSet.next())
			{
				subBrokerCode	=	objResultSet.getString("ARN").trim().toUpperCase();
				if((subBrokerCode.length()>=0)&&(!subBrokerCode.startsWith("ARN-")))
				{
					subBrokerCode	=	"";
				}
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getSubBrokerCode = " + totalTime);
		}catch(Exception e){
			logger.error("Result getSubBrokerCode Exp. : "+e);	
			subBrokerCode	=	"";
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return  subBrokerCode;
	}
	public PreparedStatement getPreparedStmtAMCNominee(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select amcCode from AMCNominee where flag='A' and amcCode = ? "; 
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public boolean getAMCNominee(String amcCode, PreparedStatement objPreparedStatement) throws Exception
	{
		boolean returnFlag	=	true;
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, amcCode);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetAMCNominee = " + totalTime);
			if(objResultSet.next())
			{
				returnFlag	=	false;
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getAMCNominee = " + totalTime);
		}catch(Exception e){
			logger.error("Result getAMCNominee Exp. : "+e);	
			returnFlag	=	true;
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return  returnFlag;
	}
	public PreparedStatement getPreparedStmtNoOfInstallmentsForFT(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select d.NoOfInstalment, count(*) as  paidInstallments "
				+ " from SIP_TRANSACTION_TRACK k"
				+ " join InvestorTransaction t on t.UserTransRefID = k.USERTRANSREFID and t.TransactionStatus='TS'"
				+ " join SIPDetails d on d.SIPID = k.SIPID"
				+ " where k.SIPID = (select SIPID from SIP_TRANSACTION_TRACK where USERTRANSREFID = ? and TransType='SIP' )" 
				+ " group by d.NoOfInstalment "; 
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public TransactionBO getNoOfInstallmentsForFT(String userTransRefId, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, userTransRefId);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetNoOfInstallmentsForFT = " + totalTime);
			if(objResultSet.next())
			{
				objTransactionBO.setNoOfInstalment(objResultSet.getString("NoOfInstalment")); 
				objTransactionBO.setPaidInstallments(objResultSet.getString("paidInstallments"));
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getNoOfInstallmentsForFT = " + totalTime);
		}catch(Exception e){
			logger.error("Result getNoOfInstallmentsForFT Exp: "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return  objTransactionBO;
	}
	public PreparedStatement getPreparedStmtUTISIPRegistration(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct S.SIPID, isNull((select top 1 T.TransactionDate from InvestorTransaction T where T.UserTransRefID = S.UserTransRefID and T.TransactionStatus='TS' and T.Active='A'),DATEADD(MI, 330, GETDATE()))   as SIPStartDate" +
				" from SIP_TRANSACTION_TRACK K" +
				" join SIPDetails S on S.SIPID = K.SIPID and S.schemecode = ?" +
				" where K.USERTRANSREFID = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public TransactionBO UTISIPRegistration(int schemeCode, String userTransRefId, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, schemeCode);
			objPreparedStatement.setString(2, userTransRefId);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTUTISIPRegistration = " + totalTime);
			if(objResultSet.next())
			{
				objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
				objTransactionBO.setSipReferenceId(objResultSet.getString("SIPID"));  //SipReferenceNo//
				objTransactionBO.setSIP_Reg_Date(new ConstansBO(objResultSet.getString("SIPStartDate")));
			}
			else{
				objTransactionBO.setSubTransactionType("Normal");  //Sub_TRXN_T
				//objAccountDetailsBO.setSipReferenceNo("");  //SipReferenceNo
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : UTISIPRegistration = " + totalTime);
		}catch(Exception e){
			logger.error("Result UTISIPRegistration Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return objTransactionBO;
	}
	public PreparedStatement getPreparedStmtFolioCloseValidation(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select sum(Units) as units, PortfolioID from PositionsInfo where Active = 'A' and Units>0 and HoldingProfileID = ? and FolioNumber = ? and SchemeCode = ? group by PortfolioID ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public void FolioCloseValidation(int holdingProfile, int schemeCode, int intPortfolioNo, TransactionBO objTransactionBO, AccountDetailsBO objAccountDetailsBO, PreparedStatement objPreparedStatement) throws Exception
	{
		int noOfPortfolio	=	0;
		double closingUnits	=	0.0d;
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, holdingProfile);
			objPreparedStatement.setString(2, objTransactionBO.getFolioNo());
			objPreparedStatement.setInt(3, schemeCode);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : FolioCloseValidation = " + totalTime);
			while(objResultSet.next())
			{
				if(intPortfolioNo== objResultSet.getInt("PortfolioID"))
					closingUnits	=	objResultSet.getDouble("units");
				noOfPortfolio++;
			}
			
			if(noOfPortfolio>1){
				logger.info("Closer Acc. But more than one portflio.. Holdings:"+holdingProfile+" Folio:"+objTransactionBO.getFolioNo()+" ShmCode:"+schemeCode+" Units:"+closingUnits );
				objTransactionBO.setUnits(closingUnits);
			}
			else{
				logger.info("Closer Acc. success:: Flag : Y::::: Holdings:"+holdingProfile+" Folio:"+objTransactionBO.getFolioNo()+" ShmCode:"+schemeCode+" Units:"+closingUnits );
				objAccountDetailsBO.setClos_Ac_Ch("Y");  //Clos_Ac_Ch
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : FolioCloseValidation = " + totalTime);
		}catch(Exception e){
			logger.error("Result FolioCloseValidation Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
	}
	public ArrayList<TransactionBO> getNFOSchemes(String fromDate, String toDate, Connection objConnection) throws Exception
	{
		ResultSet objResultSet	= 	null;
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		ConstansBO objConstansBO	=	new ConstansBO("");
		TransactionBO objTransactionBO	=	new TransactionBO();
		ArrayList<TransactionBO> arrTransactionBO	=	new ArrayList<TransactionBO>();
		try{
			
			SQLQuery = " Select V.RTAMCCode, V.AMCSchemeCode, M.BankAccNo, S.S_NAME, B.InvestorName, T.UserTransRefID, T.TransactionDate," +
					" T.TransactionTime, T.Amount from AFT_SCHEME_DETAILS S" +
					" inner join InvestorTransaction T on T.SchemeCode = S.SCHEMECODE and T.TransactionStatus='PS'" +
					" inner join InvestorAdditionalInfo A on A.HoldingProfileID = T.HoldingProfileID and A.RelationshipID='F'" +
					" inner join InvestorBasicDetail B on B.InvestorID = A.PrimaryInvestorID" +
					" inner join AFT_VRO_Mapping V on V.AFT_Scheme_Code = S.SCHEMECODE" +
					" inner join AMCBankMaster M on M.SchemeCode = S.SCHEMECODE and M.AMCCode = S.AMC_CODE and M.RTCode = S.RT_CODE" +
					//" where NFO_CLOSE >= '2011/01/17' and NFO_CLOSE <= '2011/03/17'  " ;
					" where T.Active = 'A' and NFO_CLOSE >= '"+fromDate+"' and NFO_CLOSE <= '"+toDate+"' " +
					" order by V.RTAMCCode, V.AMCSchemeCode, B.InvestorName ";
			objPreparedStatement = objConnection.prepareStatement(SQLQuery);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getNFOSchemes = " + totalTime);
			while(objResultSet.next())
			{
				objTransactionBO	=	new TransactionBO();
				objTransactionBO.setAmcCode(objResultSet.getString("RTAMCCode").trim());
				objTransactionBO.setSchemeCode(objResultSet.getString("AMCSchemeCode").trim());
				objTransactionBO.setApplNo(objResultSet.getString("BankAccNo").trim());
				objTransactionBO.setSchemeName(objResultSet.getString("S_NAME").trim());
				objTransactionBO.setInvestorName(objResultSet.getString("InvestorName").trim());
				objTransactionBO.setUserTransactionNo(objResultSet.getString("UserTransRefID").trim());
				objConstansBO	=	new ConstansBO(objResultSet.getString("TransactionDate"));
				objTransactionBO.setTransactionDate(objConstansBO);
				objTransactionBO.setTransactionTime(objResultSet.getString("TransactionTime").trim());
				objTransactionBO.setAmount(objResultSet.getBigDecimal("Amount"));
				arrTransactionBO.add(objTransactionBO);
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getNFOSchemes = " + totalTime);
		}catch(Exception e){
			logger.error("Result getNFOSchemes Exp. : "+e);	
		}finally{		if(objResultSet != null) objResultSet.close();		}
		return  arrTransactionBO; 
	}
	public PreparedStatement getPreparedStmtPortfolioId(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select PortfolioID from InvestorTransactByPortfolio where UserTrnsRefID = ? and TransType = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public int getPortfolioId(String userTransRefId, String transType, PreparedStatement objPreparedStatement) throws Exception
	{
		int portfolioId	=	0;
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, userTransRefId);
			objPreparedStatement.setString(2, transType);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery();
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getPortfolioId = " + totalTime);
			if(objResultSet.next())
			{
				portfolioId	=	objResultSet.getInt("PortfolioID");
			}
			logger.info("UserTransRefId:"+userTransRefId+" TransType:"+transType+" PortfoiloId:"+portfolioId);
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getPortfolioId = " + totalTime);
		}catch(Exception e){
			logger.error("Result getPortfolioId Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return  portfolioId;
	}
	public PreparedStatement getPreparedStmtEUINValidate(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select distinct EUIN_NO From Advisor_EUIN_Details where EndDate> = DATEADD(MI, 330, GETDATE()) and  EUIN_NO =  ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public boolean getEUINValidate(String euinId, PreparedStatement objPreparedStatement) throws Exception
	{
		boolean flag	=	false;
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, euinId);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetEUINValidate = " + totalTime);
			if(objResultSet.next())
				flag	=	 true;
			else
				flag	=	 false;
			
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getEUINValidate = " + totalTime);
		}catch(Exception e){
			logger.error("Result getEUINValidate Exp. : "+e);	
			flag	=	 false;
		}finally{	if(objResultSet != null) objResultSet.close();
			
		}
		
		return flag;
	}
	/*public PreparedStatement getPreparedStmtEuinId(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select EUIN_NO from Advisor_EUIN_Details where Active = 1 and (EndDate >= DATEADD(MI, 330, GETDATE()) or enddate is null) and ARN_NO = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public TransactionBO getEuinId(String arnNo, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, arnNo);
			objResultSet	=	objPreparedStatement.executeQuery(); 
			if(objResultSet.next())
			{
				objTransactionBO.setEuinOpted("N");
				objTransactionBO.setEuinId(objResultSet.getString("EUIN_NO").trim());
			}
		}catch(Exception e){
			logger.error("Result getPortfolioId Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return  objTransactionBO;
	}*/
	public String getNextWorkingDay(String compareDate) throws Exception {
		
		String holidayDate = "";
		String nextDate = "";
		 
		WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
		Connection connection = null;
		PreparedStatement pstmDATEADDSelect = null;
		PreparedStatement pstmHolidayDATESelect = null;
		ResultSet rstDATEADDSelect = null;
		ResultSet rstHolidayDATESelect = null;

		try{
			 connection = objWIFSSQLCon.getWifsSQLConnect();
			 
			 pstmDATEADDSelect = connection.prepareStatement(
                     "IF (DATENAME(DW, ?) = 'Saturday' AND DATENAME(DW, DATEADD(dd, 1, ?)) = 'Sunday') " +
                                    "    SELECT CONVERT(VARCHAR(10), DATEADD(dd, 2, ?), 120) AS NextDate " +
					"ELSE IF (DATENAME(DW, DATEADD(dd, 1, ?)) != 'Saturday' AND DATENAME(DW, DATEADD(dd, 2, ?)) != 'Sunday') " +
			 		"    SELECT CONVERT(VARCHAR(10), DATEADD(dd, 1, ?), 120) AS NextDate " +
              			 		"ELSE " +
			 		"    SELECT CONVERT(VARCHAR(10), DATEADD(dd, 3, ?), 120) AS NextDate ");
			 pstmHolidayDATESelect = connection.prepareStatement("SELECT CONVERT(VARCHAR(10), DateOfHoliday, 120) AS DateOfHoliday " +
			 		"FROM HolidayCalendar WHERE CONVERT(VARCHAR(10), DateOfHoliday, 120) = ? ");
			 
			 do{
			 	 pstmDATEADDSelect.setString(1, compareDate);
			 	 pstmDATEADDSelect.setString(2, compareDate);
			 	 pstmDATEADDSelect.setString(3, compareDate);
			 	 pstmDATEADDSelect.setString(4, compareDate);
                 pstmDATEADDSelect.setString(5, compareDate);
                 pstmDATEADDSelect.setString(6, compareDate);
                 pstmDATEADDSelect.setString(7, compareDate);
                 //long startTime = System.currentTimeMillis();
			 	 rstDATEADDSelect = pstmDATEADDSelect.executeQuery();
			 	 //long totalTime = System.currentTimeMillis() - startTime;
			 	 //logger.info("TIMER : RSTgetNextWorkingDay = " + totalTime);
			 	 if(rstDATEADDSelect.next()){
			 		 nextDate = rstDATEADDSelect.getString("NextDate");
			 		 pstmHolidayDATESelect.setString(1, nextDate);
			 		//long startTime1 = System.currentTimeMillis();
			 		 rstHolidayDATESelect = pstmHolidayDATESelect.executeQuery();
			 		// long totalTime1 = System.currentTimeMillis() - startTime1;
			 		 //logger.info("TIMER : RSTholidayCalenderNextWorkingDay = " + totalTime1);
			 		 if(rstHolidayDATESelect.next()){
			 			 holidayDate = rstHolidayDATESelect.getString("DateOfHoliday");
			 			compareDate = holidayDate;
			 		 }
			 	 }
			 }while(nextDate.trim().equalsIgnoreCase(holidayDate.trim()));
		}
		catch (Exception daoEx) {
			logger.error("DAO - NXT WRK'G DAY # I/P : "+compareDate+" Exception : "+daoEx);
			throw new Exception(daoEx);
		}
		finally{
			if(rstDATEADDSelect != null)rstDATEADDSelect.close();
			if(rstHolidayDATESelect != null)rstHolidayDATESelect.close();
			if(pstmDATEADDSelect != null)pstmDATEADDSelect.close();
			if(pstmHolidayDATESelect != null)pstmHolidayDATESelect.close();
			if(connection != null)connection.close();
		}
		
		return nextDate;
	}
	public ArrayList<NewFolioBO> getFTNewFolioDetails(String userTransRefId, Connection objConnection) throws Exception
	{
		ResultSet objResultSet	= 	null;
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		
		NewFolioBO objNewFolioBO	=	new NewFolioBO();
		ArrayList<NewFolioBO> arrNewFolioBO	=	new ArrayList<NewFolioBO>();
		
		try{
			
			SQLQuery = " Select distinct t.UserTransRefID as USR_TRXN_NO, b.InvestorName, n.lookUpDescription as NATIONALITY,"
					+ " (Address1+', '+address2+', '+city) as ADDRESS,	b.PAN, b.AnnualIncome as Fin_sta1,"
					+ " (select b.AnnualIncome from InvestorBasicDetail b"
					+ " join InvestorAdditionalInfo s on s.HoldingProfileID = t.HoldingProfileID and s.RelationshipID='S'"
					+ " where b.InvestorID = s.AssociatedInvestorID) as Fin_sta2,"
					+ " (select b.AnnualIncome from InvestorBasicDetail b"
					+ " join InvestorAdditionalInfo st on st.HoldingProfileID = t.HoldingProfileID and st.RelationshipID='T'"
					+ " where b.InvestorID = st.AssociatedInvestorID) as Fin_sta3, "
					+ " (select b.AnnualIncome from InvestorBasicDetail b"
					+ " join InvestorAdditionalInfo g on g.HoldingProfileID = t.HoldingProfileID and g.RelationshipID='G'"
					+ " where b.InvestorID = g.AssociatedInvestorID) as Fin_stag,"
					+ " (select l.lookUpDescription  from InvestorBasicDetail b"
					+ " join InvestorAdditionalInfo s on s.HoldingProfileID = t.HoldingProfileID and s.RelationshipID='S'"
					+ " join Lookup l on l.lookUpId = b.Occupation and l.lookUpType='Occupation'"
					+ " where b.InvestorID = s.AssociatedInvestorID) as OCC_COD2,"
					+ " (select l.lookUpDescription  from InvestorBasicDetail b"
					+ " join InvestorAdditionalInfo st on st.HoldingProfileID = t.HoldingProfileID and st.RelationshipID='T'"
					+ " join Lookup l on l.lookUpId = b.Occupation and l.lookUpType='Occupation'"
					+ " where b.InvestorID = st.AssociatedInvestorID) as OCC_COD3,"
					+ " (select l.lookUpDescription  from InvestorBasicDetail b"
					+ " join InvestorAdditionalInfo g on g.HoldingProfileID = t.HoldingProfileID and g.RelationshipID='G'"
					+ " join Lookup l on l.lookUpId = b.Occupation and l.lookUpType='Occupation'"
					+ " where b.InvestorID = g.AssociatedInvestorID) as OCC_CODG"
					+ " from InvestorTransaction t"
					+ " join InvestorAdditionalInfo a on a.HoldingProfileID = t.HoldingProfileID and a.RelationshipID='F'"
					+ " join InvestorBasicDetail b on b.InvestorID = a.PrimaryInvestorID"
					+ " join Lookup n on n.lookUpId = b.Nationality and n.lookUpType='Nationality'"
					+ " join InvestorAddress r on r.investorid = b.investorid and r.MailingAddress = r.AddressType and r.InvestorType='MF'"
					+ " where usertransrefid in("+userTransRefId+")";
			objPreparedStatement = objConnection.prepareStatement(SQLQuery);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTFTNewFolioDetails = " + totalTime);
			while(objResultSet.next()){
				objNewFolioBO	=	new NewFolioBO();
				
				objNewFolioBO.setUserTransRefId(objResultSet.getString("USR_TRXN_NO"));
				objNewFolioBO.setInvestorName(objResultSet.getString("InvestorName"));
				objNewFolioBO.setNationality(objResultSet.getString("NATIONALITY"));
				objNewFolioBO.setAddress(objResultSet.getString("ADDRESS"));
				objNewFolioBO.setPanNo(objResultSet.getString("PAN"));
				objNewFolioBO.setFAnnualIncome(objResultSet.getString("Fin_sta1"));
				objNewFolioBO.setSAnnualIncome(objResultSet.getString("Fin_sta2"));
				objNewFolioBO.setTAnnualIncome(objResultSet.getString("Fin_sta3"));
				objNewFolioBO.setGAnnualIncome(objResultSet.getString("Fin_stag"));
				objNewFolioBO.setSOccupation(objResultSet.getString("OCC_COD2"));
				objNewFolioBO.setTOccupation(objResultSet.getString("OCC_COD3"));
				objNewFolioBO.setGOccupation(objResultSet.getString("OCC_CODG")); 
				
				arrNewFolioBO.add(objNewFolioBO);
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : FTNewFolioDetails = " + totalTime);
		}catch(Exception e){
			logger.error("Result getFTNewFolioDetails Exp. : "+e);	
		}finally{	
			if(objResultSet != null) objResultSet.close();
			if(objPreparedStatement != null) objPreparedStatement.close();
			
		}
		return arrNewFolioBO;
	}
	
	public PreparedStatement getPreparedStmtFatcaOccupationCodes(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct RTMapId as OccupationCode,"
				+ " isNull((select distinct top 1 RTMapId from AMCRTMasterCodesFatca where RTCode = ? and lookupid = ? and lookuptype = 'SourceOfWealth') ,'08') as SourceOfWealthCode,"
				+ " isNull((select distinct top 1 RTMapId from AMCRTMasterCodesFatca where RTCode = ? and lookupid = ? and lookuptype = 'OccupationType') ,'O') as OccupationtypeCode,"
				+ " isNull((Select distinct top 1 L.lookUpDescription from Lookup L where L.lookUpId = ? and L.lookUpType = 'Country'), 'India') as CountryOfBirth,"
				+ " isNull((Select distinct top 1 L1.lookUpDescription from Lookup L1 where L1.lookUpId = ? and L1.lookUpType = 'Country'), 'India') as Nationality,"
				+ " isNull((select distinct top 1 RTMapId from AMCRTMasterCodesFatca where lookupid = ? and lookuptype = 'Country') ,'IN') as countryOfBirthCode,"
				+ " isNull((select distinct top 1 RTMapId from AMCRTMasterCodesFatca where lookupid = ? and lookuptype = 'Country') ,'IN') as nationalityCode"
				+ " from AMCRTMasterCodesFatca where RTCode = ? and lookupid = ? and lookuptype = 'Occupation' ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public FatcaDocBO getFatcaOccupationCodes(int rtCode, String occupationCode, String nationality, String countryOfBirth, FatcaDocBO objFatcaDocBO, PreparedStatement objPreparedStatement) throws Exception
	{
		String birthPlace	=	"";
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, rtCode);
			objPreparedStatement.setString(2, occupationCode);
			objPreparedStatement.setInt(3, rtCode);
			objPreparedStatement.setString(4, occupationCode);
			objPreparedStatement.setString(5, countryOfBirth);
			objPreparedStatement.setString(6, nationality);
			objPreparedStatement.setString(7, countryOfBirth);
			objPreparedStatement.setString(8, nationality);
			objPreparedStatement.setInt(9, rtCode);
			objPreparedStatement.setString(10, occupationCode);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetFatcaOccupationCodes = " + totalTime);
			if(objResultSet.next())
			{
				objFatcaDocBO.setOccupationCode(objResultSet.getString("OccupationCode"));
				objFatcaDocBO.setOccupationTypeCode(objResultSet.getString("OccupationtypeCode"));
				objFatcaDocBO.setSourceOfWealthCode(objResultSet.getString("SourceOfWealthCode"));
				
				objFatcaDocBO.setNationality(objResultSet.getString("Nationality"));
				objFatcaDocBO.setNationalityCode(objResultSet.getString("NationalityCode"));
				
				birthPlace	=	ConstansBO.NullCheck(objResultSet.getString("CountryOfBirth"));
				
				if(birthPlace.equals("")){
					objFatcaDocBO.setCountryOfBirthCode("IN");
					objFatcaDocBO.setCountryOfBirth("India");
				}else{
					objFatcaDocBO.setCountryOfBirthCode(ConstansBO.NullCheck(objResultSet.getString("CountryOfBirthCode")));
					objFatcaDocBO.setCountryOfBirth(birthPlace);
				}
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getFatcaOccupationCodes = " + totalTime);
		}catch(Exception e){
			logger.error("Result getFatcaOccupationCodes Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return objFatcaDocBO;
	}
	public PreparedStatement getPreparedStmtTaxStatusCodes(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct b.investorid, b.TaxStatus,  a.ReferenceCode as taxCode, d.ReferenceCode as NRITaxCode"
				+ " from InvestorBasicDetail b"
				+ " left join AMCRTMasterCodes a on a.amc = 400004 and a.lookupcode = 'TaxCode' and  b.TaxStatus = a.mapid"
				+ " left join AMCRTMasterCodes d on d.amc = 400004 and d.lookupcode = 'TaxCode' and d.mapid = (select taxstatus from InvestorBankInfo c where b.InvestorID = c.Investor_ID  and c.investortype = 'MF' and c.userbankid = 1)"
				+ " where b.investorid = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public FatcaDocBO getTaxStatusCodes(int investorId, FatcaDocBO objFatcaDocBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, investorId);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetTaxStatusCodes = " + totalTime);
			if(objResultSet.next())
			{
				if((objResultSet.getInt("TaxStatus")) == 99)
					objFatcaDocBO.setTaxStatus(objResultSet.getString("NRITaxCode"));
				else
					objFatcaDocBO.setTaxStatus(objResultSet.getString("taxCode"));
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getTaxStatusCodes = " + totalTime);
		}catch(Exception e){
			logger.error("Result getFatcaOccupationCodes Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return objFatcaDocBO;
	}
	public FatcaUBODeclarationBO getFatcaUBODeclarations(String pan, Connection objConnection) throws Exception 
	{
		FatcaUBODeclarationBO objFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
		PreparedStatement objPreparedStatement	=	null;
		ResultSet objResultSet	= 	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct f.FATCADeclarationId, f.EntityExemptionCode, f.NFEsType, f.NFEs_GIINCode, f.NFEs_GIIN_SP_Entity, f.NFEs_GIIN_NA_Flag, f.NFFE_CATG, f.activeNFESubCategory, f.activeNFEBusiness, f.passiveNFEBusiness,"
				+ " f.REPTCRelation, f.REPTCName, f.PTCStockExchange, f.REPTCExchangeName, f.NFEs_GIIN_NA_Subcategory, l.lookUpDescription as city, f.NetWorthAmount, f.netWorthDate"
				+ " from FATCADeclaration f"
				+ " join lookup l on l.lookUpType = 'Location' and l.lookUpId = f.City where f.PAN = ?";
		
		try{
			objPreparedStatement = objConnection.prepareStatement(SQLQuery);
			objPreparedStatement.setString(1, pan);
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetFatcaUBODeclarations = " + totalTime);
			if(objResultSet.next())
			{
				objFatcaUBODeclarationBO.setFATCADeclarationId(objResultSet.getInt("FATCADeclarationId"));
				objFatcaUBODeclarationBO.setEntityExemptionCode(ConstansBO.NullCheck(objResultSet.getString("EntityExemptionCode")));
				objFatcaUBODeclarationBO.setNFEsType(ConstansBO.NullCheck(objResultSet.getString("NFEsType")));
				objFatcaUBODeclarationBO.setNFEs_GIINCode(ConstansBO.NullCheck(objResultSet.getString("NFEs_GIINCode")));
				objFatcaUBODeclarationBO.setNFEs_GIIN_SP_Entity(ConstansBO.NullCheck(objResultSet.getString("NFEs_GIIN_SP_Entity")));
				objFatcaUBODeclarationBO.setNFEs_GIIN_NA_Flag(ConstansBO.NullCheck(objResultSet.getString("NFEs_GIIN_NA_Flag")));
				objFatcaUBODeclarationBO.setNFFE_CATG_Flag(ConstansBO.NullCheck(objResultSet.getString("NFFE_CATG")));
				objFatcaUBODeclarationBO.setActiveNFESubCategory(ConstansBO.NullCheck(objResultSet.getString("activeNFESubCategory")));
				objFatcaUBODeclarationBO.setActiveNFEBusiness(ConstansBO.NullCheck(objResultSet.getString("activeNFEBusiness")));
				objFatcaUBODeclarationBO.setPassiveNFEBusiness(ConstansBO.NullCheck(objResultSet.getString("passiveNFEBusiness")));
				objFatcaUBODeclarationBO.setREPTCRelation(ConstansBO.NullCheck(objResultSet.getString("REPTCRelation")));
				objFatcaUBODeclarationBO.setREPTCName(ConstansBO.NullCheck(objResultSet.getString("REPTCName")));
				objFatcaUBODeclarationBO.setPTCStockExchange(ConstansBO.NullCheck(objResultSet.getString("EntityExemptionCode")));
				objFatcaUBODeclarationBO.setREPTCExchangeName(ConstansBO.NullCheck(objResultSet.getString("EntityExemptionCode")));
				objFatcaUBODeclarationBO.setPTCStockExchange(ConstansBO.NullCheck(objResultSet.getString("PTCStockExchange")));
				objFatcaUBODeclarationBO.setREPTCExchangeName(ConstansBO.NullCheck(objResultSet.getString("REPTCExchangeName")));
				objFatcaUBODeclarationBO.setNFEs_GIIN_NA_Subcategory(ConstansBO.NullCheck(objResultSet.getString("NFEs_GIIN_NA_Subcategory")));
				objFatcaUBODeclarationBO.setUboCity(ConstansBO.NullCheck(objResultSet.getString("city")));
				objFatcaUBODeclarationBO.setNetWorthDate(new ConstansBO(ConstansBO.NullCheck(objResultSet.getString("netWorthDate"))));
				objFatcaUBODeclarationBO.setNetWorthAmount(ConstansBO.NullCheck(objResultSet.getString("NetWorthAmount")));
				 
				if((objFatcaUBODeclarationBO.getNFFE_CATG_Flag().equals("A"))||(objFatcaUBODeclarationBO.getNFFE_CATG_Flag().equals("P")))
					objFatcaUBODeclarationBO.setUboApplicable("Y");
				else
					objFatcaUBODeclarationBO.setUboApplicable("N");
					
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getFatcaUBODeclarations = " + totalTime);
		}catch(Exception e){	logger.error("Exp:"+e);	}
		finally{	
			if(objResultSet != null) objResultSet.close();
			if(objPreparedStatement != null) objPreparedStatement.close();
		}
		return objFatcaUBODeclarationBO;
	} 
	public ArrayList<FatcaUBODeclarationBO> getFatcaUBODeclarationsActive(FatcaUBODeclarationBO objFatcaUBODeclarationBO, Connection objConnection) throws Exception 
	{
		FatcaUBODeclarationBO tempFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
		ArrayList<FatcaUBODeclarationBO> arrFatcaUBODeclarationBO	=	new ArrayList<FatcaUBODeclarationBO>();
		PreparedStatement objPreparedStatement	=	null;
		ResultSet objResultSet	= 	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct name, c.RTMapId as countryCode, Address1, Address2, Address3, Pin, State, ac.RTMapId as acountryCode, a.Address_Type, a.Tax_Id_No, a.Tax_Id_Type, a.type_code"
				+ " from UBODeclarationActive a"
				+ " left join AMCRTMasterCodesFatca c on a.Country = c.LookupId and c.lookuptype = 'CountryCode'"
				+ " left join AMCRTMasterCodesFatca ac on a.aCountry = ac.LookupId and ac.lookuptype = 'CountryCode'"
				+ " where a.FatcaDeclarationId = ?";
		
		try{
			objPreparedStatement = objConnection.prepareStatement(SQLQuery); 
			objPreparedStatement.setInt(1, objFatcaUBODeclarationBO.getFATCADeclarationId());
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetFATCAUBODeclarationActive = " + totalTime);
			while(objResultSet.next())
			{
				tempFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
				tempFatcaUBODeclarationBO.setName(ConstansBO.NullCheck(objResultSet.getString("name")));
				tempFatcaUBODeclarationBO.setCountryCode(ConstansBO.NullCheck(objResultSet.getString("countryCode")));
				tempFatcaUBODeclarationBO.setAcountryCode(ConstansBO.NullCheck(objResultSet.getString("acountryCode")));
				tempFatcaUBODeclarationBO.setAddress1(ConstansBO.NullCheck(objResultSet.getString("Address1")));
				tempFatcaUBODeclarationBO.setAddress2(ConstansBO.NullCheck(objResultSet.getString("Address2")));
				tempFatcaUBODeclarationBO.setAddress3(ConstansBO.NullCheck(objResultSet.getString("Address3")));
				tempFatcaUBODeclarationBO.setPin(ConstansBO.NullCheck(objResultSet.getString("pin")));
				tempFatcaUBODeclarationBO.setState(ConstansBO.NullCheck(objResultSet.getString("state")));
				tempFatcaUBODeclarationBO.setAcountryCode(ConstansBO.NullCheck(objResultSet.getString("acountryCode")));
				tempFatcaUBODeclarationBO.setAddressType(ConstansBO.NullCheck(objResultSet.getString("Address_Type")));
				tempFatcaUBODeclarationBO.setTaxIdNo(ConstansBO.NullCheck(objResultSet.getString("Tax_Id_No")));
				tempFatcaUBODeclarationBO.setTaxIdType(ConstansBO.NullCheck(objResultSet.getString("Tax_Id_Type")));
				tempFatcaUBODeclarationBO.setUboCode(ConstansBO.NullCheck(objResultSet.getString("type_code")));
				
				arrFatcaUBODeclarationBO.add(tempFatcaUBODeclarationBO);
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getFATCAUBODeclarationActive = " + totalTime);
		}catch(Exception e){	logger.error("Exp:"+e);	}
		finally{	
			if(objResultSet != null) objResultSet.close();
			if(objPreparedStatement != null) objPreparedStatement.close();
		}
		//logger.info("FATCA Active size:"+arrFatcaUBODeclarationBO.size()+" FatcaDeclarationId :"+ objFatcaUBODeclarationBO.getFATCADeclarationId());
		return arrFatcaUBODeclarationBO;
	}
	public ArrayList<FatcaUBODeclarationBO> getFatcaUBODeclarationsPassive(int rtCode, FatcaUBODeclarationBO objFatcaUBODeclarationBO, Connection objConnection) throws Exception 
	{
		FatcaUBODeclarationBO tempFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
		ArrayList<FatcaUBODeclarationBO> arrFatcaUBODeclarationBO	=	new ArrayList<FatcaUBODeclarationBO>();
		PreparedStatement objPreparedStatement	=	null;
		ResultSet objResultSet	= 	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct b.Passivepan, l.lookUpDescription as placeOfBirth, dob, gender, fatherName, o.RTMapId as occupationCode, ot.RTMapId as occupationType   "
				+ " from UBODeclarationPassive b"
				+ " left join lookup l on l.lookUpType = 'Location' and b.cityOfBirth = l.lookUpId "
				+ " left join AMCRTMasterCodesFatca o on b.occupationType = o.LookupId and o.LookupType = 'Occupation' and o.rtcode = ?"
				+ " left join AMCRTMasterCodesFatca ot on b.occupationType = ot.LookupId and ot.LookupType = 'OccupationType' and ot.rtcode = ?"
				+ " where b.FatcaDeclarationId = ?";
		
		try{
			objPreparedStatement = objConnection.prepareStatement(SQLQuery);
			objPreparedStatement.setInt(1, rtCode);
			objPreparedStatement.setInt(2, rtCode);
			objPreparedStatement.setInt(3, objFatcaUBODeclarationBO.getFATCADeclarationId());
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatement.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : RSTgetFatcaUBODeclarationPassive = " + totalTime);
			while(objResultSet.next())
			{
				tempFatcaUBODeclarationBO	=	new FatcaUBODeclarationBO();
				tempFatcaUBODeclarationBO.setUboPan(ConstansBO.NullCheck(objResultSet.getString("Passivepan")));
				tempFatcaUBODeclarationBO.setPlaceOfBirth(ConstansBO.NullCheck(objResultSet.getString("placeOfBirth")));
				tempFatcaUBODeclarationBO.setDob(new ConstansBO(ConstansBO.NullCheck(objResultSet.getString("dob"))));
				tempFatcaUBODeclarationBO.setGender(ConstansBO.NullCheck(objResultSet.getString("gender")));
				tempFatcaUBODeclarationBO.setFatherName(ConstansBO.NullCheck(objResultSet.getString("fatherName")));
				tempFatcaUBODeclarationBO.setOccupationCode(ConstansBO.NullCheck(objResultSet.getString("occupationCode")));
				tempFatcaUBODeclarationBO.setOccupationType(ConstansBO.NullCheck(objResultSet.getString("occupationType")));
				arrFatcaUBODeclarationBO.add(tempFatcaUBODeclarationBO);
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : getFatcaUBODeclarationPassive = " + totalTime);
		}catch(Exception e){	logger.error("Exp:"+e);	}
		finally{	
			if(objResultSet != null) objResultSet.close();
			if(objPreparedStatement != null) objPreparedStatement.close();
		}
		//logger.info("FATCA Passive size:"+arrFatcaUBODeclarationBO.size()+" FatcaDeclarationId :"+ objFatcaUBODeclarationBO.getFATCADeclarationId());
		return arrFatcaUBODeclarationBO;
	}
	
	public CallableStatement pstmtSIPOpenMandateNoOfInstallmentsPaid(Connection con) throws Exception
	{
		CallableStatement cs = null;
		cs = con.prepareCall("{ call WF_SIP_NoOfInstallmentsPaid ( ?, ? ) }");
		return cs;
	}
	public int SIPOpenMandateNoOfInstallmentsPaid(String c_code, int sip_id, CallableStatement cs) throws Exception
	{
		ResultSet rs = null;
		int installments = 0;
		try{
			cs.setString(1, c_code);
			cs.setInt(2, sip_id);								
			rs=cs.executeQuery();
			while (rs.next()){
				installments = installments+1;	
			}						
		}
		catch (Exception daoEx) {
			logger.error("Exception : "+daoEx.getMessage());
		}
		finally{
			if(rs != null)rs.close();
		}
		return installments;
	}
	public PreparedStatement getPreparedStmtSIPRegistrationFlag(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct S.SIPID, S.ECSDate, S.USERTRANSREFID, isNull((T.NAVDate),DATEADD(MI, 330, GETDATE())) as SIPStartDate,  DATEADD(MONTH,(S.NoOfInstalment-1),T.NAVDate) as SIPEndDate, "
				+ " S.NoOfInstalment, T.Amount, S.SIPType"
				+ " from SIP_TRANSACTION_TRACK K"
				+ " join SIPDetails S on S.SIPID = K.SIPID and S.schemecode = ?"
				+ " join InvestorTransaction T on S.UserTransRefID = T.UserTransRefID and T.TransactionStatus in('PS','TS') and T.Active='A'"
				+ " where K.USERTRANSREFID = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	} 
	public AccountDetailsBO SIPRegistrationFlag(int intRTCode, String strAMCCode, AccountDetailsBO objAccountDetailsBO, TransactionBO objTransactionBO, PreparedStatement objPreparedStatementSIPRegistrationFlag, PreparedStatement pstmtAlertSIPRegistration, PreparedStatement pstmtAMCSIPStartDates, CallableStatement objPreparedStatementSIPInstallmentsPaid) throws Exception
	{
                String sipStartDate     =       "";
		String SIPType		=	"";
		int paidInstallments	=	0;
		String sipEnddate	=	"";
		ResultSet objResultSet	= 	null;
		ResultSet rstAlertSIPRegistration	= 	null;
                DBFFilesDAO objDBFFilesDAO   =   new DBFFilesDAO();
		try{
			objPreparedStatementSIPRegistrationFlag.setInt(1, objTransactionBO.getAftSchemeCode());
			objPreparedStatementSIPRegistrationFlag.setString(2, objTransactionBO.getUserTransactionNo());
			//long startTime = System.currentTimeMillis();
			objResultSet	=	objPreparedStatementSIPRegistrationFlag.executeQuery(); 
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : SIPRegistrationFlag = " + totalTime);
			if(objResultSet.next())
			{
				SIPType		=	ConstansBO.NullCheck(objResultSet.getString("SIPType"));
				paidInstallments	=	SIPOpenMandateNoOfInstallmentsPaid("", objResultSet.getInt("SIPID"), objPreparedStatementSIPInstallmentsPaid);
				objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
				objTransactionBO.setSipReferenceId(objResultSet.getString("SIPID"));  //SipReferenceNo//
				objTransactionBO.setSIP_Reg_Date(new ConstansBO(objResultSet.getString("SIPStartDate")));
				objTransactionBO.setNoOfInstalment(objResultSet.getString("NoOfInstalment"));
				objTransactionBO.setPaidInstallments(String.valueOf(paidInstallments));
				objTransactionBO.setSipStartDate(new ConstansBO(objResultSet.getString("SIPStartDate")));
				objTransactionBO.setSipEndDate(new ConstansBO(objResultSet.getString("SIPEndDate")));
				objTransactionBO.setSipAmount(objResultSet.getDouble("Amount"));
				objTransactionBO.setSipDate(objResultSet.getInt("ECSDate"));
				
				if(SIPType.equalsIgnoreCase("INSSIP")){//SIP insuer flags

					sipEnddate	=	objResultSet.getString("SIPEndDate");
					sipEnddate	=	sipEnddate.substring(0,8);
					sipEnddate	=	sipEnddate+(objResultSet.getInt("EcsDate")+1);
					objTransactionBO.setSipEndDate(new ConstansBO(sipEnddate));
					objTransactionBO.setSipDate(objResultSet.getInt("EcsDate")+1);
                                        
                                        sipStartDate    =       objDBFFilesDAO.getAMCSIPStartDates(Integer.parseInt(strAMCCode), (objResultSet.getInt("EcsDate")+1), objResultSet.getString("SIPStartDate"), pstmtAMCSIPStartDates);
                                        if(!sipStartDate.equals(""))
                                            objTransactionBO.setSipStartDate(new ConstansBO(sipStartDate));
					
					if(intRTCode==1){
						objTransactionBO.setSubTransactionType("U"); 
						objTransactionBO.setSipFrequency("OM"); 
					}else{
						objTransactionBO.setSubTransactionType("I");
						objTransactionBO.setSipFrequency("M"); // only karvy SIP Insure
					}
					
				}else{
					//Flag
					objTransactionBO.setSipFrequency("OM");
				}
				
			}
			else{
				pstmtAlertSIPRegistration.setInt(1, Integer.parseInt(objTransactionBO.getUserTransactionNo()));
				rstAlertSIPRegistration	=	pstmtAlertSIPRegistration.executeQuery();
				if(rstAlertSIPRegistration.next()){
					objTransactionBO.setSubTransactionType("S");  //Sub_TRXN_T
					objTransactionBO.setSipReferenceId(rstAlertSIPRegistration.getString("SIPID"));  //SipReferenceNo//
					objTransactionBO.setSIP_Reg_Date(new ConstansBO(rstAlertSIPRegistration.getString("StartDate")));
					objTransactionBO.setNoOfInstalment(rstAlertSIPRegistration.getString("NoOfInstalment"));
					objTransactionBO.setPaidInstallments(rstAlertSIPRegistration.getString("paidInstallments"));
					objTransactionBO.setSipStartDate(new ConstansBO(rstAlertSIPRegistration.getString("StartDate")));
					objTransactionBO.setSipEndDate(new ConstansBO(rstAlertSIPRegistration.getString("EndDate")));
					objTransactionBO.setSipAmount(rstAlertSIPRegistration.getDouble("Amount"));
					objTransactionBO.setSipDate(rstAlertSIPRegistration.getInt("ECSDate"));
					objTransactionBO.setSipFrequency("OM");
				}else{
					objTransactionBO.setSubTransactionType("Normal");  //Sub_TRXN_T
					objTransactionBO.setSipReferenceId("");  //SipReferenceNo
				}
			}
			//totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : SIPRegistrationFlag = " + totalTime);
		}catch(Exception e){
			logger.error("Result SIPRegistrationFlag Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close(); if(rstAlertSIPRegistration != null)	 rstAlertSIPRegistration.close();	}
		return objAccountDetailsBO;
	}
	public PreparedStatement getPreparedStmtAlertSIPRegistration(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct d.SIPID, t.amount, d.ECSDate, d.StartDate, d.EndDate, d.NoOfInstalment, (select count( distinct PAYMENT_ID ) from alertsip a"
				+ " join InvestorTransaction t on t.PaymentID = a.PAYMENT_ID and t.active = 'A' and t.TransactionStatus in('TS','PS')"
				+ " where a.sipid = s.sipid)  as paidInstallments"
				+ " from ALERTSIP s"
				+ " join InvestorTransaction t on t.PaymentID = s.PAYMENT_ID"
				+ " join SIPDetails d on d.SIPID = s.SIPID"
				+ " where t.UserTransRefID = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	
	/**
         * Update direct upload (STP) forward feed generation status in DB. Updation will happen in 'InvestorTransactionTrack' table
         * @param modifiedUserId
         * @param feedSuccessList
         * @param feedFailedList
         * @throws Exception 
         */
	public void updateSTPForwardFeedStatus(int modifiedUserId, ArrayList<STPProcessBO> feedSuccessList, ArrayList<STPProcessBO> feedFailedList) throws Exception {
		
		logger.info("## -- START - CAMS server upload status update in DB -- #");
                if (feedSuccessList == null)
                    feedSuccessList = new ArrayList<STPProcessBO>();
                if (feedFailedList == null)
                    feedFailedList = new ArrayList<STPProcessBO>();

    	if (modifiedUserId < 1 
                || (feedSuccessList.size() < 1 && feedFailedList.size() < 1))
    		throw new Exception("Insufficient Data.");

        boolean isAllFeedFailed = (feedSuccessList.size() < 1) ? true : false;
    	Connection connectionObject = null;
    	PreparedStatement pstm_ITT_Update = null;
		WIFSSQLCon wifsSQLCon = new WIFSSQLCon();

        try {

            connectionObject = wifsSQLCon.getWifsSQLConnect();
            
            String sqlQuery_ITT_Update = ""
            		+ "UPDATE "
            		+ "	InvestorTransactionTrack "
            		+ "SET "
                        +(isAllFeedFailed ? "	ForwardFeed = DATEADD(MI, 330, GETDATE()), " : "")
            		+ "	DirectUpload_Status = ?, DirectUpload_Remarks = ?, "
                        + "	ModifiedUser = "+ modifiedUserId +", ModifiedDate = DATEADD(MI, 330, GETDATE()) "
            		+ "WHERE "
            		+ "	UserTransRefID = ?";
            
            pstm_ITT_Update = connectionObject.prepareStatement(sqlQuery_ITT_Update);
            
            for (int i=0; i<2; i++){
                String status = "";
                ArrayList<STPProcessBO> feedList = new ArrayList<STPProcessBO>();
                
                //Update failed status
                if (i == 0){
                    status = "Fail";
                    feedList = feedFailedList;
                }
                //Update success status
                else if (i == 1){
                    status = "Yes";
                    feedList = feedSuccessList;
                }
                if (feedList.size() > 0){
                pstm_ITT_Update.clearBatch();
                pstm_ITT_Update.setString(1, status);
                for (STPProcessBO stpBO : feedList) {
                    try {
                    String message = stpBO.getMessage();
                    message = (message.length() > 250)?message.substring(0, 250):message;
                    pstm_ITT_Update.setString(2, message);
                    pstm_ITT_Update.setInt(3, stpBO.getUserTransRefId());
                    pstm_ITT_Update.addBatch();
                    }
                    catch (Exception idaoEx){
                        logger.error("I-DAO - Update batch # EX: "+idaoEx);
                    }
                }
                int [] updateCount = pstm_ITT_Update.executeBatch();
                logger.info("'"+ status +"' status Update in DB. UPDATE: "+Arrays.toString(updateCount));
                }
            }
        
        } catch (Exception daoEx) {
            logger.error("DAO - UPDATE FORWARD FEED STATUS $ UID: " + modifiedUserId
                     + " SUCCESD A.SZ: "+ feedSuccessList.size() 
                    + "| FAIL A.SZ: "+ feedFailedList.size() + " # EX:" +daoEx);
        } finally {
        	if (pstm_ITT_Update != null) pstm_ITT_Update.close();
        	if (connectionObject != null) connectionObject.close();
        }
        logger.info("## -- END - CAMS server upload status update in DB -- #");

    }
        
        public PreparedStatement getPreparedStmtDPId(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select CLIENTID, DPID from dbo.EQ_CLIENT_INVESTOR_MAP where INVESTORID = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public String getDPId(int investorId, TransactionBO objTransactionBO, PreparedStatement objPreparedStatement) throws Exception
	{
		String aAdhaarNo	=	"0";
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setInt(1, investorId);
			objResultSet	=	objPreparedStatement.executeQuery(); 
			if(objResultSet.next())
			{
                                objTransactionBO.setDpId(ConstansBO.NullCheck(objResultSet.getString("DPID")));
				objTransactionBO.setClientCode(ConstansBO.NullCheck(objResultSet.getString("CLIENTID")));
			}else{
                            logger.error("DP not available");
                        }
			logger.info("InvId:"+investorId+" DPId:"+objTransactionBO.getDpId()+" client:"+objTransactionBO.getClientCode());
		}catch(Exception e){
			logger.error("Result getDPId Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return aAdhaarNo;
	}

	public PreparedStatement getPreparedStmtOTPResponseByPANAadhaarNo(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " Select distinct UID_No from OTPResponse where pan = ? ";
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public String getOTPResponseByPANAadhaarNo(String pan, InvestorDetailsBO objInvestorDetailsBO, PreparedStatement objPreparedStatement) throws Exception
	{
		String aAdhaarNo	=	"0";
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, pan);
			objResultSet	=	objPreparedStatement.executeQuery(); 
			if(objResultSet.next())
			{
				aAdhaarNo	=	ConstansBO.NullCheck(objResultSet.getString("UID_No"));
			}
			logger.info("Aadhaar number by pan:"+pan+" AadharNumber:"+aAdhaarNo);
		}catch(Exception e){
			logger.error("Result getOTPResponseByPANAadhaarNo Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return aAdhaarNo;
	}
	
	public PreparedStatement getPreparedStmtCKYCDetails(Connection objConnection) throws Exception 
	{
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		SQLQuery = " select distinct l.lookUpDescription as Salutation, b.PAN, b.InvestorName,b.motherName, b.TaxStatus, i.TaxStatus as NRITaxstatus, b.Occupation, b.CountryOfBirth,b.Nationality, r.Address1, r.Address2, r.City,"
				+ "  r.State, r.Country, r.PINCODE, b.CKYCNumber"
				+ "  from InvestorTransaction t"
				+ "  inner join InvestorAdditionalInfo a on a.HoldingProfileID = t.HoldingProfileID and a.RelationshipID = 'F'"
				+ "  inner join InvestorBasicDetail b on b.InvestorID = a.PrimaryInvestorID"
				+ "  inner join dbo.Lookup l on l.lookUpId = b.Salutation and l.lookUpType = 'Salutation'"
				+ "   inner join InvestorBankInfo i on i.Investor_ID = b.InvestorID and i.InvestorType='MF' and i.userBankId = isnull((Select UserBankId from TRANSACTION_BANK_INFO where UserTransRefId = ? ),1)"
				+ "  inner join InvestorAddress r on r.InvestorID = b.InvestorID and r.InvestorType = 'MF' and r.AddressType = r.MailingAddress"
				+ "  where t.usertransrefid = ? ";
		 
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	public InvestorDetailsBO getCKYCDetails(String userTrxnNo, PreparedStatement objPreparedStatement) throws Exception
	{
		InvestorDetailsBO objInvestorDetailsBO	=	new InvestorDetailsBO();
		ResultSet objResultSet	= 	null;
		try{
			objPreparedStatement.setString(1, userTrxnNo);
			objPreparedStatement.setString(2, userTrxnNo);
			objResultSet	=	objPreparedStatement.executeQuery(); 
			if(objResultSet.next())
			{
				objInvestorDetailsBO.setSalutation(ConstansBO.NullCheck(objResultSet.getString("Salutation")));
				objInvestorDetailsBO.setTaxNo(ConstansBO.NullCheck(objResultSet.getString("PAN")));
				objInvestorDetailsBO.setFirstName(ConstansBO.NullCheck(objResultSet.getString("InvestorName")));
				objInvestorDetailsBO.setMotherName(ConstansBO.NullCheck(objResultSet.getString("motherName")));
				
				if(objResultSet.getString("TaxStatus").equals("99"))
					objInvestorDetailsBO.setTaxStatus(ConstansBO.NullCheck(objResultSet.getString("NRITaxstatus")));
				else
					objInvestorDetailsBO.setTaxStatus(ConstansBO.NullCheck(objResultSet.getString("TaxStatus")));
				
				objInvestorDetailsBO.setOccupationCode(ConstansBO.NullCheck(objResultSet.getString("Occupation")));
				objInvestorDetailsBO.setPlaceOfBirth(ConstansBO.NullCheck(objResultSet.getString("City")));
				objInvestorDetailsBO.setCountryOfBirthCode(ConstansBO.NullCheck(objResultSet.getString("CountryOfBirth")));
				objInvestorDetailsBO.setNationality(ConstansBO.NullCheck(objResultSet.getString("Nationality")));
				objInvestorDetailsBO.setAddress1(ConstansBO.NullCheck(objResultSet.getString("Address1")));
				objInvestorDetailsBO.setAddress2(ConstansBO.NullCheck(objResultSet.getString("Address2")));
				objInvestorDetailsBO.setCity(ConstansBO.NullCheck(objResultSet.getString("City")));
				objInvestorDetailsBO.setStateWIFS(ConstansBO.NullCheck(objResultSet.getString("State")));
				objInvestorDetailsBO.setCountry(ConstansBO.NullCheck(objResultSet.getString("Country")));
				objInvestorDetailsBO.setPinCode(ConstansBO.NullCheck(objResultSet.getString("PINCODE")));
				objInvestorDetailsBO.setcKYCRefId1(ConstansBO.NullCheck(objResultSet.getString("CKYCNumber")));
			}
		}catch(Exception e){
			logger.error("Result getCKYCDetails Exp. : "+e);	
		}finally{	if(objResultSet != null) objResultSet.close();		}
		return objInvestorDetailsBO;
	}
	
	public PreparedStatement getPreparedCKYCMasterCodes(Connection objConnection) throws Exception 
	{ 
		PreparedStatement objPreparedStatement	=	null;
		String SQLQuery	=	"";
		
		SQLQuery = " select"
				+ " (select MapID from dbo.CKYCMasterCodes where lookuptype = 'taxcode' and lookupid = ? ) as taxcode,"
				+ " (select MapID from dbo.CKYCMasterCodes where lookuptype = 'Occupation' and lookupid = ? ) as Occupation,"
				+ " (select MapID from dbo.CKYCMasterCodes where lookuptype = 'country' and lookupid = ? ) as countryOfBirth,"
				+ " (select MapID from dbo.CKYCMasterCodes where lookuptype = 'country' and lookupid = ? ) as country,"
				+ " (select MapID from dbo.CKYCMasterCodes where lookuptype = 'State' and lookupid = ? ) as State, "
				+ " (Select top 1 District from PincodeMaster_CKYCMaster order by abs(110006-Pincode)) as District ";	
		
		objPreparedStatement = objConnection.prepareStatement(SQLQuery);
		return objPreparedStatement;
	}
	
	public InvestorDetailsBO getCKYCMasterCodes(InvestorDetailsBO objInvestorDetailsBO, PreparedStatement objPreparedStatement) throws Exception
	{
		ResultSet objResultSet 		= 	null;
		try{
			objPreparedStatement.setString(1, objInvestorDetailsBO.getTaxStatus());
			objPreparedStatement.setString(2, objInvestorDetailsBO.getOccupationCode());
			objPreparedStatement.setString(3, objInvestorDetailsBO.getCountryOfBirthCode());
			objPreparedStatement.setString(4, objInvestorDetailsBO.getCountry());
			objPreparedStatement.setString(5, objInvestorDetailsBO.getStateWIFS());
			objPreparedStatement.setString(5, objInvestorDetailsBO.getPinCode());
			objResultSet	=	objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
				objInvestorDetailsBO.setTaxStatus(ConstansBO.NullCheck(objResultSet.getString("taxcode")));
				if(objInvestorDetailsBO.getTaxStatus().equals(""))
					objInvestorDetailsBO.setTaxStatus("01");
				
				objInvestorDetailsBO.setOccupationCode(ConstansBO.NullCheck(objResultSet.getString("Occupation")));
				if(objInvestorDetailsBO.getOccupationCode().equals(""))
					objInvestorDetailsBO.setOccupationCode("X-01");
				
				objInvestorDetailsBO.setCountryOfBirthCode(ConstansBO.NullCheck(objResultSet.getString("countryOfBirth")));
				if(objInvestorDetailsBO.getCountryOfBirthCode().equals(""))
					objInvestorDetailsBO.setCountryOfBirthCode("IN");
							
				objInvestorDetailsBO.setCountry(ConstansBO.NullCheck(objResultSet.getString("country")));
				if(objInvestorDetailsBO.getCountry().equals(""))
					objInvestorDetailsBO.setCountry("IN");
				
				objInvestorDetailsBO.setState(ConstansBO.NullCheck(objResultSet.getString("State")));
				if(objInvestorDetailsBO.getState().equals(""))
					objInvestorDetailsBO.setState("TN");
				
				objInvestorDetailsBO.setDistrict(ConstansBO.NullCheck(objResultSet.getString("District")));
				if(objInvestorDetailsBO.getDistrict().equals(""))
					objInvestorDetailsBO.setDistrict("Chennai");
				
				
				logger.info(" *** CKYCMaster Codes. Success tax : "+objInvestorDetailsBO.getTaxStatus()+" occ : "+objInvestorDetailsBO.getOccupationCode()+" cob : "+objInvestorDetailsBO.getCountryOfBirthCode()+" cntry : "+objInvestorDetailsBO.getCountry()+" state : "+objInvestorDetailsBO.getState()+" *** ");
			}
		}catch(Exception e){
			logger.error("Exp in CKYCMaster:"+e);
		}finally{
			if(objResultSet != null) objResultSet.close();
		}
		return objInvestorDetailsBO;
	}

    public void writeCAMSDB(Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO, HashMap<Integer, String> failedList) throws Exception {

        logger.info("## -- START - CAMS server upload status insert in DB -- #");

        Connection connectionObject = null;
        PreparedStatement CamsDBInsert = null;
        WIFSSQLCon wifsSQLCon = new WIFSSQLCon();
        java.sql.Date dateConversionToSQL = null;
        int intMainBORowSize = 0;
        String remarksCAMS = "";
        String paidThrough = "";
        String strKeyValue = "";
        Set<String> set = hashTransactionMainBO.keySet();
        Iterator<String> itr = set.iterator();
        int[] insertedRows = {};
        try {

            connectionObject = wifsSQLCon.getWifsSQLConnect();

            String sqlQuery_ITT_Update = "Insert into CAMS_Forward_Feed (AMC_CODE	,\n"
                    + "                     BROKE_CD	,\n"
                    + "                     SBBR_CODE	,\n"
                    + "                     User_Code	,\n"
                    + "                     USR_TXN_NO	,\n"
                    + "                     Appl_No	,\n"
                    + "                     FOLIO_NO	,\n"
                    + "                     Ck_DIG_NO	,\n"
                    + "                     TRXN_TYPE	,\n"
                    + "                     SCH_CODE	,\n"
                    + "                     FIRST_NAME	,\n"
                    + "                     JONT_NAME1	,\n"
                    + "                     JONT_NAME2	,\n"
                    + "                     ADD1	,\n"
                    + "                     ADD2	,\n"
                    + "                     ADD3	,\n"
                    + "                     CITY	,\n"
                    + "                     PINCODE	,\n"
                    + "                     PHONE_OFF	,\n"
                    + "                     TRXN_DATE	,\n"
                    + "                     TRXN_TIME	,\n"
                    + "                     UNITS	,\n"
                    + "                     AMOUNT	,\n"
                    + "                     CLOS_AC_CH	,\n"
                    + "                     DOB	,\n"
                    + "                     GUARDIAN	,\n"
                    + "                     TAX_NUMBER	,\n"
                    + "                     PHONE_RES	,\n"
                    + "                     FAX_OFF	,\n"
                    + "                     FAX_RES	,\n"
                    + "                     EMAIL	,\n"
                    + "                     ACCT_NO	,\n"
                    + "                     ACCT_TYPE	,\n"
                    + "                     BANK_NAME	,\n"
                    + "                     BR_NAME	,\n"
                    + "                     BANK_CITY	,\n"
                    + "                     REINV_TAG	,\n"
                    + "                     HOLD_NATUR	,\n"
                    + "                     OCC_CODE	,\n"
                    + "                     TAX_STATUS	,\n"
                    + "                     REMARKS	,\n"
                    + "                     STATE	,\n"
                    + "                     SUB_TRXN_T	,\n"
                    + "                     DIV_PY_MEC	,\n"
                    + "                     ECS_NO	,\n"
                    + "                     BANK_CODE	,\n"
                    + "                     ALT_FOLIO	,\n"
                    + "                     ALT_BROKER	,\n"
                    + "                     LOCATION	,\n"
                    + "                     PAY_MEC	,\n"
                    + "                     PRICING	,\n"
                    + "                     PAN_2_HLDR	,\n"
                    + "                     PAN_3_HLDR	,\n"
                    + "                     NOM_NAME	,\n"
                    + "                     NOM_RELA	,\n"
                    + "                     Guard_PAN	,\n"
                    + "                     INSTRMNO	,\n"
                    + "                     UINno	,\n"
                    + "                     VALID_PAN	,\n"
                    + "                     GVALID_PAN	,\n"
                    + "                     JH1VALPAN	,\n"
                    + "                     JH2VALPAN	,\n"
                    + "                     SIP_REG_DATE	,\n"
                    + "                     FIRST_HDLR_MIN	,\n"
                    + "                     JH1_MIN	,\n"
                    + "                     JH2_MIN	,\n"
                    + "                     GUARDIAN_MIN	,\n"
                    + "                     NEFT_CD	,\n"
                    + "                     RTGS_CD	,\n"
                    + "                     EMAIL_ACST	,\n"
                    + "                     MOBILE_NO	,\n"
                    + "                     DP_ID	,\n"
                    + "                     POA_type	,\n"
                    + "                     Trxn_Mode	,\n"
                    + "                     Trxn_Sign_Confn	,\n"
                    + "                     Addl_Addr1	,\n"
                    + "                     Addl_Addr2	,\n"
                    + "                     Addl_Addr3	,\n"
                    + "                     CITY_NRI	,\n"
                    + "                     Country	,\n"
                    + "                     PIN_NRI,\n"
                    + "					 Check_Flag,\n"
                    + "                     FIRC_STA	,\n"
                    + "                     Nom1_Applicable,\n"
                    + "					 TH_P_PYT,\n"
                    + "                     KYC	,\n"
                    + "					 SIP_RFNO	,\n"
                    + "                     NO_INST	,\n"
                    + "                     SIP_FEQUENCY	,\n"
                    + "                     ST_DATE	,\n"
                    + "                     END_DATE	,\n"
                    + "                     INSTL_NO,\n"
                    + "					 PAN1_Exempt	,\n"
                    + "                     JH1_VALID_PAN	,\n"
                    + "                     JH2_VALID_PAN	,\n"
                    + "                     GPAN_Exempt,\n"
                    + "					 EUIN_OPT	,\n"
                    + "                     EUIN	,\n"
                    + "                     Nom_not_opted	,\n"
                    + "                     SUB_BRCD	,\n"
                    + "					 BankMandateProof	,\n"
                    + "                     FHLD_CKYC	,\n"
                    + "                     SHLD_CKYC	,\n"
                    + "                     THLD_CKYC	,\n"
                    + "                     GURD_CKYC	,\n"
                    + "                     JH1_DOB	,\n"
                    + "                     JH2_DOB	,\n"
                    + "                     GUARDN_DOB, Created_Date) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,DATEADD(d,DATEDIFF(d,'20130101',CURRENT_TIMESTAMP),'20130101'))";
            logger.info(sqlQuery_ITT_Update);
            CamsDBInsert = connectionObject.prepareStatement(sqlQuery_ITT_Update);
            while (itr.hasNext()) {
                strKeyValue = itr.next();
                logger.info(strKeyValue);
                ArrayList<TransactionMainBO> arryTransactionMainBO = new ArrayList<>();
                arryTransactionMainBO = hashTransactionMainBO.get(strKeyValue);
                intMainBORowSize = arryTransactionMainBO.size();
                int count = 0, insertCount = arryTransactionMainBO.size();

                //logger.info(intMainBORowSize+" ======= ");
                for (int DBFRow = 0; DBFRow < intMainBORowSize; DBFRow++) {
                    try{
                    TransactionMainBO objTransactionMainBO = new TransactionMainBO();
                    objTransactionMainBO = arryTransactionMainBO.get(DBFRow);
                    InvestorDetailsBO objInvestorDetailsBO = new InvestorDetailsBO();
                    AccountDetailsBO objAccountDetailsBO = new AccountDetailsBO();
                    TransactionBO objTransactionBO = new TransactionBO();

                    objTransactionBO = objTransactionMainBO.getTransactionBO();
                    objTransactionBO = objTransactionMainBO.getTransactionBO();
                    objInvestorDetailsBO = objTransactionMainBO.getInvestorDetailsBO();
                    objAccountDetailsBO = objTransactionMainBO.getAccountDetailsBO();
                    if (!failedList.containsKey(Integer.valueOf(objTransactionBO.getUserTransactionNo()))) {
                        count++;
                        if (objTransactionMainBO != null) {
                            paidThrough = objTransactionBO.getPaidThru();
                            if (paidThrough.equalsIgnoreCase("NEFT")) {
                                remarksCAMS = objTransactionBO.getPaymentId() + " " + paidThrough + "    |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                            } else if ((paidThrough.equals("")) && (objTransactionBO.getTransactionType().equals("SI") || objTransactionBO.getTransactionType().equals("SO"))) {
                                remarksCAMS = "66 SW" + "        |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$72.32.69.244$72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                            } else if (paidThrough.equals("") && (objTransactionBO.getTransactionType().equals("R"))) {
                                remarksCAMS = "77 Red" + "       |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$72.32.69.244$72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                            } else if (paidThrough.equalsIgnoreCase("DD")) {
                                remarksCAMS = objTransactionBO.getPaymentId() + " DD      |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                            } else if (paidThrough.equalsIgnoreCase("ECS")) {
                                remarksCAMS = objTransactionBO.getPaymentId() + " ECS     |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                            } else {
                                remarksCAMS = objTransactionBO.getPaymentId() + " " + paidThrough + "    |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                            }

                            CamsDBInsert.setString(1, objTransactionBO.getAmcCode()); //AMCCode
                            CamsDBInsert.setString(2, "WEALTH");//objTransactionBO.getBrokerCode()); // Broker Code
                            CamsDBInsert.setString(3, objInvestorDetailsBO.getSubBrokerCode()); // Subbroker Code
                            CamsDBInsert.setString(4, objInvestorDetailsBO.getUserCode()); // User code
                            CamsDBInsert.setDouble(5, new Double(objTransactionBO.getUserTransactionNo())); // Transaction No
                            CamsDBInsert.setString(6, objTransactionBO.getApplNo()); // Application No
                            CamsDBInsert.setString(7, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo())); // Folio No				
                            CamsDBInsert.setString(8, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())); // Cheque degit No
                            CamsDBInsert.setString(9, objTransactionBO.getTransactionType());
                            CamsDBInsert.setString(10, objTransactionBO.getSchemeCode());
                            CamsDBInsert.setString(11, objInvestorDetailsBO.getFirstName());
                            CamsDBInsert.setString(12, objInvestorDetailsBO.getJointName1());
                            CamsDBInsert.setString(13, objInvestorDetailsBO.getJointName2());
                            CamsDBInsert.setString(14, objInvestorDetailsBO.getAddress1());
                            CamsDBInsert.setString(15, objInvestorDetailsBO.getAddress2());
                            CamsDBInsert.setString(16, objInvestorDetailsBO.getAddress3());
                            CamsDBInsert.setString(17, objInvestorDetailsBO.getCity());
                            CamsDBInsert.setString(18, objInvestorDetailsBO.getPinCode());
                            CamsDBInsert.setString(19, objInvestorDetailsBO.getPhoneOffice());
                            dateConversionToSQL = new ConstansBO().getSQLDate(objTransactionBO.getTransactionDateMIS().getMonthFirstView());
                            CamsDBInsert.setDate(20, (java.sql.Date) dateConversionToSQL);
                            CamsDBInsert.setString(21, objTransactionBO.getTransactionTimeMIS());
                            //logger.info("Data Writer : "+(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))+" == "+objAccountDetailsBO.getClos_Ac_Ch()));
                            if (objAccountDetailsBO.getClos_Ac_Ch().equals("Y")) {
                                CamsDBInsert.setDouble(22, new Double(0.0d));
                                CamsDBInsert.setDouble(23, new Double(0.0d));
                            } else {
                                CamsDBInsert.setDouble(22, objTransactionBO.getUnits());
                                CamsDBInsert.setDouble(23, objTransactionBO.getAmount().doubleValue());
                            }
                            CamsDBInsert.setString(24, objAccountDetailsBO.getClos_Ac_Ch());
                            try {
                                dateConversionToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirth().getMonthFirstView());
                                CamsDBInsert.setDate(25, (java.sql.Date) dateConversionToSQL);
                            } catch (Exception e) {
                            }
                            CamsDBInsert.setString(26, objInvestorDetailsBO.getGuardianName());
                            CamsDBInsert.setString(27, objInvestorDetailsBO.getTaxNo());
                            CamsDBInsert.setString(28, objInvestorDetailsBO.getPhoneRes());
                            CamsDBInsert.setString(29, objInvestorDetailsBO.getFaxOff());
                            CamsDBInsert.setString(30, objInvestorDetailsBO.getFaxRes());
                            CamsDBInsert.setString(31, objInvestorDetailsBO.getEmail());
                            CamsDBInsert.setString(32, objAccountDetailsBO.getAccountNo());
                            CamsDBInsert.setString(33, objAccountDetailsBO.getAccountType());
                            CamsDBInsert.setString(34, objAccountDetailsBO.getBankName());
                            CamsDBInsert.setString(35, objAccountDetailsBO.getBranchName());
                            CamsDBInsert.setString(36, objAccountDetailsBO.getBankCity());
                            CamsDBInsert.setString(37, objAccountDetailsBO.getReinv_Tag());
                            CamsDBInsert.setString(38, objAccountDetailsBO.getHoldingNature());
                            CamsDBInsert.setString(39, objInvestorDetailsBO.getOccupationCode());
                            CamsDBInsert.setString(40, objAccountDetailsBO.getTaxStatus());

                            // ---- New Fields ----
                            CamsDBInsert.setString(41, remarksCAMS);

                            if ((objInvestorDetailsBO.getState().equals("")) && (objInvestorDetailsBO.getNRIInvestor() == 1)) {
                                CamsDBInsert.setString(42, "OV");
                            } else {
                                CamsDBInsert.setString(42, objInvestorDetailsBO.getState());
                            }

                            CamsDBInsert.setString(43, objTransactionBO.getSubTransactionType());
                            CamsDBInsert.setString(44, objAccountDetailsBO.getPaymentMechanism());

                            if (!objAccountDetailsBO.getMICR_CD().equals("")) {
                                CamsDBInsert.setDouble(45, new Double(objAccountDetailsBO.getMICR_CD()));
                            } else {
                                CamsDBInsert.setDouble(45, new Double(0.0d));
                            }
                            CamsDBInsert.setString(46, objAccountDetailsBO.getBank_Code());

                            if (!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals("")) {
                                CamsDBInsert.setDouble(47, new Double(objAccountDetailsBO.getALT_Folio()));
                            } else {
                                CamsDBInsert.setDouble(47, new Double(0.0d));
                            }

                            CamsDBInsert.setString(48, objAccountDetailsBO.getALT_Broker());
                            CamsDBInsert.setString(49, objInvestorDetailsBO.getLocationCode());
                            CamsDBInsert.setString(50, objAccountDetailsBO.getPaymentMechanism());
                            CamsDBInsert.setString(51, objTransactionBO.getPricing());
                            CamsDBInsert.setString(52, objInvestorDetailsBO.getPanHolder2());
                            CamsDBInsert.setString(53, objInvestorDetailsBO.getPanHolder3());
                            CamsDBInsert.setString(54, objAccountDetailsBO.getNomineeName());
                            CamsDBInsert.setString(55, objAccountDetailsBO.getNomineeRelation());
                            CamsDBInsert.setString(56, objInvestorDetailsBO.getGuardianPanNo());
                            CamsDBInsert.setString(57, objAccountDetailsBO.getInstrmno());
                            CamsDBInsert.setString(58, objAccountDetailsBO.getUINNo());
                            CamsDBInsert.setString(59, objInvestorDetailsBO.getValid_Pan());
                            CamsDBInsert.setString(60, objInvestorDetailsBO.getGValid_Pan());
                            CamsDBInsert.setString(61, objInvestorDetailsBO.getJH1ValidPan());
                            CamsDBInsert.setString(62, objInvestorDetailsBO.getJH2ValidPan());
                            try{
                            if (objTransactionBO.getSIP_Reg_Date() != null) {
                                dateConversionToSQL = new ConstansBO().getSQLDate(objTransactionBO.getSIP_Reg_Date().getMonthFirstView());
                                CamsDBInsert.setDate(63, (java.sql.Date) dateConversionToSQL);
                            }
                            } catch (Exception ex) {
                                logger.error("objTransactionBO.getSIP_Reg_Date() : DATE NULL : CAMS : "+objTransactionBO.getUserTransactionNo());
                                CamsDBInsert.setDate(63, null);
                            }
                            CamsDBInsert.setString(64, objAccountDetailsBO.getFirst_Hldr_Min());
                            CamsDBInsert.setString(65, objAccountDetailsBO.getJH1_Min());
                            CamsDBInsert.setString(66, objAccountDetailsBO.getJH2_Min());
                            CamsDBInsert.setString(67, objAccountDetailsBO.getGuardian_Min());
                            CamsDBInsert.setString(68, objAccountDetailsBO.getNEFT_CD());
                            CamsDBInsert.setString(69, objAccountDetailsBO.getRTGS_CD());
                            CamsDBInsert.setString(70, "E");//EMAIL_ACST
                            CamsDBInsert.setString(71, objInvestorDetailsBO.getMobileNo());
                            CamsDBInsert.setString(72, objTransactionBO.getDpId()); // Dip_Id
                            // ---- New Fields ----
                            CamsDBInsert.setString(73, "N"); // POA_type
                            CamsDBInsert.setString(74, "W"); // Trxn Mode
                            CamsDBInsert.setString(75, "Y"); // Trxn_Sign_Confn

                            if (objInvestorDetailsBO.getNRIInvestor() == 1)// For NRI Investor send NRI Addr. detials
                            {
                                CamsDBInsert.setString(76, "NRI Ad: " + objInvestorDetailsBO.getNRIAddress1());
                                CamsDBInsert.setString(77, objInvestorDetailsBO.getNRIAddress2());
                                CamsDBInsert.setString(78, objInvestorDetailsBO.getNRIAddress3());

                                if (!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState())) {
                                    CamsDBInsert.setString(79, objInvestorDetailsBO.getNRICity() + " " + objInvestorDetailsBO.getNRIState());
                                } else {
                                    CamsDBInsert.setString(79, objInvestorDetailsBO.getNRICity());
                                }

                                CamsDBInsert.setString(80, objInvestorDetailsBO.getNRICountry());
                                CamsDBInsert.setString(81, objInvestorDetailsBO.getPinCode());
                                CamsDBInsert.setString(82, "Y"); // Check_Flag
                                CamsDBInsert.setString(83, "Y"); // FIRC_STA 

                            } else {
                                CamsDBInsert.setString(76, "");
                                CamsDBInsert.setString(77, "");
                                CamsDBInsert.setString(78, "");

                                CamsDBInsert.setString(79, "");

                                CamsDBInsert.setString(80, "");
                                CamsDBInsert.setString(81, "");
                                CamsDBInsert.setString(83, ""); // FIRC_STA 
                                CamsDBInsert.setString(82, "N"); // Check_Flag
                            }
                            if (objAccountDetailsBO.getNomineePercent() > 0) // if Nominee is available then 100% goes to that nominee
                            {
                                CamsDBInsert.setDouble(84, new Double(objAccountDetailsBO.getNomineePercent())); //Nom1_Applicable -	Number -  5,0
                            } else {
                                CamsDBInsert.setDouble(84, new Double(0.0d));
                            }
                            if (objTransactionBO.getTransactionType().equals("P")) {
                                CamsDBInsert.setString(85, "N"); // TH_P_PYT ( Third Party Payment)
                                CamsDBInsert.setString(86, objInvestorDetailsBO.getKycFlag()); // KYC
                            } else {
                                CamsDBInsert.setString(85, ""); // TH_P_PYT ( Third Party Payment)
                                CamsDBInsert.setString(86, objInvestorDetailsBO.getKycFlag()); // KYC
                                CamsDBInsert.setString(83, ""); // FIRC_STA 
                            }

                            if ((objTransactionBO.getSipStartDate() != null) && (objTransactionBO.getSipEndDate() != null)) {
                                CamsDBInsert.setDouble(87, new Double(objTransactionBO.getSipReferenceId()));
                                CamsDBInsert.setDouble(88, new Double(objTransactionBO.getNoOfInstalment()));
                                CamsDBInsert.setString(89, objTransactionBO.getSipFrequency());
                                dateConversionToSQL = new ConstansBO().getSQLDate(objTransactionBO.getSipStartDate().getMonthFirstView()); // Convert sip start date to java.sql.Date
                                CamsDBInsert.setDate(90, (java.sql.Date) dateConversionToSQL);
                                dateConversionToSQL = new ConstansBO().getSQLDate(objTransactionBO.getSipEndDate().getMonthFirstView()); // convert sip end date to java.sql.Date format
                                CamsDBInsert.setDate(91, (java.sql.Date) dateConversionToSQL);
                                CamsDBInsert.setDouble(92, new Double(objTransactionBO.getPaidInstallments()));
                            } else {
                                CamsDBInsert.setDouble(87, new Double(0.0d));
                                CamsDBInsert.setDouble(88, new Double(0.0d));
                                CamsDBInsert.setString(89, "");
                                logger.info("objTransactionBO.getSipStartDate() : objTransactionBO.getSipEndDate() : DATE NULL : CAMS : "+ objTransactionBO.getUserTransactionNo());
                                CamsDBInsert.setDate(90, null);
                                CamsDBInsert.setDate(91, null);
                                CamsDBInsert.setDouble(92, new Double(0.0d));
                            }

                            CamsDBInsert.setString(93, "N"); // First Holder PAN Exempt
                            CamsDBInsert.setString(94, "N"); // JH1 PAN Exempt
                            CamsDBInsert.setString(95, "N"); // JH2 PAN Exempt
                            CamsDBInsert.setString(96, "N"); // Guardian PAN Exempt

                            CamsDBInsert.setString(97, objTransactionBO.getEuinOpted());
                            CamsDBInsert.setString(98, objTransactionBO.getEuinId());
                            CamsDBInsert.setString(99, objAccountDetailsBO.getNomineeOpted());
                            CamsDBInsert.setString(100, objInvestorDetailsBO.getSubBrokerCode()); // Subbroker Code

                            CamsDBInsert.setString(101, "Y");//BankMandateProof

                            if (!objInvestorDetailsBO.getcKYCRefId1().equals("")) {
                                CamsDBInsert.setDouble(102, new Double(objInvestorDetailsBO.getcKYCRefId1()));//Dummy4
                            } else {
                                CamsDBInsert.setDouble(102, new Double(0.0d));
                            }
                            if (!objInvestorDetailsBO.getcKYCRefId2().equals("")) {
                                CamsDBInsert.setDouble(103, new Double(objInvestorDetailsBO.getcKYCRefId2()));//Dummy5
                            } else {
                                CamsDBInsert.setDouble(103, new Double(0.0d));
                            }
                            if (!objInvestorDetailsBO.getcKYCRefId3().equals("")) {
                                CamsDBInsert.setDouble(104, new Double(objInvestorDetailsBO.getcKYCRefId3()));//Dummy6
                            } else {
                                CamsDBInsert.setDouble(104, new Double(0.0d));
                            }
                            if (!objInvestorDetailsBO.getcKYCRefIdG().equals("")) {
                                CamsDBInsert.setDouble(105, new Double(objInvestorDetailsBO.getcKYCRefIdG()));//Dummy7
                            } else {
                                CamsDBInsert.setDouble(105, new Double(0.0d));
                            }
                            if (objInvestorDetailsBO.getDateOfBirth2() != null) {
                                dateConversionToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirth2().getMonthFirstView()); // convert dob2 to java.sql.Date format
                                CamsDBInsert.setDate(106, (java.sql.Date) dateConversionToSQL);//Dummy8
                            } else {
                                logger.info("objInvestorDetailsBO.getDateOfBirth2() : DATE  NULL : CAMS : "+ objTransactionBO.getUserTransactionNo());
                                CamsDBInsert.setDate(106, null);
                            }
                            if (objInvestorDetailsBO.getDateOfBirth3() != null) {
                                dateConversionToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirth3().getMonthFirstView());// convert dob3 to java.sql.Date format
                                CamsDBInsert.setDate(107, (java.sql.Date) dateConversionToSQL);//Dummy9
                            } else {
                                logger.info("objInvestorDetailsBO.getDateOfBirth3() : DATE  NULL : CAMS : "+ objTransactionBO.getUserTransactionNo());
                                CamsDBInsert.setDate(107, null);
                            }
                            if (objInvestorDetailsBO.getDateOfBirthG() != null) {
                                dateConversionToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirthG().getMonthFirstView());  // convert guardian dob to java.sql.Date format
                                CamsDBInsert.setDate(108, (java.sql.Date) dateConversionToSQL);//Dummy10
                            } else {
                                logger.info("objInvestorDetailsBO.getDateOfBirthG() : DATE  NULL : CAMS : "+ objTransactionBO.getUserTransactionNo());
                                CamsDBInsert.setDate(108, null);
                            }


                            /*	
				 * CamsDBInsert.setString(107, objInvestorDetailsBO.getKycFlag2());//KYC_Type_Second Holder
					CamsDBInsert.setString(110, objInvestorDetailsBO.getKycFlag3());//KYC_Type_Third Holder
					CamsDBInsert.setString(111, objInvestorDetailsBO.getKycFlagG());//KYC_Type_Gaurdian
					
				 	CamsDBInsert.setString(136, new Double(objInvestorDetailsBO.getcKYCRefId1()));
					CamsDBInsert.setString(137, new Double(objInvestorDetailsBO.getcKYCRefId2()));
					CamsDBInsert.setString(138, new Double(objInvestorDetailsBO.getcKYCRefId3()));
					CamsDBInsert.setString(139, new Double(objInvestorDetailsBO.getcKYCRefIdG()));*/
 /*if(objInvestorDetailsBO.getKycFlag().equals("C"))
						CamsDBInsert.setString(140, "N");
					else
						CamsDBInsert.setString(140, "Y");
					
					if(objInvestorDetailsBO.getKycFlag2().equals("C"))
						CamsDBInsert.setString(141, "N");
					else
						CamsDBInsert.setString(141, "Y");
					
					if(objInvestorDetailsBO.getKycFlag3().equals("C"))
						CamsDBInsert.setString(142, "N");
					else
						CamsDBInsert.setString(142, "Y");
					
					if(objInvestorDetailsBO.getKycFlagG().equals("C"))
						CamsDBInsert.setString(143, "N");
					else
						CamsDBInsert.setString(143, "Y");*/
                            //for (Object i : rowData) {
                            //	pipe.append(DELIMETER).append(i));
                            //  DELIMETER = "|");
                        }
                        CamsDBInsert.addBatch();
                    } else {
                        insertCount--;
                    }
                            insertedRows = CamsDBInsert.executeBatch();
                            logger.info("Executed batch : " + strKeyValue + " successfully. Batch size : " + arryTransactionMainBO.size());
                            CamsDBInsert.clearBatch();
//                    insertedRows = CamsDBInsert.executeBatch();
//                    logger.info(insertedRows);
                    } catch(Exception ex){
                        logger.error(ex);
                        continue;
                    }
                }

            }

        } catch (Exception daoEx) {
            logger.info(daoEx);
        } finally {
            if (CamsDBInsert != null) {
                CamsDBInsert.close();
            }
            if (connectionObject != null) {
                connectionObject.close();
            }
        }
        logger.info("## -- END - CAMS DB insertion");
    }

    public void writeKARVYDB(Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO, ArrayList<STPProcessBO> failedSList) throws Exception {
        Connection connectionObject = null;
        PreparedStatement KarvyDBInsert = null;
        WIFSSQLCon wifsSQLCon = new WIFSSQLCon();

        int intMainBORowSize = 0;
        ArrayList<FatcaDocBO> arrFatcaDocBO = new ArrayList<FatcaDocBO>();
        String strKeyValue = "";
        int insertedRows[] = {};
        java.sql.Date dateConvertedToSQL = null;
        Set<String> set = hashTransactionMainBO.keySet();
        Iterator<String> itr = set.iterator();
        HashMap<Integer, String> failedHash = new HashMap<>();
        for (STPProcessBO item : failedSList) {
            if (!failedHash.containsKey(item.getUserTransRefId())) {
                failedHash.put(item.getUserTransRefId(), item.getMessage());
            }
        }
        try {
            connectionObject = wifsSQLCon.getWifsSQLConnect();

            String sqlQuery_ITT_Update = "Insert into KARVY_Forward_Feed (AMC_CODE,\n"
                    + "BROKE_CD,\n"
                    + "SBBR_CODE,\n"
                    + "User_Code,\n"
                    + "USR_TXN_NO,\n"
                    + "Appl_No,\n"
                    + "FOLIO_NO,\n"
                    + "Ck_DIG_NO,\n"
                    + "TRXN_TYPE,\n"
                    + "SCH_CODE,\n"
                    + "FIRST_NAME,\n"
                    + "JONT_NAME1,\n"
                    + "JONT_NAME2,\n"
                    + "ADD1,\n"
                    + "ADD2,\n"
                    + "ADD3,\n"
                    + "CITY,\n"
                    + "PINCODE,\n"
                    + "PHONE_OFF,\n"
                    + "MOBILE_NO,\n"
                    + "TRXN_DATE,\n"
                    + "TRXN_TIME,\n"
                    + "UNITS,\n"
                    + "AMOUNT,\n"
                    + "CLOS_AC_CH,\n"
                    + "DOB,\n"
                    + "GUARDIAN,\n"
                    + "TAX_NUMBER,\n"
                    + "PHONE_RES,\n"
                    + "FAX_OFF,\n"
                    + "FAX_RES,\n"
                    + "EMAIL,\n"
                    + "ACCT_NO,\n"
                    + "ACCT_TYPE,\n"
                    + "BANK_NAME,\n"
                    + "BR_NAME,\n"
                    + "BANK_CITY,\n"
                    + "REINV_TAG,\n"
                    + "HOLD_NATUR,\n"
                    + "OCC_CODE,\n"
                    + "TAX_STATUS,\n"
                    + "REMARKS,\n"
                    + "STATE,\n"
                    + "PAN_2_HLDR,\n"
                    + "PAN_3_HLDR,\n"
                    + "Guard_PAN,\n"
                    + "LOCATION,\n"
                    + "UINno,\n"
                    + "PAY_MEC,\n"
                    + "RTGS_CD,\n"
                    + "NEFT_CD,\n"
                    + "MICR_CD,\n"
                    + "DEPBANK,\n"
                    + "DEP_ACNO,\n"
                    + "DEP_DATE,\n"
                    + "DEP_RFNo,\n"
                    + "SUB_TRXN_T,\n"
                    + "SIP_RFNO,\n"
                    + "SIP_RGDT,\n"
                    + "NOM_NAME,\n"
                    + "NOM_RELA,\n"
                    + "KYC_FLG,\n"
                    + "POA_STAT,\n"
                    + "MOD_TRXN,\n"
                    + "SIGN_VF,\n"
                    + "CUST_ID,\n"
                    + "LOG_WT,\n"
                    + "LOG_PE,\n"
                    + "DPID,\n"
                    + "ClientID,\n"
                    + "NRI_SOF,\n"
                    + "EUIN,\n"
                    + "EUIN_OPT,\n"
                    + "INCSLAB,\n"
                    + "INCSLAB_J1,\n"
                    + "PEP_J1,\n"
                    + "INCSLAB_J2,\n"
                    + "PEP_J2,\n"
                    + "INCSLAB_J3,\n"
                    + "PEP_J3,\n"
                    + "INCSLAB_GR,\n"
                    + "PEP_GR,\n"
                    + "Forex_MCS,\n"
                    + "GAME_GABLE,\n"
                    + "LS_ML_PA,\n"
                    + "SIP_ST_DT,\n"
                    + "SIP_FQ,\n"
                    + "SIP_END_DT,\n"
                    + "SIP_amt,\n"
                    + "NRI_ADD1,\n"
                    + "NRI_ADD2,\n"
                    + "NRI_ADD3,\n"
                    + "NRI_CITY,\n"
                    + "NRI_State,\n"
                    + "NRI_CON,\n"
                    + "NRI_PIN,\n"
                    + "Nom2_Name,\n"
                    + "NOM2_REL,\n"
                    + "Nom3_Name,\n"
                    + "NOM3_REL,\n"
                    + "NOM_PER,\n"
                    + "NOM2_PER,\n"
                    + "NOM3_PER,\n"
                    + "FATCA_FLAG,\n"
                    + "DUMMY4,\n"
                    + "DUMMY5,\n"
                    + "DUMMY6,\n"
                    + "Dummy7,\n"
                    + "Dummy8,\n"
                    + "Dummy9,\n"
                    + "Dummy10) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            KarvyDBInsert = connectionObject.prepareStatement(sqlQuery_ITT_Update);
            while (itr.hasNext()) {
                strKeyValue = itr.next();
                ArrayList<TransactionMainBO> arryTransactionMainBO = new ArrayList<>();
                arryTransactionMainBO = hashTransactionMainBO.get(strKeyValue);
                intMainBORowSize = arryTransactionMainBO.size();
                int count = 0, insertCount = arryTransactionMainBO.size();

                for (int DBFRow = 0; DBFRow < intMainBORowSize; DBFRow++) {
                    arrFatcaDocBO = new ArrayList<FatcaDocBO>();
                    TransactionMainBO objTransactionMainBO = new TransactionMainBO();
                    InvestorDetailsBO objInvestorDetailsBO = new InvestorDetailsBO();
                    AccountDetailsBO objAccountDetailsBO = new AccountDetailsBO();
                    TransactionBO objTransactionBO = new TransactionBO();
                    FatcaDocBO objFatcaDocBO = new FatcaDocBO();
                    objTransactionMainBO = arryTransactionMainBO.get(DBFRow);
                    objTransactionBO = objTransactionMainBO.getTransactionBO();
                    objInvestorDetailsBO = objTransactionMainBO.getInvestorDetailsBO();
                    objAccountDetailsBO = objTransactionMainBO.getAccountDetailsBO();
                    if (!failedHash.containsKey(Integer.valueOf(objTransactionBO.getUserTransactionNo()))) {
                        count++;
                        //logger.info("karvy file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());

                        KarvyDBInsert.setString(1, objTransactionBO.getAmcCode()); //AMCCode
                        KarvyDBInsert.setString(2, "ARN-69583");//objTransactionBO.getBrokerCode()); // Broker Code
                        KarvyDBInsert.setString(3, objInvestorDetailsBO.getSubBrokerCode()); // Subbroker Code
                        KarvyDBInsert.setString(4, objInvestorDetailsBO.getUserCode()); // User Code
                        KarvyDBInsert.setString(5, objTransactionBO.getUserTransactionNo()); // Transaction No
                        KarvyDBInsert.setString(6, objTransactionBO.getApplNo()); // Application No
                        KarvyDBInsert.setString(7, ConstansBO.NullCheck(objTransactionBO.getFolioNo())); // Folio No
                        KarvyDBInsert.setString(8, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())); // Cheque degit No
                        KarvyDBInsert.setString(9, objTransactionBO.getTransactionType());
                        KarvyDBInsert.setString(10, objTransactionBO.getSchemeCode());
                        KarvyDBInsert.setString(11, objInvestorDetailsBO.getFirstName());
                        KarvyDBInsert.setString(12, objInvestorDetailsBO.getJointName1());
                        KarvyDBInsert.setString(13, objInvestorDetailsBO.getJointName2());
                        KarvyDBInsert.setString(14, objInvestorDetailsBO.getAddress1());
                        KarvyDBInsert.setString(15, objInvestorDetailsBO.getAddress2());
                        KarvyDBInsert.setString(16, objInvestorDetailsBO.getAddress3());
                        KarvyDBInsert.setString(17, objInvestorDetailsBO.getCity());
                        KarvyDBInsert.setString(18, objInvestorDetailsBO.getPinCode());
                        KarvyDBInsert.setString(19, objInvestorDetailsBO.getPhoneOffice());
                        KarvyDBInsert.setString(20, objInvestorDetailsBO.getMobileNo());
                        dateConvertedToSQL = new ConstansBO().getSQLDate(objTransactionBO.getTransactionDate().getMonthFirstView());
                        KarvyDBInsert.setDate(21, (java.sql.Date) dateConvertedToSQL);
                        KarvyDBInsert.setString(22, objTransactionBO.getTransactionTime());
                        if (objAccountDetailsBO.getClos_Ac_Ch().equals("Y")) {
                            KarvyDBInsert.setString(23, "");
                            KarvyDBInsert.setString(24, "");
                        } else {
                            KarvyDBInsert.setString(23, String.valueOf(objTransactionBO.getUnits()));
                            KarvyDBInsert.setString(24, String.valueOf(objTransactionBO.getAmount()));
                        }
                        KarvyDBInsert.setString(25, objAccountDetailsBO.getClos_Ac_Ch());
                        dateConvertedToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirth().getMonthFirstView());
                        KarvyDBInsert.setDate(26, (java.sql.Date) dateConvertedToSQL);
                        KarvyDBInsert.setString(27, objInvestorDetailsBO.getGuardianName());
                        KarvyDBInsert.setString(28, objInvestorDetailsBO.getTaxNo());
                        KarvyDBInsert.setString(29, objInvestorDetailsBO.getPhoneRes());
                        KarvyDBInsert.setString(30, objInvestorDetailsBO.getFaxOff());
                        KarvyDBInsert.setString(31, objInvestorDetailsBO.getFaxRes());
                        KarvyDBInsert.setString(32, objInvestorDetailsBO.getEmail());
                        KarvyDBInsert.setString(33, objAccountDetailsBO.getAccountNo());
                        KarvyDBInsert.setString(34, objAccountDetailsBO.getAccountType());
                        KarvyDBInsert.setString(35, objAccountDetailsBO.getBankName());
                        KarvyDBInsert.setString(36, objAccountDetailsBO.getBranchName());
                        KarvyDBInsert.setString(37, objAccountDetailsBO.getBankCity());
                        KarvyDBInsert.setString(38, objAccountDetailsBO.getReinv_Tag());
                        KarvyDBInsert.setString(39, objAccountDetailsBO.getHoldingNature());
                        KarvyDBInsert.setString(40, objInvestorDetailsBO.getOccupationCode());
                        KarvyDBInsert.setString(41, objAccountDetailsBO.getTaxStatus());
                        KarvyDBInsert.setString(42, objTransactionBO.getPaymentId() + " " + objAccountDetailsBO.getRemarks());
                        KarvyDBInsert.setString(43, objInvestorDetailsBO.getState());
                        KarvyDBInsert.setString(44, objInvestorDetailsBO.getPanHolder2());
                        KarvyDBInsert.setString(45, objInvestorDetailsBO.getPanHolder3());
                        KarvyDBInsert.setString(46, objInvestorDetailsBO.getGuardianPanNo());
                        KarvyDBInsert.setString(47, objInvestorDetailsBO.getLocationCode());
                        KarvyDBInsert.setString(48, objAccountDetailsBO.getUINNo());
                        KarvyDBInsert.setString(49, objAccountDetailsBO.getPaymentMechanism());
                        KarvyDBInsert.setString(50, objAccountDetailsBO.getRTGS_CD());
                        KarvyDBInsert.setString(51, objAccountDetailsBO.getNEFT_CD());
                        KarvyDBInsert.setString(52, objAccountDetailsBO.getMICR_CD());
                        KarvyDBInsert.setString(53, objAccountDetailsBO.getDepBankName());
                        KarvyDBInsert.setString(54, objAccountDetailsBO.getDepAccountNo());
                        dateConvertedToSQL = new ConstansBO().getSQLDate(objAccountDetailsBO.getDepDate().getMonthFirstView());
                        KarvyDBInsert.setDate(55, (java.sql.Date) dateConvertedToSQL);
                        KarvyDBInsert.setString(56, objAccountDetailsBO.getDepReferenceNo());
                        KarvyDBInsert.setString(57, objTransactionBO.getSubTransactionType());
                        if (!objTransactionBO.getSipReferenceId().equals("0")) {
                            KarvyDBInsert.setString(58, objTransactionBO.getSipReferenceId());
                        } else {
                            KarvyDBInsert.setString(58, "");
                        }
                        if (objTransactionBO.getSIP_Reg_Date() != null) {
                            dateConvertedToSQL = new ConstansBO().getSQLDate(objTransactionBO.getSIP_Reg_Date().getMonthFirstView());
                            KarvyDBInsert.setDate(59, (java.sql.Date) dateConvertedToSQL);
                        } else {
                            KarvyDBInsert.setDate(59, (java.sql.Date) null);
                            logger.info("objTransactionBO.getSIP_Reg_Date() : DATE  NULL : Karvy : "+ objTransactionBO.getUserTransactionNo());
                        }
                        KarvyDBInsert.setString(60, objAccountDetailsBO.getNomineeName());
                        KarvyDBInsert.setString(61, objAccountDetailsBO.getNomineeRelation());
                        KarvyDBInsert.setString(62, objInvestorDetailsBO.getKycFlag());
                        // ---- New Fields ----
                        KarvyDBInsert.setString(63, "N"); //POA_STAT
                        KarvyDBInsert.setString(64, "W"); //MOD_TRXN
                        KarvyDBInsert.setString(65, "Y"); //SIGN_VF
                        KarvyDBInsert.setString(66, objInvestorDetailsBO.getInvestorId()); //CUST_ID
                        KarvyDBInsert.setString(67, objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo()); // LOG_WT
                        KarvyDBInsert.setString(68, ""); //LOG_PE
                        KarvyDBInsert.setString(69, objTransactionBO.getDpId()); //DPID
                        KarvyDBInsert.setString(70, objTransactionBO.getClientCode()); //ClientID
                        if ((objAccountDetailsBO.isNriInvestor()) && (objTransactionBO.getTransactionType().equals("P"))) {
                            KarvyDBInsert.setString(71, objAccountDetailsBO.getAccountNo());//NRI_SOF
                        } else {
                            KarvyDBInsert.setString(71, ""); //NRI_SOF
                        }
                        KarvyDBInsert.setString(72, objTransactionBO.getEuinId()); // EUIN
                        KarvyDBInsert.setString(73, objTransactionBO.getEuinOpted()); // EUIN_OPT
                        KarvyDBInsert.setString(74, "");
                        KarvyDBInsert.setString(75, "");
                        KarvyDBInsert.setString(76, "");
                        KarvyDBInsert.setString(77, "");
                        KarvyDBInsert.setString(78, "");
                        KarvyDBInsert.setString(79, "");
                        KarvyDBInsert.setString(80, "");
                        KarvyDBInsert.setString(81, "");
                        KarvyDBInsert.setString(82, "");

                        arrFatcaDocBO = objInvestorDetailsBO.getArrFatcaDocBO();

                        for (int fat = 0; fat < arrFatcaDocBO.size(); fat++) {
                            objFatcaDocBO = arrFatcaDocBO.get(fat);
                            if (objFatcaDocBO.getInvestorRelationship().equals("H")) {
                                KarvyDBInsert.setString(74, String.valueOf(objFatcaDocBO.getAnnualIncomeCode()));//INCSLAB
                                //KarvyDBInsert.setString(77,objTransactionBO.getNetworth_j1());NET_WOR_
                                //KarvyDBInsert.setString(78,objTransactionBO.getNetworthDate_j1());NETWOR_DT
                                //---KarvyDBInsert.setString(0,objFatcaDocBO.getPoliticallyExpPer());
                            } else if (objFatcaDocBO.getInvestorRelationship().equals("F")) {
                                KarvyDBInsert.setString(75, String.valueOf(objFatcaDocBO.getAnnualIncomeCode()));//INCSLAB_J1
                                //KarvyDBInsert.setString(80,objTransactionBO.getNetworth_j2());NET_WOR_J1
                                //KarvyDBInsert.setString(81,objTransactionBO.getNetworthDate_j2());NETDATE_J1
                                KarvyDBInsert.setString(76, objFatcaDocBO.getPoliticallyExpPer());//PEP_J1
                            } else if (objFatcaDocBO.getInvestorRelationship().equals("S")) {
                                KarvyDBInsert.setString(77, String.valueOf(objFatcaDocBO.getAnnualIncomeCode()));//INCSLAB_J2
                                //KarvyDBInsert.setString(84,objTransactionBO.getNetworth_j3());NET_WOR_J2
                                //KarvyDBInsert.setString(85,objTransactionBO.getNetworthDate_j3());NETDATE_J2
                                KarvyDBInsert.setString(78, objFatcaDocBO.getPoliticallyExpPer());//PEP_J2
                            } else if (objFatcaDocBO.getInvestorRelationship().equals("T")) {
                                KarvyDBInsert.setString(79, String.valueOf(objFatcaDocBO.getAnnualIncomeCode()));//INCSLAB_J3
                                //KarvyDBInsert.setString(88,objTransactionBO.getNetworth());NET_WOR_J3
                                //KarvyDBInsert.setString(89,objTransactionBO.getNetworthDate());NETDATE_J3
                                KarvyDBInsert.setString(80, objFatcaDocBO.getPoliticallyExpPer());//PEP_J3
                            } else if (objFatcaDocBO.getInvestorRelationship().equals("G")) {
                                KarvyDBInsert.setString(81, String.valueOf(objFatcaDocBO.getAnnualIncomeCode()));//INCSLAB_GR
                                //KarvyDBInsert.setString(92,objTransactionBO.getNetworth_gr());NET_WOR_GR
                                //KarvyDBInsert.setString(93,objTransactionBO.getNetworthDate_gr());NETDATE_GR
                                KarvyDBInsert.setString(82, objFatcaDocBO.getPoliticallyExpPer());//PEP_GR
                            }
                        }

                        KarvyDBInsert.setString(83, objTransactionBO.getForex_MCS());
                        KarvyDBInsert.setString(84, objTransactionBO.getGAME_GABLE());
                        KarvyDBInsert.setString(85, objTransactionBO.getLS_ML_PA());

                        if (objTransactionBO.getSipStartDate() != null) {
                            dateConvertedToSQL = new ConstansBO().getSQLDate(objTransactionBO.getSipStartDate().getMonthFirstView());
                            KarvyDBInsert.setDate(86, (java.sql.Date) dateConvertedToSQL);
                            KarvyDBInsert.setDouble(87, new Double(objTransactionBO.getSipDate()));
                        } else {
                            logger.info("objTransactionBO.getSipStartDate() : DATE  NULL : Karvy : "+ objTransactionBO.getUserTransactionNo());
                            KarvyDBInsert.setDate(86, (java.sql.Date) null);
                            KarvyDBInsert.setDouble(87, new Double(0.0d));
                        }
                        if (objTransactionBO.getSipEndDate() != null) {
                            dateConvertedToSQL = new ConstansBO().getSQLDate(objTransactionBO.getSipEndDate().getMonthFirstView());
                            KarvyDBInsert.setDate(88, (java.sql.Date) dateConvertedToSQL);
                        } else {
                            logger.info("objTransactionBO.getSipEndDate() : DATE  NULL : Karvy : "+ objTransactionBO.getUserTransactionNo());
                            KarvyDBInsert.setDate(88, (java.sql.Date) null);
                        }

                        KarvyDBInsert.setDouble(89, objTransactionBO.getSipAmount());
                        KarvyDBInsert.setString(90, objInvestorDetailsBO.getNRIAddress1());
                        KarvyDBInsert.setString(91, objInvestorDetailsBO.getNRIAddress2());
                        KarvyDBInsert.setString(92, objInvestorDetailsBO.getNRIAddress3());
                        KarvyDBInsert.setString(93, objInvestorDetailsBO.getNRICity());
                        KarvyDBInsert.setString(94, objInvestorDetailsBO.getNRIState());
                        KarvyDBInsert.setString(95, objInvestorDetailsBO.getNRICountry());
                        KarvyDBInsert.setString(96, objInvestorDetailsBO.getNRIPinCode());
                        KarvyDBInsert.setString(97, objAccountDetailsBO.getNomineeName2());
                        KarvyDBInsert.setString(98, objAccountDetailsBO.getNomineeRelation2());
                        KarvyDBInsert.setString(99, objAccountDetailsBO.getNomineeName3());
                        KarvyDBInsert.setString(100, objAccountDetailsBO.getNomineeRelation3());
                        KarvyDBInsert.setDouble(101, new Double(objAccountDetailsBO.getNomineePercent()));
                        KarvyDBInsert.setDouble(102, new Double(objAccountDetailsBO.getNomineePercent1()));
                        KarvyDBInsert.setDouble(103, new Double(objAccountDetailsBO.getNomineePercent2()));
                        //EMP_CODE
                        //SUB_ARN
                        KarvyDBInsert.setString(104, objInvestorDetailsBO.getFatcaFlag());//FATCA_FLAG
                        /*Dummy1
				Dummy2
				Dummy3*/
                        if (!objInvestorDetailsBO.getcKYCRefId1().equals("")) {
                            KarvyDBInsert.setDouble(105, new Double(objInvestorDetailsBO.getcKYCRefId1()));//Dummy4
                        } else {
                            KarvyDBInsert.setDouble(105, new Double(0.0d));
                        }
                        if (!objInvestorDetailsBO.getcKYCRefId2().equals("")) {
                            KarvyDBInsert.setDouble(106, new Double(objInvestorDetailsBO.getcKYCRefId2()));//Dummy5
                        } else {
                            KarvyDBInsert.setDouble(106, new Double(0.0d));
                        }
                        if (!objInvestorDetailsBO.getcKYCRefId3().equals("")) {
                            KarvyDBInsert.setDouble(107, new Double(objInvestorDetailsBO.getcKYCRefId3()));//Dummy6
                        } else {
                            KarvyDBInsert.setDouble(107, new Double(0.0d));
                        }
                        if (!objInvestorDetailsBO.getcKYCRefIdG().equals("")) {
                            KarvyDBInsert.setDouble(108, new Double(objInvestorDetailsBO.getcKYCRefIdG()));//Dummy7
                        } else {
                            KarvyDBInsert.setDouble(108, new Double(0.0d));
                        }
                        if (objInvestorDetailsBO.getDateOfBirth2() != null) {
                            dateConvertedToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirth2().getMonthFirstView());
                            KarvyDBInsert.setDate(109, (java.sql.Date) dateConvertedToSQL);//Dummy8
                        } else {
                            logger.info("objInvestorDetailsBO.getDateOfBirth2() : DATE  NULL : Karvy : "+ objTransactionBO.getUserTransactionNo());
                            KarvyDBInsert.setDate(109, (java.sql.Date) null);
                        }
                        if (objInvestorDetailsBO.getDateOfBirth3() != null) {
                            dateConvertedToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirth3().getMonthFirstView());
                            KarvyDBInsert.setDate(110, (java.sql.Date) dateConvertedToSQL);//Dummy9
                        } else {
                            logger.info("objInvestorDetailsBO.getDateOfBirth3() : DATE  NULL : Karvy : "+ objTransactionBO.getUserTransactionNo());
                            KarvyDBInsert.setDate(110, (java.sql.Date) null);
                        }
                        if (objInvestorDetailsBO.getDateOfBirthG() != null) {
                            dateConvertedToSQL = new ConstansBO().getSQLDate(objInvestorDetailsBO.getDateOfBirthG().getMonthFirstView());
                            KarvyDBInsert.setDate(111, (java.sql.Date) dateConvertedToSQL);//Dummy10
                        } else {
                            logger.info("objInvestorDetailsBO.getDateOfBirthG() : DATE  NULL : Karvy : "+ objTransactionBO.getUserTransactionNo());
                            KarvyDBInsert.setDate(111, (java.sql.Date) null);
                        }
                        //Based on the UserTransactionNo Update the ForwardFeed - date
                        KarvyDBInsert.addBatch();
                        if (count % 500 == 0 || count == insertCount) {
                            insertedRows = KarvyDBInsert.executeBatch();
                            logger.info("Executed batch : " + strKeyValue + " successfully. Batch size : " + arryTransactionMainBO.size());
                            KarvyDBInsert.clearBatch();
                        }
                    } else {
                        insertCount--;
                    }
                }

            }
        } catch (Exception ex) {
            logger.error("Error inserting into KARVY : " + ex);
        }

    }

    public void writeFranklinDB(ArrayList<TransactionMainBO> arryTransactionMainBO) throws Exception {
        Connection connectionObject = null;
        PreparedStatement FranklinDBInsert = null;
        WIFSSQLCon wifsSQLCon = new WIFSSQLCon();

        int intMainBORowSize = 0;
        String webLog = "";
        TransactionMainBO objTransactionMainBO = new TransactionMainBO();
        InvestorDetailsBO objInvestorDetailsBO = new InvestorDetailsBO();
        AccountDetailsBO objAccountDetailsBO = new AccountDetailsBO();
        TransactionBO objTransactionBO = new TransactionBO();

        intMainBORowSize = arryTransactionMainBO.size();
        connectionObject = wifsSQLCon.getWifsSQLConnect();

        String sqlQuery_ITT_Update = "";
        FranklinDBInsert = connectionObject.prepareStatement(sqlQuery_ITT_Update);

        //logger.info(intMainBORowSize+" ======= ");
        for (int DBFRow = 0; DBFRow < intMainBORowSize; DBFRow++) {
            try {
                objTransactionMainBO = arryTransactionMainBO.get(DBFRow);

                if (objTransactionMainBO != null) {
                    try {
                        objTransactionBO = objTransactionMainBO.getTransactionBO();
                        objTransactionBO = objTransactionMainBO.getTransactionBO();
                        objInvestorDetailsBO = objTransactionMainBO.getInvestorDetailsBO();
                        objAccountDetailsBO = objTransactionMainBO.getAccountDetailsBO();
                        //logger.info("Franklin file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());
                        FranklinDBInsert.setString(0, objTransactionBO.getAmcCode()); //AMCCode
                        FranklinDBInsert.setString(1, "WEALTH"); // Broker Code
                        FranklinDBInsert.setString(2, objInvestorDetailsBO.getSubBrokerCode()); // Subbroker Code
                        FranklinDBInsert.setString(3, objInvestorDetailsBO.getUserCode()); // User code
                        FranklinDBInsert.setString(4, objTransactionBO.getUserTransactionNo()); // Transaction No
                        FranklinDBInsert.setString(5, objTransactionBO.getApplNo()); // Application No
                        FranklinDBInsert.setString(6, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo())); // Folio No				
                        FranklinDBInsert.setString(7, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())); // Cheque degit No
                        FranklinDBInsert.setString(8, objTransactionBO.getTransactionType());
                        FranklinDBInsert.setString(9, objTransactionBO.getSchemeCode());
                        FranklinDBInsert.setString(10, objInvestorDetailsBO.getFirstName());
                        FranklinDBInsert.setString(11, objInvestorDetailsBO.getJointName1());
                        FranklinDBInsert.setString(12, objInvestorDetailsBO.getJointName2());
                        FranklinDBInsert.setString(13, objInvestorDetailsBO.getAddress1());
                        FranklinDBInsert.setString(14, objInvestorDetailsBO.getAddress2());
                        FranklinDBInsert.setString(15, objInvestorDetailsBO.getAddress3());
                        FranklinDBInsert.setString(16, objInvestorDetailsBO.getCity());
                        FranklinDBInsert.setString(17, objInvestorDetailsBO.getPinCode());
                        FranklinDBInsert.setString(18, objInvestorDetailsBO.getPhoneOffice());
                        FranklinDBInsert.setDate(19, (java.sql.Date) objTransactionBO.getTransactionDate().getDateFirstView());
                        FranklinDBInsert.setString(20, objTransactionBO.getTransactionTime());
                        if (objAccountDetailsBO.getClos_Ac_Ch().equals("Y")) {
                            FranklinDBInsert.setDouble(21, new Double(0.0d));
                            FranklinDBInsert.setDouble(22, new Double(0.0d));
                        } else {
                            FranklinDBInsert.setDouble(21, objTransactionBO.getUnits());
                            FranklinDBInsert.setDouble(22, objTransactionBO.getAmount().doubleValue());
                        }
                        FranklinDBInsert.setString(23, objAccountDetailsBO.getClos_Ac_Ch());
                        FranklinDBInsert.setDate(24, (java.sql.Date) objInvestorDetailsBO.getDateOfBirth().getDateFirstView());
                        FranklinDBInsert.setString(25, objInvestorDetailsBO.getGuardianName());
                        FranklinDBInsert.setString(26, objInvestorDetailsBO.getTaxNo());
                        FranklinDBInsert.setString(27, objInvestorDetailsBO.getPhoneRes());
                        FranklinDBInsert.setString(28, objInvestorDetailsBO.getFaxOff());
                        FranklinDBInsert.setString(29, objInvestorDetailsBO.getFaxRes());
                        FranklinDBInsert.setString(30, objInvestorDetailsBO.getEmail());
                        FranklinDBInsert.setString(31, objAccountDetailsBO.getAccountNo());
                        FranklinDBInsert.setString(32, objAccountDetailsBO.getAccountType());
                        FranklinDBInsert.setString(33, objAccountDetailsBO.getBankName());
                        FranklinDBInsert.setString(34, objAccountDetailsBO.getBranchName());
                        FranklinDBInsert.setString(35, objAccountDetailsBO.getBankCity());
                        FranklinDBInsert.setString(36, objAccountDetailsBO.getReinv_Tag());
                        FranklinDBInsert.setString(37, objAccountDetailsBO.getHoldingNature());
                        FranklinDBInsert.setString(38, objInvestorDetailsBO.getOccupationCode());
                        FranklinDBInsert.setString(39, objAccountDetailsBO.getTaxStatus());
                        FranklinDBInsert.setString(40, objTransactionBO.getPaymentId() + " " + objAccountDetailsBO.getRemarks());
                        FranklinDBInsert.setString(41, objInvestorDetailsBO.getState());
                        FranklinDBInsert.setString(42, objTransactionBO.getSubTransactionType());
                        FranklinDBInsert.setString(43, "DC"); // DIV_PY_MEC - default value
                        if (!objAccountDetailsBO.getMICR_CD().equals("")) {
                            FranklinDBInsert.setDouble(44, new Double(objAccountDetailsBO.getMICR_CD()));
                        }
                        FranklinDBInsert.setString(45, objAccountDetailsBO.getBank_Code());
                        FranklinDBInsert.setString(46, objAccountDetailsBO.getALT_Folio());
                        FranklinDBInsert.setString(47, objAccountDetailsBO.getALT_Broker());
                        FranklinDBInsert.setString(48, objInvestorDetailsBO.getLocationCode());
                        FranklinDBInsert.setString(49, "DC"); // RED_PY_MEC - default value
                        FranklinDBInsert.setString(50, objTransactionBO.getPricing());
                        FranklinDBInsert.setString(51, objInvestorDetailsBO.getPanHolder2());
                        FranklinDBInsert.setString(52, objInvestorDetailsBO.getPanHolder3());
                        FranklinDBInsert.setString(53, objAccountDetailsBO.getNomineeName());
                        FranklinDBInsert.setString(54, objAccountDetailsBO.getNomineeRelation());
                        FranklinDBInsert.setString(55, objInvestorDetailsBO.getGuardianPanNo());
                        FranklinDBInsert.setString(56, objAccountDetailsBO.getInstrmno());
                        FranklinDBInsert.setString(57, objAccountDetailsBO.getUINNo());
                        FranklinDBInsert.setString(58, objInvestorDetailsBO.getValid_Pan());
                        FranklinDBInsert.setString(59, objInvestorDetailsBO.getGValid_Pan());
                        FranklinDBInsert.setString(60, objInvestorDetailsBO.getJH1ValidPan());
                        FranklinDBInsert.setString(61, objInvestorDetailsBO.getJH2ValidPan());
                        FranklinDBInsert.setString(62, "");//FORM6061RE
                        FranklinDBInsert.setString(63, "");//JH1F6061RE
                        FranklinDBInsert.setString(64, "");//JH2F6061RE
                        FranklinDBInsert.setString(65, "");//GF6061RE
                        if (objTransactionBO.getSIP_Reg_Date() != null) {
                            FranklinDBInsert.setDate(66, (java.sql.Date) objTransactionBO.getSIP_Reg_Date().getDateFirstView());
                        }
                        FranklinDBInsert.setString(67, objAccountDetailsBO.getFirst_Hldr_Min());
                        FranklinDBInsert.setString(68, objAccountDetailsBO.getJH1_Min());
                        FranklinDBInsert.setString(69, objAccountDetailsBO.getJH2_Min());
                        FranklinDBInsert.setString(70, objAccountDetailsBO.getGuardian_Min());
                        FranklinDBInsert.setString(71, objAccountDetailsBO.getNEFT_CD());
                        FranklinDBInsert.setString(72, objAccountDetailsBO.getRTGS_CD());
                        // ---- New Fields ----
                        FranklinDBInsert.setString(73, objInvestorDetailsBO.getKycFlag()); // KYC Flag
                        FranklinDBInsert.setString(74, "N"); // POA State
                        FranklinDBInsert.setString(75, "W"); // Mode Trxn.
                        FranklinDBInsert.setString(76, "N"); // Sign

   						
                        webLog = "Web Log: " + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$"
                                + objTransactionBO.getTransactionTime() + "$$"
                                + objTransactionBO.getUserTransactionNo() + "$"
                                + objTransactionBO.getIPAddress()
                                + "$$$$$$$$$$$$$$$$$$";

                        if (objTransactionBO.getTransactionType().equals("P")) {
                            webLog += "PB$Y$" + objTransactionBO.getCreatedDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$"; // Log
                        } else {
                            webLog += "$$$$"; // Log
                        }
                        FranklinDBInsert.setString(77, webLog);

                        // ---- New Fields ----
                        if (objInvestorDetailsBO.getNRIInvestor() == 1)// For NRI Investor send NRI Addr. detials
                        {
                            FranklinDBInsert.setString(78, "NRI Ad: " + objInvestorDetailsBO.getNRIAddress1());
                            FranklinDBInsert.setString(79, objInvestorDetailsBO.getNRIAddress2());
                            FranklinDBInsert.setString(80, objInvestorDetailsBO.getNRIAddress3());

                            if (!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState())) {
                                FranklinDBInsert.setString(81, objInvestorDetailsBO.getNRICity() + " " + objInvestorDetailsBO.getNRIState());
                            } else {
                                FranklinDBInsert.setString(81, objInvestorDetailsBO.getNRICity());
                            }

                            FranklinDBInsert.setString(82, "");//objInvestorDetailsBO.getNRIState());
                            FranklinDBInsert.setString(83, objInvestorDetailsBO.getNRICountry());
                            FranklinDBInsert.setString(84, objInvestorDetailsBO.getPinCode());

                            //FranklinDBInsert.setString(85, new Double(0.0d)); // Nom1_Applicable -  Number -  5,2
                            FranklinDBInsert.setString(86, ""); // Nom2_Name
                            FranklinDBInsert.setString(87, ""); // Nom2_Relationship

                            //FranklinDBInsert.setString(88, new Double(0.0d)); // Nom2_Applicable -  Number -  5,2
                            FranklinDBInsert.setString(89, ""); // Nom3_Name
                            FranklinDBInsert.setString(90, ""); // Nom3_Relationship
                            //FranklinDBInsert.setString(91, new Double(0.0d)); // Nom2_Applicable -  Number -  5,2
                            FranklinDBInsert.setString(92, "Y"); // Check_Flag
                            FranklinDBInsert.setString(95, "Y"); // FIRC_STA 
                        } else {
                            FranklinDBInsert.setString(92, "N"); // Check_Flag
                        }
                        if (objTransactionBO.getTransactionType().equals("P")) {
                            FranklinDBInsert.setString(93, "N"); // TH_P_PYT ( Third Party Payment)
                            FranklinDBInsert.setString(94, objInvestorDetailsBO.getKycFlag()); // KYC
                            FranklinDBInsert.setString(102, "Y"); // Che_copy
                        } else {
                            FranklinDBInsert.setString(93, ""); // TH_P_PYT ( Third Party Payment)
                            FranklinDBInsert.setString(94, objInvestorDetailsBO.getKycFlag()); // KYC
                            FranklinDBInsert.setString(95, ""); // FIRC_STA 
                        }

                        if (!objTransactionBO.getPaidInstallments().equals("0")) {
                            FranklinDBInsert.setDouble(96, new Double(objTransactionBO.getPaidInstallments())); // INSTL_NO
                            FranklinDBInsert.setDouble(97, new Double(objTransactionBO.getNoOfInstalment())); // TOTAL_INST
                        }

                        FranklinDBInsert.setString(99, objTransactionBO.getEuinOpted()); // EUIN_OPT
                        FranklinDBInsert.setString(100, objTransactionBO.getEuinId()); // EUIN
                        FranklinDBInsert.setDouble(101, new Double(objTransactionBO.getSipReferenceId()));
                        FranklinDBInsert.setString(102, objTransactionBO.getFreshInvestor()); // Fres_Exi
//						Che_copy
                        FranklinDBInsert.setString(104, objInvestorDetailsBO.getMobileNo()); // Mob_no

                        if (objAccountDetailsBO.getNomineeName().equals("")) {
                            FranklinDBInsert.setString(105, "N"); // Nomi_avbl
                        } else {
                            FranklinDBInsert.setString(105, "Y"); // Nomi_avbl
                        }
                        if ((objInvestorDetailsBO.getIrType().equals("OE")) && (objTransactionBO.getTransactionType().equals("P"))) {
                            FranklinDBInsert.setString(77, "Web Log: " + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$"
                                    + objTransactionBO.getTransactionTime() + "$$"
                                    + objTransactionBO.getUserTransactionNo() + "$"
                                    + objTransactionBO.getIPAddress() + "$"
                                    + objTransactionBO.getCreatedUser() + "$"
                                    + objInvestorDetailsBO.getTcVersion() + "$"
                                    + "USCAN$Y$"
                                    + objTransactionBO.getCreatedDate().getDateMonthView().replace("-", "") + "$"
                                    + objTransactionBO.getTransactionTime() + "$"
                                    + "TC$Y$" + objTransactionBO.getCreatedDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$"
                                    + "IAG$Y$" + objTransactionBO.getCreatedDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$"
                                    + "SD$Y$" + objTransactionBO.getCreatedDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$"
                                    + "PB$Y$" + objTransactionBO.getCreatedDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$");

                            FranklinDBInsert.setString(107, "CVL KRA"); // KRA_agency
                        }
                        FranklinDBInsert.setString(106, "Y"); // KRA_Verified
                        FranklinDBInsert.setString(108, objInvestorDetailsBO.getSubBrokerCode()); // Subbroker code

                        if (!objInvestorDetailsBO.getcKYCRefId1().equals("")) {
                            FranklinDBInsert.setDouble(109, new Double(objInvestorDetailsBO.getcKYCRefId1()));
                        }

                        if (!objInvestorDetailsBO.getcKYCRefId2().equals("")) {
                            FranklinDBInsert.setDouble(110, new Double(objInvestorDetailsBO.getcKYCRefId2()));
                        }

                        if (!objInvestorDetailsBO.getcKYCRefId3().equals("")) {
                            FranklinDBInsert.setDouble(111, new Double(objInvestorDetailsBO.getcKYCRefId3()));
                        }

                        if (!objInvestorDetailsBO.getcKYCRefIdG().equals("")) {
                            FranklinDBInsert.setDouble(112, new Double(objInvestorDetailsBO.getcKYCRefIdG()));
                        }

                        if (objInvestorDetailsBO.getDateOfBirth2() != null) {
                            FranklinDBInsert.setDate(113, (java.sql.Date) objInvestorDetailsBO.getDateOfBirth2().getDateFirstView());
                        }

                        if (objInvestorDetailsBO.getDateOfBirth3() != null) {
                            FranklinDBInsert.setDate(114, (java.sql.Date) objInvestorDetailsBO.getDateOfBirth3().getDateFirstView());
                        }

                        if (objInvestorDetailsBO.getDateOfBirthG() != null) {
                            FranklinDBInsert.setDate(115, (java.sql.Date) objInvestorDetailsBO.getDateOfBirthG().getDateFirstView());
                        }

                        FranklinDBInsert.setDouble(116, new Double(objInvestorDetailsBO.getaAdhaarNo_fhld()));
                        FranklinDBInsert.setDouble(117, new Double(objInvestorDetailsBO.getaAdhaarNo_shld()));
                        FranklinDBInsert.setDouble(118, new Double(objInvestorDetailsBO.getaAdhaarNo_thld()));
                        FranklinDBInsert.setDouble(119, new Double(objInvestorDetailsBO.getaAdhaarNo_gurd()));

                        if ((!objInvestorDetailsBO.getaAdhaarNo_fhld().equals("0")) || (!objInvestorDetailsBO.getaAdhaarNo_shld().equals("0")) || (!objInvestorDetailsBO.getaAdhaarNo_thld().equals("0")) || (!objInvestorDetailsBO.getaAdhaarNo_gurd().equals("0"))) {
                            FranklinDBInsert.setString(120, "O");
                        }

                        //if(!objAccountDetailsBO.getSipReferenceNo().equals("0"))
                        //rowData[108] = new Double(objAccountDetailsBO.getSipReferenceNo());
                        //Based on the UserTransactionNo Update the ForwardFeed - date
                        FranklinDBInsert.addBatch();
                    } catch (Exception e) {
                        logger.error("Exp:row " + e);
                    }
                }
            } catch (Exception e) {
                logger.info("Exp while file writing.. row:" + DBFRow + " exp:" + e);
            }
        }
        int insertedRows[] = FranklinDBInsert.executeBatch();
        logger.info("AMC:" + objTransactionBO.getAmcCode() + " Rows:" + insertedRows.length);
        //long totalTime = System.currentTimeMillis() - startTime;
        //logger.info("TIMER : FranklinBatchUpdate = " + totalTime);

        FranklinDBInsert.clearBatch();
    }

    public void writeSundaramDB(ArrayList<TransactionMainBO> arryTransactionMainBO) throws Exception {
        Connection connectionObject = null;
        PreparedStatement SundaramDBInsert = null;
        WIFSSQLCon wifsSQLCon = new WIFSSQLCon();
        String remarksCAMS = "";
        String paidThrough = "";

        int intMainBORowSize = 0;
        String webLog = "";
        TransactionMainBO objTransactionMainBO = new TransactionMainBO();
        InvestorDetailsBO objInvestorDetailsBO = new InvestorDetailsBO();
        AccountDetailsBO objAccountDetailsBO = new AccountDetailsBO();
        TransactionBO objTransactionBO = new TransactionBO();

        intMainBORowSize = arryTransactionMainBO.size();
        connectionObject = wifsSQLCon.getWifsSQLConnect();

        String sqlQuery_ITT_Update = "";
        SundaramDBInsert = connectionObject.prepareStatement(sqlQuery_ITT_Update);

        //logger.info(intMainBORowSize+" ======= ");
        for (int DBFRow = 0; DBFRow < intMainBORowSize; DBFRow++) {
            try {
                objTransactionMainBO = arryTransactionMainBO.get(DBFRow);

                if (objTransactionMainBO != null) {
                    objTransactionBO = objTransactionMainBO.getTransactionBO();
                    objTransactionBO = objTransactionMainBO.getTransactionBO();
                    objInvestorDetailsBO = objTransactionMainBO.getInvestorDetailsBO();
                    objAccountDetailsBO = objTransactionMainBO.getAccountDetailsBO();
                    //logger.info("Sundram file Row ::::::: "+(DBFRow+1)+" UserTransRefId "+objTransactionBO.getUserTransactionNo());

                    /*
					 	for Sundram remarks
						PGID HDFC    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID TPSL    |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID DD       |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID ECS     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						3262 NEFT   |25052010$13:41:01$119.82.100.2$72.32.69.244$32323
						PGID SW     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
						PGID Red     |25052010$13:41:01$119.82.100.2$72.32.69.244$32323 
                     */
                    paidThrough = objTransactionBO.getPaidThru();
                    if (paidThrough.equalsIgnoreCase("NEFT")) {
                        remarksCAMS = objTransactionBO.getPaymentId() + " " + paidThrough + "    |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                    } else if ((paidThrough.equals("")) && (objTransactionBO.getTransactionType().equals("SI") || objTransactionBO.getTransactionType().equals("SO"))) {
                        remarksCAMS = "66 SW" + "        |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$72.32.69.244$72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                    } else if (paidThrough.equals("") && (objTransactionBO.getTransactionType().equals("R"))) {
                        remarksCAMS = "77 Red" + "       |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$72.32.69.244$72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                    } else if (paidThrough.equalsIgnoreCase("DD")) {
                        remarksCAMS = objTransactionBO.getPaymentId() + " DD      |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                    } else if (paidThrough.equalsIgnoreCase("ECS")) {
                        remarksCAMS = objTransactionBO.getPaymentId() + " ECS     |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                    } else {
                        remarksCAMS = objTransactionBO.getPaymentId() + " " + paidThrough + "    |" + objTransactionBO.getTransactionDate().getDateMonthView().replace("-", "") + "$" + objTransactionBO.getTransactionTime() + "$" + objTransactionBO.getIPAddress() + "$" + "72.32.69.244$" + objTransactionBO.getUserTransactionNo();
                    }

                    SundaramDBInsert.setString(0, objTransactionBO.getAmcCode()); //AMCCode
                    SundaramDBInsert.setString(1, "WEALTH");//objTransactionBO.getBrokerCode()); // Broker Code
                    SundaramDBInsert.setString(2, objInvestorDetailsBO.getSubBrokerCode()); // Subbroker Code
                    SundaramDBInsert.setString(3, objInvestorDetailsBO.getUserCode()); // User code
                    SundaramDBInsert.setDouble(4, new Double(objTransactionBO.getUserTransactionNo())); // Transaction No
                    SundaramDBInsert.setString(5, objTransactionBO.getApplNo()); // Application No
                    SundaramDBInsert.setString(6, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_FolioNo())); // Folio No				
                    SundaramDBInsert.setString(7, ConstansBO.NullCheck(objTransactionBO.getCk_Dig_No())); // Cheque degit No
                    SundaramDBInsert.setString(8, objTransactionBO.getTransactionType());
                    SundaramDBInsert.setString(9, objTransactionBO.getSchemeCode());
                    SundaramDBInsert.setString(10, objInvestorDetailsBO.getFirstName());
                    SundaramDBInsert.setString(11, objInvestorDetailsBO.getJointName1());
                    SundaramDBInsert.setString(12, objInvestorDetailsBO.getJointName2());
                    SundaramDBInsert.setString(13, objInvestorDetailsBO.getAddress1());
                    SundaramDBInsert.setString(14, objInvestorDetailsBO.getAddress2());
                    SundaramDBInsert.setString(15, objInvestorDetailsBO.getAddress3());
                    SundaramDBInsert.setString(16, objInvestorDetailsBO.getCity());
                    SundaramDBInsert.setString(17, objInvestorDetailsBO.getPinCode());
                    SundaramDBInsert.setString(18, objInvestorDetailsBO.getPhoneOffice());
                    SundaramDBInsert.setDate(19, (java.sql.Date) objTransactionBO.getTransactionDate().getDateFirstView());
                    SundaramDBInsert.setString(20, objTransactionBO.getTransactionTime());
                    //logger.info("Data Writer : "+(objAccountDetailsBO.getClos_Ac_Ch().equals("Y"))+" == "+objAccountDetailsBO.getClos_Ac_Ch()));
                    if (objAccountDetailsBO.getClos_Ac_Ch().equals("Y")) {
                        SundaramDBInsert.setDouble(21, new Double(0.0d));
                        SundaramDBInsert.setDouble(22, new Double(0.0d));
                    } else {
                        SundaramDBInsert.setDouble(21, objTransactionBO.getUnits());
                        SundaramDBInsert.setDouble(22, objTransactionBO.getAmount().doubleValue());
                    }
                    SundaramDBInsert.setString(23, objAccountDetailsBO.getClos_Ac_Ch());
                    SundaramDBInsert.setDate(24, (java.sql.Date) objInvestorDetailsBO.getDateOfBirth().getDateFirstView());
                    SundaramDBInsert.setString(25, objInvestorDetailsBO.getGuardianName());
                    SundaramDBInsert.setString(26, objInvestorDetailsBO.getTaxNo());
                    SundaramDBInsert.setString(27, objInvestorDetailsBO.getPhoneRes());
                    SundaramDBInsert.setString(28, objInvestorDetailsBO.getFaxOff());
                    SundaramDBInsert.setString(29, objInvestorDetailsBO.getFaxRes());
                    SundaramDBInsert.setString(30, objInvestorDetailsBO.getEmail());
                    SundaramDBInsert.setString(31, objAccountDetailsBO.getAccountNo());
                    SundaramDBInsert.setString(32, objAccountDetailsBO.getAccountType());
                    SundaramDBInsert.setString(33, objAccountDetailsBO.getBankName());
                    SundaramDBInsert.setString(34, objAccountDetailsBO.getBranchName());
                    SundaramDBInsert.setString(35, objAccountDetailsBO.getBankCity());
                    SundaramDBInsert.setString(36, objAccountDetailsBO.getReinv_Tag());
                    SundaramDBInsert.setString(37, objAccountDetailsBO.getHoldingNature());
                    SundaramDBInsert.setString(38, objInvestorDetailsBO.getOccupationCode());
                    SundaramDBInsert.setString(39, objAccountDetailsBO.getTaxStatus());

                    // ---- New Fields ----
                    SundaramDBInsert.setString(40, remarksCAMS);

                    SundaramDBInsert.setString(41, objInvestorDetailsBO.getState());
                    SundaramDBInsert.setString(42, objTransactionBO.getSubTransactionType());
                    SundaramDBInsert.setString(43, objAccountDetailsBO.getPaymentMechanism());

                    if (!objAccountDetailsBO.getMICR_CD().equals("")) {
                        SundaramDBInsert.setDouble(44, new Double(objAccountDetailsBO.getMICR_CD()));
                    }
                    SundaramDBInsert.setString(45, objAccountDetailsBO.getBank_Code());

                    if (!ConstansBO.NullCheck(objAccountDetailsBO.getALT_Folio()).equals("")) {
                        SundaramDBInsert.setDouble(46, new Double(objAccountDetailsBO.getALT_Folio()));
                    }

                    SundaramDBInsert.setString(47, objAccountDetailsBO.getALT_Broker());
                    SundaramDBInsert.setString(48, objInvestorDetailsBO.getLocationCode());
                    SundaramDBInsert.setString(49, objAccountDetailsBO.getPaymentMechanism());
                    SundaramDBInsert.setString(50, objTransactionBO.getPricing());
                    SundaramDBInsert.setString(51, objInvestorDetailsBO.getPanHolder2());
                    SundaramDBInsert.setString(52, objInvestorDetailsBO.getPanHolder3());
                    SundaramDBInsert.setString(53, objAccountDetailsBO.getNomineeName());
                    SundaramDBInsert.setString(54, objAccountDetailsBO.getNomineeRelation());
                    SundaramDBInsert.setString(55, objInvestorDetailsBO.getGuardianPanNo());
                    SundaramDBInsert.setString(56, objAccountDetailsBO.getInstrmno());
                    SundaramDBInsert.setString(57, objAccountDetailsBO.getUINNo());
                    SundaramDBInsert.setString(58, objInvestorDetailsBO.getValid_Pan());
                    SundaramDBInsert.setString(59, objInvestorDetailsBO.getGValid_Pan());
                    SundaramDBInsert.setString(60, objInvestorDetailsBO.getJH1ValidPan());
                    SundaramDBInsert.setString(61, objInvestorDetailsBO.getJH2ValidPan());
					
                    if (objInvestorDetailsBO.getDateOfBirth2() != null) {
                        SundaramDBInsert.setDate(62, (java.sql.Date) objInvestorDetailsBO.getDateOfBirth2().getDateFirstView());
                    }

                    if (objInvestorDetailsBO.getDateOfBirth3() != null) {
                        SundaramDBInsert.setDate(63, (java.sql.Date) objInvestorDetailsBO.getDateOfBirth3().getDateFirstView());
                    }

                    if (objInvestorDetailsBO.getDateOfBirthG() != null) {
                        SundaramDBInsert.setDate(64, (java.sql.Date) objInvestorDetailsBO.getDateOfBirthG().getDateFirstView());
                    }

                    SundaramDBInsert.setString(65, "");//objAccountDetailsBO.getGF6061RE());
                    SundaramDBInsert.setString(66, "");//objAccountDetailsBO.getSipReferenceNo());
                    if (objTransactionBO.getSIP_Reg_Date() != null) {
                        SundaramDBInsert.setDate(67, (java.sql.Date) objTransactionBO.getSIP_Reg_Date().getDateFirstView());
                    }
                    SundaramDBInsert.setString(68, objAccountDetailsBO.getFirst_Hldr_Min());
                    SundaramDBInsert.setString(69, objAccountDetailsBO.getJH1_Min());
                    SundaramDBInsert.setString(70, objAccountDetailsBO.getJH2_Min());
                    SundaramDBInsert.setString(71, objAccountDetailsBO.getGuardian_Min());
                    SundaramDBInsert.setString(72, objAccountDetailsBO.getNEFT_CD());
                    SundaramDBInsert.setString(73, objAccountDetailsBO.getRTGS_CD());
                    SundaramDBInsert.setString(74, "E");//EMAIL_ACST
                    SundaramDBInsert.setString(75, objInvestorDetailsBO.getMobileNo());
                    SundaramDBInsert.setString(76, objTransactionBO.getDpId()); // Dip_Id
                    // ---- New Fields ----
                    SundaramDBInsert.setString(77, "N"); // POA_Type
                    SundaramDBInsert.setString(78, "W"); // Trxn Mode
                    SundaramDBInsert.setString(79, "Y"); // Trxn_sign_ver

                    if (objInvestorDetailsBO.getNRIInvestor() == 1)// For NRI Investor send NRI Addr. detials
                    {
                        SundaramDBInsert.setString(80, "NRI Ad: " + objInvestorDetailsBO.getNRIAddress1());
                        SundaramDBInsert.setString(81, objInvestorDetailsBO.getNRIAddress2());
                        SundaramDBInsert.setString(82, objInvestorDetailsBO.getNRIAddress3());

                        if (!objInvestorDetailsBO.getNRICity().equals(objInvestorDetailsBO.getNRIState())) {
                            SundaramDBInsert.setString(83, objInvestorDetailsBO.getNRICity() + " " + objInvestorDetailsBO.getNRIState());
                        } else {
                            SundaramDBInsert.setString(83, objInvestorDetailsBO.getNRICity());
                        }

                        SundaramDBInsert.setString(84, "");//objInvestorDetailsBO.getNRIState());
                        SundaramDBInsert.setString(85, objInvestorDetailsBO.getNRICountry());
                        SundaramDBInsert.setString(86, objInvestorDetailsBO.getPinCode());
                        SundaramDBInsert.setString(88, ""); // Nom2_Name
                        SundaramDBInsert.setString(89, ""); // Nom2_Relationship
                        //SundaramDBInsert.setString(90, new Double(0.0d)); // Nom2_Applicable -  Number -  5,2
                        SundaramDBInsert.setString(91, ""); // Nom3_Name
                        SundaramDBInsert.setString(92, ""); // Nom3_Relationship
                        //SundaramDBInsert.setString(93, new Double(0.0d)); // Nom3_Applicable -  Number -  5,2
                        SundaramDBInsert.setString(94, "Y"); // Check_Flag
                        SundaramDBInsert.setString(97, "Y"); // FIRC_STA 

                    } else {
                        SundaramDBInsert.setString(94, "N"); // Check_Flag
                    }
                    if (objAccountDetailsBO.getNomineePercent() > 0) // if Nominee is available then 100% goes to that nominee
                    {
                        SundaramDBInsert.setDouble(87, new Double(objAccountDetailsBO.getNomineePercent())); //Nom1_Applicable -	Number -  5,0
                    }
                    if (objTransactionBO.getTransactionType().equals("P")) {
                        SundaramDBInsert.setString(95, "N"); // TH_P_PYT ( Third Party Payment)
                        SundaramDBInsert.setString(96, objInvestorDetailsBO.getKycFlag()); // KYC
                    } else {
                        SundaramDBInsert.setString(95, ""); // TH_P_PYT ( Third Party Payment)
                        SundaramDBInsert.setString(96, objInvestorDetailsBO.getKycFlag()); // KYC
                        SundaramDBInsert.setString(97, ""); // FIRC_STA 
                    }

                    if ((objTransactionBO.getSipStartDate() != null) && (objTransactionBO.getSipEndDate() != null)) {
                        SundaramDBInsert.setDouble(98, new Double(objTransactionBO.getSipReferenceId()));
                        SundaramDBInsert.setDouble(99, new Double(objTransactionBO.getNoOfInstalment()));
                        SundaramDBInsert.setString(100, objTransactionBO.getSipFrequency());
                        SundaramDBInsert.setDate(101, (java.sql.Date) objTransactionBO.getSipStartDate().getDateFirstView());
                        SundaramDBInsert.setDate(102, (java.sql.Date) objTransactionBO.getSipEndDate().getDateFirstView());
                        SundaramDBInsert.setDouble(103, new Double(objTransactionBO.getPaidInstallments()));
                    }

                    SundaramDBInsert.setString(107, objInvestorDetailsBO.getKycFlag2());
                    SundaramDBInsert.setString(110, objInvestorDetailsBO.getKycFlag3());
                    SundaramDBInsert.setString(111, objInvestorDetailsBO.getKycFlagG());

                    SundaramDBInsert.setString(113, "N"); // First Holder PAN Exempt
                    SundaramDBInsert.setString(114, "N"); // JH1 PAN Exempt
                    SundaramDBInsert.setString(115, "N"); // JH2 PAN Exempt
                    SundaramDBInsert.setString(116, "N"); // Guardian PAN Exempt

                    SundaramDBInsert.setString(125, objTransactionBO.getEuinOpted());
                    SundaramDBInsert.setString(126, objTransactionBO.getEuinId());
                    //Nomination not opted
                    SundaramDBInsert.setString(128, "WEALTH");

                    if (!objInvestorDetailsBO.getcKYCRefId1().equals("")) {
                        SundaramDBInsert.setDouble(136, new Double(objInvestorDetailsBO.getcKYCRefId1()));
                    }

                    if (!objInvestorDetailsBO.getcKYCRefId2().equals("")) {
                        SundaramDBInsert.setDouble(137, new Double(objInvestorDetailsBO.getcKYCRefId2()));
                    }

                    if (!objInvestorDetailsBO.getcKYCRefId3().equals("")) {
                        SundaramDBInsert.setDouble(138, new Double(objInvestorDetailsBO.getcKYCRefId3()));
                    }

                    if (!objInvestorDetailsBO.getcKYCRefIdG().equals("")) {
                        SundaramDBInsert.setDouble(139, new Double(objInvestorDetailsBO.getcKYCRefIdG()));
                    }

                    if (objInvestorDetailsBO.getKycFlag().equals("C")) {
                        SundaramDBInsert.setString(140, "N");
                    } else {
                        SundaramDBInsert.setString(140, "Y");
                    }

                    if (objInvestorDetailsBO.getKycFlag2().equals("C")) {
                        SundaramDBInsert.setString(141, "N");
                    } else {
                        SundaramDBInsert.setString(141, "Y");
                    }

                    if (objInvestorDetailsBO.getKycFlag3().equals("C")) {
                        SundaramDBInsert.setString(142, "N");
                    } else {
                        SundaramDBInsert.setString(142, "Y");
                    }

                    if (objInvestorDetailsBO.getKycFlagG().equals("C")) {
                        SundaramDBInsert.setString(143, "N");
                    } else {
                        SundaramDBInsert.setString(143, "Y");
                    }


                    //Based on the UserTransactionNo Update the ForwardFeed - date
                    SundaramDBInsert.addBatch();
                }
            } catch (Exception e) {
                logger.info("Exp while file writing.. row:" + DBFRow + " exp:" + e);
            }
        }
        int insertedRows[] = SundaramDBInsert.executeBatch();
        logger.info("AMC:" + objTransactionBO.getAmcCode() + " Rows:" + insertedRows.length);
        SundaramDBInsert.clearBatch();
    }
    
    public Hashtable<String, ArrayList<TransactionMainBO>> getFinalForwardFeedListCAMS(Connection objConnection) throws SQLException {
        ArrayList<String> transactionDetailsList = new ArrayList<>();
        Hashtable<String, ArrayList<TransactionMainBO>> finalFeedHash = new Hashtable<>();
        ArrayList<TransactionMainBO> arryTransactionMainBO = new ArrayList<TransactionMainBO>();
        PreparedStatement psAMC = null;
        PreparedStatement psFeedList = null;
        ArrayList<String> AMC = new ArrayList<String>();
        ResultSet rsFeedList = null;
        String todayDate = "";

        try {
            ConstansBO objConstansBO = new ConstansBO("");
            todayDate = objConstansBO.getTodayDate("yyyy-MM-dd");
            String sql = "Select distinct AMC_Code from CAMS_Forward_Feed where Created_Date = '"+todayDate+"'";
            psFeedList = objConnection.prepareStatement(sql);
            rsFeedList = psFeedList.executeQuery();
            while (rsFeedList.next()) {
                AMC.add(rsFeedList.getString("AMC_Code"));
            }
        }catch (Exception ex){
            logger.info(ex);
        }
        for (String AMC_Code : AMC){
                ResultSet rs = null;
                String sqlFullList = "Select AMC_CODE	,\n"
                    + "                     BROKE_CD	,\n"
                    + "                     SBBR_CODE	,\n"
                    + "                     User_Code	,\n"
                    + "                     USR_TXN_NO	,\n"
                    + "                     Appl_No	,\n"
                    + "                     FOLIO_NO	,\n"
                    + "                     Ck_DIG_NO	,\n"
                    + "                     TRXN_TYPE	,\n"
                    + "                     SCH_CODE	,\n"
                    + "                     FIRST_NAME	,\n"
                    + "                     JONT_NAME1	,\n"
                    + "                     JONT_NAME2	,\n"
                    + "                     ADD1	,\n"
                    + "                     ADD2	,\n"
                    + "                     ADD3	,\n"
                    + "                     CITY	,\n"
                    + "                     PINCODE	,\n"
                    + "                     PHONE_OFF	,\n"
                    + "                     TRXN_DATE	,\n"
                    + "                     TRXN_TIME	,\n"
                    + "                     UNITS	,\n"
                    + "                     AMOUNT	,\n"
                    + "                     CLOS_AC_CH	,\n"
                    + "                     DOB	,\n"
                    + "                     GUARDIAN	,\n"
                    + "                     TAX_NUMBER	,\n"
                    + "                     PHONE_RES	,\n"
                    + "                     FAX_OFF	,\n"
                    + "                     FAX_RES	,\n"
                    + "                     EMAIL	,\n"
                    + "                     ACCT_NO	,\n"
                    + "                     ACCT_TYPE	,\n"
                    + "                     BANK_NAME	,\n"
                    + "                     BR_NAME	,\n"
                    + "                     BANK_CITY	,\n"
                    + "                     REINV_TAG	,\n"
                    + "                     HOLD_NATUR	,\n"
                    + "                     OCC_CODE	,\n"
                    + "                     TAX_STATUS	,\n"
                    + "                     REMARKS	,\n"
                    + "                     STATE	,\n"
                    + "                     SUB_TRXN_T	,\n"
                    + "                     DIV_PY_MEC	,\n"
                    + "                     ECS_NO	,\n"
                    + "                     BANK_CODE	,\n"
                    + "                     ALT_FOLIO	,\n"
                    + "                     ALT_BROKER	,\n"
                    + "                     LOCATION	,\n"
                    + "                     PAY_MEC	,\n"
                    + "                     PRICING	,\n"
                    + "                     PAN_2_HLDR	,\n"
                    + "                     PAN_3_HLDR	,\n"
                    + "                     NOM_NAME	,\n"
                    + "                     NOM_RELA	,\n"
                    + "                     Guard_PAN	,\n"
                    + "                     INSTRMNO	,\n"
                    + "                     UINno	,\n"
                    + "                     VALID_PAN	,\n"
                    + "                     GVALID_PAN	,\n"
                    + "                     JH1VALPAN	,\n"
                    + "                     JH2VALPAN	,\n"
                    + "                     SIP_REG_DATE	,\n"
                    + "                     FIRST_HDLR_MIN	,\n"
                    + "                     JH1_MIN	,\n"
                    + "                     JH2_MIN	,\n"
                    + "                     GUARDIAN_MIN	,\n"
                    + "                     NEFT_CD	,\n"
                    + "                     RTGS_CD	,\n"
                    + "                     EMAIL_ACST	,\n"
                    + "                     MOBILE_NO	,\n"
                    + "                     DP_ID	,\n"
                    + "                     POA_type	,\n"
                    + "                     Trxn_Mode	,\n"
                    + "                     Trxn_Sign_Confn	,\n"
                    + "                     Addl_Addr1	,\n"
                    + "                     Addl_Addr2	,\n"
                    + "                     Addl_Addr3	,\n"
                    + "                     CITY_NRI	,\n"
                    + "                     Country	,\n"
                    + "                     PIN_NRI,\n"
                    + "					 Check_Flag,\n"
                    + "                     FIRC_STA	,\n"
                    + "                     Nom1_Applicable,\n"
                    + "					 TH_P_PYT,\n"
                    + "                     KYC	,\n"
                    + "					 SIP_RFNO	,\n"
                    + "                     NO_INST	,\n"
                    + "                     SIP_FEQUENCY	,\n"
                    + "                     ST_DATE	,\n"
                    + "                     END_DATE	,\n"
                    + "                     INSTL_NO,\n"
                    + "					 PAN1_Exempt	,\n"
                    + "                     JH1_VALID_PAN	,\n"
                    + "                     JH2_VALID_PAN	,\n"
                    + "                     GPAN_Exempt,\n"
                    + "					 EUIN_OPT	,\n"
                    + "                     EUIN	,\n"
                    + "                     Nom_not_opted	,\n"
                    + "                     SUB_BRCD	,\n"
                    + "					 BankMandateProof	,\n"
                    + "                     FHLD_CKYC	,\n"
                    + "                     SHLD_CKYC	,\n"
                    + "                     THLD_CKYC	,\n"
                    + "                     GURD_CKYC	,\n"
                    + "                     JH1_DOB	,\n"
                    + "                     JH2_DOB	,\n"
                    + "                     GUARDN_DOB, STATE_NRI,Nom2_Name,Nom2_Relationship,Nom3_Name,Nom3_Relationship  from CAMS_Forward_Feed where AMC_CODE = ('" + AMC_Code+"') and created_date = '"+todayDate+"'";
                psAMC = objConnection.prepareStatement(sqlFullList);
                rs = psAMC.executeQuery();
                try{
                while (rs.next()) {
                    TransactionMainBO objTransactionMainBO = new TransactionMainBO();
                    InvestorDetailsBO objInvestorDetailsBO = new InvestorDetailsBO();
                    AccountDetailsBO objAccountDetailsBO = new AccountDetailsBO();
                    TransactionBO objTransactionBO = new TransactionBO();
                    objTransactionBO.setAmcCode(isNull(rs.getString("AMC_CODE")));//AMCCode
                    objInvestorDetailsBO.setSubBrokerCode(isNull(rs.getString("SBBR_CODE"))); // Subbroker Code
                    objInvestorDetailsBO.setUserCode(isNull(rs.getString("User_Code"))); // User code
                    objTransactionBO.setUserTransactionNo(isNull(rs.getString("USR_TXN_NO"))); // Transaction No
                    objTransactionBO.setApplNo(isNull(rs.getString("Appl_No"))); // Application No
                    objTransactionBO.setCk_Dig_FolioNo(isNull(rs.getString("FOLIO_NO"))); // Folio No				
                    objTransactionBO.setCk_Dig_No(isNull(rs.getString("Ck_DIG_NO"))); // Cheque degit No
                    objTransactionBO.setTransactionType(isNull(rs.getString("TRXN_TYPE")));
                    objTransactionBO.setSchemeCode(isNull(rs.getString("SCH_CODE")));
                    objInvestorDetailsBO.setFirstName(isNull(rs.getString("FIRST_NAME")));
                    objInvestorDetailsBO.setJointName1(isNull(rs.getString("JONT_NAME1")));
                    objInvestorDetailsBO.setJointName2(isNull(rs.getString("JONT_NAME2")));
                    objInvestorDetailsBO.setAddress1(isNull(rs.getString("ADD1")));
                    objInvestorDetailsBO.setAddress2(isNull(rs.getString("ADD2")));
                    objInvestorDetailsBO.setAddress3(isNull(rs.getString("ADD3")));
                    objInvestorDetailsBO.setCity(isNull(rs.getString("CITY")));
                    objInvestorDetailsBO.setPinCode(isNull(rs.getString("PINCODE")));
                    objInvestorDetailsBO.setPhoneOffice(isNull(rs.getString("PHONE_OFF")));
                    ConstansBO TRXN_DATE = new ConstansBO(String.valueOf(rs.getDate("TRXN_DATE")));
                    objTransactionBO.setTransactionDate(TRXN_DATE);
                    objTransactionBO.setTransactionTime(isNull(rs.getString("TRXN_TIME")));
                    objTransactionBO.setUnits(rs.getDouble("UNITS"));
                    objTransactionBO.setAmount(BigDecimal.valueOf(rs.getDouble("AMOUNT")));
                    objAccountDetailsBO.setClos_Ac_Ch(isNull(rs.getString("CLOS_AC_CH")));
                    ConstansBO DOB = new ConstansBO(String.valueOf(rs.getDate("DOB")));
                    objInvestorDetailsBO.setDateOfBirth(DOB);
                    objInvestorDetailsBO.setGuardianName(isNull(rs.getString("GUARDIAN")));
                    objInvestorDetailsBO.setTaxNo(isNull(rs.getString("TAX_NUMBER")));
                    objInvestorDetailsBO.setPhoneRes(isNull(rs.getString("PHONE_RES")));
                    objInvestorDetailsBO.setFaxOff(isNull(rs.getString("FAX_OFF")));
                    objInvestorDetailsBO.setFaxRes(isNull(rs.getString("FAX_RES")));
                    objInvestorDetailsBO.setEmail(isNull(rs.getString("EMAIL")));
                    objAccountDetailsBO.setAccountNo(isNull(rs.getString("ACCT_NO")));
                    objAccountDetailsBO.setAccountType(isNull(rs.getString("ACCT_TYPE")));
                    objAccountDetailsBO.setBankName(isNull(rs.getString("BANK_NAME")));
                    objAccountDetailsBO.setBranchName(isNull(rs.getString("BR_NAME")));
                    objAccountDetailsBO.setBankCity(isNull(rs.getString("BANK_CITY")));
                    objAccountDetailsBO.setReinv_Tag(isNull(rs.getString("REINV_TAG")));
                    objAccountDetailsBO.setHoldingNature(isNull(rs.getString("HOLD_NATUR")));
                    objInvestorDetailsBO.setOccupationCode(isNull(rs.getString("OCC_CODE")));
                    objAccountDetailsBO.setTaxStatus(isNull(rs.getString("TAX_STATUS")));
                    objAccountDetailsBO.setRemarks(isNull(rs.getString("REMARKS")));
                    objInvestorDetailsBO.setState(isNull(rs.getString("STATE")));
                    objTransactionBO.setSubTransactionType(isNull(rs.getString("SUB_TRXN_T")));
                    objAccountDetailsBO.setPaymentMechanism(isNull(rs.getString("DIV_PY_MEC")));
                    objAccountDetailsBO.setMICR_CD(isNull(rs.getString(String.valueOf("ECS_NO"))));
                    objAccountDetailsBO.setBank_Code(isNull(rs.getString("BANK_CODE")));
                    objAccountDetailsBO.setALT_Folio(isNull(rs.getString(String.valueOf("ALT_FOLIO"))));
                    objAccountDetailsBO.setALT_Broker(isNull(rs.getString("ALT_BROKER")));
                    objInvestorDetailsBO.setLocationCode(isNull(rs.getString("LOCATION")));
                    objAccountDetailsBO.setPaymentMechanism(isNull(rs.getString("PAY_MEC")));
                    objTransactionBO.setPricing(isNull(rs.getString("PRICING")));
                    objInvestorDetailsBO.setPanHolder2(isNull(rs.getString("PAN_2_HLDR")));
                    objInvestorDetailsBO.setPanHolder3(isNull(rs.getString("PAN_3_HLDR")));
                    objAccountDetailsBO.setNomineeName(isNull(rs.getString("NOM_NAME")));
                    objAccountDetailsBO.setNomineeRelation(isNull(rs.getString("NOM_RELA")));
                    objInvestorDetailsBO.setGuardianPanNo(isNull(rs.getString("Guard_PAN")));
                    objAccountDetailsBO.setInstrmno(isNull(rs.getString("INSTRMNO")));
                    objAccountDetailsBO.setUINNo(isNull(rs.getString("UINno")));
                    objInvestorDetailsBO.setValid_Pan(isNull(rs.getString("VALID_PAN")));
                    objInvestorDetailsBO.setGValid_Pan(isNull(rs.getString("GVALID_PAN")));
                    objInvestorDetailsBO.setJH1ValidPan(isNull(rs.getString("JH1VALPAN")));
                    objInvestorDetailsBO.setJH2ValidPan(isNull(rs.getString("JH2VALPAN")));
                    ConstansBO SIP_REG_DATE = new ConstansBO(isNull(rs.getString("SIP_REG_DATE")));
                    objTransactionBO.setSIP_Reg_Date(SIP_REG_DATE);
                    objAccountDetailsBO.setFirst_Hldr_Min(isNull(rs.getString("FIRST_HDLR_MIN")));
                    objAccountDetailsBO.setJH1_Min(isNull(rs.getString("JH1_MIN")));
                    objAccountDetailsBO.setJH2_Min(isNull(rs.getString("JH2_MIN")));
                    objAccountDetailsBO.setGuardian_Min(isNull(rs.getString("GUARDIAN_MIN")));
                    objAccountDetailsBO.setNEFT_CD(isNull(rs.getString("NEFT_CD")));
                    objAccountDetailsBO.setRTGS_CD(isNull(rs.getString("RTGS_CD")));
                    objInvestorDetailsBO.setMobileNo(isNull(rs.getString("MOBILE_NO")));
                    objTransactionBO.setDpId(isNull(rs.getString("DP_ID"))); // Dip_Id
                    objInvestorDetailsBO.setNRIAddress1(isNull(rs.getString("Addl_Addr1")));
                    objInvestorDetailsBO.setNRIAddress2(isNull(rs.getString("Addl_Addr2")));
                    objInvestorDetailsBO.setNRIAddress3(isNull(rs.getString("Addl_Addr3")));
                    objInvestorDetailsBO.setNRICity(isNull(rs.getString("CITY_NRI")));
                    objInvestorDetailsBO.setNRIState(isNull(rs.getString("STATE_NRI")));
                    objInvestorDetailsBO.setNRICountry(isNull(rs.getString("Country")));
                    objInvestorDetailsBO.setPinCode(isNull(rs.getString("PIN_NRI")));
                    objAccountDetailsBO.setNomineeName2(isNull(rs.getString("Nom2_Name")));// Nom2_Name
                    objAccountDetailsBO.setNomineeRelation2(isNull(rs.getString("Nom2_Relationship")));// Nom2_Relationship
                    objAccountDetailsBO.setNomineeName3(isNull(rs.getString("Nom3_Name")));// Nom3_Name
                    objAccountDetailsBO.setNomineeRelation3(isNull(rs.getString("Nom3_Relationship")));// Nom3_Relationship
                    objAccountDetailsBO.setNomineePercent(rs.getInt("Nom1_Applicable")); //Nom1_Applicable -	Number -  5,0
                    objInvestorDetailsBO.setKycFlag(isNull(rs.getString("KYC"))); // KYC
                    objTransactionBO.setSipReferenceId(isNull(rs.getString("SIP_RFNO")));
                    objTransactionBO.setNoOfInstalment(isNull(rs.getString("NO_INST")));
                    objTransactionBO.setSipFrequency(isNull(rs.getString("SIP_FEQUENCY")));
                    ConstansBO ST_DATE = new ConstansBO(String.valueOf(rs.getDate("ST_DATE")));
                    objTransactionBO.setSipStartDate(ST_DATE);
                    ConstansBO END_DATE = new ConstansBO(String.valueOf(isNull(rs.getString("END_DATE"))));
                    objTransactionBO.setSipEndDate(END_DATE);
                    objTransactionBO.setPaidInstallments(isNull(rs.getString("INSTL_NO")));
                    objTransactionBO.setEuinOpted(isNull(rs.getString("EUIN_OPT")));
                    objTransactionBO.setEuinId(isNull(rs.getString("EUIN")));
                    objAccountDetailsBO.setNomineeOpted(isNull(rs.getString("Nom_not_opted")));
                    objInvestorDetailsBO.setSubBrokerCode(isNull(rs.getString("SUB_BRCD"))); // Subbroker Code
                    objInvestorDetailsBO.setcKYCRefId1(isNull(rs.getString("FHLD_CKYC")));//Dummy4
                    objInvestorDetailsBO.setcKYCRefId2(isNull(rs.getString("SHLD_CKYC")));//Dummy5
                    objInvestorDetailsBO.setcKYCRefId3(isNull(rs.getString("THLD_CKYC")));//Dummy6
                    objInvestorDetailsBO.setcKYCRefIdG(isNull(rs.getString("GURD_CKYC")));//Dummy7
                    ConstansBO JH1_DOB = new ConstansBO(String.valueOf(isNull(rs.getString("JH1_DOB"))));
                    ConstansBO JH2_DOB = new ConstansBO(String.valueOf(isNull(rs.getString("JH2_DOB"))));
                    ConstansBO GUARDN_DOB = new ConstansBO(String.valueOf(isNull(rs.getString("GUARDN_DOB"))));
                    objInvestorDetailsBO.setDateOfBirth2(JH1_DOB);//Dummy8
                    objInvestorDetailsBO.setDateOfBirth3(JH2_DOB);//Dummy9
                    objInvestorDetailsBO.setDateOfBirthG(GUARDN_DOB);
                    objTransactionMainBO.setAccountDetailsBO(objAccountDetailsBO);
                    objTransactionMainBO.setInvestorDetailsBO(objInvestorDetailsBO);
                    objTransactionMainBO.setTransactionBO(objTransactionBO);
                    arryTransactionMainBO.add(objTransactionMainBO);
                }
                } catch(Exception ex){
                    logger.info(ex);
                    continue;
                }
                
                finalFeedHash.put("1@" + AMC_Code, arryTransactionMainBO);
                arryTransactionMainBO = new ArrayList<>();
            }
        logger.info(finalFeedHash.size());
        return finalFeedHash;
    }

    public Hashtable<String, ArrayList<TransactionMainBO>> getFinalForwardFeedListKarvy(Connection objConnection) throws SQLException {
        ArrayList<String> transactionDetailsList = new ArrayList<>();
        Hashtable<String, ArrayList<TransactionMainBO>> finalFeedHash = new Hashtable<>();
        ArrayList<TransactionMainBO> arryTransactionMainBO = new ArrayList<TransactionMainBO>();
        PreparedStatement psAMC = null;
        PreparedStatement psFeedList = null;
        ResultSet rs = null;
        ResultSet rsFeedList = null;

        try {
            String sql = "Select distinct AMC_Code from Karvy_Forward_Feed";
            psAMC = objConnection.prepareStatement(sql);
            rs = psAMC.executeQuery();
            while (rs.next()) {
                String sqlFullList = "Select * from CAMS_Forward_Feed where AMC_Code = " + rs.getString("AMC_Code");
                psFeedList = objConnection.prepareStatement(sqlFullList);
                rsFeedList = psFeedList.executeQuery();
                while (rsFeedList.next()) {
                    TransactionMainBO objTransactionMainBO = new TransactionMainBO();
                    InvestorDetailsBO objInvestorDetailsBO = new InvestorDetailsBO();
                    AccountDetailsBO objAccountDetailsBO = new AccountDetailsBO();
                    TransactionBO objTransactionBO = new TransactionBO();
                    objTransactionBO.setAmcCode(rs.getString("AMC_CODE"));//AMCCode
                    objInvestorDetailsBO.setSubBrokerCode(rs.getString("SBBR_CODE")); // Subbroker Code
                    objInvestorDetailsBO.setUserCode(rs.getString("User_Code")); // User code
                    objTransactionBO.setUserTransactionNo(rs.getString("USR_TXN_NO")); // Transaction No
                    objTransactionBO.setApplNo(rs.getString("Appl_No")); // Application No
                    objTransactionBO.setCk_Dig_FolioNo(rs.getString("FOLIO_NO")); // Folio No				
                    objTransactionBO.setCk_Dig_No(rs.getString("Ck_DIG_NO")); // Cheque degit No
                    objTransactionBO.setTransactionType(rs.getString("TRXN_TYPE"));
                    objTransactionBO.setSchemeCode(rs.getString("SCH_CODE"));
                    objInvestorDetailsBO.setFirstName(rs.getString("FIRST_NAME"));
                    objInvestorDetailsBO.setJointName1(rs.getString("JONT_NAME1"));
                    objInvestorDetailsBO.setJointName2(rs.getString("JONT_NAME2"));
                    objInvestorDetailsBO.setAddress1(rs.getString("ADD1"));
                    objInvestorDetailsBO.setAddress2(rs.getString("ADD2"));
                    objInvestorDetailsBO.setAddress3(rs.getString("ADD3"));
                    objInvestorDetailsBO.setCity(rs.getString("CITY"));
                    objInvestorDetailsBO.setPinCode(rs.getString("PINCODE"));
                    objInvestorDetailsBO.setPhoneOffice(rs.getString("PHONE_OFF"));
                    ConstansBO TRXN_DATE = new ConstansBO(rs.getString("TRXN_DATE"));
                    objTransactionBO.setTransactionDate(TRXN_DATE);
                    objTransactionBO.setTransactionTime(rs.getString("TRXN_TIME"));
                    objTransactionBO.setUnits(Double.valueOf(rs.getString("UNITS")));
                    objTransactionBO.setAmount(BigDecimal.valueOf(rs.getDouble("AMOUNT")));
                    objAccountDetailsBO.setClos_Ac_Ch(rs.getString("CLOS_AC_CH"));
                    ConstansBO DOB = new ConstansBO(rs.getString("DOB"));
                    objInvestorDetailsBO.setDateOfBirth(DOB);
                    objInvestorDetailsBO.setGuardianName(rs.getString("GUARDIAN"));
                    objInvestorDetailsBO.setTaxNo(rs.getString("TAX_NUMBER"));
                    objInvestorDetailsBO.setPhoneRes(rs.getString("PHONE_RES"));
                    objInvestorDetailsBO.setFaxOff(rs.getString("FAX_OFF"));
                    objInvestorDetailsBO.setFaxRes(rs.getString("FAX_RES"));
                    objInvestorDetailsBO.setEmail(rs.getString("EMAIL"));
                    objAccountDetailsBO.setAccountNo(rs.getString("ACCT_NO"));
                    objAccountDetailsBO.setAccountType(rs.getString("ACCT_TYPE"));
                    objAccountDetailsBO.setBankName(rs.getString("BANK_NAME"));
                    objAccountDetailsBO.setBranchName(rs.getString("BR_NAME"));
                    objAccountDetailsBO.setBankCity(rs.getString("BANK_CITY"));
                    objAccountDetailsBO.setReinv_Tag(rs.getString("REINV_TAG"));
                    objAccountDetailsBO.setHoldingNature(rs.getString("HOLD_NATUR"));
                    objInvestorDetailsBO.setOccupationCode(rs.getString("OCC_CODE"));
                    objAccountDetailsBO.setTaxStatus(rs.getString("TAX_STATUS"));
                    objAccountDetailsBO.setRemarks(rs.getString("REMARKS"));
                    objInvestorDetailsBO.setState(rs.getString("STATE"));
                    objInvestorDetailsBO.setPanHolder2(rs.getString("PAN_2_HLDR"));
                    objInvestorDetailsBO.setPanHolder3(rs.getString("PAN_3_HLDR"));
                    objInvestorDetailsBO.setGuardianPanNo(rs.getString("Guard_PAN"));
                    objInvestorDetailsBO.setLocationCode(rs.getString("LOCATION"));
                    objAccountDetailsBO.setUINNo(rs.getString("UINno"));
                    objTransactionBO.setSubTransactionType(rs.getString("SUB_TRXN_T"));
                    objAccountDetailsBO.setPaymentMechanism(rs.getString("PAY_MEC"));
                    objAccountDetailsBO.setRTGS_CD(rs.getString("RTGS_CD"));
                    objAccountDetailsBO.setNEFT_CD(rs.getString("NEFT_CD"));
                    objAccountDetailsBO.setMICR_CD(rs.getString("MICR_CD"));
                    objAccountDetailsBO.setDepBankName(rs.getString("DEPBANK"));
                    objAccountDetailsBO.setDepAccountNo(rs.getString("DEP_ACNO"));
                    ConstansBO DEP_DATE = new ConstansBO((rs.getString("DEP_DATE")));
                    objAccountDetailsBO.setDepDate(DEP_DATE);
                    objAccountDetailsBO.setDepReferenceNo(rs.getString("DEP_RFNo"));
                    objTransactionBO.setSubTransactionType(rs.getString("SUB_TRXN_T"));
                    objTransactionBO.setSipReferenceId(rs.getString("SIP_RFNO"));
                    ConstansBO SIP_RGDT = new ConstansBO(rs.getString("SIP_RGDT"));
                    objTransactionBO.setSIP_Reg_Date(SIP_RGDT);
                    objAccountDetailsBO.setNomineeName(rs.getString("NOM_NAME"));
                    objAccountDetailsBO.setNomineeRelation(rs.getString("NOM_RELA"));
                    objInvestorDetailsBO.setKycFlag(rs.getString("KYC_FLG"));
                    objInvestorDetailsBO.setInvestorId(rs.getString("CUST_ID")); //CUST_ID
                    objTransactionBO.setTransactionDate(TRXN_DATE);
                    objTransactionBO.setTransactionTime(rs.getString("TRXN_TIME"));
                    objTransactionBO.setIPAddress(rs.getString("SIGN_VF"));
                    objTransactionBO.setUserTransactionNo(rs.getString("LOG_WT")); // LOG_WT
                    objTransactionBO.setDpId(rs.getString("DPID")); //DPID
                    objTransactionBO.setClientCode(rs.getString("ClientID")); //ClientID
                    objAccountDetailsBO.setAccountNo(rs.getString("NRI_SOF"));//NRI_SOF
                    objTransactionBO.setEuinId(rs.getString("EUIN")); // EUIN
                    objTransactionBO.setEuinOpted(rs.getString("EUIN_OPT")); // EUIN_OPT

//				arrFatcaDocBO	=	objInvestorDetailsBO.setArrFatcaDocBO(rs.getString("text"));
//				
//				for(int fat = 0; fat<arrFatcaDocBO.size(rs.getString("text")); fat++){
//					objFatcaDocBO	=	arrFatcaDocBO.set(fat);
//					if(objFatcaDocBO.setInvestorRelationship(rs.getString("text")).equals("H")){
//						rowData[76] = String.valueOf(objFatcaDocBO.setAnnualIncomeCode(rs.getString("text")));//INCSLAB
//						//rowData[77] = objTransactionBO.setNetworth_j1(rs.getString("text"));NET_WOR_
//						//rowData[78] = objTransactionBO.setNetworthDate_j1(rs.getString("text"));NETWOR_DT
//						//---rowData[0] = objFatcaDocBO.setPoliticallyExpPer(rs.getString("text"));
//					}else if(objFatcaDocBO.setInvestorRelationship(rs.getString("text")).equals("F")){
//						rowData[79] = String.valueOf(objFatcaDocBO.setAnnualIncomeCode(rs.getString("text")));//INCSLAB_J1
//						//rowData[80] = objTransactionBO.setNetworth_j2(rs.getString("text"));NET_WOR_J1
//						//rowData[81] = objTransactionBO.setNetworthDate_j2(rs.getString("text"));NETDATE_J1
//						rowData[82] = objFatcaDocBO.setPoliticallyExpPer(rs.getString("text"));//PEP_J1
//					}else if(objFatcaDocBO.setInvestorRelationship(rs.getString("text")).equals("S")){
//						rowData[83] = String.valueOf(objFatcaDocBO.setAnnualIncomeCode(rs.getString("text")));//INCSLAB_J2
//						//rowData[84] = objTransactionBO.setNetworth_j3(rs.getString("text"));NET_WOR_J2
//						//rowData[85] = objTransactionBO.setNetworthDate_j3(rs.getString("text"));NETDATE_J2
//						rowData[86] = objFatcaDocBO.setPoliticallyExpPer(rs.getString("text"));//PEP_J2
//					}else if(objFatcaDocBO.setInvestorRelationship(rs.getString("text")).equals("T")){
//						rowData[87] = String.valueOf(objFatcaDocBO.setAnnualIncomeCode(rs.getString("text")));//INCSLAB_J3
//						//rowData[88] = objTransactionBO.setNetworth(rs.getString("text"));NET_WOR_J3
//						//rowData[89] = objTransactionBO.setNetworthDate(rs.getString("text"));NETDATE_J3
//						rowData[90] = objFatcaDocBO.setPoliticallyExpPer(rs.getString("text"));//PEP_J3
//					}else if(objFatcaDocBO.setInvestorRelationship(rs.getString("text")).equals("G")){
//						rowData[91] =  String.valueOf(objFatcaDocBO.setAnnualIncomeCode(rs.getString("text")));//INCSLAB_GR
//						//rowData[92] = objTransactionBO.setNetworth_gr(rs.getString("text"));NET_WOR_GR
//						//rowData[93] = objTransactionBO.setNetworthDate_gr(rs.getString("text"));NETDATE_GR
//						rowData[94] = objFatcaDocBO.setPoliticallyExpPer(rs.getString("text"));//PEP_GR
//					}
//				}
                    objTransactionBO.setForex_MCS(rs.getString("Forex_MCS"));
                    objTransactionBO.setGAME_GABLE(rs.getString("GAME_GABLE"));
                    objTransactionBO.setLS_ML_PA(rs.getString("LS_ML_PA"));
                    ConstansBO SIP_ST_DT = new ConstansBO(rs.getString("SIP_ST_DT"));
                    ConstansBO SIP_END_DT = new ConstansBO(rs.getString("SIP_END_DT"));
                    objTransactionBO.setSipStartDate(SIP_ST_DT);
                    objTransactionBO.setSipDate(rs.getInt("SIP_FQ"));//Double
                    objTransactionBO.setSipEndDate(SIP_END_DT);
                    objTransactionBO.setSipAmount(rs.getDouble("SIP_amt"));
                    objInvestorDetailsBO.setNRIAddress1(rs.getString("NRI_ADD1"));
                    objInvestorDetailsBO.setNRIAddress2(rs.getString("NRI_ADD2"));
                    objInvestorDetailsBO.setNRIAddress3(rs.getString("NRI_ADD3"));
                    objInvestorDetailsBO.setNRICity(rs.getString("NRI_CITY"));
                    objInvestorDetailsBO.setNRIState(rs.getString("NRI_State"));
                    objInvestorDetailsBO.setNRICountry(rs.getString("NRI_CON"));
                    objInvestorDetailsBO.setNRIPinCode(rs.getString("NRI_PIN"));
                    objAccountDetailsBO.setNomineeName2(rs.getString("Nom2_Name"));
                    objAccountDetailsBO.setNomineeRelation2(rs.getString("NOM2_REL"));
                    objAccountDetailsBO.setNomineeName3(rs.getString("Nom3_Name"));
                    objAccountDetailsBO.setNomineeRelation3(rs.getString("NOM3_REL"));
                    objAccountDetailsBO.setNomineePercent(rs.getInt("NOM_PER"));
                    objAccountDetailsBO.setNomineePercent1(rs.getInt("NOM2_PER"));
                    objAccountDetailsBO.setNomineePercent2(rs.getInt("NOM3_PER"));
                    objInvestorDetailsBO.setFatcaFlag(rs.getString("FATCA_FLAG"));//FATCA_FLAG
                    objInvestorDetailsBO.setcKYCRefId1(rs.getString("DUMMY4"));//Dummy4
                    objInvestorDetailsBO.setcKYCRefId2(rs.getString("DUMMY5"));//Dummy5
                    objInvestorDetailsBO.setcKYCRefId3(rs.getString("DUMMY6"));//Dummy6
                    objInvestorDetailsBO.setcKYCRefIdG(rs.getString("Dummy7"));//Dummy7
                    ConstansBO Dummy8 = new ConstansBO(rs.getString("Dummy8"));
                    objInvestorDetailsBO.setDateOfBirth2(Dummy8);//Dummy8
                    ConstansBO Dummy9 = new ConstansBO((rs.getString("Dummy9")));
                    objInvestorDetailsBO.setDateOfBirth3(Dummy9);//Dummy9
                    ConstansBO Dummy10 = new ConstansBO((rs.getString("Dummy10")));
                    objInvestorDetailsBO.setDateOfBirthG(Dummy10);//Dummy10
                    objInvestorDetailsBO.setArrFatcaDocBO(new ArrayList<>());
                    objTransactionMainBO.setAccountDetailsBO(objAccountDetailsBO);
                    objTransactionMainBO.setInvestorDetailsBO(objInvestorDetailsBO);
                    objTransactionMainBO.setTransactionBO(objTransactionBO);
                    arryTransactionMainBO.add(objTransactionMainBO);
                }
                finalFeedHash.put("2@" + rs.getString("AMC_Code"), arryTransactionMainBO);
            }
        } catch (Exception ex) {
            logger.error("Exception in getting list from CAMS " + ex.getStackTrace());
        }
        return finalFeedHash;
    }
    
    
    public String isNull(String textValue){
        if (textValue == null)
            return "";
        else 
            return textValue;
    }
        
        
}
