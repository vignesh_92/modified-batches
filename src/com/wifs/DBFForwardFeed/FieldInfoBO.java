package com.wifs.DBFForwardFeed;

public class FieldInfoBO {
	
	private String RTCode		=	"";
	private int intFieldId		=	0;
	private int intOrderId		=	0;
	private String strOptional	=	"";
	private int intWidth		=	0;
	private int intDecimal		=	0;
	private String strFieldName	=	"";
	private ConstansBO DataType;
	
	public void setRTCode(String RTCode){this.RTCode=RTCode;}
	public void setFieldId(int intFieldId){this.intFieldId=intFieldId;}
	public void setOrderId(int intOrderId){this.intOrderId=intOrderId;}
	public void setWidth(int intWidth){this.intWidth=intWidth;}
	public void setDecimal(int intDecimal){this.intDecimal=intDecimal;}
	public void setFieldName(String strFieldName){this.strFieldName=strFieldName;}
	public void setOptional(String strOptional){this.strOptional=strOptional;}
	public void setDataType(ConstansBO DataType){this.DataType=DataType;}
	
	public String getRTCode(){return(this.RTCode);}
	public int getFieldId(){return(this.intFieldId);}
	public int getOrderId(){return(this.intOrderId);}
	public int getWidth(){return(this.intWidth);}
	public int getDecimal(){return(this.intDecimal);}
	public String getFieldName(){return(this.strFieldName);}
	public String getOptional(){return(this.strOptional);}
	public ConstansBO getDataType(){return(this.DataType);}
	
}
