package com.wifs.DBFForwardFeed;

/**
 *
 * @author senthil
 */
public class ReportBO {

    private int rtCode;
    private String amcCode;
    private int successCount;
    private int failedCount;
    
    public ReportBO (int rtCode, String amcCode, int successCount, int failedCount){
        this.rtCode = rtCode;
        this.amcCode = amcCode;
        this.successCount = successCount;
        this.failedCount = failedCount;
    }
    
    public int getRtCode() {
        return rtCode;
    }
    public void setRtCode(int rtCode) {
        this.rtCode = rtCode;
    }
    public String getAmcCode() {
        return amcCode;
    }
    public void setAmcCode(String amcCode) {
        this.amcCode = amcCode;
    }
    public int getSuccessCount() {
        return successCount;
    }
    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }
    public int getFailedCount() {
        return failedCount;
    }
    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }
}