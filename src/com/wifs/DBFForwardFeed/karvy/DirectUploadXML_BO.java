package com.wifs.DBFForwardFeed.karvy;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//XML Tag level 1
@XmlRootElement(name = "file")
public class DirectUploadXML_BO{

	//XML Tag level 2
	@XmlElement(name = "request_dtl")
	private ArrayList<RequestDetails> request_Details;

	public void setRequestDetails(ArrayList<RequestDetails> requestDetails) {
		this.request_Details = requestDetails;
	}
}

//XML Tag level 3 (All fields)
@XmlType(propOrder = 
	{"AMC_CODE", "BROKE_CD", "SBBR_CODE", "USER_CODE", "USR_TXN_NO", "APPL_NO", "FOLIO_NO", "CK_DIG_NO", "TRXN_TYPE", "SCH_CODE", "FIRST_NAME", 
		"JONT_NAME1", "JONT_NAME2", "ADD1", "ADD2", "ADD3", "CITY", "PINCODE", "PHONE_OFF", "MOBILE_NO", "TRXN_DATE", "TRXN_TIME", "UNITS", 
		"AMOUNT", "CLOS_AC_CH", "DOB", "GUARDIAN", "TAX_NUMBER", "PHONE_RES", "FAX_OFF", "FAX_RES", "EMAIL", "ACCT_NO", "ACCT_TYPE", "BANK_NAME", 
		"BR_NAME", "BANK_CITY", "REINV_TAG", "HOLD_NATUR", "OCC_CODE", "TAX_STATUS", "REMARKS", "STATE", "PAN_2_HLDR", "PAN_3_HLDR", "GUARD_PAN", 
		"LOCATION", "UINNO", "FORM6061", "FORM6061J1", "FORM6061J2", "PAY_MEC", "RTGS_CD", "NEFT_CD", "MICR_CD", "DEPBANK", "DEP_ACNO", "DEP_DATE", 
		"DEP_RFNO", "SUB_TRXN_T", "SIP_RFNO", "SIP_RGDT", "NOM_NAME", "NOM_RELA", "KYC_FLG", "POA_STAT", "MOD_TRXN", "SIGN_VF", "CUST_ID", "LOG_WT", "LOG_PE", "DPID", 
		"CLIENTID", "NRI_SOF", "GUARD_RELA", "PAN_VALI", "GPAN_VALI", "j1PAN_VALI", "j2PAN_VALI", "NRI_ADD1", "NRI_ADD2", "NRI_ADD3", "NRI_CITY", 
		"NRI_STATE", "NRI_COUN", "NRI_PIN", "NOM_DOB", "NOM_PERC", "NOM_ADD1", "NOM_ADD2", "NOM_ADD3", "NOM_CITY", 
		"NOM_STATE", "NOM_COUN", "NOM_PIN", "NOM_EMAIL", "NOM_MOB", "NOM1_NAME", "NOM1_RELA", "NOM1_DOB", "NOM1_PERC", "NOM1_ADD1", "NOM1_ADD2", 
		"NOM1_ADD3", "NOM1_CITY", "NOM1_STATE", "NOM1_COUN", "NOM1_PIN", "NOM1_EMAIL", "NOM1_MOB", "NOM2_NAME", "NOM2_RELA", "NOM2_DOB", 
		"NOM2_PERC", "NOM2_ADD1", "NOM2_ADD2", "NOM2_ADD3", "NOM2_CITY", "NOM2_STATE", "NOM2_COUN", "NOM2_PIN", "NOM2_EMAIL", "NOM2_MOB", 
		"FIRC_STA", "SIP_STDT", "SIP_ENDT", "SIP_NOINST", "SIP_FREQ", "EUIN", "EUIN_OPTIN", "e1", "e2", "e3", "e4", "e5", "e6"})
class RequestDetails {
	private String AMC_CODE = "";
    private String BROKE_CD = "";
    private String SBBR_CODE = "";
    private String USER_CODE = "";
    private String USR_TXN_NO = "";
    private String APPL_NO = "";
    private String FOLIO_NO = "";
    private String CK_DIG_NO = "";
    private String TRXN_TYPE = "";
    private String SCH_CODE = "";
    private String FIRST_NAME = "";
    private String JONT_NAME1 = "";
    private String JONT_NAME2 = "";
    private String ADD1 = "";
    private String ADD2 = "";
    private String ADD3 = "";
    private String CITY = "";
    private String PINCODE = "";
    private String PHONE_OFF = "";
    private String MOBILE_NO = "";
    private String TRXN_DATE = "";
    private String TRXN_TIME = "";
    private String UNITS = "";
    private String AMOUNT = "";
    private String CLOS_AC_CH = "";
    private String DOB = "";
    private String GUARDIAN = "";
    private String TAX_NUMBER = "";
    private String PHONE_RES = "";
    private String FAX_OFF = "";
    private String FAX_RES = "";
    private String EMAIL = "";
    private String ACCT_NO = "";
    private String ACCT_TYPE = "";
    private String BANK_NAME = "";
    private String BR_NAME = "";
    private String BANK_CITY = "";
    private String REINV_TAG = "";
    private String HOLD_NATUR = "";
    private String OCC_CODE = "";
    private String TAX_STATUS = "";
    private String REMARKS = "";
    private String STATE = "";
    private String PAN_2_HLDR = "";
    private String PAN_3_HLDR = "";
    private String GUARD_PAN = "";
    private String LOCATION = "";
    private String UINNO = "";
    private String FORM6061 = "";
    private String FORM6061J1 = "";
    private String FORM6061J2 = "";
    private String PAY_MEC = "";
    private String RTGS_CD = "";
    private String NEFT_CD = "";
    private String MICR_CD = "";
    private String DEPBANK = "";
    private String DEP_ACNO = "";
    private String DEP_DATE = "";
    private String DEP_RFNO = "";
    private String SUB_TRXN_T = "";
    private String SIP_RFNO = "";
    private String SIP_RGDT = "";
    private String KYC_FLG = "";
    private String POA_STAT = "";
    private String MOD_TRXN = "";
    private String SIGN_VF = "";
    private String CUST_ID = "";
    private String LOG_WT = "";
    private String LOG_PE = "";
    private String DPID = "";
    private String CLIENTID = "";
    private String NRI_SOF = "";
    private String GUARD_RELA = "";
    private String PAN_VALI = "";
    private String GPAN_VALI = "";
    private String J1PAN_VALI = "";
    private String J2PAN_VALI = "";
    private String NRI_ADD1 = "";
    private String NRI_ADD2 = "";
    private String NRI_ADD3 = "";
    private String NRI_CITY = "";
    private String NRI_STATE = "";
    private String NRI_COUN = "";
    private String NRI_PIN = "";
    private String NOM_DOB = "";
    private String NOM_NAME = "";
    private String NOM_RELA = "";
    private String NOM_PERC = "";
    private String NOM_ADD1 = "";
    private String NOM_ADD2 = "";
    private String NOM_ADD3 = "";
    private String NOM_CITY = "";
    private String NOM_STATE = "";
    private String NOM_COUN = "";
    private String NOM_PIN = "";
    private String NOM_EMAIL = "";
    private String NOM_MOB = "";
    private String NOM1_NAME = "";
    private String NOM1_RELA = "";
    private String NOM1_DOB = "";
    private String NOM1_PERC = "";
    private String NOM1_ADD1 = "";
    private String NOM1_ADD2 = "";
    private String NOM1_ADD3 = "";
    private String NOM1_CITY = "";
    private String NOM1_STATE = "";
    private String NOM1_COUN = "";
    private String NOM1_PIN = "";
    private String NOM1_EMAIL = "";
    private String NOM1_MOB = "";
    private String NOM2_NAME = "";
    private String NOM2_RELA = "";
    private String NOM2_DOB = "";
    private String NOM2_PERC = "";
    private String NOM2_ADD1 = "";
    private String NOM2_ADD2 = "";
    private String NOM2_ADD3 = "";
    private String NOM2_CITY = "";
    private String NOM2_STATE = "";
    private String NOM2_COUN = "";
    private String NOM2_PIN = "";
    private String NOM2_EMAIL = "";
    private String NOM2_MOB = "";
    private String FIRC_STA = "";
    private String SIP_STDT = "";
    private String SIP_ENDT = "";
    private String SIP_NOINST = "";
    private String SIP_FREQ = "";
    private String EUIN = "";
    private String EUIN_OPTIN = "";
    private String E1 = "";
    private String E2 = "";
    private String E3 = "";
    private String E4 = "";
    private String E5 = "";
    private String E6 = "";
	    
	    
		public String getAMC_CODE() {
			return AMC_CODE;
		}
		public void setAMC_CODE(String aMC_CODE) {
			AMC_CODE = aMC_CODE;
		}
		public String getBROKE_CD() {
			return BROKE_CD;
		}
		public void setBROKE_CD(String bROKE_CD) {
			BROKE_CD = bROKE_CD;
		}
		public String getSBBR_CODE() {
			return SBBR_CODE;
		}
		public void setSBBR_CODE(String sBBR_CODE) {
			SBBR_CODE = sBBR_CODE;
		}
		public String getUSER_CODE() {
			return USER_CODE;
		}
		public void setUSER_CODE(String uSER_CODE) {
			USER_CODE = uSER_CODE;
		}
		public String getUSR_TXN_NO() {
			return USR_TXN_NO;
		}
		public void setUSR_TXN_NO(String uSR_TXN_NO) {
			USR_TXN_NO = uSR_TXN_NO;
		}
		public String getAPPL_NO() {
			return APPL_NO;
		}
		public void setAPPL_NO(String aPPL_NO) {
			APPL_NO = aPPL_NO;
		}
		public String getFOLIO_NO() {
			return FOLIO_NO;
		}
		public void setFOLIO_NO(String fOLIO_NO) {
			FOLIO_NO = fOLIO_NO;
		}
		public String getCK_DIG_NO() {
			return CK_DIG_NO;
		}
		public void setCK_DIG_NO(String cK_DIG_NO) {
			CK_DIG_NO = cK_DIG_NO;
		}
		public String getTRXN_TYPE() {
			return TRXN_TYPE;
		}
		public void setTRXN_TYPE(String tRXN_TYPE) {
			TRXN_TYPE = tRXN_TYPE;
		}
		public String getSCH_CODE() {
			return SCH_CODE;
		}
		public void setSCH_CODE(String sCH_CODE) {
			SCH_CODE = sCH_CODE;
		}
		public String getFIRST_NAME() {
			return FIRST_NAME;
		}
		public void setFIRST_NAME(String fIRST_NAME) {
			FIRST_NAME = fIRST_NAME;
		}
		public String getJONT_NAME1() {
			return JONT_NAME1;
		}
		public void setJONT_NAME1(String jONT_NAME1) {
			JONT_NAME1 = jONT_NAME1;
		}
		public String getJONT_NAME2() {
			return JONT_NAME2;
		}
		public void setJONT_NAME2(String jONT_NAME2) {
			JONT_NAME2 = jONT_NAME2;
		}
		public String getADD1() {
			return ADD1;
		}
		public void setADD1(String aDD1) {
			ADD1 = aDD1;
		}
		public String getADD2() {
			return ADD2;
		}
		public void setADD2(String aDD2) {
			ADD2 = aDD2;
		}
		public String getADD3() {
			return ADD3;
		}
		public void setADD3(String aDD3) {
			ADD3 = aDD3;
		}
		public String getCITY() {
			return CITY;
		}
		public void setCITY(String cITY) {
			CITY = cITY;
		}
		public String getPINCODE() {
			return PINCODE;
		}
		public void setPINCODE(String pINCODE) {
			PINCODE = pINCODE;
		}
		public String getPHONE_OFF() {
			return PHONE_OFF;
		}
		public void setPHONE_OFF(String pHONE_OFF) {
			PHONE_OFF = pHONE_OFF;
		}
		public String getMOBILE_NO() {
			return MOBILE_NO;
		}
		public void setMOBILE_NO(String mOBILE_NO) {
			MOBILE_NO = mOBILE_NO;
		}
		public String getTRXN_DATE() {
			return TRXN_DATE;
		}
		public void setTRXN_DATE(String tRXN_DATE) {
			TRXN_DATE = tRXN_DATE;
		}
		public String getTRXN_TIME() {
			return TRXN_TIME;
		}
		public void setTRXN_TIME(String tRXN_TIME) {
			TRXN_TIME = tRXN_TIME;
		}
		public String getUNITS() {
			return UNITS;
		}
		public void setUNITS(String uNITS) {
			UNITS = uNITS;
		}
		public String getAMOUNT() {
			return AMOUNT;
		}
		public void setAMOUNT(String aMOUNT) {
			AMOUNT = aMOUNT;
		}
		public String getCLOS_AC_CH() {
			return CLOS_AC_CH;
		}
		public void setCLOS_AC_CH(String cLOS_AC_CH) {
			CLOS_AC_CH = cLOS_AC_CH;
		}
		public String getDOB() {
			return DOB;
		}
		public void setDOB(String dOB) {
			DOB = dOB;
		}
		public String getGUARDIAN() {
			return GUARDIAN;
		}
		public void setGUARDIAN(String gUARDIAN) {
			GUARDIAN = gUARDIAN;
		}
		public String getTAX_NUMBER() {
			return TAX_NUMBER;
		}
		public void setTAX_NUMBER(String tAX_NUMBER) {
			TAX_NUMBER = tAX_NUMBER;
		}
		public String getPHONE_RES() {
			return PHONE_RES;
		}
		public void setPHONE_RES(String pHONE_RES) {
			PHONE_RES = pHONE_RES;
		}
		public String getFAX_OFF() {
			return FAX_OFF;
		}
		public void setFAX_OFF(String fAX_OFF) {
			FAX_OFF = fAX_OFF;
		}
		public String getFAX_RES() {
			return FAX_RES;
		}
		public void setFAX_RES(String fAX_RES) {
			FAX_RES = fAX_RES;
		}
		public String getEMAIL() {
			return EMAIL;
		}
		public void setEMAIL(String eMAIL) {
			EMAIL = eMAIL;
		}
		public String getACCT_NO() {
			return ACCT_NO;
		}
		public void setACCT_NO(String aCCT_NO) {
			ACCT_NO = aCCT_NO;
		}
		public String getACCT_TYPE() {
			return ACCT_TYPE;
		}
		public void setACCT_TYPE(String aCCT_TYPE) {
			ACCT_TYPE = aCCT_TYPE;
		}
		public String getBANK_NAME() {
			return BANK_NAME;
		}
		public void setBANK_NAME(String bANK_NAME) {
			BANK_NAME = bANK_NAME;
		}
		public String getBR_NAME() {
			return BR_NAME;
		}
		public void setBR_NAME(String bR_NAME) {
			BR_NAME = bR_NAME;
		}
		public String getBANK_CITY() {
			return BANK_CITY;
		}
		public void setBANK_CITY(String bANK_CITY) {
			BANK_CITY = bANK_CITY;
		}
		public String getREINV_TAG() {
			return REINV_TAG;
		}
		public void setREINV_TAG(String rEINV_TAG) {
			REINV_TAG = rEINV_TAG;
		}
		public String getHOLD_NATUR() {
			return HOLD_NATUR;
		}
		public void setHOLD_NATUR(String hOLD_NATUR) {
			HOLD_NATUR = hOLD_NATUR;
		}
		public String getOCC_CODE() {
			return OCC_CODE;
		}
		public void setOCC_CODE(String oCC_CODE) {
			OCC_CODE = oCC_CODE;
		}
		public String getTAX_STATUS() {
			return TAX_STATUS;
		}
		public void setTAX_STATUS(String tAX_STATUS) {
			TAX_STATUS = tAX_STATUS;
		}
		public String getREMARKS() {
			return REMARKS;
		}
		public void setREMARKS(String rEMARKS) {
			REMARKS = rEMARKS;
		}
		public String getSTATE() {
			return STATE;
		}
		public void setSTATE(String sTATE) {
			STATE = sTATE;
		}
		public String getPAN_2_HLDR() {
			return PAN_2_HLDR;
		}
		public void setPAN_2_HLDR(String pAN_2_HLDR) {
			PAN_2_HLDR = pAN_2_HLDR;
		}
		public String getPAN_3_HLDR() {
			return PAN_3_HLDR;
		}
		public void setPAN_3_HLDR(String pAN_3_HLDR) {
			PAN_3_HLDR = pAN_3_HLDR;
		}
		public String getGUARD_PAN() {
			return GUARD_PAN;
		}
		public void setGUARD_PAN(String GUARD_PAN) {
			this.GUARD_PAN = GUARD_PAN;
		}
		public String getLOCATION() {
			return LOCATION;
		}
		public void setLOCATION(String lOCATION) {
			LOCATION = lOCATION;
		}
		public String getUINNO() {
			return UINNO;
		}
		public void setUINNO(String UINNO) {
			this.UINNO = UINNO;
		}
		public String getFORM6061() {
			return FORM6061;
		}
		public void setFORM6061(String fORM6061) {
			FORM6061 = fORM6061;
		}
		public String getFORM6061J1() {
			return FORM6061J1;
		}
		public void setFORM6061J1(String fORM6061J1) {
			FORM6061J1 = fORM6061J1;
		}
		public String getFORM6061J2() {
			return FORM6061J2;
		}
		public void setFORM6061J2(String fORM6061J2) {
			FORM6061J2 = fORM6061J2;
		}
		public String getPAY_MEC() {
			return PAY_MEC;
		}
		public void setPAY_MEC(String pAY_MEC) {
			PAY_MEC = pAY_MEC;
		}
		public String getRTGS_CD() {
			return RTGS_CD;
		}
		public void setRTGS_CD(String rTGS_CD) {
			RTGS_CD = rTGS_CD;
		}
		public String getNEFT_CD() {
			return NEFT_CD;
		}
		public void setNEFT_CD(String nEFT_CD) {
			NEFT_CD = nEFT_CD;
		}
		public String getMICR_CD() {
			return MICR_CD;
		}
		public void setMICR_CD(String mICR_CD) {
			MICR_CD = mICR_CD;
		}
		public String getDEPBANK() {
			return DEPBANK;
		}
		public void setDEPBANK(String dEPBANK) {
			DEPBANK = dEPBANK;
		}
		public String getDEP_ACNO() {
			return DEP_ACNO;
		}
		public void setDEP_ACNO(String dEP_ACNO) {
			DEP_ACNO = dEP_ACNO;
		}
		public String getDEP_DATE() {
			return DEP_DATE;
		}
		public void setDEP_DATE(String dEP_DATE) {
			DEP_DATE = dEP_DATE;
		}
		public String getDEP_RFNO() {
			return DEP_RFNO;
		}
		public void setDEP_RFNO(String DEP_RFNO) {
			this.DEP_RFNO = DEP_RFNO;
		}
		public String getSUB_TRXN_T() {
			return SUB_TRXN_T;
		}
		public void setSUB_TRXN_T(String sUB_TRXN_T) {
			SUB_TRXN_T = sUB_TRXN_T;
		}
		public String getSIP_RFNO() {
			return SIP_RFNO;
		}
		public void setSIP_RFNO(String sIP_RFNO) {
			SIP_RFNO = sIP_RFNO;
		}
		public String getSIP_RGDT() {
			return SIP_RGDT;
		}
		public void setSIP_RGDT(String sIP_RGDT) {
			SIP_RGDT = sIP_RGDT;
		}
		public String getKYC_FLG() {
			return KYC_FLG;
		}
		public void setKYC_FLG(String kYC_FLG) {
			KYC_FLG = kYC_FLG;
		}
		public String getPOA_STAT() {
			return POA_STAT;
		}
		public void setPOA_STAT(String pOA_STAT) {
			POA_STAT = pOA_STAT;
		}
		public String getMOD_TRXN() {
			return MOD_TRXN;
		}
		public void setMOD_TRXN(String mOD_TRXN) {
			MOD_TRXN = mOD_TRXN;
		}
		public String getSIGN_VF() {
			return SIGN_VF;
		}
		public void setSIGN_VF(String sIGN_VF) {
			SIGN_VF = sIGN_VF;
		}
		public String getCUST_ID() {
			return CUST_ID;
		}
		public void setCUST_ID(String cUST_ID) {
			CUST_ID = cUST_ID;
		}
		public String getLOG_WT() {
			return LOG_WT;
		}
		public void setLOG_WT(String lOG_WT) {
			LOG_WT = lOG_WT;
		}
		public String getLOG_PE() {
			return LOG_PE;
		}
		public void setLOG_PE(String lOG_PE) {
			LOG_PE = lOG_PE;
		}
		public String getDPID() {
			return DPID;
		}
		public void setDPID(String dPID) {
			DPID = dPID;
		}
		public String getCLIENTID() {
			return CLIENTID;
		}
		public void setCLIENTID(String CLIENTID) {
			this.CLIENTID = CLIENTID;
		}
		public String getNRI_SOF() {
			return NRI_SOF;
		}
		public void setNRI_SOF(String nRI_SOF) {
			NRI_SOF = nRI_SOF;
		}
		public String getGUARD_RELA() {
			return GUARD_RELA;
		}
		public void setGUARD_RELA(String gUARD_RELA) {
			GUARD_RELA = gUARD_RELA;
		}
		public String getPAN_VALI() {
			return PAN_VALI;
		}
		public void setPAN_VALI(String pAN_VALI) {
			PAN_VALI = pAN_VALI;
		}
		public String getGPAN_VALI() {
			return GPAN_VALI;
		}
		public void setGPAN_VALI(String gPAN_VALI) {
			GPAN_VALI = gPAN_VALI;
		}
		@XmlElement(name = "J1PAN_VALI")
		public String getJ1PAN_VALI() {
			return J1PAN_VALI;
		}
		public void setJ1PAN_VALI(String j1PAN_VALI) {
			J1PAN_VALI = j1PAN_VALI;
		}
		@XmlElement(name = "J2PAN_VALI")
		public String getJ2PAN_VALI() {
			return J2PAN_VALI;
		}
		public void setJ2PAN_VALI(String j2pan_VALI) {
			J2PAN_VALI = j2pan_VALI;
		}
		public String getNRI_ADD1() {
			return NRI_ADD1;
		}
		public void setNRI_ADD1(String nRI_ADD1) {
			NRI_ADD1 = nRI_ADD1;
		}
		public String getNRI_ADD2() {
			return NRI_ADD2;
		}
		public void setNRI_ADD2(String nRI_ADD2) {
			NRI_ADD2 = nRI_ADD2;
		}
		public String getNRI_ADD3() {
			return NRI_ADD3;
		}
		public void setNRI_ADD3(String nRI_ADD3) {
			NRI_ADD3 = nRI_ADD3;
		}
		public String getNRI_CITY() {
			return NRI_CITY;
		}
		public void setNRI_CITY(String nRI_CITY) {
			NRI_CITY = nRI_CITY;
		}
		public String getNRI_STATE() {
			return NRI_STATE;
		}
		public void setNRI_STATE(String nRI_STATE) {
			NRI_STATE = nRI_STATE;
		}
		public String getNRI_COUN() {
			return NRI_COUN;
		}
		public void setNRI_COUN(String nRI_COUN) {
			NRI_COUN = nRI_COUN;
		}
		public String getNRI_PIN() {
			return NRI_PIN;
		}
		public void setNRI_PIN(String nRI_PIN) {
			NRI_PIN = nRI_PIN;
		}
		public String getNOM_DOB() {
			return NOM_DOB;
		}
		public void setNOM_DOB(String nOM_DOB) {
			NOM_DOB = nOM_DOB;
		}
		public String getNOM_NAME() {
			return NOM_NAME;
		}
		public void setNOM_NAME(String nOM_NAME) {
			NOM_NAME = nOM_NAME;
		}
		public String getNOM_RELA() {
			return NOM_RELA;
		}
		public void setNOM_RELA(String nOM_RELA) {
			NOM_RELA = nOM_RELA;
		}
		public String getNOM_PERC() {
			return NOM_PERC;
		}
		public void setNOM_PERC(String nOM_PERC) {
			NOM_PERC = nOM_PERC;
		}
		public String getNOM_ADD1() {
			return NOM_ADD1;
		}
		public void setNOM_ADD1(String nOM_ADD1) {
			NOM_ADD1 = nOM_ADD1;
		}
		public String getNOM_ADD2() {
			return NOM_ADD2;
		}
		public void setNOM_ADD2(String nOM_ADD2) {
			NOM_ADD2 = nOM_ADD2;
		}
		public String getNOM_ADD3() {
			return NOM_ADD3;
		}
		public void setNOM_ADD3(String nOM_ADD3) {
			NOM_ADD3 = nOM_ADD3;
		}
		public String getNOM_CITY() {
			return NOM_CITY;
		}
		public void setNOM_CITY(String nOM_CITY) {
			NOM_CITY = nOM_CITY;
		}
		public String getNOM_STATE() {
			return NOM_STATE;
		}
		public void setNOM_STATE(String nOM_STATE) {
			NOM_STATE = nOM_STATE;
		}
		public String getNOM_COUN() {
			return NOM_COUN;
		}
		public void setNOM_COUN(String nOM_COUN) {
			NOM_COUN = nOM_COUN;
		}
		public String getNOM_PIN() {
			return NOM_PIN;
		}
		public void setNOM_PIN(String nOM_PIN) {
			NOM_PIN = nOM_PIN;
		}
		public String getNOM_EMAIL() {
			return NOM_EMAIL;
		}
		public void setNOM_EMAIL(String nOM_EMAIL) {
			NOM_EMAIL = nOM_EMAIL;
		}
		public String getNOM_MOB() {
			return NOM_MOB;
		}
		public void setNOM_MOB(String nOM_MOB) {
			NOM_MOB = nOM_MOB;
		}
		public String getNOM1_NAME() {
			return NOM1_NAME;
		}
		public void setNOM1_NAME(String nOM1_NAME) {
			NOM1_NAME = nOM1_NAME;
		}
		public String getNOM1_RELA() {
			return NOM1_RELA;
		}
		public void setNOM1_RELA(String nOM1_RELA) {
			NOM1_RELA = nOM1_RELA;
		}
		public String getNOM1_DOB() {
			return NOM1_DOB;
		}
		public void setNOM1_DOB(String nOM1_DOB) {
			NOM1_DOB = nOM1_DOB;
		}
		public String getNOM1_PERC() {
			return NOM1_PERC;
		}
		public void setNOM1_PERC(String nOM1_PERC) {
			NOM1_PERC = nOM1_PERC;
		}
		public String getNOM1_ADD1() {
			return NOM1_ADD1;
		}
		public void setNOM1_ADD1(String nOM1_ADD1) {
			NOM1_ADD1 = nOM1_ADD1;
		}
		public String getNOM1_ADD2() {
			return NOM1_ADD2;
		}
		public void setNOM1_ADD2(String nOM1_ADD2) {
			NOM1_ADD2 = nOM1_ADD2;
		}
		public String getNOM1_ADD3() {
			return NOM1_ADD3;
		}
		public void setNOM1_ADD3(String nOM1_ADD3) {
			NOM1_ADD3 = nOM1_ADD3;
		}
		public String getNOM1_CITY() {
			return NOM1_CITY;
		}
		public void setNOM1_CITY(String nOM1_CITY) {
			NOM1_CITY = nOM1_CITY;
		}
		public String getNOM1_STATE() {
			return NOM1_STATE;
		}
		public void setNOM1_STATE(String nOM1_STATE) {
			NOM1_STATE = nOM1_STATE;
		}
		public String getNOM1_COUN() {
			return NOM1_COUN;
		}
		public void setNOM1_COUN(String nOM1_COUN) {
			NOM1_COUN = nOM1_COUN;
		}
		public String getNOM1_PIN() {
			return NOM1_PIN;
		}
		public void setNOM1_PIN(String nOM1_PIN) {
			NOM1_PIN = nOM1_PIN;
		}
		public String getNOM1_EMAIL() {
			return NOM1_EMAIL;
		}
		public void setNOM1_EMAIL(String nOM1_EMAIL) {
			NOM1_EMAIL = nOM1_EMAIL;
		}
		public String getNOM1_MOB() {
			return NOM1_MOB;
		}
		public void setNOM1_MOB(String nOM1_MOB) {
			NOM1_MOB = nOM1_MOB;
		}
		public String getNOM2_NAME() {
			return NOM2_NAME;
		}
		public void setNOM2_NAME(String nOM2_NAME) {
			NOM2_NAME = nOM2_NAME;
		}
		public String getNOM2_RELA() {
			return NOM2_RELA;
		}
		public void setNOM2_RELA(String nOM2_RELA) {
			NOM2_RELA = nOM2_RELA;
		}
		public String getNOM2_DOB() {
			return NOM2_DOB;
		}
		public void setNOM2_DOB(String nOM2_DOB) {
			NOM2_DOB = nOM2_DOB;
		}
		public String getNOM2_PERC() {
			return NOM2_PERC;
		}
		public void setNOM2_PERC(String nOM2_PERC) {
			NOM2_PERC = nOM2_PERC;
		}
		public String getNOM2_ADD1() {
			return NOM2_ADD1;
		}
		public void setNOM2_ADD1(String nOM2_ADD1) {
			NOM2_ADD1 = nOM2_ADD1;
		}
		public String getNOM2_ADD2() {
			return NOM2_ADD2;
		}
		public void setNOM2_ADD2(String nOM2_ADD2) {
			NOM2_ADD2 = nOM2_ADD2;
		}
		public String getNOM2_ADD3() {
			return NOM2_ADD3;
		}
		public void setNOM2_ADD3(String nOM2_ADD3) {
			NOM2_ADD3 = nOM2_ADD3;
		}
		public String getNOM2_CITY() {
			return NOM2_CITY;
		}
		public void setNOM2_CITY(String nOM2_CITY) {
			NOM2_CITY = nOM2_CITY;
		}
		public String getNOM2_STATE() {
			return NOM2_STATE;
		}
		public void setNOM2_STATE(String nOM2_STATE) {
			NOM2_STATE = nOM2_STATE;
		}
		public String getNOM2_COUN() {
			return NOM2_COUN;
		}
		public void setNOM2_COUN(String nOM2_COUN) {
			NOM2_COUN = nOM2_COUN;
		}
		public String getNOM2_PIN() {
			return NOM2_PIN;
		}
		public void setNOM2_PIN(String nOM2_PIN) {
			NOM2_PIN = nOM2_PIN;
		}
		public String getNOM2_EMAIL() {
			return NOM2_EMAIL;
		}
		public void setNOM2_EMAIL(String nOM2_EMAIL) {
			NOM2_EMAIL = nOM2_EMAIL;
		}
		public String getNOM2_MOB() {
			return NOM2_MOB;
		}
		public void setNOM2_MOB(String nOM2_MOB) {
			NOM2_MOB = nOM2_MOB;
		}
		public String getFIRC_STA() {
			return FIRC_STA;
		}
		public void setFIRC_STA(String fIRC_STA) {
			FIRC_STA = fIRC_STA;
		}
		public String getSIP_STDT() {
			return SIP_STDT;
		}
		public void setSIP_STDT(String sIP_STDT) {
			SIP_STDT = sIP_STDT;
		}
		public String getSIP_ENDT() {
			return SIP_ENDT;
		}
		public void setSIP_ENDT(String sIP_ENDT) {
			SIP_ENDT = sIP_ENDT;
		}
		public String getSIP_NOINST() {
			return SIP_NOINST;
		}
		public void setSIP_NOINST(String sIP_NOINST) {
			SIP_NOINST = sIP_NOINST;
		}
		public String getSIP_FREQ() {
			return SIP_FREQ;
		}
		public void setSIP_FREQ(String sIP_FREQ) {
			SIP_FREQ = sIP_FREQ;
		}
		public String getEUIN() {
			return EUIN;
		}
		public void setEUIN(String eUIN) {
			EUIN = eUIN;
		}
		public String getEUIN_OPTIN() {
			return EUIN_OPTIN;
		}
		public void setEUIN_OPTIN(String eUIN_OPTIN) {
			EUIN_OPTIN = eUIN_OPTIN;
		}
		@XmlElement(name = "E1")
		public String getE1() {
			return E1;
		}
		public void setE1(String e1) {
			E1 = e1;
		}
		@XmlElement(name = "E2")
		public String getE2() {
			return E2;
		}
		public void setE2(String e2) {
			E2 = e2;
		}
		@XmlElement(name = "E3")
		public String getE3() {
			return E3;
		}
		public void setE3(String e3) {
			E3 = e3;
		}
		@XmlElement(name = "E4")
		public String getE4() {
			return E4;
		}
		public void setE4(String e4) {
			E4 = e4;
		}
		@XmlElement(name = "E5")
		public String getE5() {
			return E5;
		}
		public void setE5(String e5) {
			E5 = e5;
		}
		@XmlElement(name = "E6")
		public String getE6() {
			return E6;
		}
		public void setE6(String e6) {
			E6 = e6;
		}
}