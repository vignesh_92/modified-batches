package com.wifs.DBFForwardFeed.karvy;

import java.io.File;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.wifs.DBFForwardFeed.AccountDetailsBO;
import com.wifs.DBFForwardFeed.BackgroundThreadProcess;
import com.wifs.DBFForwardFeed.ConstansBO;
import com.wifs.DBFForwardFeed.DBFFilesDAO;
import com.wifs.DBFForwardFeed.InvestorDetailsBO;
import static com.wifs.DBFForwardFeed.STPProcessBL.bkThreadProc;
import com.wifs.DBFForwardFeed.STPProcessBO;
import com.wifs.DBFForwardFeed.TransactionBO;
import com.wifs.DBFForwardFeed.TransactionMainBO;
import com.wifs.utilities.PropertiesConstant;
import com.wifs.utilities.PropertyFileReader;
import java.io.StringWriter;
import org.tempuri.ISTPService;
import org.tempuri.STPService;

public class ProcessBL {

	public static Logger logger = Logger.getLogger(ProcessBL.class);
        public static BackgroundThreadProcess bkThreadProc = new BackgroundThreadProcess();


	public static void main(String[] temp) {

		/*
		 * DirectUploadXML_BO directUploadXML_BO = new DirectUploadXML_BO();
		 * ArrayList<RequestDetails> al_rd = new ArrayList<RequestDetails>();
		 * 
		 * RequestDetails rd = new RequestDetails(); rd.setAMC_CODE("TEST");
		 * rd.setACCT_NO("AC123"); rd.setBROKE_CD("");
		 * rd.setTRXN_TYPE("tRXN_TYPE"); al_rd.add(rd);
		 * 
		 * rd = new RequestDetails(); rd.setAMC_CODE("2 TEST");
		 * rd.setACCT_NO("2 AC123"); rd.setBROKE_CD("2");
		 * rd.setTRXN_TYPE("2 tRXN_TYPE"); al_rd.add(rd);
		 * 
		 * rd = new RequestDetails(); rd.setAMC_CODE("3 TEST");
		 * rd.setACCT_NO("3 AC123"); rd.setBROKE_CD("");
		 * rd.setTRXN_TYPE("3 tRXN_TYPE"); al_rd.add(rd);
		 * 
		 * directUploadXML_BO.setRequestDetails(al_rd);
		 */

		String responseString =
						 "<?xml version=\"1.0\" encoding=\"utf-8\"?> "
						+ "<Services> "
						+ "  <Information> "
						+ "    <Return_Code>-1</Return_Code> "
						+ "    <Return_Msg>Message from FundsIndia server - Error in upload KARVY server</Return_Msg> "
						+ "  </Information> "
						+ "  <ReverseDetails>"
						+ "  </ReverseDetails>"
						+ " </Services>";
				//"<?xml version=\"1.0\" encoding=\"utf-8\"?><Services>  <Information>    <Return_Code>0</Return_Code>    <Return_Msg>File Uploaded Successfully</Return_Msg>  </Information>  <ReverseDetails>    <PortRemarks>Invalid / Mismatch Schemecode for User Tr No: 1234</PortRemarks>    <AMC_CODE>ABC</AMC_CODE>    <BROKE_CD>ARN-0000</BROKE_CD>    <SBBR_CODE>1234</SBBR_CODE>    <USER_CODE>Testing</USER_CODE>    <USR_TXN_NO>123</USR_TXN_NO>    <APPL_NO>A1234</APPL_NO>    <FOLIO_NO>12345678901</FOLIO_NO>    <CK_DIG_NO></CK_DIG_NO>    <TRXN_TYPE>P</TRXN_TYPE>    <SCH_CODE>GFGP</SCH_CODE>    <FIRST_NAME>Name</FIRST_NAME>    <JONT_NAME1>Joint Name 1</JONT_NAME1>    <JONT_NAME2>Joint Name 2</JONT_NAME2>    <ADD1>House No. etc</ADD1>    <ADD2>Street Name</ADD2>    <ADD3>Area Name</ADD3>    <CITY>City Name</CITY>    <PINCODE>Pin Code</PINCODE>    <PHONE_OFF>02212345678</PHONE_OFF>    <MOBILE_NO></MOBILE_NO>    <TRXN_DATE>MM/dd/yyyy</TRXN_DATE>    <TRXN_TIME>13:14:21</TRXN_TIME>    <UNITS></UNITS>    <AMOUNT>5000.00</AMOUNT>    <CLOS_AC_CH></CLOS_AC_CH>    <DOB>MM/dd/yyyy</DOB>    <GUARDIAN></GUARDIAN>    <TAX_NUMBER>1st Holder Pan</TAX_NUMBER>    <PHONE_RES>02287654321</PHONE_RES>    <FAX_OFF>02223456789</FAX_OFF>    <FAX_RES>02298765432</FAX_RES>    <EMAIL>email@email.com</EMAIL>    <ACCT_NO>0012345678901</ACCT_NO>    <ACCT_TYPE>SB</ACCT_TYPE>    <BANK_NAME>Bank</BANK_NAME>    <BR_NAME>Street Name etc</BR_NAME>    <BANK_CITY>City Name</BANK_CITY>    <REINV_TAG>Z</REINV_TAG>    <HOLD_NATUR>JO </HOLD_NATUR>    <OCC_CODE>1</OCC_CODE>    <TAX_STATUS>01</TAX_STATUS>    <REMARKS></REMARKS>    <STATE></STATE>    <PAN_2_HLDR>2nd Holder PAN</PAN_2_HLDR>    <PAN_3_HLDR>3rd Holder PAN</PAN_3_HLDR>    <Guard_PAN>3rd Holder PAN</Guard_PAN>    <LOCATION>M4</LOCATION>    <UINno></UINno>    <FORM6061></FORM6061>    <FORM6061J1></FORM6061J1>    <FORM6061J2></FORM6061J2>    <PAY_MEC></PAY_MEC>    <RTGS_CD></RTGS_CD>    <NEFT_CD></NEFT_CD>    <MICR_CD></MICR_CD>    <DEPBANK>Bank Name</DEPBANK>    <DEP_ACNO>00123456789</DEP_ACNO>    <DEP_DATE>MM/dd/yyyy</DEP_DATE>    <DEP_RFNo>111111</DEP_RFNo>    <SUB_TRXN_T>Normal / S</SUB_TRXN_T>    <SIP_RFNO>22222222</SIP_RFNO>    <SIP_RGDT>MM/dd/yyyy</SIP_RGDT>    <KYC_FLG>Y / N</KYC_FLG>    <POA_STAT>Y</POA_STAT>    <MOD_TRXN>W</MOD_TRXN>    <SIGN_VF>Y</SIGN_VF>    <CUST_ID>236632</CUST_ID>    <LOG_WT>10022010$14:52:50$</LOG_WT>    <LOG_PE>ABC~0000~987654</LOG_PE>    <DPID></DPID>    <ClientID></ClientID>    <NRI_SOF>000011112222</NRI_SOF>    <GUARD_RELA>FATHER</GUARD_RELA>    <PAN_VALI>Y</PAN_VALI>    <GPAN_VALI>Y</GPAN_VALI>    <J1PAN_VALI>Y</J1PAN_VALI>    <J2PAN_VALI>Y</J2PAN_VALI>    <NRI_ADD1></NRI_ADD1>    <NRI_ADD2></NRI_ADD2>    <NRI_ADD3></NRI_ADD3>    <NRI_CITY></NRI_CITY>    <NRI_STATE></NRI_STATE>    <NRI_COUN></NRI_COUN>    <NRI_PIN></NRI_PIN>    <NOM_DOB>mm/dd/yyyy</NOM_DOB>    <NOM_NAME>SANTOSH SINHA</NOM_NAME>    <NOM_RELA>Son</NOM_RELA>    <NOM_PERC>33.3</NOM_PERC>    <NOM_ADD1>add1</NOM_ADD1>    <NOM_ADD2>add2</NOM_ADD2>    <NOM_ADD3>add3</NOM_ADD3>    <NOM_CITY>HYDEARBAD</NOM_CITY>    <NOM_STATE>AP</NOM_STATE>    <NOM_COUN>INDIA</NOM_COUN>    <NOM_PIN>123456</NOM_PIN>    <NOM_EMAIL>abc@domain.com</NOM_EMAIL>    <NOM_MOB>mm/dd/yyyy</NOM_MOB>    <NOM1_NAME>SUJATA SINHA</NOM1_NAME>    <NOM1_RELA>Son</NOM1_RELA>    <NOM1_DOB>mm/dd/yyyy</NOM1_DOB>    <NOM1_PERC>33.3</NOM1_PERC>    <NOM1_ADD1>add1</NOM1_ADD1>    <NOM1_ADD2>add2</NOM1_ADD2>    <NOM1_ADD3>add3</NOM1_ADD3>    <NOM1_CITY>HYDEARBAD</NOM1_CITY>    <NOM1_STATE>AP</NOM1_STATE>    <NOM1_COUN>INDIA</NOM1_COUN>    <NOM1_PIN>123456</NOM1_PIN>    <NOM1_EMAIL>abc@domain.com</NOM1_EMAIL>    <NOM1_MOB>mm/dd/yyyy</NOM1_MOB>    <NOM2_NAME>SANTOSH SINHA</NOM2_NAME>    <NOM2_RELA>Son</NOM2_RELA>    <NOM2_DOB>mm/dd/yyyy</NOM2_DOB>    <NOM2_PERC>33.3</NOM2_PERC>    <NOM2_ADD1>add1</NOM2_ADD1>    <NOM2_ADD2>add2</NOM2_ADD2>    <NOM2_ADD3>add3</NOM2_ADD3>    <NOM2_CITY>HYDEARBAD</NOM2_CITY>    <NOM2_STATE>AP</NOM2_STATE>    <NOM2_COUN>INDIA</NOM2_COUN>    <NOM2_PIN>123456</NOM2_PIN>    <NOM2_EMAIL>abc@domain.com</NOM2_EMAIL>    <NOM2_MOB>1234567890</NOM2_MOB>    <FIRC_STA>mm/dd/yyyy</FIRC_STA>    <SIP_STDT>mm/dd/yyyy</SIP_STDT>    <SIP_ENDT>mm/dd/yyyy</SIP_ENDT>    <SIP_NOINST>12</SIP_NOINST>    <SIP_FREQ>M</SIP_FREQ>    <EUIN>E145456</EUIN>    <EUIN_OPTIN>Y</EUIN_OPTIN>    <E1></E1>    <E2></E2>    <E3></E3>    <E4></E4>    <E5></E5>    <E6></E6>  </ReverseDetails><ReverseDetails>    <PortRemarks>Invalid / Mismatch Schemecode for User Tr No: 1234</PortRemarks>    <AMC_CODE>ABC</AMC_CODE>    <BROKE_CD>ARN-0000</BROKE_CD>    <SBBR_CODE>1234</SBBR_CODE>    <USER_CODE>Testing</USER_CODE>    <USR_TXN_NO>12345</USR_TXN_NO>    <APPL_NO>A1234</APPL_NO>    <FOLIO_NO>12345678901</FOLIO_NO>    <CK_DIG_NO></CK_DIG_NO>    <TRXN_TYPE>P</TRXN_TYPE>    <SCH_CODE>GFGP</SCH_CODE>    <FIRST_NAME>Name</FIRST_NAME>    <JONT_NAME1>Joint Name 1</JONT_NAME1>    <JONT_NAME2>Joint Name 2</JONT_NAME2>    <ADD1>House No. etc</ADD1>    <ADD2>Street Name</ADD2>    <ADD3>Area Name</ADD3>    <CITY>City Name</CITY>    <PINCODE>Pin Code</PINCODE>    <PHONE_OFF>02212345678</PHONE_OFF>    <MOBILE_NO></MOBILE_NO>    <TRXN_DATE>MM/dd/yyyy</TRXN_DATE>    <TRXN_TIME>13:14:21</TRXN_TIME>    <UNITS></UNITS>    <AMOUNT>5000.00</AMOUNT>    <CLOS_AC_CH></CLOS_AC_CH>    <DOB>MM/dd/yyyy</DOB>    <GUARDIAN></GUARDIAN>    <TAX_NUMBER>1st Holder Pan</TAX_NUMBER>    <PHONE_RES>02287654321</PHONE_RES>    <FAX_OFF>02223456789</FAX_OFF>    <FAX_RES>02298765432</FAX_RES>    <EMAIL>email@email.com</EMAIL>    <ACCT_NO>0012345678901</ACCT_NO>    <ACCT_TYPE>SB</ACCT_TYPE>    <BANK_NAME>Bank</BANK_NAME>    <BR_NAME>Street Name etc</BR_NAME>    <BANK_CITY>City Name</BANK_CITY>    <REINV_TAG>Z</REINV_TAG>    <HOLD_NATUR>JO </HOLD_NATUR>    <OCC_CODE>1</OCC_CODE>    <TAX_STATUS>01</TAX_STATUS>    <REMARKS></REMARKS>    <STATE></STATE>    <PAN_2_HLDR>2nd Holder PAN</PAN_2_HLDR>    <PAN_3_HLDR>3rd Holder PAN</PAN_3_HLDR>    <Guard_PAN>3rd Holder PAN</Guard_PAN>    <LOCATION>M4</LOCATION>    <UINno></UINno>    <FORM6061></FORM6061>    <FORM6061J1></FORM6061J1>    <FORM6061J2></FORM6061J2>    <PAY_MEC></PAY_MEC>    <RTGS_CD></RTGS_CD>    <NEFT_CD></NEFT_CD>    <MICR_CD></MICR_CD>    <DEPBANK>Bank Name</DEPBANK>    <DEP_ACNO>00123456789</DEP_ACNO>    <DEP_DATE>MM/dd/yyyy</DEP_DATE>    <DEP_RFNo>111111</DEP_RFNo>    <SUB_TRXN_T>Normal / S</SUB_TRXN_T>    <SIP_RFNO>22222222</SIP_RFNO>    <SIP_RGDT>MM/dd/yyyy</SIP_RGDT>    <KYC_FLG>Y / N</KYC_FLG>    <POA_STAT>Y</POA_STAT>    <MOD_TRXN>W</MOD_TRXN>    <SIGN_VF>Y</SIGN_VF>    <CUST_ID>236632</CUST_ID>    <LOG_WT>10022010$14:52:50$</LOG_WT>    <LOG_PE>ABC~0000~987654</LOG_PE>    <DPID></DPID>    <ClientID></ClientID>    <NRI_SOF>000011112222</NRI_SOF>    <GUARD_RELA>FATHER</GUARD_RELA>    <PAN_VALI>Y</PAN_VALI>    <GPAN_VALI>Y</GPAN_VALI>    <J1PAN_VALI>Y</J1PAN_VALI>    <J2PAN_VALI>Y</J2PAN_VALI>    <NRI_ADD1></NRI_ADD1>    <NRI_ADD2></NRI_ADD2>    <NRI_ADD3></NRI_ADD3>    <NRI_CITY></NRI_CITY>    <NRI_STATE></NRI_STATE>    <NRI_COUN></NRI_COUN>    <NRI_PIN></NRI_PIN>    <NOM_DOB>mm/dd/yyyy</NOM_DOB>    <NOM_NAME>SANTOSH SINHA</NOM_NAME>    <NOM_RELA>Son</NOM_RELA>    <NOM_PERC>33.3</NOM_PERC>    <NOM_ADD1>add1</NOM_ADD1>    <NOM_ADD2>add2</NOM_ADD2>    <NOM_ADD3>add3</NOM_ADD3>    <NOM_CITY>HYDEARBAD</NOM_CITY>    <NOM_STATE>AP</NOM_STATE>    <NOM_COUN>INDIA</NOM_COUN>    <NOM_PIN>123456</NOM_PIN>    <NOM_EMAIL>abc@domain.com</NOM_EMAIL>    <NOM_MOB>mm/dd/yyyy</NOM_MOB>    <NOM1_NAME>SUJATA SINHA</NOM1_NAME>    <NOM1_RELA>Son</NOM1_RELA>    <NOM1_DOB>mm/dd/yyyy</NOM1_DOB>    <NOM1_PERC>33.3</NOM1_PERC>    <NOM1_ADD1>add1</NOM1_ADD1>    <NOM1_ADD2>add2</NOM1_ADD2>    <NOM1_ADD3>add3</NOM1_ADD3>    <NOM1_CITY>HYDEARBAD</NOM1_CITY>    <NOM1_STATE>AP</NOM1_STATE>    <NOM1_COUN>INDIA</NOM1_COUN>    <NOM1_PIN>123456</NOM1_PIN>    <NOM1_EMAIL>abc@domain.com</NOM1_EMAIL>    <NOM1_MOB>mm/dd/yyyy</NOM1_MOB>    <NOM2_NAME>SANTOSH SINHA</NOM2_NAME>    <NOM2_RELA>Son</NOM2_RELA>    <NOM2_DOB>mm/dd/yyyy</NOM2_DOB>    <NOM2_PERC>33.3</NOM2_PERC>    <NOM2_ADD1>add1</NOM2_ADD1>    <NOM2_ADD2>add2</NOM2_ADD2>    <NOM2_ADD3>add3</NOM2_ADD3>    <NOM2_CITY>HYDEARBAD</NOM2_CITY>    <NOM2_STATE>AP</NOM2_STATE>    <NOM2_COUN>INDIA</NOM2_COUN>    <NOM2_PIN>123456</NOM2_PIN>    <NOM2_EMAIL>abc@domain.com</NOM2_EMAIL>    <NOM2_MOB>1234567890</NOM2_MOB>    <FIRC_STA>mm/dd/yyyy</FIRC_STA>    <SIP_STDT>mm/dd/yyyy</SIP_STDT>    <SIP_ENDT>mm/dd/yyyy</SIP_ENDT>    <SIP_NOINST>12</SIP_NOINST>    <SIP_FREQ>M</SIP_FREQ>    <EUIN>E145456</EUIN>    <EUIN_OPTIN>Y</EUIN_OPTIN>    <E1></E1>    <E2></E2>    <E3></E3>    <E4></E4>    <E5></E5>    <E6></E6>  </ReverseDetails> </Services>";

		ProcessBL pl = new ProcessBL();
		
		/*try {
			pl.uploadXML_ToKARVY(1, "/data/WIFS_LOGS/JBoss/wifs/DBFForwardFeed", null, null);
		} catch (Exception ex) {
			System.out.println(ex);
		}*/

		try {
		 DirectUpload_Response_XML_BO bo = pl.generate_DirectUpload_Response_XML_BO(responseString);
		 
		 for (ReverseDetails rd : bo.reverseDetails){
		 System.out.println("USR TXN NO: "+rd.getUSR_TXN_NO()); }
		}catch (Exception ex){System.out.println(ex);}

		/*
		 * DirectUploadXML_BO directUploadXML_BO =
		 * pl.loadDirectUploadXML_BO(transactionMainBO_List) File xmlFile =
		 * pl.generate_DirectUploadXML_File( directUploadXML_BO);
		 */

	}
        
    private static String stpUpload(String fundCode, String userID, String password, String branch, String arnCode, String ipAddress, String xmlFile) {
        STPService service = new STPService();
        ISTPService port = service.getBasicHttpBindingISTPService();
        return port.stpUpload(fundCode, userID, password, branch, arnCode, ipAddress, xmlFile);
    }

	/**
	 * Direct upload forward feed as XML file to KARVY server
	 * 
	 * @param uploadUserId
	 * @param outputXML_FilePath
	 * @param transactionMainBO_Hash
	 * @param pstmForwardFeedUpdate
	 * @throws Exception
	 */
	public void uploadXML_ToKARVY(
			int uploadUserId,
			String outputXML_FilePath,
			Hashtable<String, ArrayList<TransactionMainBO>> transactionMainBO_Hash,
			PreparedStatement pstmForwardFeedUpdate) throws Exception {

		logger.info("## -- START - Direct upload to KARVY server -- #");

		try {

			// Validations
			if (transactionMainBO_Hash == null
					|| transactionMainBO_Hash.size() < 1
					|| pstmForwardFeedUpdate == null)
				throw new Exception("Insufficient input values");

			String uploadXML_FilePath = "";

			logger.info("# -- START - GENERATE XML -- #");
			try {

				// Generate XML Files
				Enumeration<String> hashKeys = transactionMainBO_Hash.keys();

				while (hashKeys.hasMoreElements()) {
                                    int checkPoint = 0;

					String key = hashKeys.nextElement(); // Main BO Hash key, value will be like RTCode@AMCCode

					ArrayList<TransactionMainBO> transactionMainBO_List = transactionMainBO_Hash.get(key);

					if (transactionMainBO_List != null
							&& transactionMainBO_List.size() > 0) {

						String xmlFile_String = generate_DirectUploadXML(
								outputXML_FilePath, key,
								transactionMainBO_List, pstmForwardFeedUpdate);
                                                logger.info("# -- START - Upload to server -- #");
			
// Upload to KARVY server
				if (!ConstansBO.isEmpty(xmlFile_String)){
checkPoint = 1;
                                String response = "";
				//ArrayList<TransactionMainBO> transactionMainBO_List = null;

				try {
						// Get configuration
						PropertyFileReader propertyFileReader = new PropertyFileReader();
						Properties properties = propertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);

						String url = properties.getProperty("KARVY.DIRECT.FEED.URL");
						String fundCode = key.substring(key.indexOf("@")+1, key.length()), 
								branch = properties.getProperty("KARVY.DIRECT.FEED.BRANCH"), 
								arnCode = properties.getProperty("KARVY.DIRECT.FEED.ARNCODE"), 
								auth_UserId = properties.getProperty("KARVY.DIRECT.FEED.LOGIN"), 
								auth_Password = properties.getProperty("KARVY.DIRECT.FEED.PASSWORD"), 
                                                                ipAddress = "";// Since IP address is optional

						response = stpUpload(fundCode, auth_UserId, auth_Password, branch, arnCode, ipAddress, xmlFile_String);

						logger.info("RESPONSE: " + response);
						checkPoint = 2;

				} catch (Exception iblEx2) {
					logger.error("iBL - DIRECT TO KARVY - UPLOAD FILE. $ FILE: " + uploadXML_FilePath + " # EX: " + iblEx2);

				}
                                
				// To roll back all changes
				if (checkPoint != 2){
                                        response = ""
                                                + "<?xml version=\"1.0\" encoding=\"utf-8\"?> "
                                                + "<Services> "
                                                + "  <Information> "
                                                + "    <Return_Code>-1</Return_Code> "
                                                + "    <Return_Msg>Message from FundsIndia server - Error in upload KARVY server</Return_Msg> "
                                                + "  </Information> "
                                                + "  <ReverseDetails>"
                                                + "  </ReverseDetails>"
                                                + "</Services>";

				logger.info("Failed FI RESPONSE: " + response);
                                }

				if (!ConstansBO.isEmpty(response)) {
					try {
						doResponseProcess_ForwardFeed(uploadUserId,
								response,
								transactionMainBO_List);

					} catch (Exception iblEx3) {
						logger.error("iBL - DIRECT TO KARVY - RESPONSE PROCESS. EX: "+ iblEx3);
					}
				}
                                }
			}

			logger.info("# -- END - Upload to server -- #");
					}
				
			} catch (Exception iblEx1) {
				logger.error("iBL - DIRECT TO KARVY - XML GENRATE. $ FILE: "
						+ uploadXML_FilePath + " # EX: " + iblEx1);
			}
			logger.info("# -- END - GENERATE XML -- #");

			

		} catch (Exception blEx) {
			logger.error("BL - DIRECT TO KARVY - $ PATH: " + outputXML_FilePath + " # EX: " + blEx);
		}

		logger.info("## -- END - Direct upload to KARVY server -- #");
	}

	/**
	 * Load 'DirectUploadXML_BO' from transaction details BO & update forward
	 * feed flag in DB
	 * 
	 * @param transactionMainBO_List
	 * @param pstmForwardFeedUpdate
	 * @return
	 */
	public DirectUploadXML_BO loadDirectUploadXML_BO(
			ArrayList<TransactionMainBO> transactionMainBO_List,
			PreparedStatement pstmForwardFeedUpdate) {

		DirectUploadXML_BO returnBO = null;

		try {

			ArrayList<RequestDetails> requestDetails_List = new ArrayList<RequestDetails>();

			for (TransactionMainBO transactionMainBO : transactionMainBO_List) {

				try {

					RequestDetails requestDetails = new RequestDetails();
					TransactionBO transactionBO = transactionMainBO.getTransactionBO();
					InvestorDetailsBO investorDetailsBO = transactionMainBO.getInvestorDetailsBO();
					AccountDetailsBO accountDetailsBO = transactionMainBO.getAccountDetailsBO();
					String userTransactionNo = transactionBO.getUserTransactionNo();

					requestDetails.setAMC_CODE(transactionBO.getAmcCode());
					requestDetails.setBROKE_CD("ARN-69583");// Broker Code
					requestDetails.setSBBR_CODE(investorDetailsBO.getSubBrokerCode());
					requestDetails.setUSER_CODE(investorDetailsBO.getUserCode());
					requestDetails.setUSR_TXN_NO(userTransactionNo);
					requestDetails.setAPPL_NO(transactionBO.getApplNo());
					requestDetails.setFOLIO_NO(ConstansBO.NullCheck(transactionBO.getFolioNo()));
					requestDetails.setCK_DIG_NO(ConstansBO.NullCheck(transactionBO.getCk_Dig_No())); // Cheque digit No
					requestDetails.setTRXN_TYPE(transactionBO.getTransactionType());
					requestDetails.setSCH_CODE(transactionBO.getSchemeCode());
					requestDetails.setFIRST_NAME(investorDetailsBO.getFirstName());
					requestDetails.setJONT_NAME1(investorDetailsBO.getJointName1());
					requestDetails.setJONT_NAME2(investorDetailsBO.getJointName2());
					requestDetails.setADD1(investorDetailsBO.getAddress1());
					requestDetails.setADD2(investorDetailsBO.getAddress2());
					requestDetails.setADD3(investorDetailsBO.getAddress3());
					requestDetails.setCITY(investorDetailsBO.getCity());
					requestDetails.setPINCODE(investorDetailsBO.getPinCode());
					requestDetails.setPHONE_OFF(investorDetailsBO.getPhoneOffice());
					requestDetails.setMOBILE_NO(investorDetailsBO.getMobileNo());
					requestDetails.setTRXN_DATE(transactionBO.getTransactionDate().getMonthFirstViewRequiredFormat());
					requestDetails.setTRXN_TIME(transactionBO.getTransactionTime());

					String units_string = "", amount_string = "", clos_Ac_Ch = "N";
					if (!accountDetailsBO.getClos_Ac_Ch().equals("Y")) {
						units_string = String.valueOf(transactionBO.getUnits());
						amount_string = String.valueOf(transactionBO
								.getAmount());
                                                clos_Ac_Ch = "Y";
					}
					requestDetails.setUNITS(units_string);
					requestDetails.setAMOUNT(amount_string);

					requestDetails.setCLOS_AC_CH(clos_Ac_Ch);
					requestDetails.setDOB(investorDetailsBO.getDateOfBirth().getMonthFirstViewRequiredFormat());
					requestDetails.setGUARDIAN(investorDetailsBO.getGuardianName());
					requestDetails.setTAX_NUMBER(investorDetailsBO.getTaxNo());
					requestDetails.setPHONE_RES(investorDetailsBO.getPhoneRes());
					requestDetails.setFAX_OFF(investorDetailsBO.getFaxOff());
					requestDetails.setFAX_RES(investorDetailsBO.getFaxRes());
					requestDetails.setEMAIL(investorDetailsBO.getEmail());
					requestDetails.setACCT_NO(accountDetailsBO.getAccountNo());
					requestDetails.setACCT_TYPE(accountDetailsBO.getAccountType());
					requestDetails.setBANK_NAME(accountDetailsBO.getBankName());
					requestDetails.setBR_NAME(accountDetailsBO.getBranchName());
					requestDetails.setBANK_CITY(accountDetailsBO.getBankCity());
					requestDetails.setREINV_TAG(accountDetailsBO.getReinv_Tag());
					requestDetails.setHOLD_NATUR(accountDetailsBO.getHoldingNature());
					requestDetails.setOCC_CODE(investorDetailsBO.getOccupationCode());
					requestDetails.setTAX_STATUS(accountDetailsBO.getTaxStatus());
					requestDetails.setREMARKS(transactionBO.getPaymentId() + " " + accountDetailsBO.getRemarks());
					requestDetails.setSTATE(investorDetailsBO.getState());
					requestDetails.setPAN_2_HLDR(investorDetailsBO.getPanHolder2());
					requestDetails.setPAN_3_HLDR(investorDetailsBO.getPanHolder3());
					requestDetails.setGUARD_PAN(investorDetailsBO.getGuardianPanNo());
					requestDetails.setLOCATION(investorDetailsBO.getLocationCode());
					requestDetails.setUINNO(accountDetailsBO.getUINNo());
					//requestDetails.setFORM6061(accountDetailsBO.getForm6061());
					//requestDetails.setFORM6061J1(accountDetailsBO.getForm6061J1());
					//requestDetails.setFORM6061J2(accountDetailsBO.getForm6061J2());
					requestDetails.setPAY_MEC("D");//(accountDetailsBO.getPaymentMechanism());
					requestDetails.setRTGS_CD(accountDetailsBO.getRTGS_CD());
					requestDetails.setNEFT_CD(accountDetailsBO.getNEFT_CD());
					requestDetails.setMICR_CD(accountDetailsBO.getMICR_CD());
					requestDetails.setDEPBANK(accountDetailsBO.getDepBankName());
					requestDetails.setDEP_ACNO(accountDetailsBO.getDepAccountNo());
					requestDetails.setDEP_DATE(accountDetailsBO.getDepDate().getMonthFirstViewRequiredFormat());
					requestDetails.setDEP_RFNO(accountDetailsBO.getDepReferenceNo());
					requestDetails.setSUB_TRXN_T(transactionBO.getSubTransactionType());

					String sipReferenceId = "", sipRegistrationDate = "";
					if (!transactionBO.getSipReferenceId().equals("0"))
						sipReferenceId = transactionBO.getSipReferenceId();
					if (transactionBO.getSIP_Reg_Date() != null)
						sipRegistrationDate = transactionBO.getSIP_Reg_Date().getMonthFirstViewRequiredFormat();
					requestDetails.setSIP_RFNO(sipReferenceId);
					requestDetails.setSIP_RGDT(sipRegistrationDate);

					requestDetails.setKYC_FLG(investorDetailsBO.getKycFlag());
					requestDetails.setPOA_STAT("N");
					requestDetails.setMOD_TRXN("W");
					requestDetails.setSIGN_VF("Y");
					requestDetails.setCUST_ID(investorDetailsBO.getInvestorId());
					requestDetails.setLOG_WT(transactionBO.getTransactionDate()
							.getDateMonthView().replace("-", "")
							+ "$"
							+ transactionBO.getTransactionTime()
							+ "$"
							+ transactionBO.getIPAddress()
							+ "$"
							+ "72.32.69.244$" //Server Ip
                                                        + userTransactionNo);
					requestDetails.setLOG_PE("");
					requestDetails.setDPID(transactionBO.getDpId());
					requestDetails.setCLIENTID(transactionBO.getClientCode());

					String nriSOF = "";
					if ((accountDetailsBO.isNriInvestor())
							&& (transactionBO.getTransactionType().equals("P")))
						nriSOF = accountDetailsBO.getAccountNo();
					requestDetails.setNRI_SOF(nriSOF);

					requestDetails.setGUARD_RELA("");// ?
					requestDetails.setPAN_VALI("Y");//Shall be mandatory if PAN of the 1st holder is provided  ( Y is the allowed value to be populated )
					requestDetails.setGPAN_VALI("");// ?
					requestDetails.setJ1PAN_VALI("");// ?
					requestDetails.setJ2PAN_VALI("");// ?
					requestDetails.setNRI_ADD1(investorDetailsBO.getNRIAddress1());
					requestDetails.setNRI_ADD2(investorDetailsBO.getNRIAddress2());
					requestDetails.setNRI_ADD2(investorDetailsBO.getNRIAddress3());
					requestDetails.setNRI_CITY(investorDetailsBO.getNRICity());
					requestDetails.setNRI_STATE(investorDetailsBO.getNRIState());
					requestDetails.setNRI_COUN(investorDetailsBO.getNRICountry());
					requestDetails.setNRI_PIN(investorDetailsBO.getNRIPinCode());
					requestDetails.setNOM_DOB("");// ?
					requestDetails.setNOM_NAME(accountDetailsBO.getNomineeName());
					requestDetails.setNOM_RELA(accountDetailsBO.getNomineeRelation());
					requestDetails.setNOM_PERC(new Double(accountDetailsBO.getNomineePercent()).toString());
					requestDetails.setNOM_ADD1("");// ?
					requestDetails.setNOM_ADD2("");// ?
					requestDetails.setNOM_ADD3("");// ?
					requestDetails.setNOM_CITY("");// ?
					requestDetails.setNOM_STATE("");// ?
					requestDetails.setNOM_COUN("");// ?
					requestDetails.setNOM_PIN("");// ?
					requestDetails.setNOM_EMAIL("");// ?
					requestDetails.setNOM_MOB("");// ?
					requestDetails.setNOM1_NAME(accountDetailsBO.getNomineeName2());
					requestDetails.setNOM1_RELA(accountDetailsBO.getNomineeRelation2());
					requestDetails.setNOM1_DOB("");// ?
					requestDetails.setNOM1_PERC(new Double(accountDetailsBO.getNomineePercent2()).toString());
					requestDetails.setNOM1_ADD1("");// ?
					requestDetails.setNOM1_ADD2("");// ?
					requestDetails.setNOM1_ADD3("");// ?
					requestDetails.setNOM1_CITY("");// ?
					requestDetails.setNOM1_STATE("");// ?
					requestDetails.setNOM1_COUN("");// ?
					requestDetails.setNOM1_PIN("");// ?
					requestDetails.setNOM1_EMAIL("");// ?
					requestDetails.setNOM1_MOB("");// ?
					requestDetails.setNOM2_NAME(accountDetailsBO.getNomineeName3());
					requestDetails.setNOM2_RELA(accountDetailsBO.getNomineeRelation3());
					requestDetails.setNOM2_DOB("");// ?
					requestDetails.setNOM2_PERC(new Double(accountDetailsBO.getNomineePercent3()).toString());
					requestDetails.setNOM2_ADD1("");// ?
					requestDetails.setNOM2_ADD2("");// ?
					requestDetails.setNOM2_ADD3("");// ?
					requestDetails.setNOM2_CITY("");// ?
					requestDetails.setNOM2_STATE("");// ?
					requestDetails.setNOM2_COUN("");// ?
					requestDetails.setNOM2_PIN("");// ?
					requestDetails.setNOM2_EMAIL("");// ?
					requestDetails.setNOM2_MOB("");// ?
					requestDetails.setFIRC_STA("");// ?

					String sipStartDate = "", sipEndDate = "", sipFrequency = "";
					if (transactionBO.getSipStartDate() != null) {
						sipStartDate = transactionBO.getSipStartDate().getMonthFirstViewRequiredFormat();
						sipFrequency = new Double(transactionBO.getSipDate()).toString();
					}
					if (transactionBO.getSipEndDate() != null)
						sipEndDate = transactionBO.getSipEndDate().getMonthFirstViewRequiredFormat();
					requestDetails.setSIP_STDT(sipStartDate);
					requestDetails.setSIP_ENDT(sipEndDate);
					requestDetails.setSIP_NOINST(transactionBO.getNoOfInstalment());
					requestDetails.setSIP_FREQ(sipFrequency);

					requestDetails.setEUIN(transactionBO.getEuinId());
					requestDetails.setEUIN_OPTIN(transactionBO.getEuinOpted());
					requestDetails.setE1("");// ?
					requestDetails.setE1("");// ?
					requestDetails.setE2("");// ?
					requestDetails.setE3("");// ?
					requestDetails.setE4("");// ?
					requestDetails.setE5("");// ?
					requestDetails.setE6("");// ?

					requestDetails_List.add(requestDetails);

					// Based on the UserTransactionNo Update the ForwardFeed - date
					pstmForwardFeedUpdate.setString(1, userTransactionNo);
					pstmForwardFeedUpdate.addBatch();

				} catch (Exception iBlEx) {
					logger.error("i-BL - LOAD DirectUploadXML_BO $ UTXN: "
							+ transactionMainBO.getTransactionBO().getUserTransactionNo() + " # EX: " + iBlEx);
				}
			}

			int insertedRows[] = pstmForwardFeedUpdate.executeBatch();
			logger.info("Update in DB. UPDATE: " + Arrays.toString(insertedRows));
			pstmForwardFeedUpdate.clearBatch();

			if (requestDetails_List.size() > 0) {
				returnBO = new DirectUploadXML_BO();
				returnBO.setRequestDetails(requestDetails_List);
			}

		} catch (Exception blEx) {
			logger.error("BL - LOAD DirectUploadXML_BO $ SZ: "
					+ transactionMainBO_List.size() + " # EX: " + blEx);
		}
		return returnBO;
	}

	/**
	 * Generate XML file (To upload directly to KARVY server)
	 * 
	 * @param outputFilePath
	 * @param fileName_Prefix - Main BO Hash key, value will be like RTCode@AMCCode
	 * @param transactionMainBO_List
	 * @param pstmForwardFeedUpdate
	 * @return
	 */
	public String generate_DirectUploadXML(String outputFilePath,
			String fileName_Prefix,
			ArrayList<TransactionMainBO> transactionMainBO_List,
			PreparedStatement pstmForwardFeedUpdate) {

		String returnString = null;

		try {

			// Load XML parsing BO
			DirectUploadXML_BO directUploadXML_BO = loadDirectUploadXML_BO(
					transactionMainBO_List, pstmForwardFeedUpdate);

			if (directUploadXML_BO != null) {
				
				if (! new File(outputFilePath).isDirectory())
					new File(outputFilePath).mkdir();

				// fileName will be like RTCode@AMCCode_KARVY_XML_yyyyMMddHHmmss.xml
				String fileName = fileName_Prefix + "_KARVY_XML_" + ConstansBO.getTodayDate_static("yyyyMMddHHmmss") + ".xml";
				File toBackUp_File = new File(outputFilePath + File.separator + fileName);
				JAXBContext jaxbContext = JAXBContext.newInstance(DirectUploadXML_BO.class);
				Marshaller marshaller = jaxbContext.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

				// Write to File
				marshaller.marshal(directUploadXML_BO, toBackUp_File);
                                logger.info("Upload XML back up file generated. File: " + toBackUp_File.getAbsolutePath());
                                
                                //BO data to XML String
                                StringWriter sw = new StringWriter();
                                marshaller.marshal(directUploadXML_BO, sw);
                                returnString = sw.toString();
			}

		} catch (Exception blEx) {
			logger.error("BL - GENERATE XML EX: " + blEx);
		}
		return returnString;
	}

	/**
	 * Load DirectUpload_Response_XML_BO from the response string
	 * 
	 * @param responseString
	 * @return
	 */
	public DirectUpload_Response_XML_BO generate_DirectUpload_Response_XML_BO(
			String responseString) throws Exception {

		DirectUpload_Response_XML_BO returnBO = null;

		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(DirectUpload_Response_XML_BO.class);
			StringReader reader = new StringReader(responseString);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			returnBO = (DirectUpload_Response_XML_BO) unmarshaller.unmarshal(reader);

		} catch (Exception blEx) {
			logger.error("BL LOAD DirectUpload_Response_XML_BO - EX: " + blEx);
			throw blEx;
		}
		return returnBO;
	}

	/**
	 * Process the response, from direct upload to KARVY server. If any upload
	 * error this will roll back DB forward feed changes. Roll back will be done
	 * record wise or entire XML file, according to response.
	 * 
	 * @param uploadUserId
	 * @param response
	 * @param transactionMainBO_List
	 * @throws Exception
	 */
	public void doResponseProcess_ForwardFeed(int uploadUserId,
			String response, ArrayList<TransactionMainBO> transactionMainBO_List)
			throws Exception {

		logger.info("## -- START - KARVY server response -- #");
                BackgroundThreadProcess backgroundThread = new BackgroundThreadProcess();


		try {

			boolean isRevertAllFeed = false, isErrorRecords = false;
                        String revertAll_Reason = "";
			DirectUpload_Response_XML_BO responseBO = null;

			try {
				// Parse response XML & Check if error is available
				responseBO = generate_DirectUpload_Response_XML_BO(response);

				if (Integer.valueOf(responseBO.information.getReturn_Code()) != 0){
					isRevertAllFeed = true;
                                        revertAll_Reason = responseBO.information.getReturn_Msg();
                                }
				else if (responseBO.reverseDetails.size() > 0)
					isErrorRecords = true;
			} catch (Exception iblEx) {

				isRevertAllFeed = true; // If no 'Return_Code' tag, all feed is failed.
			}

			logger.info("isRevertAllFeed: " + isRevertAllFeed + " | isErrorRecords: " + isErrorRecords);

			if (isRevertAllFeed || isErrorRecords) {

				ArrayList<STPProcessBO> feedFailedList = new ArrayList<STPProcessBO>();
                                //ArrayList<Integer> feedSuccessList_Int = new ArrayList<Integer>();
                                ArrayList<STPProcessBO> feedSuccessList = new ArrayList<STPProcessBO>();
                                
                                for (TransactionMainBO transactionMainBO : transactionMainBO_List) {

                                try {
                                    feedSuccessList.add(new STPProcessBO(
                                            Integer.valueOf(transactionMainBO.getTransactionBO().getUserTransactionNo()), 
                                            revertAll_Reason));// !isRevertAllFeed then revertAll_Reason will be blank

                                } catch (Exception iBlEx2) {
                                    logger.error("iBL2 - FEED ALL FAILED USER TRANS REF ID # EX: "
                                            + iBlEx2);
                                }
                            }

				// Segregate feed failed/error user transaction reference id
				if (isErrorRecords) {
					for (ReverseDetails reverseDetails : responseBO.reverseDetails) {
						try {
							int userTransRefId = Integer.valueOf(reverseDetails.getUSR_TXN_NO());
                                                        String message = reverseDetails.getPortRemarks();
							feedFailedList.add(new STPProcessBO(userTransRefId, message));
                                                        feedSuccessList.remove(new STPProcessBO(userTransRefId, ""));

							logger.info("USR_TXN_NO: " + userTransRefId);
						} catch (Exception iBlEx2) {
							logger.error("iBL2 - FEED FAILED USER TRANS REF ID # EX: " + iBlEx2);
						}
					}
				} else if (isRevertAllFeed) {
					// Since entire feed file upload is fail
                                        feedFailedList.addAll(feedSuccessList);
                                        feedSuccessList = new ArrayList<STPProcessBO>();
				}

				logger.info("Feed failed User transaction ref id loaded in LIST. Size: " + feedFailedList.size());
                                
				// Update DB
				if (feedFailedList.size() > 0) {
					DBFFilesDAO dbfFilesDAO = new DBFFilesDAO();
					dbfFilesDAO.updateSTPForwardFeedStatus(uploadUserId, feedSuccessList, feedFailedList);
				}
//                            bkThreadProc.failedList = feedFailedList;
//                            backgroundThread.startKarvyInsertThread(bkThreadProc.hashTransactionMainBO, bkThreadProc.failedList);
			}
		} catch (Exception blEx) {
			logger.error("BL - FORWARD FEED RESPONSE PROCESS $ RESPONSE: "
					+ response + "EX: " + blEx);
		}
		logger.info("## -- END - KARVY server response -- #");
	}
}