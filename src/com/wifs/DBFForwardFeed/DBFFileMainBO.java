package com.wifs.DBFForwardFeed; 

import java.util.ArrayList;
import java.util.Hashtable;

public class DBFFileMainBO {
	
	private String dailyReportContent	=	"";
	private String fDate	=	"";
	private String tDate	=	"";
	private String ftNewFolioUserTransRefId	=	"";
	
	private double purchase	=	0.0d;
	private double addPurchase	=	0.0d;
	private double redemption	=	0.0d;
	private double switchTrxn	=	0.0d;
	
	private Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
	private Hashtable<String, ArrayList<TransactionMainBO>> hashMISReportTransactionBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
	//private Hashtable<String, ArrayList<TransactionMainBO>> hashMFUpfrontTrialTransactionBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
	private Hashtable<String, ArrayList<TransactionMainBO>> hashKarvyTransactionMainSBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
	private Hashtable<String, ArrayList<TransactionMainBO>> hashirTransactionMainSBO	=	new Hashtable<String, ArrayList<TransactionMainBO>>();
	private Hashtable<String, ArrayList<SIPInfoBO>> hashSIPInfoBO	=	new Hashtable<String, ArrayList<SIPInfoBO>>();
	private Hashtable<String, NewFolioBO> hashNewFolioBO=	new Hashtable<String, NewFolioBO>();
	Hashtable<Integer, ArrayList<TransactionMainBO>> hashTransactionFatcaBO	=	new Hashtable<Integer, ArrayList<TransactionMainBO>>();
        private ArrayList<ReportBO> reportBO_List;
    private Hashtable<String,InvestorDetailsBO> hashCKCDetails	=	new Hashtable<String,InvestorDetailsBO>();
	
	public Hashtable<String, ArrayList<TransactionMainBO>> getHashTransactionMainBO() {
		return hashTransactionMainBO;
	}
	public void setHashTransactionMainBO(Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO) {
		this.hashTransactionMainBO = hashTransactionMainBO;
	}
	public Hashtable<String, ArrayList<SIPInfoBO>> getHashSIPInfoBO() {
		return hashSIPInfoBO;
	}
	public double getPurchase() {
		return purchase;
	}
	public void setPurchase(double purchase) {
		this.purchase = purchase;
	}
	public double getAddPurchase() {
		return addPurchase;
	}
	public void setAddPurchase(double addPurchase) {
		this.addPurchase = addPurchase;
	}
	public double getRedemption() {
		return redemption;
	}
	public void setRedemption(double redemption) {
		this.redemption = redemption;
	}
	public double getSwitchTrxn() {
		return switchTrxn;
	}
	public void setSwitchTrxn(double switchTrxn) {
		this.switchTrxn = switchTrxn;
	}
	public void setHashSIPInfoBO(Hashtable<String, ArrayList<SIPInfoBO>> hashSIPInfoBO) {
		this.hashSIPInfoBO = hashSIPInfoBO;
	}
	public Hashtable<String, ArrayList<TransactionMainBO>> getHashKarvyTransactionMainSBO() {
		return hashKarvyTransactionMainSBO;
	}
	public void setHashKarvyTransactionMainSBO(
			Hashtable<String, ArrayList<TransactionMainBO>> hashKarvyTransactionMainSBO) {
		this.hashKarvyTransactionMainSBO = hashKarvyTransactionMainSBO;
	}
	public Hashtable<String, ArrayList<TransactionMainBO>> getHashMISReportTransactionBO() {
		return hashMISReportTransactionBO;
	}
	public void setHashMISReportTransactionBO(
			Hashtable<String, ArrayList<TransactionMainBO>> hashMISReportTransactionBO) {
		this.hashMISReportTransactionBO = hashMISReportTransactionBO;
	}
	/*public Hashtable<String, ArrayList<TransactionMainBO>> getHashMFUpfrontTrialTransactionBO() {
		return hashMFUpfrontTrialTransactionBO;
	}
	public void setHashMFUpfrontTrialTransactionBO(
			Hashtable<String, ArrayList<TransactionMainBO>> hashMFUpfrontTrialTransactionBO) {
		this.hashMFUpfrontTrialTransactionBO = hashMFUpfrontTrialTransactionBO;
	}*/
	public Hashtable<String, NewFolioBO> getHashNewFolioBO() {
		return hashNewFolioBO;
	}
	public void setHashNewFolioBO(Hashtable<String, NewFolioBO> hashNewFolioBO) {
		this.hashNewFolioBO = hashNewFolioBO;
	}
	public String getDailyReportContent() {
		return dailyReportContent;
	}
	public void setDailyReportContent(String dailyReportContent) {
		this.dailyReportContent = dailyReportContent;
	}
	public String getFDate() {
		return fDate;
	}
	public void setFDate(String date) {
		fDate = date;
	}
	public String getTDate() {
		return tDate;
	}
	public void setTDate(String date) {
		tDate = date;
	}
	public Hashtable<String, ArrayList<TransactionMainBO>> getHashirTransactionMainSBO() {
		return hashirTransactionMainSBO;
	}
	public void setHashirTransactionMainSBO(
			Hashtable<String, ArrayList<TransactionMainBO>> hashirTransactionMainSBO) {
		this.hashirTransactionMainSBO = hashirTransactionMainSBO;
	}
	public String getFtNewFolioUserTransRefId() {
		return ftNewFolioUserTransRefId;
	}
	public void setFtNewFolioUserTransRefId(String ftNewFolioUserTransRefId) {
		this.ftNewFolioUserTransRefId = ftNewFolioUserTransRefId;
	}
	public Hashtable<Integer, ArrayList<TransactionMainBO>> getHashTransactionFatcaBO() {
		return hashTransactionFatcaBO;
	}
	public void setHashTransactionFatcaBO(
			Hashtable<Integer, ArrayList<TransactionMainBO>> hashTransactionFatcaBO) {
		this.hashTransactionFatcaBO = hashTransactionFatcaBO;
	}
    public ArrayList<ReportBO> getReportBO_List() {
        return reportBO_List;
    }

    public void setReportBO_List(ArrayList<ReportBO> reportBO_List) {
        this.reportBO_List = reportBO_List;
    }
	public Hashtable<String, InvestorDetailsBO> getHashCKCDetails() {
		return hashCKCDetails;
	}
	public void setHashCKCDetails(
			Hashtable<String, InvestorDetailsBO> hashCKCDetails) {
		this.hashCKCDetails = hashCKCDetails;
	}
	
}