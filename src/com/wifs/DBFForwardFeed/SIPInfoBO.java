package com.wifs.DBFForwardFeed;

public class SIPInfoBO {
	
	private String fundCode	=	"";
	private String schemeCode	=	"";
	private String option		=	"";
	//private String accountNo		=	"";
	private String investorName	=	"";
	private String frequency		=	"";
	private ConstansBO StartMonth	=	new ConstansBO("");
	private ConstansBO EndMonth		=	new ConstansBO("");
	private int noOfInstalments	=	0;
	private double amount		=	0.0d;
	private String SIPRefNo			=	"";
	//private String applicationNo	=	"";
	//private String bankName		=	"";
	private ConstansBO SIPDueDate		=	new ConstansBO("");
	//private String nomineeName	=	"";
	//private String nomineeRelation	=	"";
	//private ConstansBO dateOfBirth	=	new ConstansBO("");
	//private String insuranceFlag	=	"";
	private String pan				=	"";
	private String documentCategory	=	"";
	//private String documentRefNo	=	"";
	private String remarks			=	"";
	private String depAccountNo		=	"";
	private String depBankName		=	"";
	private String investorBankName	=	"";
	private String accountNo		=	"";
	private String bankName			=	"";
	private String accountType		=	"";
	private String folioNumber		=	"";
	private String brokerCode		=	"";
	private String subBrokerCode	=	"";
	private boolean SIPTransaction = false; 
	private String sipType			=	"N";
	
	
	
	public String getBrokerCode() {
		return brokerCode;
	}
	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	public String getSubBrokerCode() {
		return subBrokerCode;
	}
	public void setSubBrokerCode(String subBrokerCode) {
		this.subBrokerCode = subBrokerCode;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getSchemeCode() {
		return schemeCode;
	}
	public void setSchemeCode(String schemeCode) {
		this.schemeCode = schemeCode;
	}
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	/*public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}*/
	public String getInvestorName() {
		return investorName;
	}
	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public ConstansBO getStartMonth() {
		return StartMonth;
	}
	public void setStartMonth(ConstansBO startMonth) {
		StartMonth = startMonth;
	}
	public ConstansBO getEndMonth() {
		return EndMonth;
	}
	public void setEndMonth(ConstansBO endMonth) {
		EndMonth = endMonth;
	}
	public int getNoOfInstalments() {
		return noOfInstalments;
	}
	public void setNoOfInstalments(int noOfInstalments) {
		this.noOfInstalments = noOfInstalments;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	/*public String getApplicationNo() {
		return applicationNo;
	}
	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	public String getNomineeRelation() {
		return nomineeRelation;
	}
	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}
	public ConstansBO getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(ConstansBO dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}*/
	/*public String getInsuranceFlag() {
		return insuranceFlag;
	}
	public void setInsuranceFlag(String insuranceFlag) {
		this.insuranceFlag = insuranceFlag;
	}*/
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public void setSIPRefNo(String refNo) {
		SIPRefNo = refNo;
	}
	public String getSIPRefNo() {
		return SIPRefNo;
	}
	public ConstansBO getSIPDueDate() {
		return SIPDueDate;
	}
	public void setSIPDueDate(ConstansBO dueDate) {
		SIPDueDate = dueDate;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getDocumentCategory() {
		return documentCategory;
	}
	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}
/*	public String getDocumentRefNo() {
		return documentRefNo;
	}
	public void setDocumentRefNo(String documentRefNo) {
		this.documentRefNo = documentRefNo;
	}*/
	public String getInvestorBankName() {
		return investorBankName;
	}
	public void setInvestorBankName(String investorBankName) {
		this.investorBankName = investorBankName;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getFolioNumber() {
		return folioNumber;
	}
	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}
	public String getDepAccountNo() {
		return depAccountNo;
	}
	public void setDepAccountNo(String depAccountNo) {
		this.depAccountNo = depAccountNo;
	}
	public String getDepBankName() {
		return depBankName;
	}
	public void setDepBankName(String depBankName) {
		this.depBankName = depBankName;
	}
	public boolean isSIPTransaction() {
		return SIPTransaction;
	}
	public void setSIPTransaction(boolean transaction) {
		SIPTransaction = transaction;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getSipType() {
		return sipType;
	}
	public void setSipType(String sipType) {
		this.sipType = sipType;
	}
}
