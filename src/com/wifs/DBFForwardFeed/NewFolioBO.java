package com.wifs.DBFForwardFeed;

import java.util.ArrayList;

public class NewFolioBO { 
	
	private int batchNo			=	0;
	private int slNo			=	0;
	private int rtCode			=	0;
	private int investorId		=	0;
	private String investorName	=	"";
	private String inHouseNo	=	"";
	private String minorIHNo	=	"";
	private String panNo		=	"";
	private String folioNo		=	"";
	
	private String userTransRefId	=	"";
	private String nationality	=	"";
	private String address		=	"";
	private String FAnnualIncome	=	"";
	private String SAnnualIncome	=	"";
	private String TAnnualIncome	=	"";
	private String GAnnualIncome	=	"";
	private String SOccupation		=	"";
	private String TOccupation		=	"";
	private String GOccupation		=	"";
	
	private String investorType		=	"";
	
	public String getInvestorType() {
		return investorType;
	}
	public void setInvestorType(String investorType) {
		this.investorType = investorType;
	}
	private ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
	
	
	public String getUserTransRefId() {
		return userTransRefId;
	}
	public void setUserTransRefId(String userTransRefId) {
		this.userTransRefId = userTransRefId;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFAnnualIncome() {
		return FAnnualIncome;
	}
	public void setFAnnualIncome(String fAnnualIncome) {
		FAnnualIncome = fAnnualIncome;
	}
	public String getSAnnualIncome() {
		return SAnnualIncome;
	}
	public void setSAnnualIncome(String sAnnualIncome) {
		SAnnualIncome = sAnnualIncome;
	}
	public String getTAnnualIncome() {
		return TAnnualIncome;
	}
	public void setTAnnualIncome(String tAnnualIncome) {
		TAnnualIncome = tAnnualIncome;
	}
	public String getGAnnualIncome() {
		return GAnnualIncome;
	}
	public void setGAnnualIncome(String gAnnualIncome) {
		GAnnualIncome = gAnnualIncome;
	}
	public String getSOccupation() {
		return SOccupation;
	}
	public void setSOccupation(String sOccupation) {
		SOccupation = sOccupation;
	}
	public String getTOccupation() {
		return TOccupation;
	}
	public void setTOccupation(String tOccupation) {
		TOccupation = tOccupation;
	}
	public String getGOccupation() {
		return GOccupation;
	}
	public void setGOccupation(String gOccupation) {
		GOccupation = gOccupation;
	}
	
	public int getRtCode() {
		return rtCode;
	}
	public void setRtCode(int rtCode) {
		this.rtCode = rtCode;
	}
	public String getInvestorName() {
		return investorName;
	}
	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}
	public String getInHouseNo() {
		return inHouseNo;
	}
	public void setInHouseNo(String inHouseNo) {
		this.inHouseNo = inHouseNo;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getFolioNo() {
		return folioNo;
	}
	public void setFolioNo(String folioNo) {
		this.folioNo = folioNo;
	}
	public int getInvestorId() {
		return investorId;
	}
	public void setInvestorId(int investorId) {
		this.investorId = investorId;
	}
	public int getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(int batchNo) {
		this.batchNo = batchNo;
	}
	public int getSlNo() {
		return slNo;
	}
	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}
	public String getMinorIHNo() {
		return minorIHNo;
	}
	public void setMinorIHNo(String minorIHNo) {
		this.minorIHNo = minorIHNo;
	}
	public ArrayList<FatcaDocBO> getArrFatcaDocBO() {
		return arrFatcaDocBO;
	}
	public void setArrFatcaDocBO(ArrayList<FatcaDocBO> arrFatcaDocBO) {
		this.arrFatcaDocBO = arrFatcaDocBO;
	}
}
