package com.wifs.DBFForwardFeed; 

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFWriter;
import com.wifs.dao.dbconnection.WIFSSQLCon;
import com.wifs.dao.helper.AutoNumberGenerator;
import com.wifs.utilities.PropertiesConstant;

public class DBFFielsWriter {
	public static Logger logger = Logger.getLogger("DBFFielsWriter.class");
	/*
	 * Hashtable<Integer, ArrayList<FieldInfoBO>> - for DBF file header write
	 * Hashtable<Integer, ArrayList<TransactionMainBO>>  - for All transaction values write
	 */
	public void DBFFileTransactionFeedWriter(String DBFFileType, String trxntype, String fileName, String fileDir, Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO, int userId, Properties properties, boolean isToCAMS_Server) throws Exception
	{ 
		logger.info("DBFFileWriter begin ");
		int intFieldInfoBOArrySize	=	0; 
		String strKeyValue			=	"";
		
		ArrayList<FieldInfoBO> arryForwardFeedFieldInfoBO				=	new ArrayList<FieldInfoBO>();
		ArrayList<TransactionMainBO> arryTransactionMainBO	=	new ArrayList<TransactionMainBO>();
		FieldInfoBO	objFieldInfoBO	=	new FieldInfoBO();
		DBFDataWriter objDBFFile	=	new DBFDataWriter();
		DBFFilesDAO objDBFFilesDAO	=	new DBFFilesDAO();
		AutoNumberGenerator numberGenerator=new AutoNumberGenerator();
		
		Connection objConnection	=	null;
		WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
		objConnection = objWIFSSQLCon.getWifsSQLConnect();
		
		PreparedStatement setPreparedFeedUpdate		=	null;
		setPreparedFeedUpdate	=	objDBFFilesDAO.setPreparedFeedUpdate(userId, objConnection);
		
		/*
		 * Based on the RT Code the file should be write
		 * get the ArrayList<FieldInfoBO>
		 */
		int DBFFile	=	0;
		int DBFRowSize	=	0;
		Set<String> set = hashTransactionMainBO.keySet(); 

	    Iterator<String> itr = set.iterator();
	    try{
	    	if(!(new File(fileDir+File.separator)).exists()){
				new File(fileDir+File.separator).mkdirs();
			}
		    while (itr.hasNext()) {
		    	strKeyValue = itr.next();
		    	//logger.info("********"+strKeyValue.split("[@]")[0]);
		    	//logger.info(strKeyValue + ": " + hashTransactionMainBO.get(strKeyValue));
		    	String [] keyValue_split = strKeyValue.split("[@]");
		    	DBFFile	=	Integer.valueOf(keyValue_split[0]);
				arryForwardFeedFieldInfoBO	=	(ArrayList<FieldInfoBO>) hashFieldInfoBO.get(DBFFile+DBFFileType);
				intFieldInfoBOArrySize	=	arryForwardFeedFieldInfoBO.size();
				DBFField fields[] 		= 	new DBFField[intFieldInfoBOArrySize];
				/*
				 * Write the 1st column as Heading in DBF file
				 * Heading length should be less than or equal to 10 characters
				*/ 
				for(int DBFColumn=0;DBFColumn<intFieldInfoBOArrySize;DBFColumn++)
				{
					objFieldInfoBO	=	arryForwardFeedFieldInfoBO.get(DBFColumn);
					fields[DBFColumn] = new DBFField();
					
					if(objFieldInfoBO.getFieldName().length()>10)
						fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0,10));
					else
						fields[DBFColumn].setName(objFieldInfoBO.getFieldName());
	
					fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
					if(objFieldInfoBO.getDataType().getDataType()!=DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
						fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
					
					if(objFieldInfoBO.getDecimal()>0) // If Decimal values given, then set the decimal for the field
						fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
				}
				arryTransactionMainBO	=	hashTransactionMainBO.get(strKeyValue); //ArrayList<TransactionMainBO>
				DBFRowSize	+=	arryTransactionMainBO.size();
				DBFWriter writer = new DBFWriter();
				writer.setFields(fields);
				
				logger.info("DBF File : "+strKeyValue+trxntype+".dbf");
				if(DBFFile==1) // 1 == Camps
				{
					//long startTime = System.currentTimeMillis();
					
					if(arryTransactionMainBO!=null){
                                            
                                            if (!isToCAMS_Server) {
						objDBFFile.writeCampsDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
                                            }
						
                                            //CAMS forward feed data in text format
                                            String directoryPath_CAMSText = fileDir + File.separator + PropertiesConstant.CAMS_TEXT_FILE_DIRECTORY;
                                            if(!(new File(directoryPath_CAMSText)).exists())
                                                    new File(directoryPath_CAMSText).mkdirs();

                                            String fileName_formatted = fileDir+File.separator+"CAMSText"+File.separator+strKeyValue+""+fileName+trxntype+".txt";
                                            if (isToCAMS_Server) {
                                            String amcCode = keyValue_split [1];
                                            fileName_formatted = directoryPath_CAMSText + direcToCAMS_FileNaming(amcCode, arryTransactionMainBO.size()) + ".txt";
                                            }
                                            
                                            objDBFFile.writeCampsTXT(arryTransactionMainBO, setPreparedFeedUpdate, fileName_formatted);
                                            //}
/*					    
					    String fileNameForFATCA = fileDir+File.separator+"CAMSFATCA"+File.separator+strKeyValue+""+fileName+trxntype+".dbf";
					    objDBFFile.writeCampsDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
*/					}
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : CAMSFile writing = " + totalTime);
					
				}
				else if(DBFFile==2) // 2 == Karvy 
				{
					//long startTime = System.currentTimeMillis();
					
					if(arryTransactionMainBO!=null)
						objDBFFile.writeKarvyDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : Karvy file writing = " + totalTime);
				}
				else if((DBFFile==6)&&(DBFFileType.equals("N"))) // 6 == FT normal feed
				{
					//long startTime = System.currentTimeMillis();
					
					if(!(new File(fileDir+File.separator+"FTText"+File.separator)).exists()){
						new File(fileDir+File.separator+"FTText"+File.separator).mkdirs();
					}
					if(arryTransactionMainBO!=null){
						objDBFFile.writeFranklinDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
						
						
						int transactionId = numberGenerator.getAutoNumberGenerated("STPProcessId");
						String fileNameForText = fileDir+File.separator+"FTText"+File.separator+"FTMF"+ConstansBO.getFourDigit(arryTransactionMainBO.size())+ConstansBO.getFourDigit(transactionId)+ConstansBO.getFTFeedDate()+".txt";
						objDBFFile.writeFranklinTXT(arryTransactionMainBO, fileNameForText);
					}
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : FranklinFile writing = " + totalTime);
					
				}
				/*else if((DBFFile==6)&&(DBFFileType.equals("O"))) // 6 == FT Online enrollment feed
				{
					if(arryTransactionMainBO!=null){
						objDBFFile.writeFranklinDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
						
						String fileNameForText = fileDir+File.separator+"TXT"+File.separator+"FTMF"+ConstansBO.getFourDigit(intFieldInfoBOArrySize)+ConstansBO.getFTFeedDate()+".txt";
						objDBFFile.writeFranklinTXT(intFieldInfoBOArrySize, arryTransactionMainBO, fileNameForText);
					}
				}*/
				else if(DBFFile==7) // 7 == DISPL
				{
					//long startTime = System.currentTimeMillis();
					
					if(arryTransactionMainBO!=null)
						objDBFFile.writeDSPLDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : DSPSL file writing = " + totalTime);
				}
				else if(DBFFile==14) // 14 == Sundram
				{
					//long startTime = System.currentTimeMillis();
					
					if(arryTransactionMainBO!=null) 
						objDBFFile.writeSumdramDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : Sundram file writing = " + totalTime);
				}
				
				if(!(new File(fileDir)).exists()){
					new File(fileDir).mkdirs();
				}
				
				/*File dffFile 	=	new File(fileDir+File.separator+strKeyValue+""+fileName+trxntype+".dbf");
			    if(!dffFile.exists())
			    	dffFile.createNewFile();*/
			    
                            /*
			    String fileName_formatted = null;
			    
			    if (isToCAMS_Server){
			    	String amcCode = keyValue_split [1];
			    	fileName_formatted = direcToCAMS_FileNaming(amcCode, arryTransactionMainBO.size());
			    }
			    
			    if (fileName_formatted == null)
			    	fileName_formatted = strKeyValue + fileName + trxntype;
                            */
                            if (!isToCAMS_Server) {
			    //fileName = strKeyValue + fileName + trxntype;
			    FileOutputStream fos = new FileOutputStream(fileDir+File.separator+ strKeyValue + fileName + trxntype + ".dbf");
				writer.write(fos);
				fos.close();
                            }

		    }
	    }catch(Exception e){
	    	logger.info("Exp. in Creation of file code:"+DBFFile+" and exp: "+e);
	    }finally{
	    	if(setPreparedFeedUpdate != null) setPreparedFeedUpdate.close();
	    	if(objConnection != null) objConnection.close();
	    }
	    logger.info("@@@@@@@@%***************%=========== Total Rows In DBF File "+DBFRowSize+" ===========%***************%@@@@@@@@");
	}
	public Hashtable<String, TransactionMainBO> DBFFileFATCAFeedWriter(String fileDir, Hashtable<Integer, ArrayList<TransactionMainBO>> hashTransactionFatcaBO, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO, Connection objConnection) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		logger.info("DBFFileFatcaWriter begin ");
		int intFieldInfoBOArrySize	=	0; 
		int strKeyValue			=	0;
		
		ArrayList<FieldInfoBO> arryForwardFeedFieldInfoBO				=	new ArrayList<FieldInfoBO>();
		ArrayList<TransactionMainBO> arryTransactionFatcaBO	=	new ArrayList<TransactionMainBO>();
		Hashtable<String, TransactionMainBO> hashFATCANotavailable	=	new Hashtable<String, TransactionMainBO>();
		FieldInfoBO	objFieldInfoBO	=	new FieldInfoBO();
		DBFDataWriter objDBFFile	=	new DBFDataWriter();
		
		Set<Integer> set = hashTransactionFatcaBO.keySet(); 
	    Iterator<Integer> itr = set.iterator();
	    try{
	    	if(!(new File(fileDir+File.separator+"FATCA Data"+File.separator)).exists()){
				new File(fileDir+File.separator+"FATCA Data"+File.separator).mkdirs();
			}
	    	arryForwardFeedFieldInfoBO	=	(ArrayList<FieldInfoBO>) hashFieldInfoBO.get("FATCAN");
			intFieldInfoBOArrySize	=	arryForwardFeedFieldInfoBO.size();
		    while (itr.hasNext()) {
		    	strKeyValue = itr.next();
				DBFField fields[] 		= 	new DBFField[intFieldInfoBOArrySize];
				/*
				 * Write the 1st column as Heading in DBF file
				 * Heading length should be less than or equal to 10 characters
				*/ 
				for(int DBFColumn=0;DBFColumn<intFieldInfoBOArrySize;DBFColumn++)
				{
					objFieldInfoBO	=	arryForwardFeedFieldInfoBO.get(DBFColumn);
					fields[DBFColumn] = new DBFField();
					
					if(objFieldInfoBO.getFieldName().length()>10)
						fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0,10));
					else
						fields[DBFColumn].setName(objFieldInfoBO.getFieldName());
	
					fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
					if(objFieldInfoBO.getDataType().getDataType()!=DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
						fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
					
					if(objFieldInfoBO.getDecimal()>0) // If Decimal values given, then set the decimal for the field
						fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
				}
				arryTransactionFatcaBO	=	hashTransactionFatcaBO.get(strKeyValue); //ArrayList<TransactionMainBO>
				DBFWriter writer = new DBFWriter();
				writer.setFields(fields);
				
				if((arryTransactionFatcaBO != null)&&(arryTransactionFatcaBO.size()>0)){ 
					objDBFFile.writeFatcaDBFFile(intFieldInfoBOArrySize, strKeyValue, arryTransactionFatcaBO,writer, hashFATCANotavailable, objConnection);
				}
				
				if(!(new File(fileDir)).exists()){
					new File(fileDir).mkdirs();
				}
				
				File dffFile 	=	new File(fileDir+File.separator+"FATCA Data"+File.separator+strKeyValue+"@Fatca.dbf");
			    if(!dffFile.exists())
			    	dffFile.createNewFile();
			    	 
			    FileOutputStream fos = new FileOutputStream(fileDir+File.separator+"FATCA Data"+File.separator+strKeyValue+"@Fatca.dbf");
				writer.write(fos);
				fos.close();
				
				logger.info("@@@@@@@@%***************%=========== Total Rows In DBF File "+arryTransactionFatcaBO.size()+" & RT:"+strKeyValue+" ===========%***************%@@@@@@@@");

		    }
			//long totalTime = System.currentTimeMillis() - startTime;
			//logger.info("TIMER : FATCA File writing = " + totalTime);
			
	    }catch(Exception e){
	    	logger.info("Exp. in Creation of FATCA file code RT:"+strKeyValue+" and exp: "+e);
	    }
	   return hashFATCANotavailable;
	}
	
	/*public void CAMSFATCADBFFile(String currentDate, String fileDir, Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO) throws Exception
	{
		boolean fatcaFlag	=	false;
		logger.info("CAMSFATCADBFFile begin ");
		int intFieldInfoBOArrySize	=	0;
		
		ArrayList<FieldInfoBO> arrySIPFieldInfoBO				=	new ArrayList<FieldInfoBO>();
		FieldInfoBO	objFieldInfoBO	=	new FieldInfoBO();
		DBFDataWriter objDBFFile	=	new DBFDataWriter();
		
		arrySIPFieldInfoBO	=	(ArrayList<FieldInfoBO>) hashFieldInfoBO.get("CAMS_FATCON");
		intFieldInfoBOArrySize	=	arrySIPFieldInfoBO.size();
    	
		DBFField fields[] 		= 	new DBFField[intFieldInfoBOArrySize];
			
		for(int DBFColumn=0;DBFColumn<intFieldInfoBOArrySize;DBFColumn++)
		{
			objFieldInfoBO	=	arrySIPFieldInfoBO.get(DBFColumn);
			fields[DBFColumn] = new DBFField();
			
			if(objFieldInfoBO.getFieldName().length()>10)
				fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0,10));
			else
				fields[DBFColumn].setName(objFieldInfoBO.getFieldName());

			fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
			if(objFieldInfoBO.getDataType().getDataType()!=DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
				fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
			
			if(objFieldInfoBO.getDecimal()>0) // If Decimal values given, then set the decimal for the field
				fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
		}
		
		DBFWriter writer = new DBFWriter();
		writer.setFields(fields);
		fatcaFlag	=	objDBFFile.writeCAMSFATCADBF(intFieldInfoBOArrySize, hashTransactionMainBO,writer); 
		
		if(fatcaFlag){
			if(!(new File(fileDir)).exists()){
				new File(fileDir).mkdirs();
			}
			File dffFile 	=	new File(fileDir+File.separator);
		    if(!dffFile.exists())
		    	dffFile.createNewFile();
		    	 
		    FileOutputStream fos = new FileOutputStream(fileDir+File.separator+currentDate+"_FATCA.dbf");
			writer.write( fos);
			fos.close();
		}
	}*/
	public void DBFFileSIPFeedWriter(String currentDate, String fileDir, Hashtable<String, ArrayList<SIPInfoBO>> hashSIPInfoBO, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO) throws Exception
	{
		logger.info("DBFFileSIPWriter begin ");
		int intFieldInfoBOArrySize	=	0;
		String strKeyValue			=	"";
		
		ArrayList<FieldInfoBO> arrySIPFieldInfoBO				=	new ArrayList<FieldInfoBO>();
		ArrayList<SIPInfoBO> arrySIPInfoBO	=	new ArrayList<SIPInfoBO>();
		FieldInfoBO	objFieldInfoBO	=	new FieldInfoBO();
		DBFDataWriter objDBFFile	=	new DBFDataWriter();
		
		/*
		 * Based on the RT Code the file should be write
		 * get the ArrayList<FieldInfoBO>
		 */
		int DBFFile	=	0;
		Set<String> set = hashSIPInfoBO.keySet();

	    Iterator<String> itr = set.iterator();
	    while(itr.hasNext()) {
	    	strKeyValue = itr.next();
	    	//logger.info("********"+strKeyValue.split("[@]")[0]);
	    	DBFFile	=	Integer.valueOf(strKeyValue.split("[@]")[0]);
			arrySIPFieldInfoBO	=	(ArrayList<FieldInfoBO>) hashFieldInfoBO.get("2S");
			intFieldInfoBOArrySize	=	arrySIPFieldInfoBO.size();
	    	
			DBFField fields[] 		= 	new DBFField[intFieldInfoBOArrySize];
			
			/*
			 * Write the 1st column as Heading in DBF file
			 * Heading length should be less than or equal to 10 characters
			*/ 
			
			for(int DBFColumn=0;DBFColumn<intFieldInfoBOArrySize;DBFColumn++)
			{
				objFieldInfoBO	=	arrySIPFieldInfoBO.get(DBFColumn);
				fields[DBFColumn] = new DBFField();
				
				if(objFieldInfoBO.getFieldName().length()>10)
					fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0,10));
				else
					fields[DBFColumn].setName(objFieldInfoBO.getFieldName()); 

				fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
				if(objFieldInfoBO.getDataType().getDataType()!=DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
					fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
				
				if(objFieldInfoBO.getDecimal()>0) // If Decimal values given, then set the decimal for the field
					fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
			}
			
			arrySIPInfoBO	=	hashSIPInfoBO.get(strKeyValue); //ArrayList<TransactionMainBO>
			DBFWriter writer = new DBFWriter();
			writer.setFields(fields);
			
			logger.info("SIP DBF File : "+strKeyValue+currentDate+"_SIP.dbf");
			if((DBFFile==2)||(DBFFile==6)) // 2 == Karvy 6 == FT
			{
				//long startTime = System.currentTimeMillis();
				
				if(arrySIPInfoBO!=null)
					objDBFFile.writeKarvySIPDBF(DBFFile, arrySIPInfoBO,writer); 
				
				//long totalTime = System.currentTimeMillis() - startTime;
				//logger.info("TIMER : Karvy sip file writing = " + totalTime);
			}
			if(!(new File(fileDir)).exists()){
				new File(fileDir).mkdirs();
			}
			File dffFile 	=	new File(fileDir+File.separator+strKeyValue+currentDate+"_SIP.dbf");
		    if(!dffFile.exists())
		    	dffFile.createNewFile();
		    	 
		    FileOutputStream fos = new FileOutputStream(fileDir+File.separator+strKeyValue+currentDate+"_SIP.dbf");
			writer.write( fos);
			fos.close();
	    }
	}
	public void DBFFileNewFolioWriter(String currentDate, String fileDir, Hashtable<String, NewFolioBO> hashNewFolioBO, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO, PreparedStatement PStmtAutoNo, PreparedStatement PStmtUpdateAutoNo, PreparedStatement NewFolioDocInsert) throws Exception
	{
		logger.info("DBFFileNewFolioWriter begin ");
		int intFieldInfoBOArrySize	=	0;
		int strKeyValue				=	0;
		String docType				=	"";
		String fileName				=	"";
		
		ArrayList<FieldInfoBO> arryNFFieldInfoBO	=	new ArrayList<FieldInfoBO>();
		ArrayList<NewFolioBO> arryNewFolioBO		=	new ArrayList<NewFolioBO>();
		Hashtable<Integer, ArrayList<NewFolioBO>> hashNewFolioBOModified	=	new Hashtable<Integer, ArrayList<NewFolioBO>>();
		FieldInfoBO	objFieldInfoBO	=	new FieldInfoBO();
		DBFDataWriter objDBFFile	=	new DBFDataWriter();
		NewFolioBO objNewFolioBO	=	new NewFolioBO();
		DBFFilesDAO objDBFFilesDAO	=	new DBFFilesDAO();
		

		int DBFFile	=	0;
		// Sort hashtable.
	    Vector<String> vectorNewFolio = new Vector(hashNewFolioBO.keySet());
	    Collections.sort(vectorNewFolio);

		// Display (sorted) hashtable.
	    for (Enumeration<String> e = vectorNewFolio.elements(); e.hasMoreElements();) {
	    	
	    	objNewFolioBO	=	new NewFolioBO();
	    	String key = (String)e.nextElement();
	    	objNewFolioBO = (NewFolioBO)hashNewFolioBO.get(key);
	    	
	    	DBFFile	=	Integer.valueOf(key.split("[@]")[0]);
	    	
	    	if((DBFFile==1)||(DBFFile==2))
	    	{
	    		try{
	    			
	    			arryNewFolioBO	=	hashNewFolioBOModified.get(DBFFile);
	    			arryNewFolioBO.add(objNewFolioBO);
	    			hashNewFolioBOModified.put(DBFFile, arryNewFolioBO);
	    			
	    		}catch(Exception ex){
	    			arryNewFolioBO	=	new ArrayList<NewFolioBO>();
	    			arryNewFolioBO.add(objNewFolioBO);
	    			hashNewFolioBOModified.put(DBFFile, arryNewFolioBO);
	    		}
	    	}
	    }
	    logger.info("fileDir ::::::: "+fileDir);
	    Set<Integer> set = hashNewFolioBOModified.keySet();

	    Iterator<Integer> itr = set.iterator();
	    while(itr.hasNext()) {
	    	
		    strKeyValue = itr.next();
	    	DBFFile	=	Integer.valueOf(strKeyValue);
    	
		    if(DBFFile==2)
	    	{
	    		docType	=	"KARVY_DOCN";
	    		fileName= 	"DOC_69583_"+currentDate+"_001.dbf";
	    	}
	    	else if(DBFFile==1)
	    	{
	    		docType	=	"CAMS_DOCN";
	    		fileName= 	"ARN-69583_"+currentDate+".dbf";
	    	}	
				
		    arryNFFieldInfoBO	=	(ArrayList<FieldInfoBO>) hashFieldInfoBO.get(docType);
		    arryNewFolioBO		=	hashNewFolioBOModified.get(DBFFile);
			intFieldInfoBOArrySize	=	arryNFFieldInfoBO.size();
	    	
			DBFField fields[] 		= 	new DBFField[intFieldInfoBOArrySize];
			
			/*
			 * Write the 1st column as Heading in DBF file
			 * Heading length should be less than or equal to 10 characters
			*/
			
			for(int DBFColumn=0;DBFColumn<intFieldInfoBOArrySize;DBFColumn++)
			{
				objFieldInfoBO	=	arryNFFieldInfoBO.get(DBFColumn);
				fields[DBFColumn] = new DBFField();
				
				if(objFieldInfoBO.getFieldName().length()>10)
					fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0,10));
				else
					fields[DBFColumn].setName(objFieldInfoBO.getFieldName());

				fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
				if(objFieldInfoBO.getDataType().getDataType()!=DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
					fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
				
				if(objFieldInfoBO.getDecimal()>0) // If Decimal values given, then set the decimal for the field
					fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
			}
			
			//arrySIPInfoBO	=	hashNewFolioBO.get(strKeyValue); //ArrayList<TransactionMainBO>
			DBFWriter writer = new DBFWriter();
			writer.setFields(fields);
			
			if(arryNewFolioBO.size()>0)
			{
				if(DBFFile==2)
				{
					int slNo		=	0;
					int batchNo		=	0;
					int incBatchNo	=	0;
					slNo			=	objDBFFilesDAO.getAutoNumber("KarvySlNo", PStmtAutoNo);
					batchNo			=	objDBFFilesDAO.getAutoNumber("KarvyBatchNo", PStmtAutoNo);
					incBatchNo		=	batchNo;
					//long startTime = System.currentTimeMillis();
					
					for(int arr=0;arr<arryNewFolioBO.size();arr++)
					{
						objNewFolioBO	=	arryNewFolioBO.get(arr);
						arryNewFolioBO.get(arr).setBatchNo(incBatchNo);
						arryNewFolioBO.get(arr).setSlNo(slNo);
						
						objDBFFilesDAO.NewFolioDocInsert(incBatchNo, slNo, objNewFolioBO, NewFolioDocInsert);
						
						slNo++;
						if(slNo%100==0)
							incBatchNo++;
					}
					objDBFFilesDAO.getAutoNumberGenerated(slNo, "KarvySlNo", PStmtUpdateAutoNo);
					
					if(batchNo<incBatchNo)
						objDBFFilesDAO.getAutoNumberGenerated(incBatchNo, "KarvyBatchNo", PStmtUpdateAutoNo);
					
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : Karvy new folio Insert = " + totalTime);
				}
				else if(DBFFile==1)
				{
					//long startTime = System.currentTimeMillis();
					
					for(int arr=0;arr<arryNewFolioBO.size();arr++)
					{
						objNewFolioBO	=	arryNewFolioBO.get(arr);
						objDBFFilesDAO.NewFolioDocInsert(0, 0, objNewFolioBO, NewFolioDocInsert);					
					}
					//long totalTime = System.currentTimeMillis() - startTime;
					//logger.info("TIMER : CAMS new folio Insert = " + totalTime);
				}
				if(DBFFile==2)
		    	{
					objDBFFile.writeKarvyNewFolioDBF(arryNewFolioBO, writer);
		    	}
				else if(DBFFile==1)
				{
					objDBFFile.writeCAMSNewFolioDBF(arryNewFolioBO, writer);
				}
			}
			
			if(!(new File(fileDir)).exists()){
				new File(fileDir).mkdirs();
			}
			File dffFile 	=	new File(fileDir+File.separator+fileName);
		    if(!dffFile.exists())
		    	dffFile.createNewFile();
		    	 
		    FileOutputStream fos = new FileOutputStream(fileDir+File.separator+fileName);
			writer.write( fos);
			fos.close();
	    }
	}
	public void DBFFileCKYCFeedWriter(String fileDir, Hashtable<String,InvestorDetailsBO> hashCKCDetails, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO) throws Exception
	{
		//long startTime = System.currentTimeMillis();
		
		logger.info("DBFFileFatcaWriter begin ");
		int intFieldInfoBOArrySize	=	0; 
		
		ArrayList<FieldInfoBO> arryForwardFeedFieldInfoBO				=	new ArrayList<FieldInfoBO>();
		FieldInfoBO	objFieldInfoBO	=	new FieldInfoBO();
		DBFDataWriter objDBFFile	=	new DBFDataWriter();
		
	    try{
	    	if(!(new File(fileDir+File.separator+"CKYCData"+File.separator)).exists()){
				new File(fileDir+File.separator+"CKYCData"+File.separator).mkdirs();
			}
	    	arryForwardFeedFieldInfoBO	=	(ArrayList<FieldInfoBO>) hashFieldInfoBO.get("CKYCN");
			intFieldInfoBOArrySize	=	arryForwardFeedFieldInfoBO.size();
			DBFField fields[] 		= 	new DBFField[intFieldInfoBOArrySize];
			for(int DBFColumn=0;DBFColumn<intFieldInfoBOArrySize;DBFColumn++)
			{
				objFieldInfoBO	=	arryForwardFeedFieldInfoBO.get(DBFColumn);
				fields[DBFColumn] = new DBFField();
				
				if(objFieldInfoBO.getFieldName().length()>10)
					fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0,10));
				else
					fields[DBFColumn].setName(objFieldInfoBO.getFieldName());

				fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
				if(objFieldInfoBO.getDataType().getDataType()!=DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
					fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
				
				if(objFieldInfoBO.getDecimal()>0) // If Decimal values given, then set the decimal for the field
					fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
			}
			DBFWriter writer = new DBFWriter();
			writer.setFields(fields);
			
			if((hashCKCDetails != null)&&(hashCKCDetails.size()>0)){ 
				objDBFFile.writeCKYCDBFFile(intFieldInfoBOArrySize, writer, hashCKCDetails);
			}
						
			File dffFile 	=	new File(fileDir+File.separator+"CKYCData"+File.separator+"14@CKYC.dbf");
		    if(!dffFile.exists())
		    	dffFile.createNewFile();
		    	 
		    FileOutputStream fos = new FileOutputStream(fileDir+File.separator+"CKYCData"+File.separator+"14@CKYC.dbf");
			writer.write(fos);
			fos.close();
			
			logger.info("@@@@@@@@%***************%=========== Total Rows In DBF File "+hashCKCDetails.size()+" & CKYC Data ===========%***************%@@@@@@@@");

			
	    }catch(Exception e){
	    	logger.error("Exp. in Creation of CKYC file and exp: "+e);
	    }
	}
	
	/**
	 * Direct upload to CAMS server - The transaction file shall be named as 'XXXXRRRRNNNNYYYYMMDDHHMMSS'<br />
	 * Description <br />
	 * XXXX - Fund House / AMC Code  - 1 to 4   ( Not validated )<br />
	 * RRRR - number of records	- 5 to 8   ( Validated while processing the records)<br />
	 * NNNN – Intra-day serial no of the file -	9 to 12 ( Not Validated)<br />
	 * YYYYMMDDHHMMSS - Date Time Stamp 13 to 26 ( Validated while processing)
	 * @param amcCode
	 * @param recordCount
	 * @return
	 */
	public String direcToCAMS_FileNaming (String amcCode, int recordCount) {
		
		String fileName = null;
		
		try {
			//Fund House / AMC Code
			fileName = (amcCode.trim() + "000").substring(0, 4);
			
			//Number of records
			fileName += String.format("%04d", recordCount);
			
			//Intra-day serial no of the file
			String autoNumberType = "DirectToCAMS_FileNumber";
			AutoNumberGenerator autoNumberGenerator = new AutoNumberGenerator();
			int fileNumber = autoNumberGenerator.getAutoNumberGenerated(autoNumberType);
			//Reset auto generate number, if reaches max.
			if (fileNumber > 9999){
				fileNumber = 1;
				autoNumberGenerator.updateAutoNumberGenerated(autoNumberType, fileNumber);
			}
			fileName += String.format("%04d", fileNumber);
		
			//Date Time Stamp
			fileName += ConstansBO.getTodayDate_static("yyyyMMddHHmmss");
		}
		catch (Exception ex){
			logger.error("EX - DIRECT TO CAMS FILE NAME. $ AMC: "+amcCode+" SZ: "+recordCount+" # EX: "+ex);
		}
		
		return fileName;
	}
        
    public void FinalDBFFileTransactionFeedWriter(String DBFFileType, String trxntype, String fileName, String fileDir, Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO, Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO, int userId, Properties properties, boolean isToCAMS_Server) throws Exception {
        logger.info("DBFFileWriter begin ");
        int intFieldInfoBOArrySize = 0;
        String strKeyValue = "";

        ArrayList<FieldInfoBO> arryForwardFeedFieldInfoBO = new ArrayList<FieldInfoBO>();
        ArrayList<TransactionMainBO> arryTransactionMainBO = new ArrayList<TransactionMainBO>();
        FieldInfoBO objFieldInfoBO = new FieldInfoBO();
        DBFDataWriter objDBFFile = new DBFDataWriter();
        DBFFilesDAO objDBFFilesDAO = new DBFFilesDAO();
        AutoNumberGenerator numberGenerator = new AutoNumberGenerator();

        Connection objConnection = null;
        WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
        objConnection = objWIFSSQLCon.getWifsSQLConnect();

        PreparedStatement setPreparedFeedUpdate = null;
        setPreparedFeedUpdate = objDBFFilesDAO.setPreparedFeedUpdate(userId, objConnection);

        /*
		 * Based on the RT Code the file should be write
		 * get the ArrayList<FieldInfoBO>
         */
        int DBFFile = 0;
        int DBFRowSize = 0;
        Set<String> set = hashTransactionMainBO.keySet();
        Iterator<String> itr = set.iterator();
        try {
            if (!(new File(fileDir + File.separator)).exists()) {
                new File(fileDir + File.separator).mkdirs();
            }
            while (itr.hasNext()) {
                strKeyValue = itr.next();
                //logger.info("********"+strKeyValue.split("[@]")[0]);
                //logger.info(strKeyValue + ": " + hashTransactionMainBO.get(strKeyValue));
                String[] keyValue_split = strKeyValue.split("[@]");
                DBFFile = Integer.valueOf(keyValue_split[0]);
                arryForwardFeedFieldInfoBO = (ArrayList<FieldInfoBO>) hashFieldInfoBO.get(DBFFile + DBFFileType);
                intFieldInfoBOArrySize = arryForwardFeedFieldInfoBO.size();
                DBFField fields[] = new DBFField[intFieldInfoBOArrySize];
                /*
				 * Write the 1st column as Heading in DBF file
				 * Heading length should be less than or equal to 10 characters
                 */
                for (int DBFColumn = 0; DBFColumn < intFieldInfoBOArrySize; DBFColumn++) {
                    objFieldInfoBO = arryForwardFeedFieldInfoBO.get(DBFColumn);
                    fields[DBFColumn] = new DBFField();

                    if (objFieldInfoBO.getFieldName().length() > 10) {
                        fields[DBFColumn].setName(objFieldInfoBO.getFieldName().substring(0, 10));
                    } else {
                        fields[DBFColumn].setName(objFieldInfoBO.getFieldName());
                    }

                    fields[DBFColumn].setDataType(objFieldInfoBO.getDataType().getDataType());
                    if (objFieldInfoBO.getDataType().getDataType() != DBFField.FIELD_TYPE_D) // No need set length for FIELD_TYPE_D 
                    {
                        fields[DBFColumn].setFieldLength(objFieldInfoBO.getWidth());
                    }

                    if (objFieldInfoBO.getDecimal() > 0) // If Decimal values given, then set the decimal for the field
                    {
                        fields[DBFColumn].setDecimalCount(objFieldInfoBO.getDecimal());
                    }
                }
                arryTransactionMainBO = hashTransactionMainBO.get(strKeyValue); //ArrayList<TransactionMainBO>
                DBFRowSize += arryTransactionMainBO.size();
                DBFWriter writer = new DBFWriter();
                writer.setFields(fields);

                logger.info("DBF File : " + strKeyValue + trxntype + ".dbf");
                if (DBFFile == 1) // 1 == Camps
                {
                    //long startTime = System.currentTimeMillis();

                    if (arryTransactionMainBO != null) {

                        if (true) {
                            objDBFFile.writeCampsDBF2(intFieldInfoBOArrySize, arryTransactionMainBO, writer, setPreparedFeedUpdate);
                        }

                        //CAMS forward feed data in text format
                        String directoryPath_CAMSText = fileDir + File.separator + PropertiesConstant.CAMS_TEXT_FILE_DIRECTORY;
                        if (!(new File(directoryPath_CAMSText)).exists()) {
                            new File(directoryPath_CAMSText).mkdirs();
                        }

                        String fileName_formatted = fileDir + File.separator + "CAMSText" + File.separator + strKeyValue + "" + fileName + trxntype + ".txt";
                        if (true) {
                            String amcCode = keyValue_split[1];
                            fileName_formatted = directoryPath_CAMSText + direcToCAMS_FileNaming(amcCode, arryTransactionMainBO.size()) + ".txt";
                        }
                        objDBFFile.writeCampsTXT2(arryTransactionMainBO, setPreparedFeedUpdate, fileName_formatted);
                        //}
/*					    
					    String fileNameForFATCA = fileDir+File.separator+"CAMSFATCA"+File.separator+strKeyValue+""+fileName+trxntype+".dbf";
					    objDBFFile.writeCampsDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
                         */                    }

                    //long totalTime = System.currentTimeMillis() - startTime;
                    //logger.info("TIMER : CAMSFile writing = " + totalTime);
                } else if (DBFFile == 2) // 2 == Karvy 
                {
                    //long startTime = System.currentTimeMillis();

                    if (arryTransactionMainBO != null) {
                        objDBFFile.writeKarvyDBF(intFieldInfoBOArrySize, arryTransactionMainBO, writer, setPreparedFeedUpdate);
                    }

                    //long totalTime = System.currentTimeMillis() - startTime;
                    //logger.info("TIMER : Karvy file writing = " + totalTime);
                } else if ((DBFFile == 6) && (DBFFileType.equals("N"))) // 6 == FT normal feed
                {
                    //long startTime = System.currentTimeMillis();

                    if (!(new File(fileDir + File.separator + "FTText" + File.separator)).exists()) {
                        new File(fileDir + File.separator + "FTText" + File.separator).mkdirs();
                    }
                    if (arryTransactionMainBO != null) {
                        objDBFFile.writeFranklinDBF(intFieldInfoBOArrySize, arryTransactionMainBO, writer, setPreparedFeedUpdate);

                        int transactionId = numberGenerator.getAutoNumberGenerated("STPProcessId");
                        String fileNameForText = fileDir + File.separator + "FTText" + File.separator + "FTMF" + ConstansBO.getFourDigit(arryTransactionMainBO.size()) + ConstansBO.getFourDigit(transactionId) + ConstansBO.getFTFeedDate() + ".txt";
                        objDBFFile.writeFranklinTXT(arryTransactionMainBO, fileNameForText);
                    }
                    //long totalTime = System.currentTimeMillis() - startTime;
                    //logger.info("TIMER : FranklinFile writing = " + totalTime);

                } /*else if((DBFFile==6)&&(DBFFileType.equals("O"))) // 6 == FT Online enrollment feed
				{
					if(arryTransactionMainBO!=null){
						objDBFFile.writeFranklinDBF(intFieldInfoBOArrySize, arryTransactionMainBO,writer,setPreparedFeedUpdate);
						
						String fileNameForText = fileDir+File.separator+"TXT"+File.separator+"FTMF"+ConstansBO.getFourDigit(intFieldInfoBOArrySize)+ConstansBO.getFTFeedDate()+".txt";
						objDBFFile.writeFranklinTXT(intFieldInfoBOArrySize, arryTransactionMainBO, fileNameForText);
					}
				}*/ else if (DBFFile == 7) // 7 == DISPL
                {
                    //long startTime = System.currentTimeMillis();

                    if (arryTransactionMainBO != null) {
                        objDBFFile.writeDSPLDBF(intFieldInfoBOArrySize, arryTransactionMainBO, writer, setPreparedFeedUpdate);
                    }

                    //long totalTime = System.currentTimeMillis() - startTime;
                    //logger.info("TIMER : DSPSL file writing = " + totalTime);
                } else if (DBFFile == 14) // 14 == Sundram
                {
                    //long startTime = System.currentTimeMillis();

                    if (arryTransactionMainBO != null) {
                        objDBFFile.writeSumdramDBF(intFieldInfoBOArrySize, arryTransactionMainBO, writer, setPreparedFeedUpdate);
                    }

                    //long totalTime = System.currentTimeMillis() - startTime;
                    //logger.info("TIMER : Sundram file writing = " + totalTime);
                }

                if (!(new File(fileDir)).exists()) {
                    new File(fileDir).mkdirs();
                }

                /*File dffFile 	=	new File(fileDir+File.separator+strKeyValue+""+fileName+trxntype+".dbf");
			    if(!dffFile.exists())
			    	dffFile.createNewFile();*/
 /*
			    String fileName_formatted = null;
			    
			    if (isToCAMS_Server){
			    	String amcCode = keyValue_split [1];
			    	fileName_formatted = direcToCAMS_FileNaming(amcCode, arryTransactionMainBO.size());
			    }
			    
			    if (fileName_formatted == null)
			    	fileName_formatted = strKeyValue + fileName + trxntype;
                 */
                if (true) {
                    //fileName = strKeyValue + fileName + trxntype;
                    FileOutputStream fos = new FileOutputStream(fileDir + File.separator + strKeyValue + fileName + trxntype + ".dbf");
                    writer.write(fos);
                    fos.close();
                }

            }
        } catch (Exception e) {
            logger.error("Exp. in Creation of file code:" + DBFFile + " and exp: " + e);
        } finally {
            if (setPreparedFeedUpdate != null) {
                setPreparedFeedUpdate.close();
            }
            if (objConnection != null) {
                objConnection.close();
            }
        }
        logger.info("@@@@@@@@%***************%=========== Total Rows In DBF File " + DBFRowSize + " ===========%***************%@@@@@@@@");
    }
        
        
        
        
}