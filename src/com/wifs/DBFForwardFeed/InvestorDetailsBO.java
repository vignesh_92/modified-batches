package com.wifs.DBFForwardFeed;

import java.util.ArrayList;

public class InvestorDetailsBO {
	
	private int intHoldingProfileId	=	0;
	private String strInvestorId	=	"";
	//private String strInvestorBankId=	"";
	private String strFirstName		=	"";
	private String strJointName1	=	"";
	private String strJointName2	=	"";
	private String strAddress1		=	"";
	private String strAddress2		=	"";
	private String strAddress3		=	"";
	private String strCity			=	"";
	private String strState			=	"";
	private String strNRIAddress1	=	"";
	private String strNRIAddress2	=	"";
	private String strNRIAddress3	=	"";
	private String strNRICity		=	"";
	private String strNRIState		=	"";
	private String strNRICountry	=	"";
	private String strNRIPinCode	=	"";
	private String strStateWIFS		=	"";
	private String strPinCode		=	"";
	private String strPhoneOffice	=	"";
	private String strMobileNo		=	"";
	private ConstansBO DateOfBirth;
	private String strGuardianName	=	"";
	private String strPhoneRes		=	"";
	private String strFaxOff		=	"";
	private String strFaxRes		=	"";
	private String strEmail			=	"";
	private String strOccupationCode=	"";
	private String strOccupation	=	"";
	private String strLocation		=	"";
	private String strLocationCode	=	"";
	private String strTaxNo			=	"";
	private String strPanHolder2	=	"";
	private String strPanHolder3	=	"";
	private String strGuardianPanNo	=	"";
	private String strModeOfHolding	=	"";
	private String strTaxStatus		=	"";
	
	private String strValid_Pan		=	"";
	private String strGValid_Pan	=	"";
	private String strJH1ValidPan	=	"";
	private String strJH2ValidPan	=	"";
	private String strIHNo			=	"";
	private int strNRIInvestor		=	0;
	
	private String brokerCode	=	"";
	private String subBrokerCode	=	"";
	private String userCode		=	""; 
	private String irType		=	"";
	private String bankVerified	=	"";
	private String tcVersion		=	"";
	private String fatcaFlag		=	"N";
	
	private String usaCanadaInvestor	=	"";
	private String kycFlag		=	"Y";
	
	private String cKYCRefId1		=	"";
	private String cKYCRefId2		=	"";
	private String cKYCRefId3		=	"";
	private String cKYCRefIdG		=	"";
	private ConstansBO DateOfBirth2;
	private ConstansBO DateOfBirth3;
	private ConstansBO DateOfBirthG;
	
	private String kycFlag2		=	"";
	private String kycFlag3		=	"";
	private String kycFlagG		=	"";
	
	private String aAdhaarNo_fhld	=	"0";
	private String aAdhaarNo_shld	=	"0";
	private String aAdhaarNo_thld	=	"0";
	private String aAdhaarNo_gurd	=	"0";
	
	private String motherName		=	"";
	private String placeOfBirth		=	"";
	private String countryOfBirthCode	=	"";
	private String nationality		=	"";
	private String country			=	"";
	private String salutation		=	"";
	private String district			=	"";
	
	public String getKycFlag() {
		return kycFlag;
	}
	public void setKycFlag(String kycFlag) {
		this.kycFlag = kycFlag;
	}
	
	private ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
	
	
	
	public void setHoldingProfileId(int intHoldingProfileId) { this.intHoldingProfileId = intHoldingProfileId; }
	public void setInvestorId(String strInvestorId){this.strInvestorId=strInvestorId;}
	public void setFirstName(String strFirstName){this.strFirstName=strFirstName;}
	public void setJointName1(String strJointName1){this.strJointName1=strJointName1;}
	public void setJointName2(String strJointName2){this.strJointName2=strJointName2;}
	public void setAddress1(String strAddress1){this.strAddress1=strAddress1;}
	public void setAddress2(String strAddress2){this.strAddress2=strAddress2;}
	public void setAddress3(String strAddress3){this.strAddress3=strAddress3;}
	public void setCity(String strCity){this.strCity=strCity;}
	public void setState(String strState){this.strState=strState;}
	public void setStateWIFS(String strStateWIFS){this.strStateWIFS=strStateWIFS;}
	public void setPinCode(String strPinCode){this.strPinCode=strPinCode;}
	public void setPhoneOffice(String strPhoneOffice){this.strPhoneOffice=strPhoneOffice;}
	public void setMobileNo(String strMobileNo){this.strMobileNo=strMobileNo;}
	public void setDateOfBirth(ConstansBO DateOfBirth){this.DateOfBirth=DateOfBirth;}
	public void setGuardianName(String strGuardianName){this.strGuardianName=strGuardianName;}
	public void setPhoneRes(String strPhoneRes){this.strPhoneRes=strPhoneRes;}
	public void setFaxOff(String strFaxOff){this.strFaxOff=strFaxOff;}
	public void setFaxRes(String strFaxRes){this.strFaxRes=strFaxRes;}
	public void setEmail(String strEmail){this.strEmail=strEmail;}
	public void setOccupationCode(String strOccupationCode){this.strOccupationCode=strOccupationCode;}
	public void setOccupation(String strOccupation){this.strOccupation=strOccupation;}
	public void setLocation(String strLocation){this.strLocation=strLocation;}
	public void setLocationCode(String strLocationCode){this.strLocationCode=strLocationCode;}
	public void setTaxNo(String strTaxNo){this.strTaxNo=strTaxNo;}
	public void setPanHolder2(String strPanHolder2){this.strPanHolder2=strPanHolder2;}
	public void setPanHolder3(String strPanHolder3){this.strPanHolder3=strPanHolder3;}
	public void setGuardianPanNo(String strGuardianPanNo){this.strGuardianPanNo=strGuardianPanNo;}
	public void setModeOfHolding(String strModeOfHolding) {this.strModeOfHolding = strModeOfHolding; }
	public void setTaxStatus(String strTaxStatus){ this.strTaxStatus = strTaxStatus; }
	public void setValid_Pan(String strValid_Pan){this.strValid_Pan=strValid_Pan;}
	public void setGValid_Pan(String strGValid_Pan){this.strGValid_Pan=strGValid_Pan;}
	public void setJH1ValidPan(String strJH1ValidPan){this.strJH1ValidPan=strJH1ValidPan;}
	public void setJH2ValidPan(String strJH2ValidPan){this.strJH2ValidPan=strJH2ValidPan;}
	public void setIHNo(String strIHNo) {	this.strIHNo = strIHNo;		}
	public void setNRIInvestor(int strNRIInvestor) {this.strNRIInvestor = strNRIInvestor;	}
	public void setNRIAddress1(String strNRIAddress1) {		this.strNRIAddress1 = strNRIAddress1;	}
	public void setNRIAddress2(String strNRIAddress2) {		this.strNRIAddress2 = strNRIAddress2;	}
	public void setNRIAddress3(String strNRIAddress3) {		this.strNRIAddress3 = strNRIAddress3;	}
	public void setNRICity(String strNRICity) {		this.strNRICity = strNRICity;	}
	public void setNRIState(String strNRIState) {		this.strNRIState = strNRIState;	}
	public void setNRICountry(String strNRICountry) {		this.strNRICountry = strNRICountry;	}
	public void setNRIPinCode(String strNRIPinCode) {	this.strNRIPinCode = strNRIPinCode;	}
	
	
	public int getHoldingProfileId(){ return(this.intHoldingProfileId); }
	public String getInvestorId(){return (this.strInvestorId);}
	public String getFirstName(){return (this.strFirstName);}
	public String getJointName1(){return (this.strJointName1);}
	public String getJointName2(){return (this.strJointName2);}
	public String getAddress1(){return (this.strAddress1);}
	public String getAddress2(){return (this.strAddress2);}
	public String getAddress3(){return (this.strAddress3);}
	public String getCity(){return (this.strCity);}
	public String getState(){return (this.strState);}
	public String getStateWIFS(){return (this.strStateWIFS);}
	public String getPinCode(){return (this.strPinCode);}
	public String getPhoneOffice(){return (this.strPhoneOffice);}
	public String getMobileNo(){return (this.strMobileNo);}
	public ConstansBO getDateOfBirth(){return (this.DateOfBirth);}
	public String getGuardianName(){return (this.strGuardianName);}
	public String getPhoneRes(){return (this.strPhoneRes);}
	public String getFaxOff(){return (this.strFaxOff);}
	public String getFaxRes(){return (this.strFaxRes);}
	public String getEmail(){return (this.strEmail);}
	public String getOccupationCode(){return (this.strOccupationCode);}
	public String getOccupation(){return (this.strOccupation);}
	public String getLocation(){return (this.strLocation);}
	public String getLocationCode(){return (this.strLocationCode);}
	public String getTaxNo(){return (this.strTaxNo);}
	public String getPanHolder2(){return (this.strPanHolder2);}
	public String getPanHolder3(){return (this.strPanHolder3);}
	public String getGuardianPanNo(){return (this.strGuardianPanNo);}
	public String getModeOfHolding(){ return(this.strModeOfHolding); }
	public String getTaxStatus(){ return(this.strTaxStatus); }
	public String getValid_Pan(){return (this.strValid_Pan);}
	public String getGValid_Pan(){return (this.strGValid_Pan);}
	public String getJH1ValidPan(){return (this.strJH1ValidPan);}
	public String getJH2ValidPan(){return (this.strJH2ValidPan);}
	public String getIHNo() {return strIHNo;	}
	public int getNRIInvestor() {return strNRIInvestor;	}
	public String getNRIAddress1() {return strNRIAddress1;	}
	public String getNRIAddress2() {	return strNRIAddress2;	}
	public String getNRIAddress3() {		return strNRIAddress3;	}
	public String getNRICity() {		return strNRICity;	}
	public String getNRIState() {		return strNRIState;	}
	public String getNRICountry() {		return strNRICountry;	}
	public String getNRIPinCode() { 		return strNRIPinCode;	}
	public String getBrokerCode() {
		return brokerCode;
	}
	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	public String getSubBrokerCode() {
		return subBrokerCode;
	}
	public void setSubBrokerCode(String subBrokerCode) {
		this.subBrokerCode = subBrokerCode;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getIrType() {
		return irType;
	}
	public void setIrType(String irType) {
		this.irType = irType;
	}
	public String getBankVerified() {
		return bankVerified;
	}
	public void setBankVerified(String bankVerified) {
		this.bankVerified = bankVerified;
	}
	public String getTcVersion() {
		return tcVersion;
	}
	public void setTcVersion(String tcVersion) {
		this.tcVersion = tcVersion;
	}
	public String getUsaCanadaInvestor() {
		return usaCanadaInvestor;
	}
	public void setUsaCanadaInvestor(String usaCanadaInvestor) {
		this.usaCanadaInvestor = usaCanadaInvestor;
	}

	public ArrayList<FatcaDocBO> getArrFatcaDocBO() {
		return arrFatcaDocBO;
	}
	public void setArrFatcaDocBO(ArrayList<FatcaDocBO> arrFatcaDocBO) {
		this.arrFatcaDocBO = arrFatcaDocBO;
	}
	public String getFatcaFlag() {
		return fatcaFlag;
	}
	public void setFatcaFlag(String fatcaFlag) {
		this.fatcaFlag = fatcaFlag;
	}
	public ConstansBO getDateOfBirth2() {
		return DateOfBirth2;
	}
	public void setDateOfBirth2(ConstansBO dateOfBirth2) {
		DateOfBirth2 = dateOfBirth2;
	}
	public ConstansBO getDateOfBirth3() {
		return DateOfBirth3;
	}
	public void setDateOfBirth3(ConstansBO dateOfBirth3) {
		DateOfBirth3 = dateOfBirth3;
	}
	
	public ConstansBO getDateOfBirthG() {
		return DateOfBirthG;
	}
	public void setDateOfBirthG(ConstansBO dateOfBirthG) {
		DateOfBirthG = dateOfBirthG;
	}
	public String getKycFlag2() {
		return kycFlag2;
	}
	public void setKycFlag2(String kycFlag2) {
		this.kycFlag2 = kycFlag2;
	}
	public String getKycFlag3() {
		return kycFlag3;
	}
	public void setKycFlag3(String kycFlag3) {
		this.kycFlag3 = kycFlag3;
	}
	public String getKycFlagG() {
		return kycFlagG;
	}
	public void setKycFlagG(String kycFlagG) {
		this.kycFlagG = kycFlagG;
	}
	
	public String getaAdhaarNo_fhld() {
		return aAdhaarNo_fhld;
	}
	public void setaAdhaarNo_fhld(String aAdhaarNo_fhld) {
		this.aAdhaarNo_fhld = aAdhaarNo_fhld;
	}
	public String getaAdhaarNo_shld() {
		return aAdhaarNo_shld;
	}
	public void setaAdhaarNo_shld(String aAdhaarNo_shld) {
		this.aAdhaarNo_shld = aAdhaarNo_shld;
	}
	public String getaAdhaarNo_thld() {
		return aAdhaarNo_thld;
	}
	public void setaAdhaarNo_thld(String aAdhaarNo_thld) {
		this.aAdhaarNo_thld = aAdhaarNo_thld;
	}
	public String getaAdhaarNo_gurd() {
		return aAdhaarNo_gurd;
	}
	public void setaAdhaarNo_gurd(String aAdhaarNo_gurd) {
		this.aAdhaarNo_gurd = aAdhaarNo_gurd;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getCountryOfBirthCode() {
		return countryOfBirthCode;
	}
	public void setCountryOfBirthCode(String countryOfBirthCode) {
		this.countryOfBirthCode = countryOfBirthCode;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getcKYCRefId1() {
		return cKYCRefId1;
	}
	public void setcKYCRefId1(String cKYCRefId1) {
		this.cKYCRefId1 = cKYCRefId1;
	}
	public String getcKYCRefId2() {
		return cKYCRefId2;
	}
	public void setcKYCRefId2(String cKYCRefId2) {
		this.cKYCRefId2 = cKYCRefId2;
	}
	public String getcKYCRefId3() {
		return cKYCRefId3;
	}
	public void setcKYCRefId3(String cKYCRefId3) {
		this.cKYCRefId3 = cKYCRefId3;
	}
	public String getcKYCRefIdG() {
		return cKYCRefIdG;
	}
	public void setcKYCRefIdG(String cKYCRefIdG) {
		this.cKYCRefIdG = cKYCRefIdG;
	}
	
}
