package com.wifs.DBFForwardFeed;
import org.apache.log4j.Logger; 

import com.wifs.utilities.PropertiesConstant;
public class MainDBF {
	public static Logger logger = Logger.getLogger("MainDTP.class");
	public static void main(String dbf[])
	{
        logger.info("test");
		long startTime = System.currentTimeMillis();
		String searchKey	=	"CAMS_DIRECT_UPLOAD";
		String searchValue	=	"";
		String TrxnOE		=	"";	
                String finalFeed        =       "CAMS";
		//String notActiveInvNFO	=	"";
		logger.info("File Forward Feed Begin");
		DBFFilesBL objDBFFilesBL	=	new DBFFilesBL();
		try{
			try{
				searchKey	=	dbf[0];
				searchValue	=	dbf[1];
				//notActiveInvNFO	=	dbf[2]; 
				TrxnOE		=	dbf[2];
                                finalFeed       =       dbf[3];
			}catch(Exception e){		TrxnOE	=	"N";	}
			
			if(searchKey.equals("1"))// Search by Liquid Shm
				objDBFFilesBL.getDBFFiles(searchValue,"","",TrxnOE,1, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 
			else if(searchKey.equals("2"))// Search by Payment ID
				objDBFFilesBL.getDBFFiles("", searchValue,"",TrxnOE,1, finalFeed);  // 1. liquid scheme 2. search key 3. AMC Search 4. UserId 
			else if(searchKey.equals("3"))// Search by AMC Code
				objDBFFilesBL.getDBFFiles("","",searchValue,TrxnOE,1, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 
			else if(searchKey.equals("4"))// Search by PaymentId on not active investor
				objDBFFilesBL.getDBFFiles("",searchValue,"","N",1, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId
                        
                        //Forward feed direct upload (STP) to CAMS server 
			else if(searchKey.equalsIgnoreCase(PropertiesConstant.CAMS_DIRECT_UPLOAD))
				objDBFFilesBL.getDBFFiles("",searchValue,"","",1, PropertiesConstant.CAMS_DIRECT_UPLOAD, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 5. Is upload to CAMS server
                        
                        //Get CAMS direct upload (STP) failed transaction DBF
                        else if(searchKey.equalsIgnoreCase(PropertiesConstant.CAMS_DIRECT_UPLOAD_FAILED_DBF))
				objDBFFilesBL.getDBFFiles("",searchValue,"","",1, PropertiesConstant.CAMS_DIRECT_UPLOAD_FAILED_DBF, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 5. To get CAMS server STP failed transaction in DBF
                        
                        //Get CAMS direct upload (STP) all transaction DBF
                        else if(searchKey.equalsIgnoreCase(PropertiesConstant.CAMS_DIRECT_UPLOAD_ALL_DBF))
				objDBFFilesBL.getDBFFiles("",searchValue,"","",1, PropertiesConstant.CAMS_DIRECT_UPLOAD_ALL_DBF, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 5. To get CAMS server STP failed transaction in DBF
                        
                        //Forward feed direct upload to KARVY server 
			else if(searchKey.equalsIgnoreCase(PropertiesConstant.KARVY_DIRECT_UPLOAD))
				objDBFFilesBL.getDBFFiles("",searchValue,"","",1, PropertiesConstant.KARVY_DIRECT_UPLOAD, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 5. Is upload to KARVY server
                        
                        else 
				objDBFFilesBL.getDBFFiles("","","",TrxnOE,1, finalFeed); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 
			//objDBFFilesBL.getDBFFiles("","","",1); 	// 1. liquid scheme 2. search key 3. AMC Search 4. UserId 
		}catch(Exception e){
                    logger.error("Exception in main"+e);
                } 
		logger.info("File Forward Feed Success ");

		long totalTime = System.currentTimeMillis() - startTime;
		logger.info("####TIMER : MainEnds = " + totalTime);
		
	}
}
