package com.wifs.DBFForwardFeed;

import java.util.ArrayList;

public class FatcaDocBO {
	
	private int investorId	=	0;
	private String pan	=	"";
	private String investorName	=	"";
	private String investorRelationship	=	"";
	private String annualIncomeCode	=	"0";
	private double annualIncome		=	0.0d;
	private String politicallyExpPer	=	"";
	private String taxResidencyCode	=	"";
	private String taxPayerIdentificationNo	=	"";
	private String identificationType	=	"X";
	private String fatcaDeclarationReceived	=	"";
	private String fatcaType			=	"";
	//private boolean newFolioFlag	=	false;
	
	private String occupationCode	=	"08";
	private String occupationTypeCode	=	"O";
	private String sourceOfWealthCode	=	"08";
	private String countryOfBirth		=	"India";
	private String countryOfBirthCode	=	"IN";
	private String nationality		=	"India";
	private String nationalityCode	=	"IN";
	private String taxStatusFlag	=	"IN";
	private String taxStatus		=	"";
	private String fatcaNotAvailablePan	=	"";
	private int minorFlag	=	0;
	
	public int getMinorFlag() {
		return minorFlag;
	}
	public void setMinorFlag(int minorFlag) {
		this.minorFlag = minorFlag;
	}
	private ConstansBO DateOfBirth;
	
	private ArrayList<FatcaDocBO> arrFatcaDocBO	=	new ArrayList<FatcaDocBO>();
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getInvestorName() {
		return investorName;
	}
	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}
	public String getPoliticallyExpPer() {
		return politicallyExpPer;
	}
	public void setPoliticallyExpPer(String politicallyExpPer) {
		this.politicallyExpPer = politicallyExpPer;
	}
	public String getFatcaDeclarationReceived() {
		return fatcaDeclarationReceived;
	}
	public void setFatcaDeclarationReceived(String fatcaDeclarationReceived) {
		this.fatcaDeclarationReceived = fatcaDeclarationReceived;
	}
	public int getInvestorId() {
		return investorId;
	}
	public void setInvestorId(int investorId) {
		this.investorId = investorId;
	}
	public String getAnnualIncomeCode() {
		return annualIncomeCode;
	}
	public void setAnnualIncomeCode(String annualIncomeCode) {
		this.annualIncomeCode = annualIncomeCode;
	}
	public String getTaxPayerIdentificationNo() {
		return taxPayerIdentificationNo;
	}
	public void setTaxPayerIdentificationNo(String taxPayerIdentificationNo) {
		this.taxPayerIdentificationNo = taxPayerIdentificationNo;
	}
	public String getInvestorRelationship() {
		return investorRelationship;
	}
	public void setInvestorRelationship(String investorRelationship) {
		this.investorRelationship = investorRelationship;
	}
	public String getFatcaType() {
		return fatcaType;
	}
	public void setFatcaType(String fatcaType) {
		this.fatcaType = fatcaType;
	}
	/*public boolean isNewFolioFlag() {
		return newFolioFlag;
	}
	public void setNewFolioFlag(boolean newFolioFlag) {
		this.newFolioFlag = newFolioFlag;
	}*/
	public ArrayList<FatcaDocBO> getArrFatcaDocBO() {
		return arrFatcaDocBO;
	}
	public void setArrFatcaDocBO(ArrayList<FatcaDocBO> arrFatcaDocBO) {
		this.arrFatcaDocBO = arrFatcaDocBO;
	}
	public String getIdentificationType() {
		return identificationType;
	}
	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}
	public String getTaxResidencyCode() {
		return taxResidencyCode;
	}
	public void setTaxResidencyCode(String taxResidencyCode) {
		this.taxResidencyCode = taxResidencyCode;
	}
	public double getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getOccupationTypeCode() {
		return occupationTypeCode;
	}
	public void setOccupationTypeCode(String occupationTypeCode) {
		this.occupationTypeCode = occupationTypeCode;
	}
	public String getSourceOfWealthCode() {
		return sourceOfWealthCode;
	}
	public void setSourceOfWealthCode(String sourceOfWealthCode) {
		this.sourceOfWealthCode = sourceOfWealthCode;
	}
	public String getCountryOfBirth() {
		return countryOfBirth;
	}
	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}
	public String getCountryOfBirthCode() {
		return countryOfBirthCode;
	}
	public void setCountryOfBirthCode(String countryOfBirthCode) {
		this.countryOfBirthCode = countryOfBirthCode;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getNationalityCode() {
		return nationalityCode;
	}
	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}
	public String getTaxStatusFlag() {
		return taxStatusFlag;
	}
	public void setTaxStatusFlag(String taxStatusFlag) {
		this.taxStatusFlag = taxStatusFlag;
	}
	public ConstansBO getDateOfBirth() {
		return DateOfBirth;
	}
	public void setDateOfBirth(ConstansBO dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}
	public String getTaxStatus() {
		return taxStatus;
	}
	public void setTaxStatus(String taxStatus) {
		this.taxStatus = taxStatus;
	}
	public String getFatcaNotAvailablePan() {
		return fatcaNotAvailablePan;
	}
	public void setFatcaNotAvailablePan(String fatcaNotAvailablePan) {
		this.fatcaNotAvailablePan = fatcaNotAvailablePan;
	}
	
}
