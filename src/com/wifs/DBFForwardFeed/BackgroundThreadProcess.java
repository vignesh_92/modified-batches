/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wifs.DBFForwardFeed;

import com.wifs.dao.dbconnection.WIFSSQLCon;
import com.wifs.utilities.FolderZiper;
import com.wifs.utilities.PropertiesConstant;
import com.wifs.utilities.PropertyFileReader;
import com.wifs.utilities.SendMail;
import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author vignesh
 */
public class BackgroundThreadProcess {

    public static Logger logger = Logger.getLogger("BackgroundThreadProcess.class");
    public static Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainBO = new Hashtable<String, ArrayList<TransactionMainBO>>();
    public static HashMap<Integer, String> failedList = new HashMap<>();
    public static Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionFailedMainBO = new Hashtable<String, ArrayList<TransactionMainBO>>();

    public void startCAMSInsertThread(Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainSBO, HashMap<Integer, String> failedSList) {
        DBFFilesDAO dbDAO = new DBFFilesDAO();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    logger.info("Cams thread started");
                    logger.info("HashTransaction size to be inserted :" + hashTransactionMainSBO.size());
                    logger.info("Failed list size : " + failedSList.size());
                    dbDAO.writeCAMSDB(hashTransactionMainSBO, failedSList);
                    hashTransactionMainBO = new Hashtable<>();
                    failedList = new HashMap<>();
                } catch (Exception ex) {
                    logger.error("Error in background process");
                }
            }
        };

        thread.start();
    }

    public void startKarvyInsertThread(Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionMainSBO, ArrayList<STPProcessBO> failedSList) {
        DBFFilesDAO dbDAO = new DBFFilesDAO();
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    logger.info("HashTransaction size to be inserted :" + hashTransactionMainSBO.size());
                    logger.info("Failed list size : " + failedSList.size());
                    dbDAO.writeKARVYDB(hashTransactionMainSBO, failedSList);
                    hashTransactionMainBO = new Hashtable<>();
                    failedList = new HashMap<>();
                } catch (Exception ex) {
                    logger.error("Error in background process");
                }
            }
        };

        thread.start();
    }

    public void startFinalDBFWriter(int userID, int rtCode, Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionFailedList) {
        try {
            ConstansBO objConstansBO = new ConstansBO("");
            PropertyFileReader objPropertyFileReader = new PropertyFileReader();
            final Properties properties = objPropertyFileReader.templatesFileReader(PropertiesConstant.FORWARDFEED_CONFIG);
            String fileDirectoryParent = properties.getProperty("Directory"),
                    fatcaDirectoryParent = properties.getProperty("FatcaDirectory"),
                    newFolioDirectoryParent = properties.getProperty("NewFolioReport");
            String date_formatted_String = (objConstansBO.getTodayDate("MM/dd/yyyy/HH/mm/SS")).replace("/", "");
            final String strFileDir = fileDirectoryParent + File.separator + date_formatted_String + "FinalDBF",
                    strFatcaFileDir = fatcaDirectoryParent,
                    newFolioDir = newFolioDirectoryParent;
            final String currentDate = objConstansBO.getTodayDateMonthView().replace("/", "");
            DBFFilesDAO objDBFFilesDAO = new DBFFilesDAO();
            WIFSSQLCon objWIFSSQLCon = new WIFSSQLCon();
            Connection objConnection = null;
            objConnection = objWIFSSQLCon.getWifsSQLConnect();
            final Hashtable<String, ArrayList<FieldInfoBO>> hashFieldInfoBO = objDBFFilesDAO.getFieldInfoTransaction(objConnection);
            DBFFielsWriter writer = new DBFFielsWriter();
            objConnection.close();
            Thread thread;
            thread = new Thread() {
                @Override
                public void run() {
                    try {
                        if (rtCode == 1) {
                            Connection objConnection = null;
                            objConnection = objWIFSSQLCon.getWifsSQLConnect();
                            Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionSuccessMainBO = objDBFFilesDAO.getFinalForwardFeedListCAMS(objConnection);
                            Iterator it = hashTransactionSuccessMainBO.entrySet().iterator();
                            ArrayList<ReportBO> reportFinalList = new ArrayList<>();
                            ReportBO reportBO = new ReportBO(0, "", 0, 0);
                            logger.info("Transaction" + hashTransactionSuccessMainBO.size());
                            while (it.hasNext()) {
                                Map.Entry pair = (Map.Entry) it.next();
                                String[] reportElements = pair.getKey().toString().split("@");
                                logger.info(pair.getKey().toString());
                                if (hashTransactionFailedList.containsKey(pair.getKey().toString())) {
                                    ArrayList<TransactionMainBO> successArrayList = hashTransactionSuccessMainBO.get(pair.getKey().toString());
                                    ArrayList<TransactionMainBO> failedArrayList = hashTransactionFailedList.get(pair.getKey().toString());
                                    reportBO = new ReportBO(rtCode, reportElements[1], successArrayList.size(), failedArrayList.size());
                                    successArrayList.addAll(failedArrayList);
                                    hashTransactionFailedList.remove(pair.getKey().toString());
                                    hashTransactionSuccessMainBO.put(pair.getKey().toString(), successArrayList);
                                } else {
                                    ArrayList<TransactionMainBO> successArrayList = hashTransactionSuccessMainBO.get(pair.getKey().toString());
                                    reportBO = new ReportBO(rtCode, reportElements[1], successArrayList.size(), 0);
                                }
                                reportFinalList.add(reportBO);
                            }
                            if (hashTransactionFailedList.size() > 0) {
                                Iterator itr = hashTransactionFailedList.entrySet().iterator();
                                while (itr.hasNext()) {
                                    Map.Entry pair = (Map.Entry) itr.next();
                                    String[] reportElements = pair.getKey().toString().split("@");
                                    ArrayList<TransactionMainBO> allFailedList = (ArrayList<TransactionMainBO>) pair.getValue();
                                    hashTransactionSuccessMainBO.put(pair.getKey().toString(), allFailedList);
                                    reportBO = new ReportBO(rtCode, reportElements[1], 0, allFailedList.size());
                                    reportFinalList.add(reportBO);
                                }
                            }
                            writer.FinalDBFFileTransactionFeedWriter("N", "", currentDate, strFileDir, hashTransactionSuccessMainBO, hashFieldInfoBO, userID, properties, true);
                            //writer.FinalDBFFileTransactionFeedWriter("N", "", currentDate, strFailedFileDir, hashTransactionFailedList, hashFieldInfoBO, userID, properties, true);
                            sendFinalMail(strFileDir, strFatcaFileDir, newFolioDir, properties, reportFinalList);
                        } else if (rtCode == 2) {
                            Connection objConnection = null;
                            objConnection = objWIFSSQLCon.getWifsSQLConnect();
                            Hashtable<String, ArrayList<TransactionMainBO>> hashTransactionSuccessMainBO = objDBFFilesDAO.getFinalForwardFeedListKarvy(objConnection);
                            objConnection.close();
                            Iterator it = hashTransactionSuccessMainBO.entrySet().iterator();
                            ArrayList<ReportBO> reportFinalList = new ArrayList<>();
                            ReportBO reportBO = new ReportBO(0, "", 0, 0);
                            while (it.hasNext()) {
                                Map.Entry pair = (Map.Entry) it.next();
                                String[] reportElements = pair.getKey().toString().split("@");
                                if (hashTransactionFailedList.containsKey(pair.getKey().toString())) {
                                    ArrayList<TransactionMainBO> successArrayList = hashTransactionSuccessMainBO.get(pair.getKey().toString());
                                    ArrayList<TransactionMainBO> failedArrayList = hashTransactionFailedList.get(pair.getKey().toString());
                                    reportBO = new ReportBO(rtCode, reportElements[1], successArrayList.size(), failedArrayList.size());
                                    successArrayList.addAll(failedArrayList);
                                    hashTransactionFailedList.remove(pair.getKey().toString());
                                    hashTransactionSuccessMainBO.put(pair.getKey().toString(), successArrayList);
                                } else {
                                    ArrayList<TransactionMainBO> successArrayList = hashTransactionSuccessMainBO.get(pair.getKey().toString());
                                    reportBO = new ReportBO(rtCode, reportElements[1], successArrayList.size(), 0);
                                }
                                reportFinalList.add(reportBO);
                            }
                            if (hashTransactionFailedList.size() > 0) {
                                Iterator itr = hashTransactionFailedList.entrySet().iterator();
                                while (itr.hasNext()) {
                                    Map.Entry pair = (Map.Entry) itr.next();
                                    String[] reportElements = pair.getKey().toString().split("@");
                                    ArrayList<TransactionMainBO> allFailedList = (ArrayList<TransactionMainBO>) pair.getValue();
                                    hashTransactionSuccessMainBO.put(pair.getKey().toString(), allFailedList);
                                    reportBO = new ReportBO(rtCode, reportElements[1], 0, allFailedList.size());
                                }
                                reportFinalList.add(reportBO);
                            }
                            writer.FinalDBFFileTransactionFeedWriter("N", "", currentDate, strFileDir, hashTransactionSuccessMainBO, hashFieldInfoBO, userID, properties, true);
                            //writer.FinalDBFFileTransactionFeedWriter("N", "", currentDate, strFailedFileDir, hashTransactionFailedList, hashFieldInfoBO, userID, properties, true);
                            sendFinalMail(strFileDir, strFatcaFileDir, newFolioDir, properties, reportFinalList);
                        }
                    } catch (Exception ex) {
                        logger.error("Error in writing final DBF Files " + ex);
                    }

                }
            };
            thread.start();
        } catch (Exception ex) {
            logger.info("Error in getting properties/connection to database to write final DBF File " + ex);
        }
    }

    public void sendFinalMail(String fileLocation, String strFatcaLocation, String newFolioDir, Properties properties, ArrayList<ReportBO> reportListBO) {
        FolderZiper objFolderZiper = new FolderZiper();
        SendMail objSendMail = new SendMail();
        ConstansBO objConstansBO = new ConstansBO("");

//        Thread thread = new Thread() {
//            public void run() {
                try {
                    String date_formatted_String = (objConstansBO.getTodayDate("MM/dd/yyyy/HH/mm/SS")).replace("/", "");
                    logger.info("Zipping DBF Files : " + fileLocation);
                    objFolderZiper.zipFolder(fileLocation, fileLocation + ".zip");
                    String dbfFiles = fileLocation + ".zip";
                    logger.info("Zipping Fatca Files : " + strFatcaLocation);
                    objFolderZiper.zipFolder(strFatcaLocation, strFatcaLocation + date_formatted_String + ".zip");
                    String fatcaFiles = strFatcaLocation + date_formatted_String + ".zip";
                    objFolderZiper.zipFolder(newFolioDir, newFolioDir + date_formatted_String + ".zip");
                    String nfFiles = newFolioDir + date_formatted_String + ".zip";
                    final String allExcelFile = PropertiesConstant.TEMP_DIRECTORY + File.separator + "FINAL_STP_LIST.xls";
                    STPProcessBL stpProcessBL = new STPProcessBL();
                    boolean allExcelFileStatus = stpProcessBL.generateDirectUploadStatusExcel(allExcelFile, reportListBO);
                    String[] attachments = new String[]{dbfFiles, fatcaFiles, nfFiles, (allExcelFileStatus ? allExcelFile : "")};
                    boolean mailFlag = objSendMail.SendMailWithDBFFile(attachments, "", "", "Direct upload (STP) all forward feed dbf files ");
                    if (mailFlag) {
                        logger.info("Mail sent successfully");
                    }
                    new File(fatcaFiles).delete(); //  Delete the  fatca copy after mail is sent
                    new File(nfFiles).delete(); // Delete NewFolio copy after mail is sent
                    if (!dbfFiles.equals("")) {
                        objFolderZiper.CopyFileToTodaysFOlder(properties.getProperty("Directory"), properties.getProperty("DirectoryHistory"));
                    }

                    if (strFatcaLocation != "") {
                        objFolderZiper.CopyFileToTodaysFOlder(properties.getProperty("FatcaDirectory"), properties.getProperty("FatcaDirectoryHistory"));
//                        File sourceParentFolder = new File(strFatcaLocation);
//                        String listOfFiles[] = sourceParentFolder.list();
//                        for (String fileName : listOfFiles) {
//
//                            String sourceFolder = strFatcaLocation + File.separator + fileName;
//                            File sourceFile = new File(sourceFolder);
//                            if (sourceFile.isDirectory()) {
//                                FileUtils.deleteDirectory(sourceFile);
//                            } else {
//                                String sourceFile_Single = strFatcaLocation + File.separator + fileName;
//                                new File(sourceFile_Single).delete();
//                            }
//                            //objFolderZiper.CopyFileToTodaysFOlder(fileDirectoryParent, properties.getProperty("DirectoryHistory"));
//                        }
                    }

                    if (newFolioDir != "") {
                        objFolderZiper.CopyFileToTodaysFOlder(properties.getProperty("NewFolioReport"), properties.getProperty("NewFolioReportHistory"));
//                        File sourceParentFolder = new File(newFolioDir);
//                        String listOfFiles[] = sourceParentFolder.list();
//                        for (String fileName : listOfFiles) {
//
//                            String sourceFolder = newFolioDir + File.separator + fileName;
//                            File sourceFile = new File(sourceFolder);
//                            if (sourceFile.isDirectory()) {
//                                FileUtils.deleteDirectory(sourceFile);
//                            } else {
//                                String sourceFile_Single = newFolioDir + File.separator + fileName;
//                                new File(sourceFile_Single).delete();
//                            }
//                            //objFolderZiper.CopyFileToTodaysFOlder(fileDirectoryParent, properties.getProperty("DirectoryHistory"));
//                        }
                    }

                } catch (Exception ex) {
                    logger.error("Error in sending final mail " + ex);
                }
            }
//        }

//        thread.start();
//    }

}
