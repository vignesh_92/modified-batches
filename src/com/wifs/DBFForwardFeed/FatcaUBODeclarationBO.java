package com.wifs.DBFForwardFeed;

public class FatcaUBODeclarationBO {
	
	private int FATCADeclarationId		=	0;
	private String entityExemptionCode	=	"";
	private String NFEsType				=	"";
	private String NFEs_GIINCode		=	"";
	private String NFEs_GIIN_SP_Entity	=	"";
	private String NFEs_GIIN_NA_Flag	=	"";
	private String NFFE_CATG_Flag		=	"";
	private String NFEs_GIIN_NA_Subcategory	=	"";
	private String activeNFESubCategory	=	"";
	private String activeNFEBusiness	=	"";
	private String passiveNFEBusiness	=	"";
	private String REPTCRelation		=	"";
	private String REPTCName			=	"";
	private String PTCStockExchange		=	"";
	private String REPTCExchangeName	=	"";
	private String uboPan				=	"";
	private String name					=	"";
	private String countryCode			=	"";
	private String address1				=	"";
	private String address2				=	"";
	private String address3				=	"";
	private String pin					=	"";
	private String state				=	"";
	private String acountryCode			=	"";
	private String addressType			=	"";
	private String taxIdNo				=	"";
	private String taxIdType			=	"";  
	private String placeOfBirth			=	"";
	private ConstansBO dob;
	private String gender				=	"";
	private String fatherName			=	"";
	private String occupationCode		=	"";
	private String occupationType		=	"";
	private String uboApplicable		=	"";
	//private String nationality			=	"";
	//private String uboTypeCode			=	"";
	private String uboCode				=	"";
	private String uboCity				=	"";
	
	private String NetWorthAmount		=	"0"; 
	private ConstansBO netWorthDate;
	
	public int getFATCADeclarationId() {
		return FATCADeclarationId;
	}
	public void setFATCADeclarationId(int fATCADeclarationId) {
		FATCADeclarationId = fATCADeclarationId;
	}
	public String getEntityExemptionCode() {
		return entityExemptionCode;
	}
	public void setEntityExemptionCode(String entityExemptionCode) {
		this.entityExemptionCode = entityExemptionCode;
	}
	public String getNFEsType() {
		return NFEsType;
	}
	public void setNFEsType(String nFEsType) {
		NFEsType = nFEsType;
	}
	public String getNFEs_GIINCode() {
		return NFEs_GIINCode;
	}
	public void setNFEs_GIINCode(String nFEs_GIINCode) {
		NFEs_GIINCode = nFEs_GIINCode;
	}
	public String getNFEs_GIIN_SP_Entity() {
		return NFEs_GIIN_SP_Entity;
	}
	public void setNFEs_GIIN_SP_Entity(String nFEs_GIIN_SP_Entity) {
		NFEs_GIIN_SP_Entity = nFEs_GIIN_SP_Entity;
	}
	public String getNFEs_GIIN_NA_Flag() {
		return NFEs_GIIN_NA_Flag;
	}
	public void setNFEs_GIIN_NA_Flag(String nFEs_GIIN_NA_Flag) {
		NFEs_GIIN_NA_Flag = nFEs_GIIN_NA_Flag;
	}
	public String getActiveNFEBusiness() {
		return activeNFEBusiness;
	}
	public void setActiveNFEBusiness(String activeNFEBusiness) {
		this.activeNFEBusiness = activeNFEBusiness;
	}
	public String getPassiveNFEBusiness() {
		return passiveNFEBusiness;
	}
	public void setPassiveNFEBusiness(String passiveNFEBusiness) {
		this.passiveNFEBusiness = passiveNFEBusiness;
	}
	public String getREPTCRelation() {
		return REPTCRelation;
	}
	public void setREPTCRelation(String rEPTCRelation) {
		REPTCRelation = rEPTCRelation;
	}
	public String getREPTCName() {
		return REPTCName;
	}
	public void setREPTCName(String rEPTCName) {
		REPTCName = rEPTCName;
	}
	public String getPTCStockExchange() {
		return PTCStockExchange;
	}
	public void setPTCStockExchange(String pTCStockExchange) {
		PTCStockExchange = pTCStockExchange;
	}
	public String getREPTCExchangeName() {
		return REPTCExchangeName;
	}
	public void setREPTCExchangeName(String rEPTCExchangeName) {
		REPTCExchangeName = rEPTCExchangeName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAcountryCode() {
		return acountryCode;
	}
	public void setAcountryCode(String acountryCode) {
		this.acountryCode = acountryCode;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getTaxIdNo() {
		return taxIdNo;
	}
	public void setTaxIdNo(String taxIdNo) {
		this.taxIdNo = taxIdNo;
	}
	public String getTaxIdType() {
		return taxIdType;
	}
	public void setTaxIdType(String taxIdType) {
		this.taxIdType = taxIdType;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public ConstansBO getDob() {
		return dob;
	}
	public void setDob(ConstansBO dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getOccupationType() {
		return occupationType;
	}
	public void setOccupationType(String occupationType) {
		this.occupationType = occupationType;
	}
	public String getNFFE_CATG_Flag() {
		return NFFE_CATG_Flag;
	}
	public void setNFFE_CATG_Flag(String nFFE_CATG_Flag) {
		NFFE_CATG_Flag = nFFE_CATG_Flag;
	}
	public String getActiveNFESubCategory() {
		return activeNFESubCategory;
	}
	public void setActiveNFESubCategory(String activeNFESubCategory) {
		this.activeNFESubCategory = activeNFESubCategory;
	}
	public String getUboApplicable() {
		return uboApplicable;
	}
	public void setUboApplicable(String uboApplicable) {
		this.uboApplicable = uboApplicable;
	}
	public String getUboPan() {
		return uboPan;
	}
	public void setUboPan(String uboPan) {
		this.uboPan = uboPan;
	}
	public String getNFEs_GIIN_NA_Subcategory() {
		return NFEs_GIIN_NA_Subcategory;
	}
	public void setNFEs_GIIN_NA_Subcategory(String nFEs_GIIN_NA_Subcategory) {
		NFEs_GIIN_NA_Subcategory = nFEs_GIIN_NA_Subcategory;
	}
	/*public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}*/
	/*public String getUboTypeCode() {
		return uboTypeCode;
	}
	public void setUboTypeCode(String uboTypeCode) {
		this.uboTypeCode = uboTypeCode;
	}*/
	public String getUboCode() {
		return uboCode;
	}
	public void setUboCode(String uboCode) {
		this.uboCode = uboCode;
	}
	public String getUboCity() {
		return uboCity;
	}
	public void setUboCity(String uboCity) {
		this.uboCity = uboCity;
	}
	public String getNetWorthAmount() {
		return NetWorthAmount;
	}
	public void setNetWorthAmount(String netWorthAmount) {
		NetWorthAmount = netWorthAmount;
	}
	public ConstansBO getNetWorthDate() {
		return netWorthDate;
	}
	public void setNetWorthDate(ConstansBO netWorthDate) {
		this.netWorthDate = netWorthDate;
	}
	
	

}
