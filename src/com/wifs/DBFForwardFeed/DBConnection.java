package com.wifs.DBFForwardFeed; 

import java.sql.Connection;
import java.sql.DriverManager;
import org.apache.log4j.Logger;

public class DBConnection { 
	public static Logger logger = Logger.getLogger("DBConnection.class");
	public Connection getWifsSQLConnect(String SQLDriver, String SQLURL, String SQLUserName, String SQLPassword) throws Exception {
		Connection objConnection = null;
		Class.forName(SQLDriver);
		objConnection = DriverManager.getConnection(SQLURL,SQLUserName,SQLPassword);
		logger.info("Connection Success");
		return objConnection;
	}
}
