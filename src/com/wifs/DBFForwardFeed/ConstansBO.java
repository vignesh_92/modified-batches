package com.wifs.DBFForwardFeed;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;

import org.apache.log4j.Logger;

public class ConstansBO
{
	public static Logger logger=Logger.getLogger(new Object().getClass().getName());
    
    private String strDataTypeId;
    private byte btDataType;
    private Date objDate;
    private static final DateFormat dateform = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat mothDateform = new SimpleDateFormat("MM-dd-yyyy");
    private static final DateFormat mothDateformForPipe = new SimpleDateFormat("MM/dd/yyyy");
    private static final DateFormat dateMonthform = new SimpleDateFormat("dd-MM-yyyy");

    public ConstansBO(String dateValue)
    {
        strDataTypeId = "";
        btDataType = 0;
        objDate = new Date();
        if(!dateValue.trim().equals(""))
            try
            {
                objDate = dateform.parse(dateValue.trim());
            }
            catch(Exception e)
            {
                logger.info((new StringBuilder("date exp ")).append(e).toString());
            }
        else
            objDate = null;
    }

    public ConstansBO() {
    } 

   /* public Date getmothDateformForPipe(String dateValue)
    {
    	objDate = new Date();
    	String returnString = "";
    	try {
    		returnString = mothDateformForPipe.format(objDate);
    		objDate = mothDateformForPipe.parse(returnString.trim());
    	
    	}catch (Exception ignore){ logger.error("Exp:"+ignore); }
    	return objDate;
    }*/
    public Date getDateFirstView()
    {
        return objDate;
    }

    public String getMonthFirstView()
    {
        return mothDateform.format(objDate);
    }

    public String getMonthFirstViewRequiredFormat()
    {
    	String returnString = "";
    	try {
    	returnString = mothDateformForPipe.format(objDate);
    	}catch (Exception ignore){ }
    	return returnString;
    }
    
    /*public String getDateFirstViewRequiredFormat()
    {
    	return dateMonthformForPipe.format(objDate);
    }*/
    public String getDateMonthView()
    {
        return dateMonthform.format(objDate);
    }

    public void setDataTypeId(String strDataTypeId)
    {
        this.strDataTypeId = strDataTypeId;
    }

    public byte getDataType()
    {
        if(strDataTypeId.equals("N"))
            btDataType = 78;
        else
        if(strDataTypeId.equals("F"))
            btDataType = 70;
        else
        if(strDataTypeId.equals("L"))
            btDataType = 76;
        else
        if(strDataTypeId.equals("D"))
            btDataType = 68;
        else
            btDataType = 67;
        return btDataType;
    }

    public static String NullCheck(String inputString)
    {
        try
        {
            if(inputString == null || inputString.trim().equals("") || inputString.trim().equalsIgnoreCase(null))
                inputString = "";
        }
        catch(Exception e)
        {
            logger.info((new StringBuilder("Exception in NullCheck ")).append(e).toString());
            inputString = "";
        }
        return inputString.trim();
    }
    
    

    public static double NullCheckDBL(double inputString)
    {
        try
        {
            if(String.valueOf(inputString) == null || String.valueOf(inputString).trim().equals("") || String.valueOf(inputString).trim().equalsIgnoreCase(null))
                inputString = 0.0D;
            else
            if(inputString < 0.0D)
                inputString *= -1D;
        }
        catch(Exception e)
        {
            logger.info((new StringBuilder("Exception in NullCheck ")).append(e).toString());
            inputString = 0.0D;
        }
        return inputString;
    }

    public static int NullCheckINT(int inputString)
    {
        try
        {
            if(String.valueOf(inputString) == null || String.valueOf(inputString).trim().equals("") || String.valueOf(inputString).trim().equalsIgnoreCase(null))
                inputString = 0;
        }
        catch(Exception e)
        {
            logger.info((new StringBuilder("Exception in NullCheck ")).append(e).toString());
            inputString = 0;
        }
        return inputString;
    }

    public static String getTodayDate_static(String format) 
    {
    	ConstansBO constanBO = new ConstansBO("");
    	return constanBO.getTodayDate(format);
    }
    public String getTodayDate(String format) 
    {
        String dirName = "";
        Date d1 = new Date();
        Format formatter = new SimpleDateFormat(format);//"yyyy-MM-dd");
        dirName = formatter.format(d1);
        return dirName;
    }

    public String getTodayDateMonthView()
    {
        String dirName = "";
        Date d1 = new Date();
        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        dirName = formatter.format(d1);
        return dirName;
    }
    public static String getEightDecimal(Double doubleValue)throws Exception
	{
		if ((doubleValue == null)||(doubleValue == 0 )) return new String("0.00000000");
		DecimalFormat df1 = new DecimalFormat("####.00000000");
		return (df1.format(doubleValue));
	}
    public static String getFourDigit(int Value)throws Exception
	{
		if (Value == 0 ) return new String("0000");
		DecimalFormat df1 = new DecimalFormat("0000");
		return (df1.format(Value));
	}
    
        public String getTomoDate(PreparedStatement objHolidayDate)
        throws Exception
    {
        String TomorrowDate = "";
        boolean holidayCheck = true;
        DBFFilesDAO objDBFFilesDAO = new DBFFilesDAO();
        Date d1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        c1.add(5, +1);
        while(holidayCheck) 
        {
            TomorrowDate = sdf.format(c1.getTime());
            if(c1.get(7) == 1 || c1.get(7) == 7)
            {
                c1.add(5, +1);
                holidayCheck = true;
            } else
            if(objDBFFilesDAO.getHoliday(TomorrowDate, objHolidayDate))
            {
                c1.add(5, +1);
                holidayCheck = true;
            } else
            {
                holidayCheck = false;
            }
        }
        logger.debug((new StringBuilder("Tomorrow Day Date  : ")).append(TomorrowDate).toString());
        return TomorrowDate;
    }
    
    
    public String getYesterDate(PreparedStatement objHolidayDate)
        throws Exception
    {
        String yesterDayDate = "";
        boolean holidayCheck = true;
        DBFFilesDAO objDBFFilesDAO = new DBFFilesDAO();
        Date d1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        c1.add(5, -1);
        while(holidayCheck) 
        {
            yesterDayDate = sdf.format(c1.getTime());
            if(c1.get(7) == 1 || c1.get(7) == 7)
            {
                c1.add(5, -1);
                holidayCheck = true;
            } else
            if(objDBFFilesDAO.getHoliday(yesterDayDate, objHolidayDate))
            {
                c1.add(5, -1);
                holidayCheck = true;
            } else
            {
                holidayCheck = false;
            }
        }
        logger.debug((new StringBuilder("Yester Day Date  : ")).append(yesterDayDate).toString());
        return yesterDayDate;
    }
    public static String getFTFeedDate() throws Exception
	{
	    Date d1 = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
	    Calendar c1 = Calendar.getInstance();
	    c1.setTime(d1);
	    return sdf.format(c1.getTime());
	}
    public boolean isHolidyaDate(PreparedStatement objHolidayDate)
        throws Exception
    {
        String todayDayDate = "";
        boolean holidayCheck = true;
        DBFFilesDAO objDBFFilesDAO = new DBFFilesDAO();
        Date d1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        todayDayDate = sdf.format(c1.getTime());
        if(objDBFFilesDAO.getHoliday(todayDayDate, objHolidayDate))
            holidayCheck = true;
        else
            holidayCheck = false;
        logger.debug((new StringBuilder("Today Date  : ")).append(todayDayDate).toString());
        return holidayCheck;
    }

    public static String getThreeDecimalNumber(BigDecimal bigDecimalValue)
        throws Exception
    {
        if(bigDecimalValue == null || bigDecimalValue.equals(new BigDecimal(0)))
        {
            return "0.000";
        } else
        {
            DecimalFormat df1 = new DecimalFormat("###0.000");
            return df1.format(bigDecimalValue);
        }
    }

    public static String getRupeeBigDecimal(BigDecimal bigDecimalValue, int intDecimals)
        throws Exception
    {
        String stringReturn = "";
        DecimalFormat df1;
        if(intDecimals == 2)
            df1 = new DecimalFormat("###0.00");
        else
        if(intDecimals == 3)
            df1 = new DecimalFormat("###0.000");
        else
        if(intDecimals == 4)
            df1 = new DecimalFormat("###0.0000");
        else
        if(intDecimals > 4)
            df1 = new DecimalFormat("###0.00000");
        else
            df1 = new DecimalFormat("###0.0");
        if(bigDecimalValue == null)
        {
            if(intDecimals == 2)
                return "0.00";
            if(intDecimals == 3)
                return "0.000";
            if(intDecimals == 4)
                return "0.0000";
            if(intDecimals > 4)
                return "0.00000";
            else
                return "0";
        }
        boolean negativeFlag = false;
        String strAmount = df1.format(bigDecimalValue).split("[.]")[0];
        String strDecimal = df1.format(bigDecimalValue).split("[.]")[1];
        if(bigDecimalValue.signum() < 0)
        {
            negativeFlag = true;
            if(strAmount.equals("-"))
                strAmount = "-0";
            strAmount = String.valueOf((new BigDecimal(strAmount)).multiply(new BigDecimal(-1)));
        }
        int stringLength = strAmount.length();
        int intSearch = 1;
        if(stringLength % 2 == 0)
            intSearch = 0;
        for(int i = 0; i < stringLength; i++)
        {
            stringReturn = (new StringBuilder(String.valueOf(stringReturn))).append(strAmount.charAt(i)).toString();
            if(stringLength - 3 > i && i % 2 == intSearch)
                stringReturn = (new StringBuilder(String.valueOf(stringReturn))).append(",").toString();
        }

        if(negativeFlag)
            return (new StringBuilder("-")).append(stringReturn).append(".").append(strDecimal).toString();
        else
            return (new StringBuilder(String.valueOf(stringReturn))).append(".").append(strDecimal).toString();
    }
    /**
     * This method checks if a String contains only numbers
     */
    public static boolean containsOnlyNumbers(String str) {
        //It can't contain only numbers if it's null or empty...
    	try{
	        if (str == null || str.length() == 0 || str.trim().equalsIgnoreCase(null))
	            return false;
	        
	        for (int i = 0; i < str.length(); i++) {
	 
	            //If we find a non-digit character we return false.
	            if (!Character.isDigit(str.charAt(i)))
	                return false;
	        }
	        return true;
    	}catch(Exception e){
    		return false;
    	}
    }
    
    /**
     * Check is string empty / null
     */
    public static boolean isEmpty(String string) {
        boolean returnflag = false;
        try{
            if ((string == null) || (string != null && string.trim().length() <= 0)) {
                returnflag = true;
            }}catch(Exception ex){

        }
        return returnflag;
    }
    
    public java.sql.Date getSQLDate(String dateValue) {
        java.sql.Date sqlDate = null;
        if (dateValue.equals("") || dateValue == null) {
            dateValue = "12-31-3015";
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
            java.util.Date date = simpleDateFormat.parse(dateValue);
            sqlDate = new java.sql.Date(date.getTime());

        } catch (ParseException ex) {
            logger.error("Error while parsing Date : " + ex.getMessage());
        }
        return sqlDate;
    }
    
    
}

